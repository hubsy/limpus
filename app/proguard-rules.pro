# AWS SDK
# Class names are needed in reflection
-keepnames class com.amazonaws.**
-keepnames class com.amazon.**
# Request handlers defined in request.handlers
-keep class com.amazonaws.services.**.*Handler

# USB serial drivers
-keepnames class com.hoho.android.usbserial.driver.*Driver

-printusage ../proguard_usage.txt
-printseeds ../proguard_seeds.txt