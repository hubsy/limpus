#include "bgsub.h"

#include <android/log.h>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include "utils.h"

namespace {

const char* const TAG = "bgsub";

/**
 * Returns true if any element of mat >= threshold. T specifies the element type
 * of mat.
 */
template<typename T>
bool anyAboveThreshold(cv::Mat mat, T threshold) {
  for (int row = 0; row < mat.rows; row++) {
    for (int col = 0; col < mat.cols; col++) {
      if (mat.at<T>(row, col) >= threshold) {
        return true;
      }
    }
  }

  return false;
}

} // namespace

namespace sensorable {
namespace bgsub {

BackgroundSubtraction::BackgroundSubtraction(const cv::Mat& kernel, const cv::Mat& imageMask)
    : mModel(cv::createBackgroundSubtractorMOG2(5, 48, false)),
      mKernel(kernel),
      mImageMask(imageMask) {

  // Invert the mask.
  if (!mImageMask.empty()) cv::bitwise_not(mImageMask, mImageMask);

  __android_log_write(ANDROID_LOG_DEBUG, TAG, "BackgroundSubtraction instance created");
}

void BackgroundSubtraction::apply(cv::Mat& image, const cv::Rect2f& roi, double learningRate) {
  if (!mImageMask.empty()) {
    // Also crop the input mask and then apply it to the image.
    cv::Mat mask(mImageMask, sensorable::cropRegion(roi, mImageMask.cols, mImageMask.rows));
    if (image.rows == mask.rows && image.cols == mask.cols) {
      // Use the inverted mask to set all values in the masked area to zero.
      cv::bitwise_and(image, 0U, image, mask);
    } else {
      __android_log_print(ANDROID_LOG_WARN, TAG, "The mask dimensions %dx%d do not match the image",
                          mask.cols, mask.rows);
    }
  }

  // Update the model.
  mModel->apply(image, mFGMask, learningRate);
}

void BackgroundSubtraction::writeForegroundMask(const std::string& path) const {
  cv::imwrite(path, mFGMask);
}

void BackgroundSubtraction::cleanForegroundMask() {
  // Close small holes in clusters of foreground pixels (false negatives).
  cv::morphologyEx(mFGMask, mFGMask, cv::MORPH_CLOSE,
                   cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)),
                   cv::Point(-1, -1), 1, cv::BORDER_CONSTANT, 0);
  // Remove noise (false positives).
  cv::morphologyEx(mFGMask, mFGMask, cv::MORPH_OPEN,
                   cv::getStructuringElement(cv::MORPH_RECT, cv::Size(2, 2)),
                   cv::Point(-1, -1), 1, cv::BORDER_CONSTANT, 0);
}

bool BackgroundSubtraction::analyse(float threshold,
                                    const std::string& outPath) {
  // Scale the mask so 0 -> -1.0 and 255 -> 1.0 and store in a temp array.
  cv::divide(mFGMask, (255.f / 2.f), mTmpMask, 1, CV_32F);
  cv::subtract(mTmpMask, 1.f, mTmpMask);

  // Adjust the threshold to the scaled range.
  threshold = threshold * 2.f - 1.f;

  // Apply the filter.
  cv::filter2D(mTmpMask, mTmpMask, -1, mKernel, cv::Point(-1, -1), 0, cv::BORDER_CONSTANT);

  // Remove values in the feature map affected by padding of the input.
  int borderW = (mKernel.cols - 1) / 2;
  int borderH = (mKernel.rows - 1) / 2;
  cv::Mat filtered(mTmpMask, cv::Rect(borderW, borderH,
                                      mTmpMask.cols - 2 * borderW,
                                      mTmpMask.rows - 2 * borderH));

  // Look for any pixels where the filter result is above the threshold.
  const bool hasChanges = anyAboveThreshold(filtered, threshold);

  // Write the filter output to an image if requested.
  if (!outPath.empty()) {
    cv::Mat img = filtered >= threshold;
    cv::imwrite(outPath, img);
  }

  return hasChanges;
}

} // namespace bgsub
} // namespace sensorable
