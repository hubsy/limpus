#pragma once

#include <opencv2/core/mat.hpp>
#include <opencv2/video/background_segm.hpp>

namespace sensorable {
namespace bgsub {

class BackgroundSubtraction {
 public:
  explicit BackgroundSubtraction(const cv::Mat& kernel,
                                 const cv::Mat& imageMask);

  BackgroundSubtraction(const BackgroundSubtraction&) = delete;

  BackgroundSubtraction& operator=(const BackgroundSubtraction&) = delete;

  virtual ~BackgroundSubtraction() = default;

  /**
   * Update the background model with the image and calculate a new foreground mask.
   *
   * @param image The image to apply.
   * @param roi The normalised region-of-interest of the imageMask to consider, if one was specified
   * in the constructor. After cropping, its dimensions must match those of the image.
   * @param learningRate The learning rate as defined by cv::BackgroundSubtractor.
   */
  virtual void apply(cv::Mat& image, const cv::Rect2f& roi, double learningRate);

  /**
   * Writes the current foreground mask to a file at path.
   *
   * May throw cv::Exception.
   *
   * @param path The output path. The file name extension determines the file
   *        type (jpg or png). PNG works best for these images.
   */
  virtual void writeForegroundMask(const std::string& path) const;

  /**
   * Cleans the foreground mask by closing small holes in clumps of foreground pixels and by
   * removing scattered noise.
   */
  virtual void cleanForegroundMask();

  /**
   * Analyses the current foreground mask.
   *
   * @param threshold The minimum value in the filtered mask to produce a match.
   * @param outPath Optionally writes the output of applying the threshold to
   *        the filtered mask to a b&w image at outPath. The extension
   *        determines the image type (PNG is a good choice). An empty string
   *        disables this.
   * @return True if signficant changes are detected, false if not.
   */
  virtual bool analyse(float threshold, const std::string& outPath);

 private:
  cv::Ptr<cv::BackgroundSubtractor> mModel; // The background model.
  const cv::Mat mKernel; // Kernel used for cross-correlation with the mask.
  const cv::Mat mImageMask; // A mask to apply to input images. May be empty.

  cv::Mat mFGMask; // The foreground mask.
  cv::Mat mTmpMask; // Temporary storage for mask.
}; // BackgroundSubtraction

} // namespace bgsub
} // namespace sensorable
