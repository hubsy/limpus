#include "bgsub_jni.h"

#include <sstream>

#include <android/log.h>
#include <opencv2/imgcodecs.hpp>

#include "bgsub.h"
#include "utils.h"

namespace {
const char* const TAG = "bgsub_jni";
}

namespace sns = sensorable;
namespace bgsub = sensorable::bgsub;

JNIEXPORT jlong JNICALL Java_io_hubsy_core_imgproc_BGSubtraction_initModelNative
    (JNIEnv* env, jobject, jfloatArray kernel, jint kernelWidth,
     jint kernelHeight, jstring maskPath) {
  // Check that the array length and dimensions match.
  int kernelLen = env->GetArrayLength(kernel);
  if (kernelLen != kernelWidth * kernelHeight) {
    std::ostringstream msg;
    msg << "Error: kernel dims do not correspond to array length: "
        << kernelWidth << "x" << kernelHeight << " != " << kernelLen;
    sns::throwException(env, sns::JEX_EXCEPTION, msg.str().c_str());
    return 0;
  }

  // Copy the kernel data into a Mat.
  jfloat* kernelPtr = env->GetFloatArrayElements(kernel, nullptr);
  if (kernelPtr == nullptr) {
    sns::throwException(env, sns::JEX_EXCEPTION, "Failed to access the kernel array");
    return 0;
  }
  cv::Mat cKernel =
      cv::Mat(kernelHeight, kernelWidth, CV_32FC1, kernelPtr).clone();
  env->ReleaseFloatArrayElements(kernel, kernelPtr, JNI_ABORT);

  // Load the image mask if a path is specified for it.
  cv::Mat imageMask;
  if (maskPath != nullptr) {
    const std::string path = sns::toStringUTF(env, maskPath);
    imageMask = cv::imread(path, cv::IMREAD_GRAYSCALE);
    if (imageMask.empty()) {
      std::string msg("Failed to load the image mask from " + path);
      sns::throwException(env, sns::JEX_IO, msg.c_str());
      return 0;
    }
    __android_log_print(ANDROID_LOG_DEBUG, TAG, "Image mask loaded from %s",
                        path.c_str());
  }

  auto* bgsub = new bgsub::BackgroundSubtraction(cKernel, imageMask);
  return reinterpret_cast<jlong>(bgsub);
}

JNIEXPORT void JNICALL Java_io_hubsy_core_imgproc_BGSubtraction_releaseNative
    (JNIEnv* env, jobject, jlong modelPtr) {
  if (modelPtr == 0) {
    sns::throwException(env, sns::JEX_ILLEGAL_STATE, "The model is not initialised");
    return;
  }
  delete reinterpret_cast<bgsub::BackgroundSubtraction*>(modelPtr);
}

JNIEXPORT void JNICALL Java_io_hubsy_core_imgproc_BGSubtraction_updateModelNative
    (JNIEnv* env, jobject, jlong modelPtr, jobject image, jint width, jint height, jint rowStride,
     jfloat roiX, jfloat roiY, jfloat roiW, jfloat roiH, jint rotation, jboolean invert,
     jdouble learningRate, jstring imageFileOut) {
  if (modelPtr == 0) {
    sns::throwException(env, sns::JEX_ILLEGAL_STATE, "The model is not initialised");
    return;
  }
  auto* bgsub = reinterpret_cast<bgsub::BackgroundSubtraction*>(modelPtr);

  // Create a Mat backed by the image data.
  cv::Mat img = sns::wrapBufferInMat8U(env, image, width, height, 1, rowStride);
  if (img.empty()) return;

  // Rotate if necessary.
  sns::rotate(img, img, rotation);

  // Crop the image.
  const cv::Rect2f roi(roiX, roiY, roiW, roiH);
  if (roiX != 0.f || roiY != 0.f || roiW != 1.f || roiH != 1.f) {
    try {
      img = cv::Mat(img, sns::cropRegion(roi, img.cols, img.rows));
    } catch (const cv::Exception& ex) {
      std::string msg = std::string("Failed to crop the input image: ") + ex.what();
      sns::throwException(env, sns::JEX_ILLEGAL_ARGUMENT, msg.c_str());
      return;
    }
  }

  // Invert black <-> white etc.
  if (invert) cv::bitwise_not(img, img);

  // Update the model.
  bgsub->apply(img, roi, learningRate);
  __android_log_write(ANDROID_LOG_DEBUG, TAG, "Model updated");

  // Write the image if an output path is given.
  if (imageFileOut != nullptr) {
    const std::string imagePath = sns::toStringUTF(env, imageFileOut);
    try {
      cv::imwrite(imagePath, img, std::vector<int>{cv::IMWRITE_JPEG_QUALITY, 90});
      __android_log_print(ANDROID_LOG_DEBUG, TAG, "Image written to %s", imagePath.c_str());
    } catch (const std::exception& ex) {
      std::string msg("Failed to write the image to " + imagePath + ": " + ex.what());
      sns::throwException(env, sns::JEX_IO, msg.c_str());
    }
  }
}

JNIEXPORT jboolean JNICALL Java_io_hubsy_core_imgproc_BGSubtraction_analyseNative
    (JNIEnv* env, jobject, jlong modelPtr, jfloat threshold, jboolean cleanFGMask,
     jstring maskFileOut, jstring cleanedMaskFileOut, jstring resultFileOut) {

  if (modelPtr == 0) {
    sns::throwException(env, sns::JEX_ILLEGAL_STATE, "The model is not initialised");
    return jboolean(false);
  }
  auto* bgsub = reinterpret_cast<bgsub::BackgroundSubtraction*>(modelPtr);

  auto writeMask = [&](jstring path) {
    const std::string maskPath = sns::toStringUTF(env, path);
    try {
      bgsub->writeForegroundMask(maskPath);
    } catch (const std::exception& ex) {
      std::string msg("Failed to write mask to " + maskPath + ": " + ex.what());
      sns::throwException(env, sns::JEX_IO, msg.c_str());
      return false;
    }
    return true;
  };

  // Write the foreground mask if an output path is given.
  if (maskFileOut != nullptr && !writeMask(maskFileOut)) return jboolean(false);

  if (cleanFGMask) {
    // Clean the fgmask.
    bgsub->cleanForegroundMask();
    // Write the cleaned mask if requested.
    if (cleanedMaskFileOut != nullptr && !writeMask(cleanedMaskFileOut)) return jboolean(false);
  }

  // Check if the image result of the analysis should be written to a file.
  const std::string resultPath = resultFileOut == nullptr
                                 ? "" : sns::toStringUTF(env, resultFileOut);

  // Analyse the mask.
  bool outcome = bgsub->analyse(threshold, resultPath);
  __android_log_print(ANDROID_LOG_DEBUG, TAG, "Model analysed: %s",
                      (outcome ? "changes detected" : "no change"));

  return jboolean(outcome);
}
