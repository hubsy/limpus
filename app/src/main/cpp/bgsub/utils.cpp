#include "utils.h"

#include <algorithm>

#include <android/log.h>

#include <opencv2/imgcodecs.hpp>

namespace sensorable {

namespace {
const char* const TAG = "utils";
}

void throwException(JNIEnv* env, const char* className, const char* message) {
  __android_log_print(ANDROID_LOG_ERROR, TAG, "%s: %s", className, message);

  jclass cls = env->FindClass(className);
  if (cls == nullptr) {
    cls = env->FindClass(JEX_EXCEPTION);
  }
  env->ThrowNew(cls, message);
}

std::string toStringUTF(JNIEnv* env, jstring str) {
  const char* cstr = env->GetStringUTFChars(str, nullptr);
  std::string s(cstr); // Copy cstr.
  env->ReleaseStringUTFChars(str, cstr);
  return s;
}

cv::Mat wrapBufferInMat(JNIEnv* env, jobject buffer, int width, int height, int type,
                        int rowStride) {
  void* buf = env->GetDirectBufferAddress(buffer);
  if (buf == nullptr) {
    throwException(env, JEX_ILLEGAL_ARGUMENT, "Cannot access the direct byte buffer");
    return cv::Mat();
  }
  return cv::Mat(height, width, type, buf, static_cast<size_t>(rowStride));
}

cv::Mat wrapBufferInMat8U(JNIEnv* env, jobject buffer, int width, int height, int channels,
                          int rowStride) {
  return wrapBufferInMat(env, buffer, width, height, CV_8UC(channels), rowStride);
}

cv::Rect cropRegion(cv::Rect2f roi, int width, int height) {
  return {(int) std::round(roi.x * width),
          (int) std::round(roi.y * height),
          (int) std::round(roi.width * width),
          (int) std::round(roi.height * height)};
}

void rotate(cv::InputArray src, cv::OutputArray dst, int rotation) {
  // The OpenCV implementation of rotate is in modules/core/src/copy.cpp.
  switch (rotation) {
    case 0: {
      break; // Nothing to do.
    }
    case 90: {
      cv::rotate(src, dst, cv::ROTATE_90_CLOCKWISE); // Transpose + flip around Y.
      break;
    }
    case 180: {
      cv::rotate(src, dst, cv::ROTATE_180); // Flip around X and Y.
      break;
    }
    case 270: {
      cv::rotate(src, dst, cv::ROTATE_90_COUNTERCLOCKWISE); // Transpose + flip around X.
      break;
    }
    default: {
      __android_log_print(ANDROID_LOG_ERROR, TAG, "Cannot rotate by %d degrees", rotation);
      break;
    }
  }
}

namespace {
// This value is 2 ^ 18 - 1, and is used to clamp the RGB values before their ranges
// are normalized to eight bits.
const int kMaxChannelValue = 262143;

/**
 * Converts the YUV colour value to a packed ARGB value with an opaque alpha value.
 *
 * Based on YUV2RGB in tensorflow/examples/android/jni/yuv2rgb.cc.
 */
inline uint32_t yuv2RGB(int nY, int nU, int nV) {
  nY -= 16;
  nU -= 128;
  nV -= 128;
  if (nY < 0) nY = 0;

  // This is the floating point equivalent. We do the conversion in integer
  // because some Android devices do not have floating point in hardware.
  // nR = (int)(1.164 * nY + 2.018 * nU);
  // nG = (int)(1.164 * nY - 0.813 * nV - 0.391 * nU);
  // nB = (int)(1.164 * nY + 1.596 * nV);

  int nR = 1192 * nY + 1634 * nV;
  int nG = 1192 * nY - 833 * nV - 400 * nU;
  int nB = 1192 * nY + 2066 * nU;

  nR = std::min(kMaxChannelValue, std::max(0, nR));
  nG = std::min(kMaxChannelValue, std::max(0, nG));
  nB = std::min(kMaxChannelValue, std::max(0, nB));

  nR = (nR >> 10) & 0xff;
  nG = (nG >> 10) & 0xff;
  nB = (nB >> 10) & 0xff;

  return 0xff000000 | (nR << 16) | (nG << 8) | nB;
}
} // namespace

/* Based on ConvertYUV420ToARGB8888 in tensorflow/examples/android/jni/yuv2rgb.cc. */
void convertYUV420ToRGB888(const uint8_t* const yData,
                           const uint8_t* const uData,
                           const uint8_t* const vData,
                           const int width, const int height,
                           const int yRowStride, const int uvRowStride,
                           const int uvPixelStride, uint8_t* out) {
  for (int y = 0; y < height; y++) {
    const uint8_t* pY = yData + y * yRowStride;

    const int uv_row_start = (y >> 1) * uvRowStride;
    const uint8_t* pU = uData + uv_row_start;
    const uint8_t* pV = vData + uv_row_start;

    for (int x = 0; x < width; x++) {
      const int uv_offset = (x >> 1) * uvPixelStride;
      uint32_t argb = yuv2RGB(pY[x], pU[uv_offset], pV[uv_offset]);
      *out++ = static_cast<uint8_t>((argb >> 16) & 0xff);
      *out++ = static_cast<uint8_t>((argb >> 8) & 0xff);
      *out++ = static_cast<uint8_t>(argb & 0xff);
    }
  }
}

} // namespace sensorable
