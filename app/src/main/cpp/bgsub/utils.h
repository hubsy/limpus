#pragma once

#include <jni.h>

#include <opencv2/core/mat.hpp>

namespace sensorable {

const char* const JEX_EXCEPTION = "java/lang/Exception";
const char* const JEX_ILLEGAL_ARGUMENT = "java/lang/IllegalArgumentException";
const char* const JEX_ILLEGAL_STATE = "java/lang/IllegalStateException";
const char* const JEX_IO = "java/io/IOException";

/**
 * Throws a JNI exception of type className with the given message.
 */
void throwException(JNIEnv* env, const char* className, const char* message);

/**
 * Returns an std::string with UTF-8 encoding for the jstring.
 */
std::string toStringUTF(JNIEnv* env, jstring str);

/**
 * Constructs a Mat header of the specified type backed by the passed in buffer. The buffer must be
 * a direct byte buffer.
 *
 * The returned Mat is only valid for the duration of the JNI function call.
 *
 * If an error is encountered, a JNI exception is thrown and the returned Mat is empty.
 *
 * @param env The JNI env.
 * @param buffer The buffer to wrap.
 * @param width The width of the data.
 * @param height The height of the data.
 * @param type The OpenCV matrix element type, e.g. CV_8U1 or CV_32FC3.
 * @param rowStride The row stride of the data.
 * @return A matrix wrapping the buffer data.
 */
cv::Mat wrapBufferInMat(JNIEnv* env, jobject buffer, int width, int height, int type,
                        int rowStride);

/**
 * Specialisation of wrapBufferInMat for unsigned 8-bit types with the given number of channels.
 *
 * @param env The JNI env.
 * @param buffer The buffer to wrap.
 * @param width The width of the data.
 * @param height The height of the data.
 * @param channels The number of channels.
 * @param rowStride The row stride of the data.
 * @return A matrix wrapping the buffer data.
 */
cv::Mat wrapBufferInMat8U(JNIEnv* env, jobject buffer, int width, int height, int channels,
                          int rowStride);

/**
 * Returns a pointer of type T to the memory of a direct byte buffer.
 *
 * @tparam T The element data type.
 * @param env  The JNI env.
 * @param buffer A direct byte buffer.
 * @return A pointer to the buffer or nullptr on error.
 */
template<class T>
T* bufferAs(JNIEnv* env, jobject buffer) {
  void* buf = env->GetDirectBufferAddress(buffer);
  if (buf == nullptr) {
    throwException(env, JEX_ILLEGAL_ARGUMENT, "Cannot access the buffer");
    return nullptr;
  }
  return reinterpret_cast<T*>(buf);
}

/**
 * Returns the absolute crop region calculated for a given normalised region-of-interest, width and
 * height.
 *
 * @param roi  The normalised crop region.
 * @param width  The absolute image width.
 * @param height  The absolute image height.
 * @return  The absolute crop region.
 */
cv::Rect cropRegion(cv::Rect2f roi, int width, int height);

/**
 * Rotates an array/image clockwise by a multiple of 90 degrees. Operates in-place if src==dst and
 * the operation supports it, only allocating new data for dst if the aspect ratio of the output is
 * different from the input (i.e. 90 and 270 degree rotations with an aspect ratio other than 1:1).
 *
 * @param src The input array.
 * @param dst The output array.
 * @param rotation Rotation angle in degrees from {0, 90, 180, 270}.
 */
void rotate(cv::InputArray src, cv::OutputArray dst, int rotation);

/**
 *  Accepts a YUV 4:2:0 image with a plane of 8 bit Y samples followed by separate u and v planes
 *  with arbitrary row and column strides, containing 8 bit 2x2 subsampled chroma samples.
 *
 *  Converts to RGB output of the same pixel dimensions.
 *
 * @param yData The y plane data.
 * @param uData The u plane data.
 * @param vData The v plane data.
 * @param width The image width.
 * @param height The image height.
 * @param yRowStride The y plane row stride.
 * @param uvRowStride The u and v plane row strides.
 * @param uvPixelStride The u and v plane pixel strides.
 * @param out The output buffer of size height * width * 3.
 */
void convertYUV420ToRGB888(const uint8_t* yData,
                           const uint8_t* uData,
                           const uint8_t* vData,
                           int width, int height, int yRowStride, int uvRowStride,
                           int uvPixelStride, uint8_t* out);

} // namespace sensorable

