#include "utils_jni.h"

#include <vector>

#include <android/log.h>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video/tracking.hpp>

#include "utils.h"

namespace sns = sensorable;

JNIEXPORT jobject JNICALL Java_io_hubsy_core_imgproc_Utils_encodeJPEGNative
    (JNIEnv* env, jclass, jobject image, jint width, jint height, jint channels, jboolean isRGB,
     jboolean toGrey, jint rowStride, jint rotation, jint left, jint top, jint right, jint bottom,
     jint quality) {
  if (channels != 1 && channels != 3) {
    sns::throwException(env, sns::JEX_ILLEGAL_ARGUMENT, "Invalid channel count, must be 1 or 3");
    return nullptr;
  }

  // Create a Mat backed by the image data.
  cv::Mat img = sns::wrapBufferInMat8U(env, image, width, height, channels, rowStride);
  if (img.empty()) return nullptr;

  // Rotate if necessary.
  sns::rotate(img, img, rotation);

  // Get the cropped view if requested.
  left = std::max(0, left);
  top = std::max(0, top);
  right = std::min(img.cols, right);
  bottom = std::min(img.rows, bottom);
  if ((left > 0 || top > 0 || right < img.cols || bottom < img.rows)
      && left < right && top < bottom) {
    img = cv::Mat(img, cv::Rect(left, top, right - left, bottom - top));
  }

  if (channels == 3 && isRGB) { // Convert from RGB to BGR or greyscale.
    cv::Mat tmp;
    if (toGrey) {
      cv::cvtColor(img, tmp, cv::COLOR_RGB2GRAY, 1);
    } else {
      cv::cvtColor(img, tmp, cv::COLOR_RGB2BGR, 3);
    }
    img = tmp;
  } else if (channels == 3 && toGrey) { // Convert from BGR to greyscale.
    cv::Mat tmp;
    cv::cvtColor(img, tmp, cv::COLOR_BGR2GRAY, 1);
    img = tmp;
  }

  // Encode.
  std::vector<uchar> jpg;
  try {
    cv::imencode(".jpg", img, jpg, std::vector<int>{cv::IMWRITE_JPEG_QUALITY, quality});
  } catch (const cv::Exception& ex) {
    sns::throwException(env, sns::JEX_EXCEPTION, "Failed to encode the image as JPEG");
    return nullptr;
  }

  // Copy the JPEG data into a byte buffer.
  const size_t size = jpg.size() * sizeof(uchar);
  void* buf = malloc(size);
  if (buf == nullptr) {
    sns::throwException(env, sns::JEX_EXCEPTION,
                        "Failed to allocate memory for the direct ByteBuffer");
    return nullptr;
  }
  std::memcpy(buf, jpg.data(), size);
  jobject bbuf = env->NewDirectByteBuffer(buf, size);

  return bbuf;
}

JNIEXPORT void JNICALL Java_io_hubsy_core_imgproc_Utils_freeBufferNative
    (JNIEnv* env, jclass, jobject buffer) {
  void* buf = env->GetDirectBufferAddress(buffer);
  if (buf == nullptr) {
    sns::throwException(env, sns::JEX_ILLEGAL_ARGUMENT, "Cannot access the buffer");
    return;
  }
  free(buf);
}

JNIEXPORT void JNICALL Java_io_hubsy_core_imgproc_Utils_convertYUV420ToRGB888Native
    (JNIEnv* env, jclass, jobject yPlane, jobject uPlane, jobject vPlane, jint width, jint height,
     jint yRowStride, jint uvRowStride, jint uvPixelStride, jint rotation,
     jint left, jint top, jint right, jint bottom, jobject out) {
  const auto* yData = sns::bufferAs<const uint8_t>(env, yPlane);
  const auto* uData = sns::bufferAs<const uint8_t>(env, uPlane);
  const auto* vData = sns::bufferAs<const uint8_t>(env, vPlane);
  auto* rgbData = sns::bufferAs<uint8_t>(env, out);
  if (!yData || !uData || !vData || !rgbData) return;

  // Offset the YUV data to only convert the desired crop region.
  if (left != 0 || top != 0 || right != width || bottom != height) {
    uint32_t uleft = std::min(width, std::max(0, left));
    uint32_t utop = std::min(height, std::max(0, top));
    uint32_t uright = std::min(width, right);
    uint32_t ubottom = std::min(height, bottom);
    if (uright <= uleft || ubottom <= utop) {
      sns::throwException(env, sns::JEX_ILLEGAL_ARGUMENT, "Empty crop region");
      return;
    }

    width = uright - uleft;
    height = ubottom - utop;

    // The offsets must be multiples of 2 to match the subsampled UV planes.
    uleft &= ~1u;
    utop &= ~1u;

    // Offset the YUV data arrays.
    yData = &yData[utop * yRowStride + uleft];
    jint uvOffset = (utop >> 1u) * uvRowStride + (uleft >> 1u) * uvPixelStride;
    uData = &uData[uvOffset];
    vData = &vData[uvOffset];
  }

  if (env->GetDirectBufferCapacity(out) < (static_cast<jlong>(height) * width * 3L)) {
    sns::throwException(env, sns::JEX_ILLEGAL_ARGUMENT, "The output buffer is too small");
    return;
  }

  sns::convertYUV420ToRGB888(yData, uData, vData, width, height, yRowStride, uvRowStride,
                             uvPixelStride, rgbData);

  // Wrap the data in a Mat and rotate it if necessary.
  if (rotation != 0) {
    cv::Mat img(height, width, CV_8UC3, rgbData);
    sns::rotate(img, img, rotation);
  }
}

JNIEXPORT void JNICALL Java_io_hubsy_core_imgproc_Utils_convertRGB888ToGreyscaleNative
    (JNIEnv* env, jclass, jobject rgb, jint width, jint height, jobject out) {
  if (env->GetDirectBufferCapacity(out) < (static_cast<jlong>(height) * width)) {
    sns::throwException(env, sns::JEX_ILLEGAL_ARGUMENT, "The output buffer is too small");
    return;
  }

  // Wrap the buffers.
  const cv::Mat img = sns::wrapBufferInMat8U(env, rgb, width, height, 3, width * 3);
  cv::Mat grey = sns::wrapBufferInMat8U(env, out, width, height, 1, width);
  if (img.empty() || grey.empty()) return;

  cv::cvtColor(img, grey, cv::COLOR_RGB2GRAY, 1);
}

JNIEXPORT void JNICALL Java_io_hubsy_core_imgproc_Utils_convertImageToFloatNative
    (JNIEnv* env, jclass, jobject image, jint width, jint height, jint channels, jfloat scale,
     jfloat add, jobject out) {
  const size_t inSize = static_cast<size_t>(height) * width * channels;
  const size_t outSize = inSize * sizeof(float);
  if (static_cast<size_t>(env->GetDirectBufferCapacity(out)) < outSize) {
    sns::throwException(env, sns::JEX_ILLEGAL_ARGUMENT, "The output buffer is too small");
    return;
  }

  const auto* inData = sns::bufferAs<const uint8_t>(env, image);
  auto* outData = sns::bufferAs<float>(env, out);
  if (!inData || !outData) return;

  for (size_t i = 0; i < inSize; i++) {
    outData[i] = static_cast<float>(inData[i]) * scale + add;
  }
}

JNIEXPORT void JNICALL Java_io_hubsy_core_imgproc_Utils_calcOpticalFlowNative
    (JNIEnv* env, jclass, jobject frame0, jobject frame1, jint width, jint height, jint quality,
     jobject out) {
  if (env->GetDirectBufferCapacity(out)
      < (static_cast<jlong>(height) * width * 2L * static_cast<jlong>(sizeof(float)))) {
    sns::throwException(env, sns::JEX_ILLEGAL_ARGUMENT, "The output buffer is too small");
    return;
  }

  // Wrap the buffers.
  const cv::Mat prev = sns::wrapBufferInMat8U(env, frame0, width, height, 1, width);
  const cv::Mat next = sns::wrapBufferInMat8U(env, frame1, width, height, 1, width);
  cv::Mat flow = sns::wrapBufferInMat(env, out, width, height, CV_32FC2, width * 2 * sizeof(float));
  if (prev.empty() || next.empty() || flow.empty()) return;

  // Select the quality/performance preset.
  decltype(cv::DISOpticalFlow::PRESET_FAST) preset;
  switch (quality) {
    case 0: preset = cv::DISOpticalFlow::PRESET_ULTRAFAST;
      break;
    case 2: preset = cv::DISOpticalFlow::PRESET_MEDIUM;
      break;
    default:preset = cv::DISOpticalFlow::PRESET_FAST;
      break;
  }

  auto alg = cv::DISOpticalFlow::create(preset);
  alg->calc(prev, next, flow);
}

JNIEXPORT jfloatArray JNICALL Java_io_hubsy_core_imgproc_Utils_calcMeanFlowVectorNative
    (JNIEnv* env, jclass, jobject flow, jint width, jint height, jint left, jint top, jint right,
     jint bottom) {
  const int rowStride = width * 2 * sizeof(float);
  if (env->GetDirectBufferCapacity(flow) < (static_cast<jlong>(height) * rowStride)) {
    sns::throwException(env, sns::JEX_ILLEGAL_ARGUMENT, "The input buffer is too small");
    return nullptr;
  }

  if (left < 0 || top < 0 || left > right || top > bottom || right >= width || bottom >= height) {
    sns::throwException(env, sns::JEX_ILLEGAL_ARGUMENT, "Invalid region of interest");
    return nullptr;
  }

  // Get the pointer to the first element within the region of interest.
  const auto* vecs = sns::bufferAs<const float>(env, flow);
  vecs = &vecs[top * width * 2 + left * 2];

  // Calculate the mean vector.
  const jint numRows = bottom - top;
  const jint numCols = right - left;
  double sumX = 0;
  double sumY = 0;
  for (jint row = 0; row < numRows; row++) {
    for (jint col = 0; col < numCols; col++) {
      const jint offset = row * width * 2 + col * 2;
      sumX += vecs[offset];
      sumY += vecs[offset + 1];
    }
  }

  const int64_t count = static_cast<int64_t>(numRows) * numCols;
  const double meanX = count > 0 ? sumX / count : 0;
  const double meanY = count > 0 ? sumY / count : 0;

  // Allocate the result array.
  jfloatArray result = env->NewFloatArray(2);
  if (result == nullptr) {
    sns::throwException(env, sns::JEX_EXCEPTION, "Failed to allocate the result array");
    return nullptr;
  }

  // START OF CRITICAL REGION. Copy the mean vector into the array.
  void* resultVPtr = env->GetPrimitiveArrayCritical(result, nullptr);
  if (resultVPtr == nullptr) {
    sns::throwException(env, sns::JEX_EXCEPTION, "Failed to access the allocated result array");
    return nullptr;
  }
  auto* resultPtr = static_cast<float*>(resultVPtr);
  resultPtr[0] = static_cast<float>(meanX);
  resultPtr[1] = static_cast<float>(meanY);
  env->ReleasePrimitiveArrayCritical(result, resultVPtr, 0);
  // END OF CRITICAL REGION.

  return result;
}

JNIEXPORT void JNICALL Java_io_hubsy_core_imgproc_Utils_resizeNative
    (JNIEnv* env, jclass, jobject image, jint width, jint height, jint channels,
     jint left, jint top, jint right, jint bottom, jint outWidth, jint outHeight,
     jboolean qualityOverSpeed, jobject out) {
  // Create a Mat backed by the image data.
  const int rowStride = width * channels * sizeof(uint8_t);
  cv::Mat src = sns::wrapBufferInMat8U(env, image, width, height, channels, rowStride);
  if (src.empty()) return;

  if (left > 0 || top > 0 || right < width || bottom < height) {
    // Resize only part of the image.
    src = cv::Mat(src, cv::Rect(left, top, right - left, bottom - top));
  }

  const int outRowStride = outWidth * channels * sizeof(uint8_t);
  cv::Mat dst = sns::wrapBufferInMat8U(env, out, outWidth, outHeight, channels, outRowStride);
  if (dst.empty()) return;

  // Resize.
  int64_t inArea = static_cast<int64_t>(src.cols) * src.rows;
  int64_t outArea = static_cast<int64_t>(dst.cols) * dst.rows;
  if (outArea < inArea) { // Downsample.
    cv::resize(src, dst, dst.size(), 0, 0, qualityOverSpeed ? cv::INTER_AREA : cv::INTER_NEAREST);
  } else { // Upsample.
    cv::resize(src, dst, dst.size(), 0, 0, qualityOverSpeed ? cv::INTER_CUBIC : cv::INTER_LINEAR);
  }
}
