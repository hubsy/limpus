package io.hubsy.core;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.handlers.AsyncHandler;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferType;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.concurrent.Executors;

import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;
import io.hubsy.core.util.Radio;

/**
 * Abstracts AWS functionality for the app.
 */
public class AwsClient {
  private static final String TAG = "AwsClient";

  /**
   * File names ending in this suffix will be uploaded to the number plate crop folder.
   */
  public static final String NUMBER_PLATE_CROP_SUFFIX = "_npcrop.jpg";

  private static BasicAWSCredentials sAwsCreds; //Aws creds are loaded once and cached
  private static final Region sS3Region = Region.getRegion(Regions.US_EAST_1);

  private static AmazonDynamoDBAsyncClient sDynamoDBClient;
  // Failed requests needing to be retried, in asc. order of the event timestamp.
  private static PriorityQueue<SensorEventWriteHandler> sFailedPutRequests;

  private static int sNumOngoingTransfers;

  private static String sAwsUserName = "";
  private static String sAwsLogPrefix = "";
  private static String sAwsMediaPrefix = "";
  private static String sAwsDataPrefix = "";
  private static String sAwsNPPrefix = "";
  private static String sAwsConfigHistPrefix = "";
  private static String sAwsConfigFilePath = "";

  /**
   * Returns a credentials object or null if no credentials are present.
   */
  public static BasicAWSCredentials getAwsCreds(Settings settings) {
    //Do we have creds loaded?
    if (sAwsCreds != null) return sAwsCreds;

    //Get the creds from settings
    String awsKeyId = settings.getString(ConfigKeys.AwsKeyId);
    String awsKeySecret = settings.getString(ConfigKeys.AwsKeySecret);
    if (awsKeyId.isEmpty() || awsKeySecret.isEmpty()) {
      Log.w(TAG, "Missing AWS credentials");
      return null;
    }

    sAwsCreds = new BasicAWSCredentials(awsKeyId, awsKeySecret);
    return sAwsCreds;
  }

  /**
   * Updates the user name and AWS paths if necessary and returns the user name.
   */
  private static String updateAwsUserNameAndPaths(Settings settings) {
    final String userName = settings.getString(ConfigKeys.AwsUserName);
    if (!userName.equals(sAwsUserName) && !userName.isEmpty()) {
      sAwsUserName = userName;

      // Construct AWS paths for user.
      sAwsLogPrefix = "log/" + userName;
      sAwsMediaPrefix = "full/" + userName;
      sAwsDataPrefix = "data/" + userName;
      sAwsNPPrefix = "np/" + userName;

      String awsConfigPrefix = "config/" + userName;
      sAwsConfigHistPrefix = awsConfigPrefix + "/hist";
      sAwsConfigFilePath = awsConfigPrefix + "/config-s3.json";
    }

    return sAwsUserName;
  }

  /**
   * Upload all files in {@link Settings#APP_DIR_NEW}.
   *
   * @param ctx             A context.
   * @param checkForUpdates If true, also check if there are any updates to be downloaded.
   */
  public static void uploadFiles(final Context ctx, final boolean checkForUpdates) {
    final Context context = ctx.getApplicationContext();
    final Settings settings = Settings.getInstance(context);

    // Get the list of files waiting to be uploaded.
    File uploadDir =
        Helpers.createDirIfNotExists(Settings.APP_DIR + "/" + Settings.APP_DIR_NEW);
    if (uploadDir == null) return;
    final File[] filesToUpload = uploadDir.listFiles();
    if (filesToUpload == null || filesToUpload.length == 0) return;

    // Turn on radios if necessary.
    Radio.toggleRadios(context, true);

    // Connect to AWS S3.
    AwsClient.getAwsCreds(settings);
    if (sAwsCreds == null) return;

    final ClientConfiguration s3Config = new ClientConfiguration();
    final AmazonS3 s3 = new AmazonS3Client(sAwsCreds, sS3Region, s3Config);
    final TransferUtility transferUtility =
        TransferUtility.builder().s3Client(s3).context(context).build();

    if (sNumOngoingTransfers != 0) {
      // Reset in case this somehow got out of sync to ensure pending transfer checks are performed.
      Log.d(TAG, "Resetting the ongoing transfer counter to zero from ", sNumOngoingTransfers);
      sNumOngoingTransfers = 0;
    }

    final String awsBucket = settings.getString(ConfigKeys.AwsBucket);
    if (awsBucket.isEmpty()) {
      Log.w(TAG, "Cannot upload files to S3: no bucket configured");
      return;
    }
    Log.d(TAG, "S3 bucket: ", awsBucket);

    final String userName = updateAwsUserNameAndPaths(settings);
    if (!userName.isEmpty()) {
      final long maxFileSize = settings.getLong(ConfigKeys.MaxUploadSize);

      // Loop through the files and initiate uploading.
      for (File file : filesToUpload) {
        uploadFile(file, transferUtility, maxFileSize, awsBucket);
      }
    } else {
      Log.w(TAG, "Cannot upload files to S3: no user name configured");
    }

    if (checkForUpdates) {
      downloadUpdates(context, transferUtility, awsBucket, true);
    }

    // Retry failed DB transfers too.
    retryDBTransfers(context);
  }

  /**
   * Uploads a single file.
   *
   * @param file            The file to upload.
   * @param transferUtility The transfer utility.
   * @param maxFileSize     The max. allowed file size.
   * @param awsBucket       The target bucket.
   */
  private static void uploadFile(final File file, final TransferUtility transferUtility,
                                 final long maxFileSize, final String awsBucket) {
    if (!file.isFile()) {
      Log.w(TAG, "Not a regular file, cannot upload: ", file.getName());
      return;
    }

    // Check that the file is not too large.
    long fileSize = file.length();
    if (fileSize > maxFileSize) {
      Log.w(TAG, "File too large (", fileSize, " bytes), not uploading: ", file.getName());
      Helpers.moveFile(file, Settings.APP_DIR_ARCHIVED);
      return;
    }

    // Prepare file metadata for S3.
    final ObjectMetadata s3ObjMeta = new ObjectMetadata();
    s3ObjMeta.setCacheControl("max-age=32000000");

    // Extract the file extension.
    final String fileName = file.getName();
    final int extPeriodAt = fileName.lastIndexOf(".");
    String fileExt;
    boolean isDataFile;
    boolean isNPFile;
    if (extPeriodAt > 0 && extPeriodAt < fileName.length() - 1) {
      fileExt = fileName.substring(extPeriodAt + 1).toLowerCase();
      isNPFile = fileName.endsWith(NUMBER_PLATE_CROP_SUFFIX);
      isDataFile = !isNPFile && fileName.charAt(extPeriodAt - 1) == '_';
    } else {
      Log.e(TAG, "Cannot determine the file type, not uploading: ", fileName);
      Helpers.moveFile(file, Settings.APP_DIR_FAILED);
      return;
    }

    // Set params depending on the file extension.
    String awsFilePrefix;
    switch (fileExt) {
      case "jpg": {
        s3ObjMeta.setContentType("image/jpeg");
        awsFilePrefix = isDataFile ? sAwsDataPrefix
            : isNPFile ? sAwsNPPrefix
            : sAwsMediaPrefix;
        break;
      }
      case "png": {
        s3ObjMeta.setContentType("image/png");
        awsFilePrefix = sAwsDataPrefix;
        break;
      }
      case "mp4": {
        s3ObjMeta.setContentType("video/mp4");
        awsFilePrefix = isDataFile ? sAwsDataPrefix : sAwsMediaPrefix;
        break;
      }
      case "log": {
        s3ObjMeta.setContentType("text/plain");
        awsFilePrefix = sAwsLogPrefix;
        break;
      }
      case "json": {
        s3ObjMeta.setContentType("application/json");
        awsFilePrefix = sAwsConfigHistPrefix;
        break;
      }
      default: {
        Log.e(TAG, "Unknown file type (not uploading): ", fileName);
        return;
      }
    }

    if (awsFilePrefix.isEmpty()) {
      Log.w(TAG, "The AWS key prefix is empty, cannot upload file: ", fileName);
      Helpers.moveFile(file, Settings.APP_DIR_FAILED);
      return;
    }

    // Move the file to the queue folder.
    final File queuedFile = Helpers.moveFile(file, Settings.APP_DIR_QUEUED);
    if (queuedFile == null) {
      Log.w(TAG, "Failed to move file to upload folder: ", fileName);
      return;
    }

    // Derive the S3 file name. Remove the NP suffix if isNPFile.
    final String awsFileName = awsFilePrefix + "/"
        + (isNPFile
        ? fileName.substring(0, fileName.length() - NUMBER_PLATE_CROP_SUFFIX.length()) + ".jpg"
        : fileName);

    // Listener to track the transfer.
    final TransferListener uploadListener = new TransferListener() {
      @Override
      public void onStateChanged(int id, TransferState state) {
        Log.d(TAG, "Upload status changed to ", state.name(), " for id=", id, ": ", awsFileName);

        if (state == TransferState.COMPLETED || state == TransferState.CANCELED
            || state == TransferState.FAILED) {
          sNumOngoingTransfers--;

          // Delete successfully uploaded files and move others.
          if (state == TransferState.COMPLETED) {
            Helpers.deleteFile(queuedFile);
          } else {
            String destFolder = state == TransferState.FAILED
                ? Settings.APP_DIR_FAILED
                : Settings.APP_DIR_ARCHIVED;
            Helpers.moveFile(queuedFile, destFolder);

            Log.d(TAG, "Deleting transfer record id=", id);
            deleteTransferRecord(transferUtility, id);
          }

          // Clean up stale transfer records.
          if (sNumOngoingTransfers <= 0) hasPendingTransfers(transferUtility);
        }
      }

      @Override
      public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
      }

      @Override
      public void onError(int id, Exception ex) {
        Log.d(TAG, "Transfer error for id=", id, ": ", ex.getMessage());
      }
    };

    // Upload to S3.
    try {
      transferUtility.upload(awsBucket, awsFileName, queuedFile, s3ObjMeta, null, uploadListener);
      sNumOngoingTransfers++;
    } catch (Exception ex) {
      Log.w(TAG, "Failed to start upload of ", queuedFile.getName(), " to ", awsFileName,
          ": ", ex.getMessage());
      return;
    }

    Log.d(TAG, "Uploading ", queuedFile.getName());
  }

  /**
   * Download updates.
   *
   * @param ctx               A context.
   * @param checkForNewConfig Optionally download a new config file if available.
   */
  public static void downloadUpdates(final Context ctx, final boolean checkForNewConfig) {
    final Context context = ctx.getApplicationContext();
    final Settings settings = Settings.getInstance(context);

    // Turn on radios if necessary.
    Radio.toggleRadios(context, true);

    // Connect to AWS S3.
    AwsClient.getAwsCreds(settings);
    if (sAwsCreds == null) return;

    final ClientConfiguration s3Config = new ClientConfiguration();
    final AmazonS3 s3 = new AmazonS3Client(sAwsCreds, sS3Region, s3Config);
    final TransferUtility transferUtility =
        TransferUtility.builder().s3Client(s3).context(context).build();

    final String awsBucket = settings.getString(ConfigKeys.AwsBucket);
    if (awsBucket.isEmpty()) {
      Log.w(TAG, "Cannot download files from S3: no bucket configured");
      return;
    }
    Log.d(TAG, "S3 bucket: ", awsBucket);

    downloadUpdates(context, transferUtility, awsBucket, checkForNewConfig);
  }

  /**
   * Download updates.
   *
   * @param context           A context.
   * @param transferUtility   The transfer utility.
   * @param awsBucket         The AWS source bucket.
   * @param checkForNewConfig Optionally download a new config file if available.
   */
  private static void downloadUpdates(final Context context, final TransferUtility transferUtility,
                                      final String awsBucket, final boolean checkForNewConfig) {
    final Settings settings = Settings.getInstance(context);
    updateAwsUserNameAndPaths(settings);

    if (checkForNewConfig && !sAwsConfigFilePath.isEmpty()) {
      // Check if there is a new config file on S3.
      queueOtaUpdate(context, transferUtility, awsBucket, sAwsConfigFilePath,
          Settings.getFileInAppDir(Settings.APP_DIR_TMP, Settings.CONFIG_FILE_NAME));
    }

    // Try to download configured updates.
    for (final Settings.Update update : settings.updates()) {
      queueOtaUpdate(context, transferUtility, awsBucket, update.remotePath,
          Settings.getFileInAppDir(Settings.APP_DIR_TMP, update.localFilename));
      queueOtaUpdate(context, transferUtility, awsBucket, update.remoteHashPath,
          Settings.getFileInAppDir(Settings.APP_DIR_TMP, update.localHashFilename));
    }
  }

  /**
   * Checks the upload queue folder and cleans up the list of transfer records both for uploads and
   * downloads.
   *
   * @param transferUtility The S3 transfer utility.
   * @return True if any pending transfers remain, false otherwise.
   */
  @SuppressWarnings("UnusedReturnValue")
  private static boolean hasPendingTransfers(final TransferUtility transferUtility) {
    Log.d(TAG, "Checking upload queue for transfers");

    // Get list of files in the queue folder.
    final File queueDir = Helpers.createDirIfNotExists(Settings.APP_DIR + "/"
        + Settings.APP_DIR_QUEUED);
    if (queueDir == null) return hasPendingTransfersDown(transferUtility);

    final File[] queuedFiles = queueDir.listFiles();
    if (queuedFiles == null) return hasPendingTransfersDown(transferUtility);

    // Get all upload records.
    final List<TransferObserver> observers =
        transferUtility.getTransfersWithType(TransferType.UPLOAD);
    int numObservers = observers.size();

    if (queuedFiles.length == 0) {
      // No files in queue, clean up all dangling transfer records.
      if (numObservers > 0) {
        Log.d(TAG, "No queued files found, deleting all transfer records");
        for (TransferObserver observer : observers) {
          int id = observer.getId();
          Log.d(TAG, "Deleting transfer record id=", id, " ; state=", observer.getState().name());
          deleteTransferRecord(transferUtility, id);
        }
      }
      return hasPendingTransfersDown(transferUtility);
    }

    // Construct a set with the names of all queued files processed by this function invocation
    // (the folder contents might change concurrently).
    final HashSet<String> queuedFileNames = new HashSet<>(queuedFiles.length);
    for (File f : queuedFiles) {
      queuedFileNames.add(f.getName());
    }

    // Delete dangling observers and files whose transfer state is final.
    final HashSet<String> ongoingTransfers = new HashSet<>(numObservers);
    for (TransferObserver observer : observers) {
      TransferState state = observer.getState();
      int id = observer.getId();
      File pendingFile = new File(observer.getAbsoluteFilePath());
      if (!pendingFile.exists()) {
        // The file no longer exists, delete the record.
        Log.d(TAG, "Observer id=", id, " ; state=", state.name(), " ; file=",
            pendingFile.getName(), ": file does not exist, deleting the transfer record");
        deleteTransferRecord(transferUtility, id);
        numObservers--;
      } else if (state == TransferState.COMPLETED || state == TransferState.CANCELED
          || state == TransferState.FAILED) {
        // The transfer is no longer in progress. Move the file and delete the transfer record.
        Log.d(TAG, "Observer id=", id, " ; state=", state.name(), " ; file=",
            pendingFile.getName(), ": transfer finished, cleaning up");
        String destFolder = (state == TransferState.FAILED)
            ? Settings.APP_DIR_FAILED
            : Settings.APP_DIR_ARCHIVED;
        Helpers.moveFile(pendingFile, destFolder);
        queuedFileNames.remove(pendingFile.getName());
        deleteTransferRecord(transferUtility, id);
        numObservers--;
        sNumOngoingTransfers--;
      } else {
        ongoingTransfers.add(pendingFile.getName());
      }
    }

    // Check if any files with no observers are in the queue.
    int numOrphaned = queuedFileNames.size() >= ongoingTransfers.size() ?
        // Should be true unless files in dir changed concurrently.
        queuedFileNames.size() - ongoingTransfers.size() : 0;
    final ArrayList<File> orphanedFiles = new ArrayList<>(numOrphaned);
    for (File f : queuedFiles) {
      String name = f.getName();
      if (!ongoingTransfers.contains(name) && queuedFileNames.contains(name)) {
        orphanedFiles.add(f);
      }
    }
    numOrphaned = orphanedFiles.size(); // Get the accurate count handled by this invocation.

    Log.d(TAG, Locale.ENGLISH, "Ongoing transfers %d; transfers cleaned up %d; orphaned files %d",
        numObservers, queuedFiles.length - queuedFileNames.size(), numOrphaned);

    // Deal with orphaned files.
    if (numOrphaned > 0) retryUploads(orphanedFiles);

    // Deal with pending downloads if all uploads are dealt with.
    if (numObservers == 0) {
      boolean hasPendingDownloads = hasPendingTransfersDown(transferUtility);
      if (!hasPendingDownloads && sNumOngoingTransfers != 0) {
        // The number of ongoing transfers got out of sync, probably due to the AWS client
        // resuming old transfers after a restart.
        Log.d(TAG, "Resetting the ongoing transfer counter to zero from ", sNumOngoingTransfers);
        sNumOngoingTransfers = 0;
      }
      return hasPendingDownloads;
    }

    // There are still some unfinished uploads.
    return true;
  }

  /**
   * Checks for pending downloads. No file checking takes place.
   *
   * @param transferUtility The S3 transfer utility.
   * @return Returns true if there are any pending downloads.
   */
  private static boolean hasPendingTransfersDown(final TransferUtility transferUtility) {
    Log.d(TAG, "Checking download queue for transfers");

    // Get all download records.
    final List<TransferObserver> observers =
        transferUtility.getTransfersWithType(TransferType.DOWNLOAD);
    int numObservers = observers.size();

    // Delete dangling observers.
    for (TransferObserver observer : observers) {
      TransferState state = observer.getState();
      int id = observer.getId();
      if (state == TransferState.COMPLETED || state == TransferState.CANCELED
          || state == TransferState.FAILED) {
        // The transfer is no longer in progress, delete the record.
        Log.d(TAG, "Observer (down) id=", id, " ; state=", state.name(),
            ": transfer finished, cleaning up");
        deleteTransferRecord(transferUtility, id);
        numObservers--;
        sNumOngoingTransfers--;
      }
    }

    return numObservers != 0;
  }

  /**
   * Download an OTA update file from S3.
   */
  private static void queueOtaUpdate(final Context context, final TransferUtility transferUtility,
                                     final String awsBucket, final String awsKey,
                                     final File saveAs) {
    final String fileName = saveAs.getName();

    // Do we have the remote file name at all?
    if (awsKey == null || awsKey.trim().isEmpty()) {
      Log.e(TAG, "Cannot download update, no remote location given for: ", fileName);
      return;
    }

    // Listener to track the transfer.
    final TransferListener downloadListener = new TransferListener() {
      @Override
      public void onStateChanged(int id, TransferState state) {
        Log.d(TAG, "Download status changed to ", state.name(), " for id=", id, ": ", awsKey);

        if (state == TransferState.COMPLETED || state == TransferState.CANCELED
            || state == TransferState.FAILED) {
          sNumOngoingTransfers--;

          if (state == TransferState.COMPLETED) {
            // Move the downloaded file, delete it from S3 and notify listeners.
            File destFile = Helpers.moveFile(saveAs, Settings.APP_DIR_OTA);
            if (destFile != null) {
              new DeleteFileFromS3Task(context, awsBucket, awsKey)
                  .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
              sendFileDownloadedEvent(context, awsKey, destFile);
            }
          } else {
            Log.d(TAG, "Deleting transfer record id=", id);
            deleteTransferRecord(transferUtility, id);
          }

          // Clean up stale transfer records.
          if (sNumOngoingTransfers <= 0) hasPendingTransfers(transferUtility);
        }
      }

      @Override
      public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
      }

      @Override
      public void onError(int id, Exception ex) {
        if (ex instanceof AmazonServiceException
            && ((AmazonServiceException) ex).getStatusCode() == 403) {
          // AWS returns 403 if file not found, even if you have permissions to download it.
          Log.d(TAG, "Nothing to download (403) for id=", id);
        } else {
          // Log the full error message as something else went wrong.
          Log.d(TAG, "Download error for id=", id, ": ", ex.getMessage());
        }
      }
    };

    // Initiate the download.
    try {
      transferUtility.download(awsBucket, awsKey, saveAs, downloadListener);
      sNumOngoingTransfers++;
    } catch (Exception ex) {
      Log.w(TAG, "Failed to start download from ", awsBucket, ": ", awsKey, ": ", ex.getMessage());
      return;
    }
    Log.d(TAG, "Downloading from ", awsBucket, ": ", awsKey);
  }

  /**
   * Deletes the transfer record with the given ID and log an error if the operation fails.
   */
  private static void deleteTransferRecord(TransferUtility transferUtility, int id) {
    if (!transferUtility.deleteTransferRecord(id)) {
      Log.d(TAG, "No transfer record to delete for id=", id);
    }
  }

  /**
   * Sends file update events for downloaded OTA files.
   *
   * @param context A context.
   * @param awsKey  The AWS key of the downloaded file.
   * @param file    The downloaded file.
   */
  private static void sendFileDownloadedEvent(final Context context, final String awsKey,
                                              final File file) {
    final Settings settings = Settings.getInstance(context);

    String action = null;
    if (awsKey.equals(sAwsConfigFilePath)) {
      action = Constants.ACTION_NEW_CONFIG_FILE;
    } else if (awsKey.equals(settings.getString(ConfigKeys.AwsAppName))) {
      action = Constants.ACTION_NEW_APP_UPDATE;
    } else if (awsKey.equals(settings.getString(ConfigKeys.AwsAppName) + ".md5")) {
      action = Constants.ACTION_NEW_APP_UPDATE_HASH;
    } else if (awsKey.equals(settings.getString(ConfigKeys.AwsHealthMonitorName))) {
      action = Constants.ACTION_NEW_HMON_UPDATE;
    } else if (awsKey.equals(settings.getString(ConfigKeys.AwsHealthMonitorName) + ".md5")) {
      action = Constants.ACTION_NEW_HMON_UPDATE_HASH;
    } else if (awsKey.equals(settings.getString(ConfigKeys.AwsImageMaskName))) {
      action = Constants.ACTION_NEW_IMAGE_MASK;
    } else if (awsKey.equals(settings.getString(ConfigKeys.AwsImageMaskName) + ".md5")) {
      action = Constants.ACTION_NEW_IMAGE_MASK_HASH;
    } else if (awsKey.equals(settings.getString(ConfigKeys.AwsToolsName))) {
      action = Constants.ACTION_NEW_TOOLS_UPDATE;
    } else if (awsKey.equals(settings.getString(ConfigKeys.AwsToolsName) + ".md5")) {
      action = Constants.ACTION_NEW_TOOLS_UPDATE_HASH;
    } else if (awsKey.equals(settings.getString(ConfigKeys.AwsUpdaterName))) {
      action = Constants.ACTION_NEW_UPDATER_UPDATE;
    } else if (awsKey.equals(settings.getString(ConfigKeys.AwsUpdaterName) + ".md5")) {
      action = Constants.ACTION_NEW_UPDATER_UPDATE_HASH;
    } else if (awsKey.equals(settings.getString(ConfigKeys.AwsUpgradeName))) {
      action = Constants.ACTION_NEW_UPGRADE_SCRIPT;
    } else if (awsKey.equals(settings.getString(ConfigKeys.AwsUpgradeName) + ".md5")) {
      action = Constants.ACTION_NEW_UPGRADE_SCRIPT_HASH;
    }

    if (action != null) {
      Intent intent = new Intent(action);
      intent.putExtra(Constants.EXTRA_AWS_KEY, awsKey);
      intent.putExtra(Constants.EXTRA_FILE_PATH, file.getAbsolutePath());

      LocalBroadcastManager bm = LocalBroadcastManager.getInstance(context);
      bm.sendBroadcast(intent);
    }
  }

  /**
   * Deletes a file from S3 using a background thread.
   */
  private static class DeleteFileFromS3Task extends AsyncTask<Void, Void, Void> {
    private final String mAwsBucket;
    private final String mAwsKey;
    private AWSCredentials mAwsCredentials;

    DeleteFileFromS3Task(final Context context, final String bucket, final String key) {
      mAwsBucket = bucket;
      mAwsKey = key;

      // Get the AWS credentials.
      final Settings settings = Settings.getInstance(context);
      mAwsCredentials = getAwsCreds(settings);
    }

    @Override
    protected Void doInBackground(Void... params) {
      if (mAwsCredentials == null) return null;

      AmazonS3Client s3 = new AmazonS3Client(mAwsCredentials, sS3Region, new ClientConfiguration());
      try {
        s3.deleteObject(mAwsBucket, mAwsKey);
        Log.d(TAG, "Deleted ", mAwsBucket, ": ", mAwsKey);
      } catch (AmazonClientException e) {
        if (e instanceof AmazonServiceException) {
          AmazonServiceException ex = (AmazonServiceException) e;
          Log.w(TAG, Locale.ENGLISH,
              "Failed to delete %s: %s\tHTTP: %d / AWS error: %s / AWS msg: %s",
              mAwsBucket, mAwsKey, ex.getStatusCode(), ex.getErrorCode(), ex.getErrorMessage());
        } else {
          Log.w(TAG, Locale.ENGLISH, "Failed to delete %s: %s\tCan retry: %b",
              mAwsBucket, mAwsKey, e.isRetryable());
        }
      }

      return null;
    }

    @Override
    protected void onCancelled() {
      Log.w(TAG, "Delete task cancelled (status unkown) ", mAwsBucket, ": ", mAwsKey);
    }
  }

  /**
   * Moves the given files to {@link Settings#APP_DIR_NEW} to retry uploading them.
   */
  private static void retryUploads(final List<File> files) {
    if (files == null) return;

    for (File f : files) {
      try {
        File dest = Settings.getFileInAppDir(Settings.APP_DIR_NEW, f.getName());
        if (f.renameTo(dest)) {
          Log.d(TAG, "Re-scheduling for upload: ", f.getName());
        } else {
          Log.e(TAG, "Failed to move file to new: trying to delete: ", f.getName());
          Helpers.deleteFile(f);
        }
      } catch (Exception e) {
        Log.e(TAG, "Failed to move file to new: ", f.getName(), ": ", e.getMessage());
      }
    }
  }

  /**
   * Get the shared DynamoDB client instance. Create a new one if necessary.
   *
   * @param settings The settings.
   * @return The database client.
   */
  @Nullable
  private static AmazonDynamoDBAsyncClient getDynamoDBClient(@NonNull final Settings settings) {
    if (sDynamoDBClient != null) return sDynamoDBClient;

    // Get our AWS credentials.
    AwsClient.getAwsCreds(settings);
    if (sAwsCreds == null) return null;

    // Create and configure the client.
    final ClientConfiguration config = new ClientConfiguration()
        .withEnableGzip(true);
    sDynamoDBClient = new AmazonDynamoDBAsyncClient(
        sAwsCreds, config, Executors.newFixedThreadPool(3));

    return sDynamoDBClient;
  }

  /**
   * Uploads a sensor event.
   *
   * @param ctx       The context to use.
   * @param timestamp The event timestamp in Unix time.
   * @param sensors   The sensors responsible for the event.
   */
  public static void uploadSensorEvent(@NonNull final Context ctx, final long timestamp,
                                       @Nullable final Set<String> sensors) {
    if (sensors == null || sensors.isEmpty()) {
      Log.d(TAG, "Empty device list, skipping sensor event upload");
      return;
    }

    // Create the settings object if necessary.
    final Context context = ctx.getApplicationContext();
    final Settings settings = Settings.getInstance(context);

    // The user name must be configured for the upload.
    final String userName = updateAwsUserNameAndPaths(settings);
    if (userName.isEmpty()) {
      Log.w(TAG, "Cannot upload sensor event: missing AWS username in config");
      return;
    }

    // Turn on radios if necessary. Assume a photo will be taken too, whose upload can take care of
    // turning radios off again.
    Radio.toggleRadios(context, true);

    // Set the table name based on the configured software stage.
    final String env = settings.getString(ConfigKeys.SoftwareEnv);
    final String table = "SensorEvent_"
        + (env.trim().isEmpty() ? "DEV" : env.toUpperCase(Locale.ROOT));

    // Get the client.
    final AmazonDynamoDBAsyncClient client = getDynamoDBClient(settings);
    if (client == null) return;

    // Format the timestamp.
    final String ts = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.ROOT)
        .format(new Date(timestamp));

    // Create the request. Do not replace any item with the same key (device, timestamp).
    final PutItemRequest request = new PutItemRequest()
        .withTableName(table)
        .withConditionExpression("attribute_not_exists(device)")
        .addItemEntry("device", new AttributeValue(userName))
        .addItemEntry("timestamp", new AttributeValue(ts))
        .addItemEntry("sensors", new AttributeValue().withSS(sensors));

    Log.d(TAG, "Uploading sensor event to ", table, " with timestamp=", timestamp);
    sendPutRequest(client, request, new SensorEventWriteHandler(request, timestamp));
  }

  /**
   * Retries all previously failed database transfers.
   */
  private static void retryDBTransfers(Context context) {
    if (sFailedPutRequests == null || sFailedPutRequests.isEmpty()) return;

    final Settings settings = Settings.getInstance(context);
    final AmazonDynamoDBAsyncClient client = getDynamoDBClient(settings);
    if (client == null) return;

    // Retry all failed sensor event put requests.
    Log.d(TAG, "Retrying ", sFailedPutRequests.size(), " sensor event put requests");
    for (int i = 0; i < sFailedPutRequests.size(); i++) {
      SensorEventWriteHandler handler = sFailedPutRequests.poll();
      if (handler == null) continue; // Just to be safe, should never be the case.

      sendPutRequest(client, handler.request, handler);
    }
  }

  /**
   * Submits an async put request.
   *
   * @param client  The client.
   * @param request The request.
   * @param handler The result handler.
   */
  private static void sendPutRequest(final AmazonDynamoDBAsyncClient client,
                                     final PutItemRequest request,
                                     final AsyncHandler<PutItemRequest, PutItemResult> handler) {
    try {
      client.putItemAsync(request, handler);
    } catch (Exception e) {
      Log.e(TAG, "Failed to submit async DB put request: ", e.getMessage());
    }
  }

  private static class SensorEventWriteHandler
      implements AsyncHandler<PutItemRequest, PutItemResult> {

    private static final String ERROR_PREFIX = "Failed to write sensor event with timestamp=";

    public final PutItemRequest request;
    public final long timestamp;

    private SensorEventWriteHandler(final PutItemRequest request, final long timestamp) {
      this.request = request;
      this.timestamp = timestamp;
    }

    @Override
    public void onError(Exception exception) {
      boolean canRetry = false;
      boolean alreadyLogged = false;
      if (exception instanceof ConditionalCheckFailedException) {
        Log.d(TAG, "Sensor event already in DB: event timestamp=", timestamp, ": ",
            exception.getMessage());
        alreadyLogged = true;
      } else if (exception instanceof AmazonServiceException) {
        AmazonServiceException e = (AmazonServiceException) exception;
        if (e.getErrorType() != AmazonServiceException.ErrorType.Client && e.isRetryable()) {
          canRetry = true;
        }
      } else if (exception instanceof AmazonClientException) {
        AmazonClientException e = (AmazonClientException) exception;
        if (e.isRetryable()) {
          canRetry = true;
        }
      }

      if (canRetry) {
        Log.d(TAG, ERROR_PREFIX, timestamp, " (can retry): ", exception.getMessage());
        scheduleRetry();
      } else if (!alreadyLogged) {
        Log.w(TAG, ERROR_PREFIX, timestamp, ": ", exception.getMessage());
      }
    }

    @Override
    public void onSuccess(PutItemRequest request, PutItemResult putItemResult) {
      Log.d(TAG, "Successfully uploaded sensor event with timestamp=", timestamp);
    }

    /**
     * Schedules the failed request to be retried later.
     */
    private void scheduleRetry() {
      final int maxQueueSize = 64;
      if (sFailedPutRequests == null) {
        sFailedPutRequests = new PriorityQueue<>(maxQueueSize,
            (o1, o2) -> Long.compare(o1.timestamp, o2.timestamp));
      }

      // Add to the queue, discarding the oldest element first if the queue is full.
      if (sFailedPutRequests.size() >= maxQueueSize) {
        SensorEventWriteHandler old = sFailedPutRequests.poll(); // Drop the oldest request.
        Log.d(TAG, "Dropping failed sensor event put request with timestamp=", old.timestamp);
      }
      sFailedPutRequests.offer(this);
    }
  } // SensorEventWriteHandler
}
