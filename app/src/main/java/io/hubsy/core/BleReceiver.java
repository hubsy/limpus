package io.hubsy.core;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import io.hubsy.core.Settings.BleActivationsKeys;
import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.beaconscanner.BeaconScannerService;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;

public class BleReceiver extends BroadcastReceiver {
  private static final String TAG = "BleReceiver";

  // The timestamp of the last event to pass the activation rules.
  private static long sLastEventTime = Long.MIN_VALUE;

  // Maps device or sensor IDs to lists of timestamps of their last activations. The activations are
  // ordered from most recent to oldest.
  private static final HashMap<String, List<Long>> sDevicesLastSeen = new HashMap<>();
  private static final int LAST_SEEN_MAX_SIZE = 1024;
  // Maps sensor IDs to the timestamp of their last activation to pass all rules.
  private static final HashMap<String, Long> sLastSensorEvent = new HashMap<>();

  private static String sActivationRulesJSON;
  private static List<ActivationRule> sActivationRules;

  @Override
  public void onReceive(Context context, Intent intent) {
    final String action = intent.getAction();
    if (!BeaconScannerService.ACTION_DEVICE_DISCOVERED.equals(action)) {
      Log.e(TAG, "Error unexpected action: ", action);
      return;
    }

    final Context appContext = context.getApplicationContext();
    final Settings settings = Settings.getInstance(appContext);
    settings.readConfigFile(true); // Config file may have changed since the last run.

    // Get the device address from the intent.
    final Bundle extras = intent.getExtras();
    final String mac = extras == null ? null
        : extras.getString(BeaconScannerService.EXTRA_DEVICE_ADDRESS);
    if (mac == null) {
      Log.e(TAG, "Error missing device address in intent");
      return;
    }

    // The device ID may include zero, one or more sensor suffixes in canonical order.
    final int sensorMask = extras.getInt(BeaconScannerService.EXTRA_SENSOR_TYPES, 0);
    final String device = canonicalDeviceID(mac, sensorMask);
    final List<String> sensors = sensorIDs(mac, sensorMask);

    // If the device does not report distinct sensor types, then the MAC is used instead.
    if (sensors.isEmpty()) sensors.add(mac);

    // Record the activation time for this device.
    final long currentUpTime = SystemClock.elapsedRealtime();
    final List<List<Long>> lastSeen = updateDevicesLastSeen(sensors, currentUpTime);

    // Rate limit requests.
    final long maxFrequency = settings.getLong(ConfigKeys.BleMaxFrequency);
    if (sLastEventTime + maxFrequency > currentUpTime) {
      // Skip this request, they are coming in too quickly.
      Log.d(TAG, "Skipping request (rate limited)");
      return;
    }

    // Rate limit activations from this device or sensor. If multiple sensors are reported in this
    // activation, then select the one with the shortest taper duration.
    Pair<Integer, Long> minTaper = minSensorTaperDuration(lastSeen, settings, currentUpTime);
    final String sensor = minTaper != null ? sensors.get(minTaper.first) : device;
    long taperDuration = minTaper != null ? minTaper.second : 0;

    // Check if the sensor's taper duration blocks the event from proceeding.
    Long lastSensorEventTime = sLastSensorEvent.get(sensor);
    long lastSensorEvent = lastSensorEventTime == null ? Long.MIN_VALUE : lastSensorEventTime;
    if (lastSensorEvent + taperDuration > currentUpTime) {
      Log.d(TAG, Locale.ENGLISH,
          "Skipping request (%s (%s) rate limited to %ds, next allowed in %ds)",
          sensor, extractSensorFlags(device), taperDuration / 1000L,
          (lastSensorEvent + taperDuration - currentUpTime) / 1000L);
      return;
    }

    // Test request against activation rules.
    loadActivationRules(settings);
    final LinkedHashSet<String> triggers = new LinkedHashSet<>();
    final ActivationRule matchedRule = sActivationRules != null
        ? matchActivationRules(currentUpTime, sensorMask, triggers) : null;
    if ((sActivationRules != null && matchedRule == null)
        || (sActivationRules == null
        && !isBasicActivationRuleSatisfied(settings, currentUpTime, triggers))) {
      Log.d(TAG, "Skipping request (insufficient activations)");
      return;
    }

    // Remember the time of the last successful (before pause) event overall and per sensor.
    sLastEventTime = currentUpTime;
    for (String s : sensors) {
      sLastSensorEvent.put(s, currentUpTime);
    }

    if (settings.getBool(ConfigKeys.Pause)) {
      Log.d(TAG, "Photo taking is on pause");
    } else {
      final long eventTimestamp = System.currentTimeMillis();

      // Prepare to take a photo.
      final Intent i = new Intent(context, CameraActivity.class);
      int reso = settings.getInt(ConfigKeys.CamReso);
      String triggerMsg = TextUtils.join(", ", triggers);
      i.putExtra(ConfigKeys.CamReso.extraName(), reso);
      i.putExtra(Constants.EXTRA_EVENT_TRIGGER, triggerMsg);
      i.putExtra(Constants.EXTRA_EVENT_SENSORS, triggers.toArray(new String[0]));
      i.putExtra(Constants.EXTRA_EVENT_TIMESTAMP, eventTimestamp);
      i.putExtra(Constants.EXTRA_REFERENCE_EVENT_TIMESTAMP, eventTimestamp);
      context.startActivity(i);
      CameraActivity.acquireWakelock(appContext);

      // Schedule delayed events if the matched rule specifies any.
      if (matchedRule != null && matchedRule.delayedEvents != null) {
        scheduleDelayedEvents(context, matchedRule.delayedEvents, currentUpTime, eventTimestamp,
            triggerMsg, reso);
      }
    }
  }

  /**
   * Inserts the currentUpTime at the beginning of the {@link #sDevicesLastSeen} lists for each
   * of the sensors.
   *
   * @param sensors       The sensors to update.
   * @param currentUpTime The timestamp to insert.
   * @return The updated list of last seen timestamps for each of the sensors, in the same order.
   */
  private List<List<Long>> updateDevicesLastSeen(List<String> sensors, long currentUpTime) {
    ArrayList<List<Long>> sensorsLastSeen = new ArrayList<>(sensors.size());
    for (String id : sensors) {
      // Insert the new activation timestamp at the front of the list.
      List<Long> lastSeen = sDevicesLastSeen.get(id);
      if (lastSeen == null) {
        lastSeen = new LinkedList<>();
        sDevicesLastSeen.put(id, lastSeen);
      }
      lastSeen.add(0, currentUpTime);

      // Drop old activation timestamps for this sensor.
      while (lastSeen.size() > LAST_SEEN_MAX_SIZE) {
        lastSeen.remove(lastSeen.size() - 1);
      }

      sensorsLastSeen.add(lastSeen);
    }

    return sensorsLastSeen;
  }

  /**
   * Schedules additional events after the given delays.
   *
   * @param context        A context.
   * @param delays         The list of delays. One event will be generated for each element.
   * @param currentUpTime  The current time, in SystemClock.elapsedRealtime.
   * @param eventTimestamp The timestamp of the initial event.
   * @param triggerMsg     The base event trigger message passed to
   *                       {@link Constants#EXTRA_EVENT_TRIGGER}.
   * @param resolution     The requested photo resolution.
   */
  private void scheduleDelayedEvents(Context context, final ArrayList<Long> delays,
                                     final long currentUpTime, final long eventTimestamp,
                                     final String triggerMsg, final int resolution) {
    Log.d(TAG, "Scheduling ", delays.size(), " delayed events");
    final String msg = triggerMsg + " (source event was at "
        + DateFormat.format("HH:mm:ss", eventTimestamp).toString() + ")";
    for (int i = 0; i < delays.size(); i++) {
      Bundle extras = new Bundle();
      extras.putInt(ConfigKeys.CamReso.extraName(), resolution);
      extras.putString(Constants.EXTRA_EVENT_TRIGGER, msg);
      extras.putLong(Constants.EXTRA_REFERENCE_EVENT_TIMESTAMP, eventTimestamp);
      ScheduleReceiver.scheduleOneOffEvent(context, currentUpTime + delays.get(i), extras);
    }
  }

  /**
   * Calculates the taper duration for each of the entries in sensorsLastSeen and selects the
   * shortest one. Returns a pair with the selected index into sensorsLastSeen and the corresponding
   * taper duration. Returns null if sensorsLastSeen is empty.
   */
  @Nullable
  private Pair<Integer, Long> minSensorTaperDuration(@NonNull List<List<Long>> sensorsLastSeen,
                                                     @NonNull Settings settings,
                                                     long currentUpTime) {
    double a = settings.getDouble(ConfigKeys.BleTaperParamA);
    double b = settings.getDouble(ConfigKeys.BleTaperParamB);
    double c = settings.getDouble(ConfigKeys.BleTaperParamC);
    double linear = settings.getDouble(ConfigKeys.BleTaperParamLinear);
    long maxDuration = settings.getInt(ConfigKeys.BleTaperMaxDuration) * 1000L;
    long window = settings.getInt(ConfigKeys.BleTaperWindow) * 1000L;

    int minIdx = -1;
    long minTaperDuration = 0;
    for (int i = 0; i < sensorsLastSeen.size(); i++) {
      long taperDuration = sensorTaperDuration(
          sensorsLastSeen.get(i), a, b, c, linear, window, maxDuration, currentUpTime);
      if (minIdx < 0 || taperDuration < minTaperDuration) {
        minIdx = i;
        minTaperDuration = taperDuration;
      }
    }

    return minIdx < 0 ? null : new Pair<>(minIdx, minTaperDuration);
  }

  /**
   * Calculates and returns the duration, in millis, for which to taper this sensor since its
   * activation last caused an event to be generated.
   *
   * @param lastSeen      The list of the sensors activations. Sorted in descending order, with the
   *                      current activation time at index 0.
   * @param a             Coefficient a to the tapering equation.
   * @param b             Coefficient b to the tapering equation.
   * @param c             Coefficient c to the tapering equation.
   * @param linear        The linear coefficient to the tapering equation.
   * @param window        The window of lastSeen activations to consider, in millis.
   * @param maxDuration   The maximum tapering duration, in millis.
   * @param currentUpTime The current uptime in millis.
   * @return The tapering duration in milliseconds.
   */
  private long sensorTaperDuration(@NonNull List<Long> lastSeen, double a, double b, double c,
                                   double linear, long window, long maxDuration,
                                   long currentUpTime) {
    if (a < 0) a = 0.;
    if (b < 0) b = 0.;
    if (c < 0) c = 0.;
    if (linear < 0) linear = 0.;
    if (window < 0) window = 0;

    // Count the activations.
    int x = 0;
    for (long t : lastSeen) {
      // The comparison must be inclusive so that it always counts the current activation.
      if (t + window >= currentUpTime) x++;
      else break; // All subsequent values of t are smaller.
    }
    if (x <= 0) x = 1;

    // y = a * log_2(x)^2 + b * log_2(x) + c + linear * x
    double log2x = Math.log10(x) / Math.log10(2.);
    double y = a * log2x * log2x + b * log2x + c + linear * x;

    // Calculate the max value for y.
    double maxLog2x = Math.log10(LAST_SEEN_MAX_SIZE) / Math.log10(2.);
    double yMax = a * maxLog2x * maxLog2x + b * maxLog2x + c + linear * LAST_SEEN_MAX_SIZE;
    if (yMax == 0.) return 0;

    // Scale to the maxDuration. This assumes that x can actually reach the max within the window.
    return Math.round((y / yMax) * maxDuration);
  }

  /**
   * Check if the activation rule defined by {@link ConfigKeys#BleMinActivations} and
   * {@link ConfigKeys#BleMinActivationsDuration} is satisfied.
   *
   * @param settings    The settings.
   * @param currentTime The current timestamp.
   * @param outDevices  Will contain the devices that contributed to satisfying the rule if the
   *                    return value is true.
   * @return True if the rule is satisfied.
   */
  private boolean isBasicActivationRuleSatisfied(final Settings settings, final long currentTime,
                                                 final Set<String> outDevices) {
    // Count the number of activations from different sensors within the allowed timeframe.
    final long minActivationsDuration = settings.getLong(ConfigKeys.BleMinActivationsDuration);
    int activations = 0;
    for (Map.Entry<String, List<Long>> entry : sDevicesLastSeen.entrySet()) {
      String sensor = entry.getKey();
      List<Long> lastSeen = entry.getValue();
      if (!lastSeen.isEmpty() && (currentTime - lastSeen.get(0) <= minActivationsDuration)) {
        activations++;
        outDevices.add(sensor);
      }
    }

    // Check if the required number of activations have occured.
    final int minActivations = settings.getInt(ConfigKeys.BleMinActivations);
    if (activations >= minActivations) {
      Log.d(TAG, "Activation rule satisfied by ", activations, " sensors: ",
          TextUtils.join(", ", outDevices));
      return true;
    }

    outDevices.clear(); // Rule not satisfied.
    return false;
  }

  /**
   * Check if any of the activation rules in {@link #sActivationRules} is satisfied.
   *
   * @param currentTime The current timestamp.
   * @param sensorMask  The bitmask of sensor types reported by the received event. If this is zero,
   *                    then the event source will be treated as a legacy device with one sensor per
   *                    MAC.
   * @param outDevices  Will contain the sensors that contributed to the satisfied rule if any.
   * @return The first satisfied activation rule, or null if no rule was satisfied.
   */
  private ActivationRule matchActivationRules(final long currentTime, final int sensorMask,
                                              final Set<String> outDevices) {
    for (int ruleIdx = 0; ruleIdx < sActivationRules.size(); ruleIdx++) {
      final ActivationRule rule = sActivationRules.get(ruleIdx);
      if (rule.sensors.isEmpty()) continue;
      if (!Helpers.isTimeInRange(rule.enabledFrom, rule.enabledTo)) {
        Log.d(TAG, "Activation rule ", (ruleIdx + 1), " is inactive at this time");
        continue;
      }

      outDevices.clear(); // Only sensors satisfying the current rule.

      int activations = 0;
      if ((sensorMask != 0 && rule.sensors.size() == 1) // Newer devices with explicit sensor types.
          || (sensorMask == 0 && rule.numUnexpandedSensors == 1)) { // Legacy devices.
        // Only one sensor in rule. The min number of activations requirement refers to this sensor.
        // For legacy devices, only the first entry in the sensor list matters, which is the MAC.
        final String sensor = rule.sensors.get(0);
        final List<Long> lastSeen = sDevicesLastSeen.get(sensor);
        if (lastSeen == null || lastSeen.size() < rule.minActivations) {
          continue; // This rule cannot be fulfilled.
        }

        // Count how many of its activations are within the configured period.
        for (int i = 0; i < lastSeen.size(); i++) {
          if (currentTime - lastSeen.get(i) <= rule.minActivationsDuration) {
            activations++;
          } else {
            break;
          }
        }

        // Check if the rule is satisfied.
        if (activations >= rule.minActivations) {
          Log.d(TAG, "Activation rule ", (ruleIdx + 1), " satisfied by ", activations,
              " activations from ", sensor);
          outDevices.add(sensor);
          return rule;
        }
      } else {
        // Multiple sensors in rule. Only the most recent activation of each sensor matters.
        for (String sensor : rule.sensors) {
          final List<Long> lastSeen = sDevicesLastSeen.get(sensor);
          if (lastSeen != null && !lastSeen.isEmpty()
              && (currentTime - lastSeen.get(0) <= rule.minActivationsDuration)) {
            activations++;
            outDevices.add(sensor);
          }
        }

        // Check if the rule is satisfied.
        if (activations >= rule.minActivations) {
          Log.d(TAG, "Activation rule ", (ruleIdx + 1), " satisfied by ", activations,
              " sensors: ", TextUtils.join(", ", outDevices));
          return rule;
        }
      }
    }

    outDevices.clear(); // Rules not satisfied.
    return null;
  }

  /**
   * Sets the activation rules in {@link #sActivationRules} from {@link ConfigKeys#BleActivations}.
   *
   * @param settings The settings from which to load the config.
   */
  private void loadActivationRules(final Settings settings) {
    final String jsonRules = settings.getString(ConfigKeys.BleActivations);
    if (jsonRules.isEmpty()) {
      // No activation rule config.
      sActivationRules = null;
      sActivationRulesJSON = null;
      return;
    }

    // Parse the JSON config only if it is not yet cached or if it has been updated.
    if (!jsonRules.equals(sActivationRulesJSON)) {
      sActivationRules = parseActivationRules(jsonRules);
      sActivationRulesJSON = jsonRules;
      if (sActivationRules != null) {
        Log.d(TAG, "Activation rules loaded: ", sActivationRules.size());
      }
    }
  }


  /**
   * Parse the JSON encoded list of activation rules.
   *
   * @param json JSON encoded rules.
   * @return The list of decoded rules.
   */
  private List<ActivationRule> parseActivationRules(final String json) {
    // Parse the JSON array.
    JSONArray jsonRules;
    try {
      jsonRules = new JSONArray(json);
    } catch (JSONException e) {
      Log.w(TAG, "Failed to parse activation rules");
      return null;
    }

    final int numRules = jsonRules.length();
    if (numRules == 0) {
      return null; // Treat an empty list the same as no list at all.
    }

    // Parse the JSON array elements.
    final List<ActivationRule> rules = new ArrayList<>(numRules);
    RuleLoop:
    for (int ruleIdx = 0; ruleIdx < numRules; ruleIdx++) {
      final JSONObject obj = jsonRules.optJSONObject(ruleIdx);
      if (obj == null) {
        Log.w(TAG, "Failed to parse rule, array element is not a JSON object");
        continue;
      }

      final JSONArray delayedEventsList = obj.optJSONArray(BleActivationsKeys.DelayedEvents.name());
      ArrayList<Long> delayedEvents = null;
      if (delayedEventsList != null) {
        delayedEvents = new ArrayList<>(delayedEventsList.length());
        for (int i = 0; i < delayedEventsList.length(); i++) {
          long delay = delayedEventsList.optLong(i);
          if (delay <= 0) {
            Log.w(TAG, "Invalid value for key ", BleActivationsKeys.DelayedEvents);
            continue;
          }
          delayedEvents.add(delay);
        }
      }

      final JSONArray deviceList = obj.optJSONArray(BleActivationsKeys.Devices.name());
      if (deviceList == null || deviceList.length() == 0) {
        Log.w(TAG, "Missing or invalid value for key ", BleActivationsKeys.Devices);
        continue;
      }

      // Extract and validate device addresses in list.
      int numDevices = deviceList.length();
      final ArrayList<String> devices = new ArrayList<>(3 * numDevices);
      int numUnexpandedSensors = 0;
      for (int deviceIdx = 0; deviceIdx < numDevices; deviceIdx++) {
        String device = deviceList.optString(deviceIdx);
//        device = canonicalDeviceID(device);
        device = device.toUpperCase(Locale.ENGLISH);
        String mac = extractMACAddress(device);

        if (mac.isEmpty() || !BluetoothAdapter.checkBluetoothAddress(mac)) {
          Log.w(TAG, "Invalid device address: ", deviceList.optString(deviceIdx));
          continue RuleLoop;
        }

        int sensorMask = sensorMask(device);
        if (sensorMask == 0) {
          // No sensor flags set. This matches "any" sensors with the same MAC or a legacy device
          // that does not report sensor types at all.
          devices.addAll(allSensorIDs(mac));
          numUnexpandedSensors++;
        } else {
          // Add an entry for each sensor type encoded in the device ID.
          List<String> sensors = sensorIDs(mac, sensorMask);
          devices.addAll(sensors);
          numUnexpandedSensors += sensors.size();
        }
      }

      String from = obj.optString(BleActivationsKeys.EnabledFrom.name());
      String to = obj.optString(BleActivationsKeys.EnabledTo.name());
      // Set to defaults if the times are unset, but not if they are invalid.
      if (from.isEmpty()) from = "00:00";
      if (to.isEmpty()) to = "23:59";
      if (Helpers.parseTimeRange(from, to, true, Calendar.getInstance()) == null) {
        Log.w(TAG, "Invalid time range: ", from, " to ", to);
        continue;
      }

      int minActivations = obj.optInt(BleActivationsKeys.MinActivations.name());
      if (minActivations <= 0) {
        minActivations = devices.size(); // Default to all configured devices.
      }

      long duration = obj.optLong(BleActivationsKeys.MinActivationsDuration.name());
      if (duration < 0) {
        Log.w(TAG, "Invalid value for key ", BleActivationsKeys.MinActivationsDuration,
            ": ", duration);
        continue;
      }

      rules.add(new ActivationRule(devices, numUnexpandedSensors, from, to, minActivations,
          duration, delayedEvents));
    }

    return rules.isEmpty() ? null : rules;
  }

//  /**
//   * Returns the canonical form of the device ID, with an optional suffix of sensor identifiers
//   * in defined order.
//   *
//   * @param device The device ID in possibly non-canonical form.
//   * @return The canonical device ID.
//   */
//  private String canonicalDeviceID(String device) {
//    device = device.toUpperCase(Locale.ENGLISH);
//
//    int suffixStart = device.lastIndexOf('-');
//    if (suffixStart < 0) return device;
//
//    String mac = device.substring(0, suffixStart);
//    int sensors = sensorMask(device);
//
//    return canonicalDeviceID(mac, sensors);
//  }

  /**
   * Returns the canonical form of the device ID, with an optional suffix of sensor identifiers
   * in defined order.
   *
   * @param mac     The device MAC address.
   * @param sensors A sensor bitmask.
   * @return The canonical device ID.
   */
  private String canonicalDeviceID(String mac, int sensors) {
    if (sensors == 0) return mac;
    return String.format("%s-%s%s", mac,
        (sensors & BeaconScannerService.EXTRA_SENSOR_TYPE_ACCELEROMETER) == 0 ? "" : "A",
        (sensors & BeaconScannerService.EXTRA_SENSOR_TYPE_PIR) == 0 ? "" : "P");
  }

  /**
   * Extracts and returns the MAC address from the device ID.
   */
  private String extractMACAddress(String device) {
    int suffixStart = device.lastIndexOf('-');
    return suffixStart < 0 ? device : device.substring(0, suffixStart);
  }

  /**
   * Extracts and returns the sensor flags in the device ID, or empty string if there are none.
   */
  private String extractSensorFlags(String device) {
    int suffixStart = device.lastIndexOf('-');
    return suffixStart < 0 ? "" : device.substring(suffixStart + 1);
  }

  /**
   * Returns a bitmask with the respective bit set for each of the sensors in the device ID.
   */
  private int sensorMask(String device) {
    int suffixStart = device.lastIndexOf('-');
    if (suffixStart < 0) return 0;

    int sensors = 0;
    for (int i = suffixStart + 1; i < device.length(); i++) {
      switch (device.charAt(i)) {
        case 'A':
          sensors |= BeaconScannerService.EXTRA_SENSOR_TYPE_ACCELEROMETER;
          break;
        case 'P':
          sensors |= BeaconScannerService.EXTRA_SENSOR_TYPE_PIR;
          break;
        default:
          Log.w(TAG, "Unknown sensor type '", device.charAt(i), "' in ", device);
          break;
      }
    }

    return sensors;
  }

  /**
   * Returns a list of sensor IDs, one for each active sensor type in sensors. The IDs are
   * specific to the device identified by the given MAC address.
   */
  private List<String> sensorIDs(String mac, int sensors) {
    final int sensorCount = Integer.bitCount(sensors);
    List<String> sensorIDs = new ArrayList<>(Math.max(sensorCount, 1));

    // Add unique IDs for all active sensors to the list.
    if ((sensors & BeaconScannerService.EXTRA_SENSOR_TYPE_ACCELEROMETER) != 0) {
      sensorIDs.add(mac + "-A");
    }
    if ((sensors & BeaconScannerService.EXTRA_SENSOR_TYPE_PIR) != 0) {
      sensorIDs.add(mac + "-P");
    }

    return sensorIDs;
  }

  /**
   * Returns the list of all known sensor ID combinations for the device MAC. This does not imply
   * that the device actually supports any of these sensor types, it may be a legacy device that
   * only ever reports the MAC. For those devices, the MAC itself is also included at index 0 of the
   * list.
   */
  private List<String> allSensorIDs(String mac) {
    List<String> sensors = new ArrayList<>(3);
    sensors.add(mac);
    sensors.add(mac + "-A");
    sensors.add(mac + "-P");
    return sensors;
  }

  /**
   * A rule defining a condition when a sensor activation may proceed to trigger further events.
   */
  private static class ActivationRule {
    @Nullable
    public final ArrayList<Long> delayedEvents;
    public final String enabledFrom;
    public final String enabledTo;
    public final int minActivations;
    public final long minActivationsDuration;
    public final int numUnexpandedSensors;
    public final List<String> sensors;

    public ActivationRule(@NonNull List<String> sensors, int numUnexpandedSensors,
                          String enabledFrom, String enabledTo,
                          int minActivations, long minActivationsDuration,
                          @Nullable ArrayList<Long> delayedEvents) {
      this.sensors = sensors;
      this.numUnexpandedSensors = numUnexpandedSensors;
      this.enabledFrom = enabledFrom;
      this.enabledTo = enabledTo;
      this.minActivations = minActivations;
      this.minActivationsDuration = minActivationsDuration;
      this.delayedEvents = delayedEvents;
    }
  }
}