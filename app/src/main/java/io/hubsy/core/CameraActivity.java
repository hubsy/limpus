package io.hubsy.core;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.beaconscanner.RelayController;
import io.hubsy.core.camera.CaptureManager;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;

/**
 * This activity attempts to capture a single photo and start the async. upload process before
 * finishing itself.
 */
public class CameraActivity extends Activity {
  private static final String TAG = "CameraActivity";

  private static WakeLock sWakeLock;

  private Settings mSettings;
  private CaptureManager mCaptureManager;
  private final List<RelayController> mLightRelays = new ArrayList<>();

  /**
   * Acquire a WakeLock for the lifetime of this activity. It will be released automatically when
   * the activity has completed its work.
   */
  @SuppressLint("WakelockTimeout")
  public static void acquireWakelock(Context context) {
    if (sWakeLock == null) {
      PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
      sWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, Constants.NS_PREFIX + ":" + TAG);
      sWakeLock.acquire();
      Log.d(TAG, "WakeLock acquired");
    }
  }

  private static void releaseWakelock() {
    if (sWakeLock != null) {
      if (sWakeLock.isHeld()) {
        sWakeLock.release();
        Log.d(TAG, "WakeLock released");
      } else {
        Log.d(TAG, "WakeLock already released");
      }
      sWakeLock = null;
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Create an empty bundle if necessary to avoid having to check for null going forward.
    Bundle extras = getIntent().getExtras();
    if (extras == null) extras = new Bundle();

    mSettings = Settings.getInstance(this);

    // Start the camera initialisation process.
    mCaptureManager = CaptureManager.create(this, extras, this::closeCameraDisableLightsAndFinish);
    if (mCaptureManager == null || !mCaptureManager.initialise(this)) {
      Log.d(TAG, "Aborting capture request");
      closeCameraDisableLightsAndFinish();
      return;
    }

    // Enable lights. If no lights are active, send the photo request immediately.
    boolean waitForLights = enableLightsIfScheduled();
    if (!waitForLights) capturePhotosOrVideo();
  }

  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);

    final Bundle extras = intent.getExtras();
    if (extras == null) return;

    if (mCaptureManager != null) mCaptureManager.updateConfig(extras);

    // Speed events come in too frequently when in use to log.
    final boolean isSpeedEvent =
        extras.getInt(Constants.EXTRA_SPEED, Integer.MIN_VALUE) != Integer.MIN_VALUE;
    if (!isSpeedEvent) Log.d(TAG, "Photo request ongoing, ignoring additional request");

    final boolean isEvent = extras.containsKey(Constants.EXTRA_EVENT_TIMESTAMP);
    if (!isEvent) {
      // This is a new non-event request and we're still busy with the previous request. Ship logs.
      Helpers.prepareAndShipLogs(this, true);
    }
  }

  /**
   * Check the configured lights and their schedule and turn them on if appropriate.
   *
   * @return True if the photo request has been scheduled to execute asynchronously once the lights
   * have been turned on; false if no lights are active and photo taking can proceed immediately.
   */
  private boolean enableLightsIfScheduled() {
    mLightRelays.clear(); // Just to be sure.

    // Check if BLE is turned on.
    if (!mSettings.getBool(ConfigKeys.BleOn)) {
      Log.d(TAG, "Bluetooth is disabled in the config");
      return false;
    }

    // Get the light schedule.
    String timeFrom = mSettings.getString(ConfigKeys.LightsFrom);
    String timeTo = mSettings.getString(ConfigKeys.LightsTo);
    if (Helpers.isTimeInRange(timeFrom, timeTo)) {
      // Get the relay controllers for all configured lights.
      final List<String> addresses = mSettings.getStringList(ConfigKeys.LightAddresses);
      for (String addr : addresses) {
        RelayController relay = RelayController.get(this, addr);
        if (relay == null) {
          Log.w(TAG, "Cannot activate light relay: ", addr);
          continue;
        }
        mLightRelays.add(relay);
      }

      // Turn all lights on.
      final LightStateHandler callback =
          new LightStateHandler(this, mLightRelays.size(), LightStateHandler.ACTION_CAPTURE);
//      for (RelayController relay : mLightRelays) {
//        relay.turnOn(callback);
//      }
      // Serialise the calls to the relay. This should be done by the relay eventually.
      turnOnLightsSerialised(0, callback);
    }

    return !mLightRelays.isEmpty();
  }

  /**
   * This is a serialisation of {@link RelayController#turnOn} for multiple relays. The second relay
   * will not be turned on until the first has notified the completion of the process, whether
   * successful or not, and so on for additional relays.
   * <p>
   * This fixes the issue of subsequent device connections failing if one device in the chain fails,
   * which exists in at least Android 5.1 and possibly other versions.
   *
   * @param idx      The index of the next relay to turn on in {@link #mLightRelays}.
   * @param callback The callback shared by all relays.
   */
  private void turnOnLightsSerialised(final int idx, final LightStateHandler callback) {
    if (idx >= mLightRelays.size()) return;
    mLightRelays.get(idx).turnOn(relayIsOn -> {
      callback.stateChanged(relayIsOn); // Forward to the shared callback.
      turnOnLightsSerialised(idx + 1, callback); // Turn on the next relay if any.
    });
  }

  /**
   * This is a serialisation of {@link RelayController#turnOff} for multiple relays. See
   * {@link #turnOnLightsSerialised} for details.
   *
   * @param reverseIdx The index starting from the largest index.
   * @param callback   The callback shared by all relays.
   */
  private void turnOffLightsSerialised(final int reverseIdx, final LightStateHandler callback) {
    if (reverseIdx < 0) return;
    mLightRelays.get(reverseIdx).turnOff(relayIsOn -> {
      callback.stateChanged(relayIsOn); // Forward to the shared callback.
      mLightRelays.remove(reverseIdx);
      turnOffLightsSerialised(reverseIdx - 1, callback); // Turn off the next relay.
    });
  }

  /**
   * Initiates the photo/video capture as per the current configuration.
   */
  private void capturePhotosOrVideo() {
    if (mCaptureManager != null) mCaptureManager.capture();
  }

  /**
   * Close the camera controller, turn off any lights in use and then call
   * {@link #cleanUpAndFinish()}.
   */
  private void closeCameraDisableLightsAndFinish() {
    if (mCaptureManager != null) {
      mCaptureManager.release();
      mCaptureManager = null;
    }

    if (mLightRelays.isEmpty()) {
      cleanUpAndFinish();
      return;
    }

    // The callback will do the call to cleanUpAndFinish when all lights have been switched off.
    final LightStateHandler callback =
        new LightStateHandler(this, mLightRelays.size(), LightStateHandler.ACTION_CLEAN_UP);
//    for (int i = mLightRelays.size() - 1; i >= 0; i--) {
//      mLightRelays.get(i).turnOff(callback);
//      mLightRelays.remove(i);
//    }
    // Serialise the calls to the relay in reverse order so they can be removed safely.
    turnOffLightsSerialised(mLightRelays.size() - 1, callback);
  }

  /**
   * Release resources and finish the activity.
   */
  private void cleanUpAndFinish() {
    finish();
    releaseWakelock();
  }

  /**
   * Handles light state changes. Does not keep the activity instance alive.
   */
  private static class LightStateHandler implements RelayController.StateCallback {
    public static final int ACTION_CAPTURE = 0;
    public static final int ACTION_CLEAN_UP = 1;

    private final WeakReference<CameraActivity> mActivity;
    private final int mAction;
    private final int mTotalLights;
    private int mLightsNotified = 0;

    LightStateHandler(CameraActivity owner, int numLights, int action) {
      mActivity = new WeakReference<>(owner);
      mTotalLights = numLights;
      mAction = action;
    }

    @Override
    public void stateChanged(boolean relayIsOn) {
      // Perform action once all lights have notified, whether they actually turned on/off or not.
      mLightsNotified++;
      if (mLightsNotified >= mTotalLights) {
        CameraActivity owner = mActivity.get();
        if (owner == null) return;
        if (mAction == ACTION_CAPTURE) owner.capturePhotosOrVideo();
        else if (mAction == ACTION_CLEAN_UP) owner.cleanUpAndFinish();
      }
    }
  } // LightStateHandler
}

