package io.hubsy.core;

/**
 * Definitions of constants for event broadcast.
 */
public final class Constants {
  public static final String NS_PREFIX = "io.hubsy.core";


  /**
   * An intent extra used to specify an AWS file key. Type String.
   */
  public static final String EXTRA_AWS_KEY = NS_PREFIX + ".extra.aws_key";
  /**
   * An intent extra specifying a local file path. Type String.
   */
  public static final String EXTRA_FILE_PATH = NS_PREFIX + ".extra.file_path";


  /**
   * A new config file is available. Intents include {@link #EXTRA_AWS_KEY} and
   * {@link #EXTRA_FILE_PATH}.
   */
  public static final String ACTION_NEW_CONFIG_FILE = NS_PREFIX + ".action.new_config_file";


  /**
   * A new application update file is available. Intents include {@link #EXTRA_AWS_KEY} and
   * {@link #EXTRA_FILE_PATH}.
   */
  public static final String ACTION_NEW_APP_UPDATE = NS_PREFIX + ".action.new_app_update";
  /**
   * A new application update hash file is available. Intents include {@link #EXTRA_AWS_KEY} and
   * {@link #EXTRA_FILE_PATH}.
   */
  public static final String ACTION_NEW_APP_UPDATE_HASH = NS_PREFIX + ".action.new_app_update_hash";

  /**
   * A new health monitor update file is available. Intents include {@link #EXTRA_AWS_KEY} and
   * {@link #EXTRA_FILE_PATH}.
   */
  public static final String ACTION_NEW_HMON_UPDATE = NS_PREFIX + ".action.new_hmon_update";
  /**
   * A new health monitor update hash file is available. Intents include {@link #EXTRA_AWS_KEY} and
   * {@link #EXTRA_FILE_PATH}.
   */
  public static final String ACTION_NEW_HMON_UPDATE_HASH = NS_PREFIX + ".action.new_hmon_update_hash";

  /**
   * A new image mask file is available. Intents include {@link #EXTRA_AWS_KEY} and
   * {@link #EXTRA_FILE_PATH}.
   */
  public static final String ACTION_NEW_IMAGE_MASK = NS_PREFIX + ".action.new_image_mask";
  /**
   * A new image mask hash file is available. Intents include {@link #EXTRA_AWS_KEY} and
   * {@link #EXTRA_FILE_PATH}.
   */
  public static final String ACTION_NEW_IMAGE_MASK_HASH = NS_PREFIX + ".action.new_image_mask_hash";

  /**
   * A new system tools update file is available. Intents include {@link #EXTRA_AWS_KEY} and
   * {@link #EXTRA_FILE_PATH}.
   */
  public static final String ACTION_NEW_TOOLS_UPDATE = NS_PREFIX + ".action.new_tools_update";
  /**
   * A new system tools update hash file hash is available. Intents include {@link #EXTRA_AWS_KEY}
   * and {@link #EXTRA_FILE_PATH}.
   */
  public static final String ACTION_NEW_TOOLS_UPDATE_HASH = NS_PREFIX + ".action.new_tools_update_hash";

  /**
   * A new updater executable update file is available. Intents include {@link #EXTRA_AWS_KEY} and
   * {@link #EXTRA_FILE_PATH}.
   */
  public static final String ACTION_NEW_UPDATER_UPDATE = NS_PREFIX + ".action.new_updater_update";
  /**
   * A new updater executable update hash file is available. Intents include {@link #EXTRA_AWS_KEY}
   * and {@link #EXTRA_FILE_PATH}.
   */
  public static final String ACTION_NEW_UPDATER_UPDATE_HASH = NS_PREFIX + ".action.new_updater_update_hash";

  /**
   * A new stand-alone upgrade script is available. Intents include {@link #EXTRA_AWS_KEY} and
   * {@link #EXTRA_FILE_PATH}.
   */
  public static final String ACTION_NEW_UPGRADE_SCRIPT = NS_PREFIX + ".action.new_upgrade_script";
  /**
   * A new stand-alone upgrade script hash file is available. Intents include {@link #EXTRA_AWS_KEY}
   * and {@link #EXTRA_FILE_PATH}.
   */
  public static final String ACTION_NEW_UPGRADE_SCRIPT_HASH = NS_PREFIX + ".action.new_upgrade_script_hash";


  /**
   * An extra identifying an event trigger. Type String.
   */
  public static final String EXTRA_EVENT_TRIGGER = NS_PREFIX + ".extra.event_trigger";
  /**
   * An extra identifying the MAC addresses of event triggers. The elements of the array should be
   * unique as in a Set<String>. Type String[].
   */
  public static final String EXTRA_EVENT_SENSORS = NS_PREFIX + ".extra.event_devices";
  /**
   * An extra specifying the timestamp of an event, in milliseconds since the epoch. Type long.
   */
  public static final String EXTRA_EVENT_TIMESTAMP = NS_PREFIX + ".extra.event_timestamp";
  /**
   * An extra specifying an event timestamp to reference. This is in addition to the current event's
   * timestamp, in millis since the epoch. For example, it may reference the event that started a
   * sequence of events or the event that caused this event. Type long.
   */
  public static final String EXTRA_REFERENCE_EVENT_TIMESTAMP =
      NS_PREFIX + ".extra.reference_event_timestamp";
  /**
   * A suffix appended to filenames, after an '_' and before the file extension. Type String.
   */
  public static final String EXTRA_FILENAME_SUFFIX = NS_PREFIX + ".extra.filename_suffix";
  /**
   * A speed reading. Type int.
   */
  public static final String EXTRA_SPEED = NS_PREFIX + ".extra.speed";


  private Constants() { /* Do not allow instances. */ }
}
