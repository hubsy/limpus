package io.hubsy.core;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Locale;

import io.hubsy.core.util.Log;

/**
 * Updates the current location every time a broadcast is received.
 */
public class LocationUpdateReceiver extends BroadcastReceiver {
  private static final String TAG = "LocationUpdateReceiver";

  private static Location sLatestLocation;

  /**
   * Schedule recurring location updates.
   *
   * @param context         A context.
   * @param intervalMinutes The interval between updates, in minutes. An interval <= 0 disables
   *                        recurring updates.
   */
  public static void scheduleRecurringUpdates(@NonNull Context context, int intervalMinutes) {
    context = context.getApplicationContext();

    Intent intent = new Intent(context, LocationUpdateReceiver.class);
    PendingIntent pending =
        PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

    // Cancel the existing alarm.
    AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    am.cancel(pending);

    if (intervalMinutes > 0) {
      // Set up recurring alarms so that the first alarm goes off after a full interval has passed.
      long intervalMillis = intervalMinutes * 60L * 1000L;
      am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
          SystemClock.elapsedRealtime() + intervalMillis, intervalMillis, pending);
      Log.d(TAG, "Update interval set to (minutes): ", intervalMinutes);

      // Request the first update immediately.
      context.sendBroadcast(intent);
    } else {
      Log.d(TAG, "Recurring updates disabled");
    }
  }

  /**
   * Returns the last known location, if any.
   */
  @Nullable
  public static Location getLatestLocation() {
    return sLatestLocation;
  }

  /**
   * Returns a formatted string with the location data. If l is null, the returned string is empty.
   */
  public static String fmt(Location l) {
    if (l == null) return "";

    final StringBuilder s = new StringBuilder();

    s.append("Location[");
    s.append(l.getProvider());
    s.append(String.format(Locale.ROOT, " %.6f,%.6f", l.getLatitude(), l.getLongitude()));

    if (l.hasAccuracy()) s.append(String.format(Locale.ROOT, " acc=%.0f", l.getAccuracy()));
    else s.append(" acc=???");

    long time = l.getTime();
    if (time > 0) s.append(" t=").append(time);
    else s.append(" t=???");

    long etime = l.getElapsedRealtimeNanos();
    if (etime > 0) s.append(" et=").append(etime / 1000000L);
    else s.append(" et=???");

    if (l.hasAltitude()) s.append(String.format(Locale.ROOT, " alt=%.0f", l.getAltitude()));
    if (Build.VERSION.SDK_INT >= 26 && l.hasVerticalAccuracy()) {
      s.append(String.format(Locale.ROOT, " vAcc=%.0f", l.getVerticalAccuracyMeters()));
    }

    if (l.hasSpeed()) s.append(String.format(Locale.ROOT, " vel=%.1f", l.getSpeed()));
    if (Build.VERSION.SDK_INT >= 26 && l.hasSpeedAccuracy()) {
      s.append(String.format(Locale.ROOT, " sAcc=%.1f", l.getSpeedAccuracyMetersPerSecond()));
    }

    if (l.hasBearing()) s.append(String.format(Locale.ROOT, " bear=%.0f", l.getBearing()));
    if (Build.VERSION.SDK_INT >= 26 && l.hasBearingAccuracy()) {
      s.append(String.format(Locale.ROOT, " bAcc=%.0f", l.getBearingAccuracyDegrees()));
    }

    s.append(']');
    return s.toString();
  }

  @Override
  public void onReceive(Context context, Intent intent) {
    Log.d(TAG, "Starting a location update");
    LocationUpdater updater = new LocationUpdater(context);
    updater.startListening();

    // Ensure the request does not stay active too long, for situations when no fix can be obtained.
    new Handler().postDelayed(() -> {
      if (updater.isListening()) {
        if (updater.getLocation() == null) {
          Log.d(TAG, "No location update obtained, stopping the current attempt");
        }
        updater.stopListening();
      }
    }, 60000L);
  }

  /**
   * Each instance waits until the target location accuracy is reached before removing itself from
   * the location manager.
   */
  private static class LocationUpdater implements android.location.LocationListener {
    private static final String TAG = "LocationUpdater";

    private final Context mContext;
    private final LocationManager mLocationManager;

    private Location mLocation;
    private boolean mIsListening;

    public LocationUpdater(Context context) {
      mContext = context.getApplicationContext();
      mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
    }

    public boolean isListening() {
      return mIsListening;
    }

    @SuppressLint("MissingPermission")
    public void startListening() {
      if (isListening()) return;

      mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
      mIsListening = true;
    }

    public void stopListening() {
      if (!isListening()) return;

      mLocationManager.removeUpdates(this);
      mIsListening = false;

      if (mLocation != null) {
        if (sLatestLocation == null
            || sLatestLocation.getElapsedRealtimeNanos() < mLocation.getElapsedRealtimeNanos()) {
          sLatestLocation = mLocation;
        }
      }
    }

    public Location getLocation() {
      return mLocation;
    }

    @Override
    public void onLocationChanged(Location location) {
      if (mLocation == null || location.getAccuracy() < mLocation.getAccuracy()) {
        mLocation = location;
      }

      // Log the result and stop listening once the target accuracy has been reached.
      if (mLocation.getAccuracy() <= 5.f) {
        stopListening();
      }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
      String stat;
      switch (status) {
        case LocationProvider.AVAILABLE:
          stat = null;
          break;
        case LocationProvider.OUT_OF_SERVICE:
          stat = "out of service";
          break;
        case LocationProvider.TEMPORARILY_UNAVAILABLE:
          stat = "temporarily unavailable";
          break;
        default:
          stat = "unknown status " + status;
          break;
      }
      if (stat != null) Log.d(TAG, provider, " status changed to: ", stat);
    }

    @Override
    public void onProviderEnabled(String provider) {
      Log.d(TAG, provider, " provider enabled");
    }

    @Override
    public void onProviderDisabled(String provider) {
      Log.d(TAG, provider, " provider disabled, trying to enable LOCATION_MODE_HIGH_ACCURACY");
      ContentResolver resolver = mContext.getContentResolver();
      if (!Settings.Secure.putInt(resolver,
          Settings.Secure.LOCATION_MODE, Settings.Secure.LOCATION_MODE_HIGH_ACCURACY)) {
        Log.e(TAG, "Failed to set LOCATION_MODE to LOCATION_MODE_HIGH_ACCURACY");
      }
    }
  }
}
