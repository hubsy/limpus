package io.hubsy.core;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttNewMessageCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttQos;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;
import io.hubsy.core.util.MoveFilesTask;

public class MqttClient {
  private static final String TAG = "MqttClient";

  private static final String ENDPOINT = "a3a0knq46gj9rb-ats.iot.us-east-1.amazonaws.com";

  private static volatile MqttClient sSingleton;

  /**
   * Returns the singleton instance of this class.
   */
  public static MqttClient getInstance(@NonNull Context context) {
    if (sSingleton == null) {
      synchronized (MqttClient.class) {
        if (sSingleton == null) {
          sSingleton = new MqttClient(context);
        }
      }
    }
    return sSingleton;
  }

  private final Context mContext;
  private final Handler mMainHandler;

  private AWSIotMqttManager mMqttManager;
  private String mClientID;

  private boolean mReconnecting;

  private static final Pattern sFileRequestNamePattern = Pattern.compile("[-_.0-9a-zA-Z]+");

  private MqttClient(Context context) {
    mContext = context.getApplicationContext();
    mMainHandler = new Handler(Looper.getMainLooper());
  }

  /**
   * Connect to the broker and establish a new or resume an existing session.
   */
  public void connect() {
    final Settings settings = Settings.getInstance(mContext);
    final AWSCredentials creds = AwsClient.getAwsCreds(settings);
    mClientID = settings.getString(ConfigKeys.AwsUserName);
    if (mClientID.isEmpty() || creds == null) return;

    mMqttManager = new AWSIotMqttManager(mClientID, ENDPOINT);
    mMqttManager.setCleanSession(false);
    mMqttManager.setKeepAlive(1200); // AWS service limit is 1200 seconds.
    mMqttManager.setAutoReconnect(true);
    mMqttManager.setMaxAutoReconnectAttempts(-1);
    mMqttManager.setReconnectRetryLimits(mMqttManager.getMinReconnectRetryTime(), 300);
    // Disable auto resubscribe so we do not have to differentiate between the initial connection
    // attempt and auto reconnects when connection status == Connected is reported.
    mMqttManager.setAutoResubscribe(false);
    mMqttManager.setOfflinePublishQueueEnabled(true);

    try {
      mMqttManager.connect(new StaticCredentialsProvider(creds), (status, throwable) ->
          mMainHandler.post(() -> {
            Log.d(TAG, status.name());
            switch (status) {
              case Connected:
                // No need to resubscribe if a persistent session is continued, except there
                // currently is no way to access the sessionPresent attribute in the CONNACK message
                // using the AWS client.
                subscribe();
                break;
              case ConnectionLost:
                // Unless reconnect() was called to establish a new connection from scratch, the
                // client should automatically attempt to reconnect.
                if (mReconnecting) {
                  mReconnecting = false;
                  mMqttManager = null;
                  mMainHandler.post(this::connect); // Let the caller return first.
                }
                break;
            }
          })
      );
    } catch (AmazonClientException e) {
      Log.w(TAG, "Connect failed: ", e.getMessage());
    }
  }

  /**
   * Disconnect from the broker before connecting again.
   */
  public void reconnect() {
    if (mMqttManager == null) {
      connect();
      return;
    }

    Log.d(TAG, "Reconnect request received");
    mReconnecting = true;

    try {
      if (mMqttManager.disconnect()) {
        Log.d(TAG, "Disconnected");
      }
    } catch (AmazonClientException e) {
      Log.w(TAG, "Disconnect failed: ", e.getMessage());
      mMainHandler.postDelayed(this::connect, 10000);
    }
  }

  /**
   * Subscribe to topics.
   */
  private void subscribe() {
    if (mMqttManager == null) return;

    final String configDLTopic = mClientID + "/config/dl";
    final String fileULTopic = mClientID + "/file/ul";
    Log.d(TAG, "Subscribing to: ", configDLTopic, ", ", fileULTopic);

    try {
      mMqttManager.subscribeToTopic(configDLTopic, AWSIotMqttQos.QOS1,
          wrapMsgCallback(this::onConfigDLMsg));
      mMqttManager.subscribeToTopic(fileULTopic, AWSIotMqttQos.QOS1,
          wrapMsgCallback(this::onFileULMsg));
    } catch (AmazonClientException e) {
      Log.w(TAG, "Failed to subscribe to topics: ", e.getMessage());
      mMainHandler.postDelayed(this::reconnect, 10000);
    }
  }

  /**
   * Wraps the message callback in a proxy that takes care of running the given callback on the main
   * thread.
   */
  private AWSIotMqttNewMessageCallback wrapMsgCallback(AWSIotMqttNewMessageCallback cb) {
    return (topic, data) -> mMainHandler.post(() -> cb.onMessageArrived(topic, data));
  }

  /**
   * Callback for config download messages.
   */
  private void onConfigDLMsg(@SuppressWarnings("unused") String topic,
                             @SuppressWarnings("unused") byte[] data) {
    Log.d(TAG, "Config download request received");
    AwsClient.downloadUpdates(mContext, true);
  }

  /**
   * Callback for file upload messages.
   */
  private void onFileULMsg(String topic, byte[] data) {
    // Decode bytes and split into lines.
    String[] lines;
    try {
      String s = new String(data, StandardCharsets.UTF_8);
      lines = s.split("\n");
    } catch (Exception e) {
      Log.w(TAG, "Failed to decode message to ", topic, ": ", e.getMessage());
      return;
    }

    // Validate file names and construct file paths.
    final File[] files = new File[lines.length];
    for (int i = 0; i < lines.length; i++) {
      if (!sFileRequestNamePattern.matcher(lines[i]).matches()) {
        Log.w(TAG, "Invalid file name in message to ", topic, ": ", lines[i]);
        return;
      }
      files[i] = Settings.getFileInAppDir(Settings.APP_DIR_ARCHIVED, lines[i]);
    }

    // Trigger log rotation and file transfers when archived files have been queued.
    Runnable callback = () -> {
      Log.d(TAG, "Starting file transfers");
      Helpers.prepareAndShipLogs(mContext, false);
    };

    // Queue files.
    Log.d(TAG, "Processing upload request for ", files.length, " files");
    File destDir = Settings.getFileInAppDir(null, Settings.APP_DIR_NEW);
    new MoveFilesTask(files, destDir, callback, mMainHandler)
        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }

  /**
   * Wraps static AWS credentials.
   */
  private static class StaticCredentialsProvider implements AWSCredentialsProvider {
    private final AWSCredentials mCredentials;

    StaticCredentialsProvider(@NonNull AWSCredentials creds) {
      mCredentials = creds;
    }

    @Override
    public AWSCredentials getCredentials() {
      return mCredentials;
    }

    @Override
    public void refresh() {
    }
  }
}
