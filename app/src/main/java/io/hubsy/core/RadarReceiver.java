package io.hubsy.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.usb.RadarService;
import io.hubsy.core.util.Log;

public class RadarReceiver extends BroadcastReceiver {
  private static final String TAG = "RadarReceiver";

  @Override
  public void onReceive(Context context, Intent intent) {
    final String action = intent.getAction();
    if (!RadarService.ACTION_SPEED_READING.equals(action)) {
      Log.e(TAG, "Error unexpected action: ", action);
      return;
    }

    final Context appContext = context.getApplicationContext();
    final Settings settings = Settings.getInstance(appContext);

    if (settings.getBool(ConfigKeys.PauseRadar)) {
      Log.d(TAG, "The radar is paused");
    } else {
      final Bundle extras = intent.getExtras();
      if (extras == null) {
        Log.e(TAG, "The intent extras are null");
        return;
      }

      // Extract the speed reading timestamp.
      int speed = extras.getInt(RadarService.EXTRA_SPEED, Integer.MIN_VALUE);
      long eventTimestamp = extras.getLong(RadarService.EXTRA_TIMESTAMP, Long.MIN_VALUE);
      if (speed == Integer.MIN_VALUE || eventTimestamp == Long.MIN_VALUE) {
        Log.e(TAG, "Missing intent extras");
        return;
      }

      // Prepare to capture the event.
      final Intent i = new Intent(context, CameraActivity.class);
      i.putExtra(Constants.EXTRA_EVENT_TRIGGER, "Radar");
      i.putExtra(Constants.EXTRA_EVENT_TIMESTAMP, eventTimestamp);
      i.putExtra(Constants.EXTRA_SPEED, speed);
      context.startActivity(i);
      CameraActivity.acquireWakelock(appContext);
    }
  }
}
