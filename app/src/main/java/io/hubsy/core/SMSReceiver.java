package io.hubsy.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import java.io.File;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;
import io.hubsy.core.util.MoveFilesTask;
import io.hubsy.core.util.WriteTextFileTask;

public class SMSReceiver extends BroadcastReceiver {
  private static final String TAG = "SMSReceiver";

  // User SMS are rate limited.
  private static final long MAX_USER_FREQUENCY_MILLIS = 30000;
  private static long sLastUserSMS = Long.MIN_VALUE;

  @Override
  public void onReceive(Context context, Intent intent) {
    // If this is the default SMS app, then we are only interested in SMS_DELIVER_ACTION. If, for
    // some reason, this is not the default SMS app, then act on SMS_RECEIVED_ACTION instead.
    if (!Telephony.Sms.Intents.SMS_DELIVER_ACTION.equals(intent.getAction())) {
      boolean isDefaultApp = context.getPackageName().equals(
          Telephony.Sms.getDefaultSmsPackage(context));
      boolean isSMSReceived = Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction());
      if (isDefaultApp && isSMSReceived) {
        return; // Quietly ignore SMS_RECEIVED_ACTION when this is the default app.
      } else if (!isSMSReceived) {
        Log.d(TAG, "Ignoring action: ", intent.getAction());
        return;
      }
    }
    Log.d(TAG, "SMS received");

    final Settings settings = Settings.getInstance(context);

    // Extract the most recent SMS message.
    final SmsMessage[] messages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
    if (messages == null || messages.length < 1) {
      Log.w(TAG, "No message extracted from PDUs");
      return;
    }

    // Construct the message body from all messages.
    final StringBuilder bodyBuilder = new StringBuilder(320);
    for (SmsMessage msg : messages) {
      String b = msg.getDisplayMessageBody();
      if (b != null) bodyBuilder.append(b);
    }
    if (bodyBuilder.length() == 0) {
      Log.w(TAG, "Message body unavailable");
      return;
    }
    final String body = bodyBuilder.toString();

    // Log the message body, it may be important. But strip the codewords from the log entry.
    final String codeword = settings.getString(ConfigKeys.SmsCodeword);
    final String userCodeword = settings.getString(ConfigKeys.SmsUserCodeword);
    if (codeword.isEmpty() && userCodeword.isEmpty()) {
      Log.w(TAG, "No codeword configured");
      Log.i(TAG, "SMS body: ", body);
    } else {
      String cleaned = codeword.isEmpty() ? body : body.replace(codeword, "*****");
      cleaned = userCodeword.isEmpty() ? cleaned : cleaned.replace(userCodeword, "*****");
      Log.i(TAG, "SMS body: ", cleaned);
    }

    // Check that the message contains a codeword before taking any action.
    final boolean fullAuth = !codeword.isEmpty() && body.contains(codeword);
    final boolean limitedAuth = !fullAuth && !userCodeword.isEmpty() && body.contains(userCodeword);
    if (!fullAuth && !limitedAuth) {
      Log.i(TAG, "SMS ignored");
      return;
    }

    // Rate limit user SMS.
    if (fullAuth) {
      Log.i(TAG, "SMS accepted");
    } else {
      final long uptime = SystemClock.elapsedRealtime();
      if (sLastUserSMS + MAX_USER_FREQUENCY_MILLIS > uptime) {
        Log.i(TAG, "SMS ignored (user rate limited)");
        return;
      } else {
        Log.i(TAG, "SMS accepted (user)");
        sLastUserSMS = uptime;
      }
    }

    // Strip the codeword.
    int codeStart = fullAuth ? body.indexOf(codeword) : body.indexOf(userCodeword);
    int codeLen = fullAuth ? codeword.length() : userCodeword.length();
    final String stripped = body.substring(codeStart + codeLen).trim();

    // Ignore case when looking for the requested command string.
    final String cmds = stripped.toLowerCase(Locale.ENGLISH);

    // Perform the requested action. User codewords only authorise photo requests.
    if (fullAuth && stripped.indexOf('{') > -1 && stripped.lastIndexOf('}') > -1) { // Config
      Log.i(TAG, "Updating config from SMS");
      updateSettings(stripped, settings);
    } else if (fullAuth && cmds.contains("reboot")) {
      Log.i(TAG, "Rebooting from SMS command");
      Helpers.reboot(context, 100);
    } else if (fullAuth && (cmds.contains("upload") || cmds.contains("download"))) {
      // Check if only the current log file plus all other queued uploads or downloads are to be
      // transferred or if archived files need to be queued first.
      int uploadIdx = cmds.indexOf("upload");
      int archivedIdx = cmds.indexOf("archived");
      if (uploadIdx > -1 && archivedIdx > uploadIdx) {
        uploadArchived(context, stripped.substring(archivedIdx + 8).trim());
      } else {
        // Prepare to ship the log and kick off file transfers.
        Log.i(TAG, "Starting file transfers");
        Helpers.prepareAndShipLogs(context, uploadIdx == -1);
      }
    } else if (fullAuth && cmds.contains("live")) { // live [duration in sec]
      startLiveView(context, cmds, settings);
    } else { // Take a photo.
      // Select the resolution.
      int reso;
      if (cmds.contains("full")) {
        Log.i(TAG, "Taking a photo from SMS (full reso)");
        reso = 10000; // Set to full reso on all devices.
      } else if (cmds.contains("fhd")) {
        Log.i(TAG, "Taking a photo from SMS (FHD reso)");
        reso = 1920;
      } else if (cmds.contains("hd")) {
        Log.i(TAG, "Taking a photo from SMS (HD reso)");
        reso = 1280;
      } else if (cmds.contains("small")) {
        Log.i(TAG, "Taking a photo from SMS (small reso)");
        reso = 640;
      } else if (cmds.contains("min")) {
        Log.i(TAG, "Taking a photo from SMS (min reso)");
        reso = 320;
      } else {
        Log.i(TAG, "Taking a photo from SMS (cam reso)");
        reso = settings.getInt(ConfigKeys.CamReso);
      }

      // Disable the configured crop region on request.
      double[] roi = cmds.contains("nocrop") ? new double[]{0, 0, 1, 1} : null;

      // Treat as computational image (i.e. upload to data folder) on request.
      boolean isData = fullAuth && cmds.contains("data");

      Intent i = new Intent(context, CameraActivity.class);
      i.putExtra(ConfigKeys.CamMode.extraName(), "photo");
      i.putExtra(ConfigKeys.CamRepeatCount.extraName(), 1);
      i.putExtra(ConfigKeys.CamReso.extraName(), reso);
      if (roi != null) i.putExtra(ConfigKeys.ImageCropRegion.extraName(), roi);
      i.putExtra(Constants.EXTRA_EVENT_TRIGGER, "SMS");
      if (isData) i.putExtra(Constants.EXTRA_FILENAME_SUFFIX, ""); // Adds an underscore.
      context.startActivity(i);
      CameraActivity.acquireWakelock(context);
    }
  }

  /**
   * Extracts the config and updates the local settings.
   *
   * @param s        The string containing the JSON config.
   * @param settings The settings to update.
   */
  private void updateSettings(String s, Settings settings) {
    // Extract the JSON document between from { and } inclusive.
    int start = s.indexOf('{');
    int end = s.lastIndexOf('}');
    if (start < 0 || end < 0 || start > end) {
      Log.w(TAG, "Invalid format for config update, missing { or }");
      return;
    }

    String json = s.substring(start, end + 1);
    if (json.length() <= 2) {
      Log.w(TAG, "Discarding empty JSON config");
      return;
    }

    // Write in the background, then read the config.
    new WriteTextFileTask(
        Settings.getFileInAppDir(Settings.APP_DIR_OTA, Settings.CONFIG_FILE_NAME),
        false, () -> settings.readConfigFile(true))
        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
  }

  /**
   * Starts the live view.
   *
   * @param context  A context.
   * @param cmd      The command string with an optional duration in seconds as argument.
   * @param settings A settings instance.
   */
  private void startLiveView(Context context, String cmd, Settings settings) {
    int count = settings.getInt(ConfigKeys.LiveViewCount);
    double fps = settings.getDouble(ConfigKeys.LiveViewFPS);
    int reso = settings.getInt(ConfigKeys.LiveViewReso);

    if (fps <= 0.) {
      fps = 1.0;
      Log.w(TAG, "FPS cannot be <= 0, setting to default: ", fps);
    }

    // Extract the optional duration from the SMS and update the count.
    Matcher matcher = Pattern.compile(".*live\\s*(\\d+).*").matcher(cmd);
    if (matcher.matches()) {
      try {
        int duration = Integer.parseInt(matcher.group(1));
        count = ((int) Math.ceil(duration * fps)) + 1; // Add one for the image taken at t=0.
      } catch (NumberFormatException e) {
        Log.e(TAG, "The matched duration failed to be parsed as int: ", e.getMessage());
      }
    }

    if (count < 1) {
      Log.w(TAG, "Invalid repeat count of ", count, ", setting it to 1");
      count = 1;
    }

    Log.i(TAG, Locale.ENGLISH, "Starting live view from SMS for %d images at %.4f fps", count, fps);
    Intent i = new Intent(context, CameraActivity.class);
    i.putExtra(ConfigKeys.CamMode.extraName(), "photo");
    i.putExtra(ConfigKeys.CamRepeatCount.extraName(), count);
    i.putExtra(ConfigKeys.CamRepeatFPS.extraName(), fps);
    i.putExtra(ConfigKeys.CamRepeatLocked.extraName(), false);
    i.putExtra(ConfigKeys.CamReso.extraName(), reso);
    i.putExtra(Constants.EXTRA_EVENT_TRIGGER, "SMS");
    i.putExtra(Constants.EXTRA_FILENAME_SUFFIX, "live");
    context.startActivity(i);
    CameraActivity.acquireWakelock(context);
  }

  /**
   * Queue files matching expr from {@link Settings#APP_DIR_ARCHIVED} for uploading.
   *
   * @param context A context.
   * @param expr    The expression to match against file names. The only special character allowed,
   *                but not required, is a single asterisk.
   */
  private void uploadArchived(Context context, String expr) {
    if (expr.isEmpty()) {
      Log.w(TAG, "Cannot match an empty pattern");
      return;
    }

    // The pattern may contain a single asterisk and no other non-literal characters. Replace the
    // asterisk with a .* and quote the rest.
    int starIdx = expr.indexOf('*');
    if (starIdx == -1) {
      expr = Pattern.quote(expr) + ".*";
    } else {
      // Make sure there is no more than one *.
      if (expr.lastIndexOf('*') != starIdx) {
        Log.w(TAG, "Cannot upload archived files: invalid pattern, only one * is allowed");
        return;
      }

      expr = Pattern.quote(expr.substring(0, starIdx)) + ".*"
          + Pattern.quote(expr.substring(starIdx + 1));
    }

    Pattern pattern;
    try {
      pattern = Pattern.compile(expr);
    } catch (Exception e) {
      Log.e(TAG, "Failed to compile pattern \"", expr, "\": ", e.getMessage());
      return;
    }

    // Trigger log rotation and file transfers when archived files have been queued.
    final Context appContext = context.getApplicationContext();
    Runnable callback = () -> {
      Log.i(TAG, "Starting file transfers");
      Helpers.prepareAndShipLogs(appContext, false);
    };

    // Queue matching files.
    Log.i(TAG, "Queueing archived files matching \"", expr, "\" for uploading");
    File srcDir = Settings.getFileInAppDir(null, Settings.APP_DIR_ARCHIVED);
    File destDir = Settings.getFileInAppDir(null, Settings.APP_DIR_NEW);
    new MoveFilesTask(srcDir, destDir, pattern, false,
        callback, new Handler(Looper.getMainLooper()))
        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }
}
