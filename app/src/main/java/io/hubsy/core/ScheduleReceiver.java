package io.hubsy.core;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.usb.RadarService;
import io.hubsy.core.util.HealthCheck;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;

public class ScheduleReceiver extends BroadcastReceiver {
  private static final String TAG = "ScheduleReceiver";

  // Extra specifying the interval of a received recurring alarm in millis.
  private static final String EXTRA_SCHEDULE_INTERVAL = Constants.NS_PREFIX
      + ".schedule_receiver.extra.schedule_interval";
  // Extra specifying the scheduled time for a fixed-time alarm, in millis since the epoch.
  private static final String EXTRA_SCHEDULE_TIME = Constants.NS_PREFIX
      + ".schedule_receiver.extra.schedule_time";

  // The current interval for recurring alarms.
  private static long sRecurringIntervalMillis = 0;

  // The next request code to use for one-off events. Must not be 0 or 1 to avoid interfering with
  // regular scheduled events.
  private static final AtomicInteger sNextUniqueRequestCode = new AtomicInteger(2);

  @Override
  public void onReceive(Context context, Intent intent) {
    final Bundle extras = intent == null ? null : intent.getExtras();
    boolean isRecurring = false;
    boolean isTimed = false;
    String eventTrigger = "Schedule";
    if (extras != null) {
      isRecurring = extras.containsKey(EXTRA_SCHEDULE_INTERVAL);
      isTimed = extras.containsKey(EXTRA_SCHEDULE_TIME);
      // Forward the event trigger if specified.
      String trigger = extras.getString(Constants.EXTRA_EVENT_TRIGGER);
      if (trigger != null) eventTrigger = trigger;
    }

    final boolean isDirect = intent == null;
    final boolean isOneOff = !isRecurring && !isTimed && !isDirect;

    if (isRecurring) Log.d(TAG, "Recurring alarm received");
    else if (isTimed) Log.d(TAG, "Timed alarm received");
    else if (isOneOff) Log.d(TAG, "One-off alarm received");
    else Log.d(TAG, "Alarm received");

    HealthCheck.getInstance(context).updateTimestamp(HealthCheck.Timestamp.ScheduledTask);
    final Context appContext = context.getApplicationContext();

    // Read the latest config, it may have changed.
    final Settings settings = Settings.getInstance(appContext);
    settings.readConfigFile(true);

    // Set the next alarm if this is a timed alarm or the schedule interval has changed.
    if (isTimed
        || (isRecurring && extras.getLong(EXTRA_SCHEDULE_INTERVAL) != sRecurringIntervalMillis)) {
      updateSchedule(appContext);
    }

    // Get schedule time range.
    String timeFrom = settings.getString(ConfigKeys.ScheduleFrom);
    String timeTo = settings.getString(ConfigKeys.ScheduleTo);

    boolean doFileTransfers = true;
    if (settings.getBool(ConfigKeys.Pause)) {
      // Not taking a photo, just do uploads / downloads.
      Log.d(TAG, "Photo taking is on pause");
    } else if ((isRecurring || isDirect) && !Helpers.isTimeInRange(timeFrom, timeTo)) {
      // The current time is outside the scheduled range. Does not apply to events scheduled for a
      // specific time.
      Log.d(TAG, "Photos not taken outside: ", timeFrom, " - ", timeTo);
    } else {
      // Determine the resolution.
      int reso = settings.getInt(ConfigKeys.ScheduleReso);
      if (extras != null && extras.containsKey(ConfigKeys.CamReso.extraName())) {
        // The intent explicitly specifies a resolution.
        reso = extras.getInt(ConfigKeys.CamReso.extraName(), reso);
      } else if (isTimed) { // Fixed-time alarms may use a different resolution.
        reso = settings.getInt(ConfigKeys.ScheduleTimesReso);
      }

      // Prepare to take a photo.
      final Intent i = new Intent(context, CameraActivity.class);
      // Pass extras along and only modify those relevant to this class.
      if (extras != null) i.putExtras(extras);
      i.putExtra(ConfigKeys.CamReso.extraName(), reso);
      i.putExtra(Constants.EXTRA_EVENT_TRIGGER, eventTrigger);
      context.startActivity(i);
      CameraActivity.acquireWakelock(appContext);

      doFileTransfers = false; // Delay transfers until after the photo has been taken.
    }

    if (doFileTransfers) {
      Helpers.prepareAndShipLogs(context, true);
    }

    // Start the radar service if it should be running but isn't.
    if (!isDirect && settings.getBool(ConfigKeys.EnableRadar)
        && !RadarService.isRunning()) {
      Log.d(TAG, "Radar service not running, trying to start it");
      RadarService.updateServiceState(appContext, false, null);
    }
  }


  /**
   * Updates the schedule based on the current config.
   *
   * @param context The context to use.
   */
  public static void updateSchedule(Context context) {
    context = context.getApplicationContext();

    // Parse schedule times if any.
    final Settings settings = Settings.getInstance(context);
    final List<String> scheduleTimes = settings.getStringList(ConfigKeys.ScheduleTimes);
    final List<Calendar> times = scheduleTimes.isEmpty() ? null : parseScheduleTimes(scheduleTimes);

    // Parse the recurring interval.
    long intervalMillis = 60000L * settings.getInt(ConfigKeys.ScheduleInterval);
    if (intervalMillis <= 0 && (times == null || times.isEmpty())) {
      // There must be at least one valid schedule.
      intervalMillis = 60 * 60000L; // 1 hour default.
    }

    final AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

    // Create a new recurring alarm if it has changed.
    if (intervalMillis != sRecurringIntervalMillis) {
      sRecurringIntervalMillis = intervalMillis;
      updateRecurringSchedule(context, am, intervalMillis);
    }

    // Set a one-off alarm for the next scheduled time.
    if (times != null && !times.isEmpty()) {
      setNextTimedSchedule(context, am, times.get(0));
    }
  }

  /**
   * Replaces the active recurring schedule with the new schedule of intervalMillis.
   */
  private static void updateRecurringSchedule(Context context, AlarmManager am,
                                              long intervalMillis) {
    // Cancel any existing alarm for this schedule.
    Intent intent = new Intent(context, ScheduleReceiver.class);
    intent.putExtra(EXTRA_SCHEDULE_INTERVAL, intervalMillis);
    PendingIntent scheduledIntent = PendingIntent.getBroadcast(context,
        0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    am.cancel(scheduledIntent);

    // Create a new recurring alarm.
    if (intervalMillis > 0) {
      am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
          SystemClock.elapsedRealtime() + intervalMillis, intervalMillis, scheduledIntent);
      Log.d(TAG, "Recurring alarm interval set to ", intervalMillis / 60000L, "m");
    } else {
      Log.d(TAG, "Recurring alarm disabled");
    }
  }

  /**
   * Sets a one-off scheduled alarm for the given dateTime.
   */
  private static void setNextTimedSchedule(Context context, AlarmManager am, Calendar dateTime) {
    long timeInMillis = dateTime.getTimeInMillis();

    // Cancel any existing alarm scheduled for a specific time.
    Intent intent = new Intent(context, ScheduleReceiver.class);
    intent.putExtra(EXTRA_SCHEDULE_TIME, timeInMillis);
    PendingIntent scheduledIntent = PendingIntent.getBroadcast(context,
        1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    am.cancel(scheduledIntent);

    // Create the new alarm.
    am.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, timeInMillis, scheduledIntent);
    Log.d(TAG, "Scheduled the next alarm for: ", dateTime.getTime().toString());
  }

  /**
   * Schedules a one-off event.
   *
   * @param context         A context.
   * @param elapsedRealtime The event time, in SystemClock.elapsedRealtime.
   * @param extras          Optional extras to add to the intent.
   */
  public static void scheduleOneOffEvent(Context context, long elapsedRealtime,
                                         @Nullable Bundle extras) {
    // Choose a unique request code for scheduled events, making sure it is never 0 or 1.
    int requestCode = sNextUniqueRequestCode.getAndIncrement();
    if (requestCode == 0 || requestCode == 1) {
      scheduleOneOffEvent(context, elapsedRealtime, extras);
      return;
    }

    // Create a one shot pending intent.
    context = context.getApplicationContext();
    Intent intent = new Intent(context, ScheduleReceiver.class);
    if (extras != null) intent.putExtras(extras);
    PendingIntent scheduledIntent = PendingIntent.getBroadcast(context,
        requestCode, intent, PendingIntent.FLAG_ONE_SHOT);

    // Create the alarm.
    final AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    am.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, elapsedRealtime,
        scheduledIntent);
  }

  /**
   * Parses the time values in the schedule and returns a list of date-times in ascending order.
   *
   * @param scheduleTimes The schedule times.
   * @return The ordered date-time values.
   */
  @Nullable
  private static List<Calendar> parseScheduleTimes(@NonNull final List<String> scheduleTimes) {
    if (scheduleTimes.isEmpty()) return null;

    // Parse the given times and create concrete calendar instances for them.
    final Calendar now = Calendar.getInstance();
    final List<Calendar> parsedTimes = new ArrayList<>(scheduleTimes.size());
    for (String time : scheduleTimes) {
      final Calendar dateTime = Helpers.parseTime(time, true);
      if (dateTime == null) {
        Log.w(TAG, "Invalid schedule time: ", time);
        continue;
      }

      // If the parsed time is not after the current time, move it to the next day.
      if (!dateTime.after(now)) dateTime.add(Calendar.DAY_OF_MONTH, 1);

      parsedTimes.add(dateTime);
    }

    // Sort the times in asceding order.
    Collections.sort(parsedTimes);

    return parsedTimes;
  }

}