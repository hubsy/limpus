package io.hubsy.core;

import android.app.Activity;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.provider.Telephony;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferService;

import java.io.File;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.beaconscanner.BeaconScannerReceiver;
import io.hubsy.core.usb.RadarService;
import io.hubsy.core.util.ClearSMSTask;
import io.hubsy.core.util.Log;
import io.hubsy.core.util.MoveFilesTask;
import io.hubsy.core.util.PhoneOrientation;
import io.hubsy.core.util.Radio;

/**
 * The main activity. Starts services and tasks.
 */
public class SecurityCamAppActivity extends Activity {
  private static final String TAG = "SecurityCamAppActivity";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    //android.os.Debug.waitForDebugger();
    final Context appContext = getApplicationContext();

    // Load new settings if any.
    final Settings settings = Settings.getInstance(appContext);
    settings.readConfigFile(false);

    final String packageName = getPackageName();
    String appVersion = null;
    try {
      appVersion = getPackageManager().getPackageInfo(packageName, 0).versionName;
    } catch (NameNotFoundException e) {
      Log.e(TAG, "Package name not found: ", e.getMessage());
    }
    setTitle("Hubsy " + appVersion);
    setContentView(R.layout.main);

    // Initialise the device orientation.
    new PhoneOrientation(appContext);

    // Check if the app is the default SMS app and make it so if not.
    if (!packageName.equals(Telephony.Sms.getDefaultSmsPackage(this))) {
      Log.d(TAG, "Making this the default SMS app");
      android.provider.Settings.Secure.putString(getContentResolver(),
          "sms_default_application", packageName);

      // Check again.
      if (packageName.equals(Telephony.Sms.getDefaultSmsPackage(this))) {
        Log.d(TAG, "Successfully changed the default SMS app setting");
      } else {
        Log.e(TAG, "Failed to make this the default SMS app");
      }
    }

    // Clear old messages. New messages are not stored when this is the default SMS app, so it may
    // be enough to do this only when the app was found to not be the default SMS app above.
    new ClearSMSTask(appContext.getContentResolver()).execute();

    if (settings.getBool(ConfigKeys.RadioAlwaysOn)) {
      // Turn on radios if required.
      Radio.toggleRadios(appContext, true);
    }

    // Remove any lingering files from the ota folder to prevent a boot loop. This must be done on
    // the main thread for now to ensure it happens before ScheduleReceiver runs for the first time.
    clearDir(Settings.APP_DIR_OTA);

    // Also clear the tmp folder, any files in there should be leftovers no longer being used.
    clearDir(Settings.APP_DIR_TMP);

    // Move all files left in the upload queue folder to failed so they are dealt with accordingly
    // by the following logic. This is done on the main thread to ensure it happens before any new
    // files are queued.
    final String appFolder = Settings.getAppFolderPath();
    new MoveFilesTask(
        new File(appFolder, Settings.APP_DIR_QUEUED),
        new File(appFolder, Settings.APP_DIR_FAILED),
        null, false, null, null)
        .executeInForeground();

    retryOrArchiveFailedUploads(settings, appFolder);

    // Set up listeners for successful OTA file downloads.
    UpdateInstaller.registerOnce(appContext);

    // Start the S3 transfer service. It monitors network connectivity and updates transfer states.
    appContext.startService(new Intent(appContext, TransferService.class));

    // Establish the MQTT connection.
    MqttClient.getInstance(appContext).connect();

    //Launch the BLE service.
    Log.d(TAG, "Manual BLE service launch");
    sendBroadcast(new Intent(appContext, BeaconScannerReceiver.class));

    // Set an alarm to run the ScheduleReceiver.
    ScheduleReceiver.updateSchedule(appContext);

    // Schedule the StorageService.
    final int storageMins = settings.getInt(ConfigKeys.StorageCheckInterval);
    final int storageMinMBytes = settings.getInt(ConfigKeys.StorageMinimum);
    final PersistableBundle storageExtras = new PersistableBundle();
    storageExtras.putLong(StorageService.EXTRA_MIN_FREE_BYTES, storageMinMBytes * 1024L * 1024L);
    final JobInfo storageJob =
        new JobInfo.Builder(StorageService.JOB_ID, new ComponentName(this, StorageService.class))
            .setPeriodic(storageMins * 60L * 1000L)
            .setExtras(storageExtras)
            .build();
    final JobScheduler scheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
    scheduler.schedule(storageJob);

    // Request the first location update.
    LocationUpdateReceiver.scheduleRecurringUpdates(this,
        settings.getInt(ConfigKeys.LocationUpdateInterval));

    final Handler mainHandler = new Handler(getMainLooper());

    // Start the radar service after a delay if enabled.
    mainHandler.postDelayed(() -> {
      if (settings.getBool(ConfigKeys.EnableRadar) && !RadarService.isRunning()) {
        RadarService.updateServiceState(this, false, null);
      }
    }, 50000);

    // Trigger the initial run, just wait a moment to initialise the device orientation first.
    mainHandler.postDelayed(() -> {
      Log.d(TAG, "Initial ScheduleReceiver run");
      ScheduleReceiver sched = new ScheduleReceiver();
      sched.onReceive(appContext, null);
    }, 1000);
  }

  /**
   * Delete all files from appDir. Must be relative to the app's root data dir.
   */
  private void clearDir(String appDir) {
    final File dir = new File(Settings.getAppFolderPath(), appDir);
    File[] files = dir.listFiles();
    if (files == null) return;

    for (File file : files) {
      if (file.delete()) {
        Log.d(TAG, "Deleted left over file ", appDir, "/", file.getName());
      } else {
        Log.w(TAG, "Failed to delete file ", appDir, "/", file.getName());
      }
    }
  }

  /**
   * Depending on {@link ConfigKeys#RetryUploadForTypes}, either retry uploading files in folder
   * {@link Settings#APP_DIR_FAILED} or archive them locally.
   */
  private void retryOrArchiveFailedUploads(Settings settings, String appFolder) {
    // Get the file types for which to retry uploading if found in the failed folder.
    final List<String> fileTypesToRetry = settings.getStringList(ConfigKeys.RetryUploadForTypes);
    if (fileTypesToRetry.isEmpty()) { // Add the defaults.
      fileTypesToRetry.add("log");
      fileTypesToRetry.add("jpg");
      fileTypesToRetry.add("json");
    }

    // Construct the regex corresponding to the retry list.
    StringBuilder movePattern = new StringBuilder(".*\\.(");
    for (int i = 0; i < fileTypesToRetry.size(); i++) {
      if (i > 0) movePattern.append("|");
      movePattern.append(fileTypesToRetry.get(i));
    }
    movePattern.append(")$");
    String movePatternStr = movePattern.toString();
    Log.d(TAG, "Retrying upload for files with regex: ", movePatternStr);

    try {
      // Schedule files matching pattern for uploading and archive the rest.
      // These two tasks must be executed sequentially on the same background thread.
      new MoveFilesTask(
          new File(appFolder, Settings.APP_DIR_FAILED),
          new File(appFolder, Settings.APP_DIR_NEW),
          Pattern.compile(movePatternStr), false, null, null)
          .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
      new MoveFilesTask(
          new File(appFolder, Settings.APP_DIR_FAILED),
          new File(appFolder, Settings.APP_DIR_ARCHIVED),
          null, false, null, null)
          .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
    } catch (PatternSyntaxException e) {
      Log.w(TAG, "Failed to compile the regex for files for which uploads are retried: ",
          e.getMessage());
    }
  }

}
