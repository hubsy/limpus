package io.hubsy.core;

import android.app.Application;
import android.content.ComponentCallbacks2;

import java.lang.Thread.UncaughtExceptionHandler;

import io.hubsy.core.util.HealthCheck;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;

/**
 * The global application class.
 */
public class SecurityCamApplication extends Application {
  private static final String TAG = "SecurityCamApplication";

  @Override
  public void onCreate() {
    super.onCreate();

    Thread.setDefaultUncaughtExceptionHandler(new AppExceptionHandler());

    // Initialise the Settings before the Log class is used and set the desired log level.
    final Settings settings = Settings.getInstance(this);
    Log.setLogLevel(Log.Level.from(settings.getString(Settings.ConfigKeys.LogLevel).trim()));

    Log.i(TAG, "Starting ", Helpers.getAppVersionString(this));

    // Initialise the health check singleton so it can be used without a Context where necessary.
    HealthCheck.getInstance(this);
  }

  @Override
  public void onTrimMemory(int level) {
    super.onTrimMemory(level);

    if (level >= ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW) {
      Log.d(TAG, "Memory running low, current level: ", level);
    }
  }

  /**
   * Uncaught exception handler.
   */
  private static class AppExceptionHandler implements UncaughtExceptionHandler {
    private UncaughtExceptionHandler mDefaultHandler;

    public AppExceptionHandler() {
      mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
      Log.e(TAG, "Handling uncaught exception for thread: " + thread.getName(), ex);
      HealthCheck.getInstance().setFlag(HealthCheck.Flag.RestartNeeded, true, true);
      if (mDefaultHandler != null) mDefaultHandler.uncaughtException(thread, ex);
    }
  }
}
