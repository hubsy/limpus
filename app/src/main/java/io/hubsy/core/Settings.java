package io.hubsy.core;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Pattern;

import io.hubsy.core.beaconscanner.BeaconScannerReceiver;
import io.hubsy.core.usb.RadarService;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;
import io.hubsy.core.util.Radio;

public final class Settings {
  private static final String TAG = "Settings";

  /**
   * A prefix for intent extras that override a device setting. The actual extras will have the
   * {@link ConfigKeys} value appended, e.g.: EXTRA_PREFIX + ".CamReso"
   */
  private static final String EXTRA_PREFIX = Constants.NS_PREFIX + "." + TAG + ".extra";

  // NOTE: Due to the static initialisation order, these arrays must be defined textually before
  // ConfigKeys so they are non-null in the static constructor calls.
  private static final String[] EMPTY_STRING_ARRAY = new String[0];
  private static final int[] EMPTY_INT_ARRAY = new int[0];
  private static final double[] EMPTY_DOUBLE_ARRAY = new double[0];

  /**
   * The keys used to access configuration values.
   */
  public enum ConfigKeys {
    /**
     * Codeword to be included in SMS messages to authenticate a superuser command.
     */
    SmsCodeword(String.class, ""),
    /**
     * Codeword to be included in SMS messages to authenticate a user command.
     */
    SmsUserCodeword(String.class, ""),
    /**
     * The public key for AWS.
     */
    AwsKeyId(String.class, ""),
    /**
     * The secret key for AWS.
     */
    AwsKeySecret(String.class, ""),
    /**
     * The AWS user name for the device.
     */
    AwsUserName(String.class, ""),
    /**
     * The AWS S3 bucket to use for uploading and downloading of files.
     */
    AwsBucket(String.class, ""),
    /**
     * S3 path to the latest APK update archive.
     */
    AwsAppName(String.class, ""),
    /**
     * S3 path to the latest health monitor service update archive.
     */
    AwsHealthMonitorName(String.class, ""),
    /**
     * S3 path to the latest image mask file.
     */
    AwsImageMaskName(String.class, ""),
    /**
     * The region to use for AWS Rekognition. Defaults to ap-southeast-2 (Sydney).
     */
    AwsRekognitionRegion(String.class, "ap-southeast-2"),
    /**
     * S3 path to the latest system tools update archive.
     */
    AwsToolsName(String.class, ""),
    /**
     * S3 path to the latest updater executable update archive.
     */
    AwsUpdaterName(String.class, ""),
    /**
     * S3 path to the latest stand-alone upgrade script file.
     */
    AwsUpgradeName(String.class, ""),
    /**
     * Time from which interval-based scheduled photo taking is enabled. Format: hh:mm.
     */
    ScheduleFrom(String.class, ""),
    /**
     * Time after which interval-based scheduled photo taking is disabled (inclusive).
     * Format: hh:mm.
     */
    ScheduleTo(String.class, ""),
    /**
     * The interval at which {@link ScheduleReceiver} is triggered, in minutes.
     */
    ScheduleInterval(Integer.class, 0),
    /**
     * The times at which {@link ScheduleReceiver} is triggered. These times are <b>not</b>
     * restricted by the time range [ScheduleFrom, ScheduleTo]. Comma-separated list of type
     * String, with elements formatted as: hh:mm. Example: "06:00, 13:30, 20:00".
     */
    ScheduleTimes(String[].class, EMPTY_STRING_ARRAY),
    /**
     * The maximum size for the longer side of the dimensions for photos taken on a recurring
     * schedule. See {@link #ScheduleInterval}.
     */
    ScheduleReso(Integer.class, Settings.DEFAULT_RESO),
    /**
     * The maximum size for the longer side of the dimensions for photos taken on a timed schedule.
     * See {@link #ScheduleTimes}.
     */
    ScheduleTimesReso(Integer.class, Settings.DEFAULT_RESO),
    /**
     * The maximum size for the longer side of the dimensions for event-triggered photos.
     */
    CamReso(Integer.class, Settings.DEFAULT_RESO),
    /**
     * The preferred aspect ratio for photos. Some {@link #CamFormat}s may support less aspect
     * ratios than others, in which case they fall back to 4:3. One of {"4:3", "16:9"}.
     */
    CamAspectRatio(String.class, "4:3"),
    /**
     * The camera configuration schedule. Format: Array of JSON objects with keys from
     * {@link CamScheduleKeys}.
     */
    CamSchedule(String.class, ""),
    /**
     * The camera edge enhancement mode. One of {off, fast, quality}.
     */
    CamEdgeMode(String.class, "fast"),
    /**
     * The camera noise reduction mode. One of {off, fast, quality}.
     */
    CamNoiseReductionMode(String.class, "fast"),
    /**
     * The camera tonemapping mode. One of {fast, quality}.
     */
    CamTonemapMode(String.class, "fast"),
    /**
     * The AE metering area. A rectangle described by percentage offsets left, top, right, bottom
     * from the top-left corner of the final image. List of int with values in range [0, 100].
     */
    CamExposureArea(int[].class, EMPTY_INT_ARRAY),
    /**
     * The camera exposure mode. One of {"auto", "manual"}.
     */
    CamExposureMode(String.class, "auto"),
    /**
     * The exposure time for use with manual exposure, in milliseconds.
     */
    CamExposureTime(Integer.class, 20),
    /**
     * The AF metering area. A rectangle described by percentage offsets left, top, right, bottom
     * from the top-left corner of the final image. List of int with values in range [0, 100].
     */
    CamFocusArea(int[].class, EMPTY_INT_ARRAY),
    /**
     * The camera focus mode. One of {"auto", "infinity"}.
     */
    CamFocusMode(String.class, "auto"),
    /**
     * The image format to capture. One of {"jpg", "raw"}.
     */
    CamFormat(String.class, "jpg"),
    /**
     * The ISO sensitivity for use with manual exposure.
     */
    CamISO(Integer.class, 200),
    /**
     * The camera capture mode. One of {photo, video}.
     */
    CamMode(String.class, "photo"),
    /**
     * The number of photos to capture per request. 0 for infinite (the sequence must be ended
     * explicitly).
     */
    CamRepeatCount(Integer.class, 1),
    /**
     * The target frame rate at which to capture repeating photos.
     */
    CamRepeatFPS(Double.class, 1.),
    /**
     * Lock capture settings (e.g. exposure) for the entire repeating photo sequence.
     */
    CamRepeatLocked(Boolean.class, false),
    /**
     * The AWB metering area. A rectangle described by percentage offsets left, top, right, bottom
     * from the top-left corner of the final image. List of int with values in range [0, 100].
     */
    CamWhiteBalanceArea(int[].class, EMPTY_INT_ARRAY),
    /**
     * Optional DCRaw arguments. These will be appended to the required options -w -c.
     */
    DCRawArgs(String.class, "-b 1.0"),
    /**
     * Enable the radar service.
     */
    EnableRadar(Boolean.class, false),
    /**
     * The region to crop images to before upload or further processing, specified as a list of
     * normalised values: x_offset, y_offset, width, height. Comma-separated list of type double
     * with 0 or 4 values.
     */
    ImageCropRegion(double[].class, EMPTY_DOUBLE_ARRAY),
    /**
     * The JPEG encoding quality. Range [0, 100].
     */
    JPEGQuality(Integer.class, 80),
    /**
     * The video encoding bit rate in bits/sec.
     */
    VideoBitrate(Integer.class, 5000000),
    /**
     * The video capture duration in milliseconds. The playback duration depends on the capture
     * duration, the capture rate and the playback rate.
     */
    VideoCaptureDuration(Integer.class, 10 * 1000),
    /**
     * The video capture rate in frames per second.
     */
    VideoCaptureFPS(Double.class, 1.),
    /**
     * The video playback rate in frames per second.
     */
    VideoPlaybackFPS(Integer.class, 5),
    /**
     * The video resolution. Format wxh. Example: 1920x1080 or 1280x720.
     */
    VideoReso(String.class, "1920x1080"),
    /**
     * The maximum size for the longer side of the dimensions for still captures taken during video
     * recording.
     */
    VideoStillReso(Integer.class, 1920),
    /**
     * The maximum size for the longer side of the dimensions for thumbnails captured during video
     * recording. A value of 0 disables thumbnail capture when possible.
     */
    VideoThumbnailReso(Integer.class, 0),
    /**
     * Enables uploading of computational data, such as frames used for image analysis.
     */
    EnableDataUpload(Boolean.class, false),
    /**
     * Enable number plate detection (localisation).
     */
    EnableNumberPlateDetection(Boolean.class, false),
    /**
     * Enable number plate recognition (OCR).
     */
    EnableNumberPlateRecognition(Boolean.class, false),
    /**
     * Enables uploading of videos.
     */
    EnableVideoUpload(Boolean.class, false),
    /**
     * The maximum size, in bytes, for a file to be uploaded.
     */
    MaxUploadSize(Long.class, 10 * 1024L * 1024L),
    /**
     * Enable Wifi.
     */
    UseWifi(Boolean.class, false),
    /**
     * Indicates whether the network broadcasts its SSID or not. Must be set to 1 (true) for hidden
     * networks.
     */
    WifiIsHidden(Boolean.class, false),
    /**
     * The WiFi pre-shared key.
     */
    WifiPSK(String.class, ""),
    /**
     * The access point SSID.
     */
    WifiSSID(String.class, ""),
    /**
     * Pause picture taking.
     */
    Pause(Boolean.class, false),
    /**
     * Pause the radar. This does not disable the service (see {@link #EnableRadar}), but it pauses
     * actions triggered by radar events.
     */
    PauseRadar(Boolean.class, false),
    /**
     * Force radios to always be enabled (i.e. disallow airplane mode).
     */
    RadioAlwaysOn(Boolean.class, true),
    /**
     * Sensor activation rules. Format: Array of JSON objects with keys from
     * {@link BleActivationsKeys}
     */
    BleActivations(String.class, ""),
    /**
     * Enable the Bluetoooth LE scanner.
     */
    BleOn(Boolean.class, false),
    /**
     * Scan duration in milliseconds.
     */
    BleDuration(Long.class, 3000L),
    /**
     * Delay between scan intervals in milliseconds.
     */
    BleDelay(Long.class, 2000L),
    /**
     * This many milliseconds may be added to BleDelay to schedule more efficiently.
     */
    BleFlex(Long.class, 200L),
    /**
     * One of the scan modes in {@link android.bluetooth.le.ScanSettings}. One of {0, 1, 2}.
     */
    BleScanMode(Integer.class, 2),
    /**
     * Device address filter. Comma-separated list of type String. Example:
     * "60:03:08:B7:F3:9B, AC:23:3F:A0:05:29".
     */
    BleDeviceAddresses(String[].class, EMPTY_STRING_ARRAY),
    /**
     * Service UUID filter. Comma-separated list of type String.
     */
    BleServiceUUIDs(String[].class, EMPTY_STRING_ARRAY),
    /**
     * The maximum frequency in milliseconds at which photos and sync tasks are triggered by BLE
     * sensors (i.e. the minimum wait between honoured requests).
     */
    BleMaxFrequency(Long.class, 30000L),
    /**
     * The minimum number of activations from different sensors in interval
     * {@link #BleMinActivationsDuration} required to trigger a photo.
     */
    BleMinActivations(Integer.class, 1),
    /**
     * The duration in milliseconds to wait for {@link #BleMinActivations} devices to trigger.
     */
    BleMinActivationsDuration(Long.class, 0L),
    /**
     * The duration, in seconds, to taper by if the maximum possible number of activations have been
     * observed during {@link #BleTaperWindow}.
     */
    BleTaperMaxDuration(Integer.class, 3600),
    /**
     * Parameter a for the tapering equation.
     */
    BleTaperParamA(Double.class, 30.),
    /**
     * Parameter b for the tapering equation.
     */
    BleTaperParamB(Double.class, 10.),
    /**
     * Parameter c for the tapering equation.
     */
    BleTaperParamC(Double.class, 30.),
    /**
     * The coefficient to the linear term of the tapering equation.
     */
    BleTaperParamLinear(Double.class, 1.),
    /**
     * The size of the window of time, ending at the current time, for which to count sensor
     * activations for the tapering equation, in seconds.
     */
    BleTaperWindow(Integer.class, 3600),
    /**
     * The duration, in milliseconds, before the output display is cleared. Zero disables automatic
     * clearing.
     */
    DisplayDuration(Integer.class, 5000),
    /**
     * The output line of the display to use for speed values. If unset or negative, the output is
     * disabled.
     */
    DisplayLineNumberPlate(Integer.class, -1),
    /**
     * The output line of the display to use for number plates. If unset or negative, the output is
     * disabled.
     */
    DisplayLineSpeed(Integer.class, -1),
    /**
     * Device address filter for light relays. Comma-separated list of type String.
     */
    LightAddresses(String[].class, EMPTY_STRING_ARRAY),
    /**
     * Time from which lights are enabled. Format: hh:mm.
     */
    LightsFrom(String.class, ""),
    /**
     * Time after which lights are disabled (inclusive). Format: hh:mm.
     */
    LightsTo(String.class, ""),
    /**
     * The number of photos to capture per live view request. Value > 0.
     */
    LiveViewCount(Integer.class, 30),
    /**
     * The target frame rate at which to capture live view photos.
     */
    LiveViewFPS(Double.class, 1.),
    /**
     * The maximum size for the longer side of the dimensions for live view photos.
     */
    LiveViewReso(Integer.class, 640),
    /**
     * The location update interval, in minutes.
     */
    LocationUpdateInterval(Integer.class, 60),
    /**
     * The log level. Only message at or above this level will be written to log files. One of
     * {VERBOSE, DEBUG, INFO, WARN, ERROR}.
     */
    LogLevel(String.class, "INFO"),
    /**
     * The region of the image used as input to the object detector. This is expressed by specifying
     * how the square receptive field is positioned in the input image. Its edge length is always
     * equal to the shorter side of the image. The alignment is specified in view-orientation of
     * the input (i.e. after rotation).
     * <p>
     * One of {start, end, centre}, where "start" refers to either left (landscape) or top
     * (portrait) alignment and "end" refers to right (landscape) or bottom (portrait) alignment.
     */
    DetectorInputAlignment(String.class, "centre"),
    /**
     * The longest exposure time for the object detector, in milliseconds. When the actual exposure
     * is longer than specified, the capture will be aborted and no further captures will be
     * performed for a fixed duration.
     */
    DetectorMaxExposureTime(Integer.class, 32),
    /**
     * The maximum number of frames in which no number plate can be detected after at least one
     * successful detection. When this threshold is exceeded, the current recording will be stopped.
     */
    NumberPlateMaxFailedDetections(Integer.class, 3),
    /**
     * The minimum confidence for number plate localisation. Range [0, 100].
     */
    NumberPlateBboxMinConfidence(Double.class, 40.),
    /**
     * The minimum confidence for number plate recognition. Range [0, 100].
     */
    NumberPlateOCRMinConfidence(Double.class, 40.),
    /**
     * The minimum confidence for vehicle localisation. Range [0, 100].
     */
    VehicleBboxMinConfidence(Double.class, 50.),
    /**
     * The angle of tolerance from the {@link #VehicleMotionFilterAngles}, in degrees.
     * Range [0, 180].
     */
    VehicleMotionFilterAngleTolerance(Double.class, 45.),
    /**
     * A filter for the allowed angles of motion of a vehicle between consecutive frames, in
     * counterclockwise degrees from the positive y-axis (i.e. the vertical image dimension, with
     * increasing values towards the bottom). Comma-separated list of type double with values in
     * [-180, 180]. If unset or empty, the filter is disabled.
     */
    VehicleMotionFilterAngles(double[].class, EMPTY_DOUBLE_ARRAY),
    /**
     * A high-pass filter for the normalised magnitude of motion of a vehicle between consecutive
     * frames. Range [0, sqrt(1+1)] (i.e. up to the normalised diagonal length).
     */
    VehicleMotionFilterMinMagnitude(Double.class, 0.007),
    /**
     * The shortest period, in milliseconds, at which radar readings are broadcast.
     */
    RadarMaxFrequency(Integer.class, 0),
    /**
     * The minimum speed reading for an event to be broadcast.
     */
    RadarMinSpeed(Integer.class, 0),
    /**
     * The file extensions of files found in {@link #APP_DIR_FAILED} for which uploading is retried.
     * All file types not listed here will be deleted instead. Comma-separated list of type String.
     */
    RetryUploadForTypes(String[].class, new String[]{"log", "jpg", "json"}),
    /**
     * The number of values to be tracked by the model.
     */
    SensorModelCapacity(Integer.class, 0),
    /**
     * The mininum difference between a measurement and the respective mean value for a measurement
     * to be considered an event.
     */
    SensorModelMinThreshold(Integer.class, 0),
    /**
     * The standard deviation factor for a measurement to be treated as an event.
     */
    SensorModelSigmaFactor(Double.class, 1.),
    /**
     * The software environment this device runs in. One of {DEV, PROD}.
     */
    SoftwareEnv(String.class, "PROD"),
    /**
     * The interval at which {@link StorageService} is triggered in minutes.
     */
    StorageCheckInterval(Integer.class, 180),
    /**
     * The minimum free storage in MiB below which some archived files will be deleted.
     */
    StorageMinimum(Integer.class, 5120),
    /**
     * The USB baud rate.
     */
    USBBaudRate(Integer.class, 19200),
    /**
     * A base64 encoded list of newline-separated commands to be sent to the USB device.
     */
    USBCommand(String.class, ""),
    /**
     * The USB data bits.
     */
    USBDataBits(Integer.class, 8),
    /**
     * The USB parity. One of {none, odd, even, mark, space}.
     */
    USBParity(String.class, "none"),
    /**
     * The USB stop bits.
     */
    USBStopBits(Integer.class, 1),
    /**
     * This is a placeholder for an unknown key with no current use.
     */
    UnknownKey(Object.class, new Object());

    @NonNull
    public final Class valueType;
    @NonNull
    private final Object defaultValue;

    <T> ConfigKeys(@NonNull Class<T> c, @NonNull T d) {
      valueType = c;
      defaultValue = d;
    }

    /**
     * Like {@link #valueOf}, but instead of throwing an exception if the given name does not match
     * one of the constants, it returns {@link #UnknownKey}.
     *
     * @param name The name of the enum constant.
     * @return The matching enum constant or {@link #UnknownKey}.
     */
    public static ConfigKeys from(String name) {
      try {
        return valueOf(name);
      } catch (IllegalArgumentException ex) {
        Log.d(TAG, "Unknown config key: ", name);
      }
      return UnknownKey;
    }

    /**
     * @return The name to use for intent extras that override this setting.
     */
    public String extraName() {
      return EXTRA_PREFIX + "." + name();
    }
  } // ConfigKeys

  /**
   * Keys supported by JSON documents in the array stored under {@link ConfigKeys#CamSchedule}.
   */
  public enum CamScheduleKeys {
    /**
     * Toggle additional cleaning of the foreground mask produced by bgsub before further analysis.
     * Type boolean. Default: true.
     */
    BGSubCleanFGMask,
    /**
     * Toggle inverting of images before they are applied to the model. Type boolean.
     * Default: false.
     */
    BGSubInvert,
    /**
     * The kernel. Type JSON String array representing BGSubKernelHeight x BGSubKernelWidth values.
     * An array element can provide a single value. It can also provide n equal values with format
     * "n: 0.1". A value can be either a float constant or a simple math expression (currently only
     * supports a single division using format "1.0 / 49").
     */
    BGSubKernel,
    /**
     * The kernel height. Type int.
     */
    BGSubKernelHeight,
    /**
     * The kernel width. Type int.
     */
    BGSubKernelWidth,
    /**
     * The threshold to apply to the output of filtering, range [0,1]. Type double.
     */
    BGSubThreshold,
    /**
     * {@link ConfigKeys#CamExposureMode}.
     */
    CamExposureMode,
    /**
     * {@link ConfigKeys#CamExposureTime}.
     */
    CamExposureTime,
    /**
     * {@link ConfigKeys#CamISO}.
     */
    CamISO,
    /**
     * Whether the configured values can be interpolated. Type boolean.
     */
    Interpolate,
    /**
     * The interval, in milliseconds, at which sequence photos are taken. Defaults to 1000.
     * Type long.
     */
    SequenceInterval,
    /**
     * The number of sequence images to be taken for an event-triggered photo. Defaults to 0, i.e.,
     * no sequence images. Type int.
     */
    SequenceLength,
    /**
     * The maximum size for the longer side of the dimensions for sequence photos. Defaults to 640.
     * Type int.
     */
    SequenceReso,
    /**
     * The time when the config first becomes active or reaches its peak effect if interpolated.
     * Type String. Format: hh:mm.
     */
    Time,
  } // CamScheduleKeys

  /**
   * Keys supported by the JSON documents in the array stored under
   * {@link ConfigKeys#BleActivations}.
   */
  public enum BleActivationsKeys {
    /**
     * An optional list of additional, delayed events to be generated when the rule is satisfied.
     * Each entry specifies an offset, in milliseconds since the rule was evaluated, after which a
     * follow-on event is produced. The initial event (t=0) should not be included here. Type long.
     */
    DelayedEvents,
    /**
     * A list of device addresses. Each device MAC address can optionally have a hyphen followed by
     * one or more suffixes for the type of sensors the rule applies to if the device can report
     * different types. Currently supported suffixes: A for accelerometers ; P for PIR. No suffix
     * implies any sensor provided by the device. Type JSON array of strings.
     */
    Devices,
    /**
     * Time from which the rule is enabled. Defaults to 00:00. Type String. Format: hh:mm.
     */
    EnabledFrom,
    /**
     * Time after which the rule is disabled (inclusive). Defaults to 23:59. Type String.
     * Format: hh:mm.
     */
    EnabledTo,
    /**
     * The minimum number of activations to satisfy this rule. Type int.
     */
    MinActivations,
    /**
     * The duration in milliseconds during which {@link #MinActivations} valid activations have to
     * occur. Type long.
     */
    MinActivationsDuration,
  } // BleActivationsKeys

  private static final String PREFS_NAME = "pref.dat";
  private static final String VERSION_PREFS_NAME = "version";
  private static final String STATE_PREFS_NAME = "state";

  private static final String AWS_FILE_NAME_FORMAT = "yyyyMMdd'T'HHmmss.SSS'GMT'Z";

  public static final String APP_DIR = "hubsy"; // The root app folder.
  private static final String APP_DIR_PATH = Environment.getExternalStorageDirectory().getPath()
      + "/" + APP_DIR + "/"; // The absolute path to the app's root folder.

  public static final String APP_DIR_NEW = "new"; // Files waiting to be uploaded.
  public static final String APP_DIR_QUEUED = "queued"; // Files managed by the AWS client.
  public static final String APP_DIR_ARCHIVED = "archived"; // Uploaded or not-to-upload files.
  public static final String APP_DIR_LOGS = "logs"; // Current application log folder.
  public static final String APP_DIR_FAILED = "failed"; // Files that failed to upload.
  public static final String APP_DIR_OTA = "ota"; // OTA files downloaded but not yet installed.
  public static final String APP_DIR_TMP = "tmp"; // Temp. storage for files being processed.
  public static final String APP_DIR_DATA = "data"; // Local application data.

  public static final int DEFAULT_RESO = 1920;

  public static final String CONFIG_FILE_NAME = "config.json";
  private static final String CONFIG_FILE_HASH = "config-hash";

  // The config keys for files that can be updated over the air.
  private static final ConfigKeys[] UPDATABLE_FILES = {
      ConfigKeys.AwsAppName, ConfigKeys.AwsHealthMonitorName, ConfigKeys.AwsImageMaskName,
      ConfigKeys.AwsToolsName, ConfigKeys.AwsUpdaterName, ConfigKeys.AwsUpgradeName
  };

  // The pattern used to split a string setting representing a list into a list of strings.
  private static final Pattern sListRegex = Pattern.compile(", *");

  private static volatile Settings sSingleton;

  /**
   * Returns the singleton instance of this class.
   */
  @NonNull
  public static Settings getInstance(@NonNull Context context) {
    if (sSingleton == null) {
      synchronized (Settings.class) {
        if (sSingleton == null) {
          sSingleton = new Settings(context);
        }
      }
    }
    return sSingleton;
  }

  /**
   * Returns the singleton instance of this class if it has already been created. Null otherwise.
   */
  @Nullable
  public static Settings getInstance() {
    return sSingleton;
  }

  private final SharedPreferences mConfig;
  private final SharedPreferences mVersions;
  private final Context mContext;

  private Settings(@NonNull Context context) {
    context = context.getApplicationContext();
    mConfig = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    mVersions = context.getSharedPreferences(VERSION_PREFS_NAME, Context.MODE_PRIVATE);
    mContext = context;
  }

  /**
   * Use enum Settings.ConfigKeys for the list of known names.
   *
   * @param name     The name of the setting.
   * @param defValue The default value.
   * @return The non-null value, or `defValue` if `name` is not found.
   */
  private String getString(@NonNull String name, @Nullable String defValue) {
    try {
      return mConfig.getString(name, defValue);
    } catch (ClassCastException e) {
      Log.e(TAG, "Unexpected type, wanted String, got: ", e.getMessage());
      return defValue;
    }
  }

  /**
   * Returns the value of the specified setting.
   *
   * @param name The name of the setting.
   * @return The value.
   */
  @NonNull
  public String getString(ConfigKeys name) {
    return getString(name.name(), getStringDefault(name));
  }

  /**
   * Returns the value of the specified setting.
   *
   * @param name     The name of the setting.
   * @param defValue The default value.
   * @return The non-null value, or `defValue` if `name` is not found.
   */
  private String getString(ConfigKeys name, @Nullable String defValue) {
    return getString(name.name(), defValue);
  }

  @NonNull
  private String getStringDefault(ConfigKeys name) {
    if (!name.valueType.equals(String.class)) {
      Log.e(TAG, name, " is a ", name.valueType.getSimpleName(), ", not a String");
      return "";
    }
    return (String) name.defaultValue;
  }

  /**
   * Retrieves a setting with type long.
   */
  public long getLong(ConfigKeys name) {
    return getLong(name, getLongDefault(name));
  }

  /**
   * Retrieves a setting with type long.
   *
   * @param name     The name of the setting.
   * @param defValue The default value.
   * @return The value, or `defValue` if the setting is unset or not of type long.
   */
  private long getLong(ConfigKeys name, long defValue) {
    final String str = getString(name, "");
    if (str.isEmpty()) return defValue;
    try {
      return Long.parseLong(str);
    } catch (NumberFormatException e) {
      Log.w(TAG, "Failed to parse as long: ", str);
      return defValue;
    }
  }

  private long getLongDefault(ConfigKeys name) {
    if (!name.valueType.equals(Long.class)) {
      Log.e(TAG, name, " is a ", name.valueType.getSimpleName(), ", not a Long");
      return 0L;
    }
    return (Long) name.defaultValue;
  }

  /**
   * Retrieves a setting with type int.
   */
  public int getInt(ConfigKeys name) {
    return getInt(name, getIntDefault(name));
  }

  /**
   * Retrieves a setting with type int.
   *
   * @param name     The name of the setting.
   * @param defValue The default value.
   * @return The value, or `defValue` if the setting is unset or not of type int.
   */
  private int getInt(ConfigKeys name, int defValue) {
    final String str = getString(name, "");
    if (str.isEmpty()) return defValue;
    try {
      return Integer.parseInt(str);
    } catch (NumberFormatException e) {
      Log.w(TAG, "Failed to parse as int: ", str);
      return defValue;
    }
  }

  private int getIntDefault(ConfigKeys name) {
    if (!name.valueType.equals(Integer.class)) {
      Log.e(TAG, name, " is a ", name.valueType.getSimpleName(), ", not an Integer");
      return 0;
    }
    return (Integer) name.defaultValue;
  }

  /**
   * Retrieves a setting with type boolean.
   */
  public boolean getBool(ConfigKeys name) {
    return getBool(name, getBoolDefault(name));
  }

  /**
   * Retrieves a setting with type bool (true is represented by an int value of 1 and false by 0).
   *
   * @param name     The name of the setting.
   * @param defValue The default value.
   * @return The value, or `defValue` if the setting is unset or its value is not of type int or
   * outside of {0,1}.
   */
  private boolean getBool(ConfigKeys name, boolean defValue) {
    final String str = getString(name, "");
    if (str.isEmpty()) return defValue;
    try {
      int i = Integer.parseInt(str);
      return i < 0 || i > 1 ? defValue : i == 1;
    } catch (NumberFormatException e) {
      Log.w(TAG, "Failed to parse as bool: ", str);
      return defValue;
    }
  }

  private boolean getBoolDefault(ConfigKeys name) {
    if (!name.valueType.equals(Boolean.class)) {
      Log.e(TAG, name, " is a ", name.valueType.getSimpleName(), ", not a Boolean");
      return false;
    }
    return (Boolean) name.defaultValue;
  }

  /**
   * Retrieves a setting with type double.
   */
  public double getDouble(ConfigKeys name) {
    return getDouble(name, getDoubleDefault(name));
  }

  /**
   * Retrieves a setting with type double.
   *
   * @param name     The name of the setting.
   * @param defValue The default value.
   * @return The value, or `defValue` if the setting is unset or not of type double.
   */
  private double getDouble(ConfigKeys name, double defValue) {
    final String str = getString(name, "");
    if (str.isEmpty()) return defValue;
    try {
      return Double.parseDouble(str);
    } catch (NumberFormatException e) {
      Log.w(TAG, "Failed to parse as double: ", str);
      return defValue;
    }
  }

  private double getDoubleDefault(ConfigKeys name) {
    if (!name.valueType.equals(Double.class)) {
      Log.e(TAG, name, " is a ", name.valueType.getSimpleName(), ", not a Double");
      return 0.;
    }
    return (Double) name.defaultValue;
  }

  /**
   * Retrieves a string setting with comma separated values split into a list.
   * <p>
   * The string is split at each comma, which may optionally be followed by one or more spaces.
   *
   * @param name The name of the setting.
   * @return The list of values. Empty if no values are found.
   */
  @NonNull
  public List<String> getStringList(ConfigKeys name) {
    final String str = getString(name, "");
    // Make a copy of the underlying array.
    if (str.isEmpty()) return new ArrayList<>(Arrays.asList(getStringArrayDefault(name)));

    final String[] values = sListRegex.split(str, 0);

    // Remove empty matches.
    final ArrayList<String> result = new ArrayList<>(values.length);
    for (String v : values) {
      if (!v.isEmpty()) {
        result.add(v);
      }
    }
    return result;
  }

  @NonNull
  private String[] getStringArrayDefault(ConfigKeys name) {
    if (!name.valueType.equals(String[].class)) {
      Log.e(TAG, name, " is a ", name.valueType.getSimpleName(), ", not a String[]");
      return EMPTY_STRING_ARRAY;
    }
    return (String[]) name.defaultValue;
  }

  /**
   * Retrieves a string setting with comma separated values split into a list of integers.
   *
   * @param name The name of the setting.
   * @return The list of values, or the default list if the setting is unset or any of its values is
   * not of type int.
   */
  @NonNull
  public List<Integer> getIntList(ConfigKeys name) {
    final String str = getString(name, "");
    // Make a copy of the underlying array.
    if (str.isEmpty()) return primitiveArrayToList(getIntArrayDefault(name));

    final String[] values = sListRegex.split(str, 0);

    // Parse strings.
    final List<Integer> result = new ArrayList<>(values.length);
    try {
      for (String v : values) {
        result.add(Integer.parseInt(v));
      }
    } catch (NumberFormatException e) {
      Log.w(TAG, "Invalid value in int[] for ", name, ": ", e.getMessage());
      return primitiveArrayToList(getIntArrayDefault(name));
    }
    return result;
  }

  @NonNull
  private int[] getIntArrayDefault(ConfigKeys name) {
    if (!name.valueType.equals(int[].class)) {
      Log.e(TAG, name, " is a ", name.valueType.getSimpleName(), ", not an int[]");
      return EMPTY_INT_ARRAY;
    }
    return (int[]) name.defaultValue;
  }

  /**
   * Retrieves a string setting with comma separated values split into a list of doubles.
   *
   * @param name The name of the setting.
   * @return The list of values, or the default list if the setting is unset or any of its values is
   * not of type double.
   */
  @NonNull
  public List<Double> getDoubleList(ConfigKeys name) {
    final String str = getString(name, "");
    // Make a copy of the underlying array.
    if (str.isEmpty()) return primitiveArrayToList(getDoubleArrayDefault(name));

    final String[] values = sListRegex.split(str, 0);

    // Parse strings.
    final List<Double> result = new ArrayList<>(values.length);
    try {
      for (String v : values) {
        result.add(Double.parseDouble(v));
      }
    } catch (NumberFormatException e) {
      Log.w(TAG, "Invalid value in double[] for ", name, ": ", e.getMessage());
      return primitiveArrayToList(getDoubleArrayDefault(name));
    }
    return result;
  }

  @NonNull
  private double[] getDoubleArrayDefault(ConfigKeys name) {
    if (!name.valueType.equals(double[].class)) {
      Log.e(TAG, name, " is a ", name.valueType.getSimpleName(), ", not a double[]");
      return EMPTY_DOUBLE_ARRAY;
    }
    return (double[]) name.defaultValue;
  }

  /**
   * Update describes the remote file paths and local file names for updates and their corresponding
   * checksums.
   */
  public static class Update {
    /**
     * The remotePath is the remote storage path (i.e. S3 key).
     */
    public final String remotePath;
    public final String remoteHashPath;

    /**
     * The localFilename is the base name of the file (not the path) on the local system.
     */
    public final String localFilename;
    public final String localHashFilename;

    private Update(String remote, String local) {
      remotePath = remote;
      remoteHashPath = remote + ".md5";

      localFilename = local;
      localHashFilename = local + ".md5";
    }
  }

  /**
   * Returns a list of files that are ready to be updated.
   *
   * @return A list of updates.
   */
  @NonNull
  public List<Update> updates() {
    final ArrayList<Update> files = new ArrayList<>();
    for (final ConfigKeys key : UPDATABLE_FILES) {
      final String remotePath = getString(key);
      if (remotePath.isEmpty()) continue; // Nothing configured.

      final String remoteVersion = getVersionFromAwsKey(remotePath);
      if (remoteVersion.isEmpty()) {
        Log.w(TAG, "Failed to extract the version string from the remote path: ", remotePath);
        continue;
      }

      // Check if the installed and remote versions differ.
      final String installedVersion = getVersion(key);
      if (remoteVersion.equals(installedVersion)) continue;

      Log.i(TAG, "New version available for ", key, ": ", installedVersion, " -> ", remoteVersion);

      // Create the update descriptor.
      final String localFilename = new File(remotePath).getName();
      files.add(new Update(remotePath, localFilename));
    }

    return files;
  }

  /**
   * Extracts the file version from an AWS key. The key may be for the install file itself or for
   * its associated checksum file.
   *
   * @param awsKey The key.
   * @return The version string. Empty if the key does not contain a valid version string.
   */
  public String getVersionFromAwsKey(@NonNull final String awsKey) {
    // Strip .md5, .tgz or .tgz.md5 from the file name.
    String fname = new File(awsKey).getName();
    fname = fname.endsWith(".md5") ? fname.substring(0, fname.length() - 4) : fname;
    fname = fname.endsWith(".tgz") ? fname.substring(0, fname.length() - 4) : fname;

    // Extract the version string starting with, but exclusive of, '.v'.
    int idx = fname.lastIndexOf(".v");
    String version;
    if (idx < 0 || (idx + 2) >= fname.length()) {
      Log.w(TAG, "Failed to extract a version string from: ", awsKey);
      version = "";
    } else {
      version = fname.substring(idx + 2);
    }

    return version;
  }

  /**
   * Returns the version string for the currently installed version of the file or other versioned
   * object identified by key.
   *
   * @param key The config identifier.
   * @return The version string, or empty string if not installed.
   */
  public String getVersion(final ConfigKeys key) {
    final String keyName = key.name();
    try {
      return mVersions.getString(keyName, "");
    } catch (ClassCastException e) {
      // Check if this is an old-style build number instead of a version string.
      final String oldKeyName = key == ConfigKeys.AwsToolsName ? "AwsTgzArchiveName" : keyName;
      long build;
      try {
        build = mVersions.getLong(oldKeyName, 0);
      } catch (ClassCastException e2) {
        Log.e(TAG, "Invalid type for version of ", key, ": ", e.getMessage());
        return "";
      }

      if (build > 0) {
        // Replace the build number with a version string.
        final String version = String.valueOf(build);
        if (mVersions.edit().remove(oldKeyName).putString(keyName, version).commit()) {
          Log.d(TAG, "Successfully converted build number ", build,
              " to a version string for ", key);
          return version;
        }
      }
      Log.e(TAG, "Failed to replace the build number with the version string for ", key);
    }

    return "";
  }

  /**
   * Returns the version database for installed files.
   */
  public SharedPreferences getVersions() {
    return mVersions;
  }

  /**
   * Read the config file from the local storage.
   *
   * @return True if a new configuration file was successfully read or no update is available.
   * False otherwise.
   */
  @SuppressWarnings("UnusedReturnValue")
  public boolean readConfigFile(boolean rebootOnChange) {
    // Check if there is a new config file.
    final File confFile = getConfigFile();
    if (confFile == null) {
      Log.d(TAG, "No config file found, using existing settings");
      return true;
    }
    Log.d(TAG, "Loading config from ", confFile);

    // Read the JSON encoded file.
    int confHash; //hash code of the config file
    String jsonString;
    try (final BufferedReader reader = new BufferedReader(
        new InputStreamReader(new FileInputStream(confFile), StandardCharsets.UTF_8))) {
      // Ideally one-byte chars but will grow as needed.
      final StringBuilder buffer = new StringBuilder((int) confFile.length());
      String line;
      // Discards newlines, but we don't need those.
      while ((line = reader.readLine()) != null) {
        buffer.append(line);
      }
      jsonString = buffer.toString();
      confHash = jsonString.hashCode(); // Calculate the hash of the string.
    } catch (Exception ex) {
      Log.w(TAG, "Failed to read the config file from ", confFile, ": ", ex.getMessage());
      return false;
    }

    // Check if the config file is actually different from the previous one.
    String lastKnownHash = mConfig.getString(CONFIG_FILE_HASH, "");
    try {
      if (confHash != 0 && lastKnownHash != null && !lastKnownHash.isEmpty()
          && Integer.parseInt(lastKnownHash) == confHash) {
        // Delete the local copy.
        Log.d(TAG, "Identical hash, won't process: ", lastKnownHash);
        if (!confFile.delete()) Log.w(TAG, "Failed to delete local config file: ", confFile);
        return true;
      }
    } catch (NumberFormatException ex) {
      Log.e(TAG, "lastKnownHash is not a valid number: ", lastKnownHash, ": ", ex.getMessage());
      // Continue as usual.
    }

    // Decode JSON.
    JSONObject jsonObj;
    try {
      jsonObj = new JSONObject(jsonString);
    } catch (Exception ex) {
      Log.w(TAG, "Failed to parse the config: ", ex.getMessage());
      return false;
    }

    // Copy new settings from the file into the local settings.
    final Editor editor = mConfig.edit();
    int propsNoReboot = 0; // Number of changed properties not requiring a reboot to apply.
    final EnumMap<ConfigKeys, String> modified = new EnumMap<>(ConfigKeys.class);
    final JSONArray jsonNames = jsonObj.names();
    for (int i = 0; i < jsonObj.length(); i++) {
      try {
        final String name = jsonNames.getString(i);
        final String valueOld = mConfig.getString(name, null);
        final ConfigKeys key = ConfigKeys.from(name);

        // Check for null values, which reset the setting to its default.
        if (JSONObject.NULL.equals(jsonObj.opt(name))) {
          if (valueOld != null) {
            // Remove the entry from the config.
            editor.remove(name);
            modified.put(key, null);
            if (!updateRequiresReboot(key)) propsNoReboot++;
            Log.i(TAG, "Key: ", name, "; value: ", valueOld, " -> <default>");
          }
          continue;
        }

        // Validate the new value.
        final String value = jsonObj.optString(name, null);
        final boolean valid = validate(key, value);
        if (valid && !value.equals(valueOld)) {
          // The new value is valid and different from the old value. Update the config.
          editor.putString(name, value);
          modified.put(key, value);
          if (!updateRequiresReboot(key)) propsNoReboot++;
          if (key == ConfigKeys.AwsKeySecret || key == ConfigKeys.SmsCodeword
              || key == ConfigKeys.SmsUserCodeword || key == ConfigKeys.WifiPSK) {
            Log.i(TAG, "Key: ", name, "; value: <redacted>");
          } else {
            Log.i(TAG, "Key: ", name, "; value: ", valueOld, " -> ", value);
          }
        } else if (!valid) {
          Log.w(TAG, "Invalid value for setting ", key, ": ", value);
        }
      } catch (Exception ex) {
        Log.w(TAG, "Failed to read key #", i, ": ", ex.getMessage());
      }
    }

    // Save the hash to avoid reprocessing the same set of config data in the future.
    String newConfHash = Integer.toString(confHash);
    editor.putString(CONFIG_FILE_HASH, newConfHash);
    Log.d(TAG, "New config hash: ", newConfHash);

    if (!editor.commit()) {
      Log.w(TAG, "Failed to commit the updated settings");
      return false;
    }

    // Delete the local copy.
    if (!confFile.delete()) Log.w(TAG, "Failed to delete local config file: ", confFile);

    if (modified.isEmpty()) return true;

    // A reboot must be performed if at least one property requiring a reboot to be applied has
    // changed. The hash check should prevent a boot loop in case confFile could not be deleted,
    // unless the editor commit also failed...
    final boolean needsReboot = rebootOnChange && (modified.size() - propsNoReboot) > 0;
    new ExportTask(mContext, this, getTimestampFileName(System.currentTimeMillis(), ".json"),
        needsReboot).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    if (!needsReboot) {
      // Perform any actions necessary for the new settings to take effect.
      performUpdateActions(modified);
    }

    return true;
  }

  /**
   * Validates the value of setting `key`.
   *
   * @param key   The configuration key.
   * @param value The value to validate for key.
   * @return True if the value is valid or no validation rules exist for key.
   */
  private boolean validate(@NonNull final ConfigKeys key, @Nullable final String value) {
    if (value == null) return false;

    switch (key) {
      // These settings must not be empty.
      case AwsBucket:
      case AwsKeyId:
      case AwsKeySecret:
      case AwsUserName:
      case SmsCodeword:
        return notEmpty(value);

      // Boolean.
      case BleOn:
      case EnableDataUpload:
      case EnableNumberPlateDetection:
      case EnableNumberPlateRecognition:
      case EnableRadar:
      case EnableVideoUpload:
      case Pause:
      case PauseRadar:
        return isEmpty(value) || isBool(value);

      case CamEdgeMode:
        return validateStringInList(value, new String[]{"", "off", "fast", "quality"});
      case CamExposureMode:
        return validateStringInList(value, new String[]{"", "auto", "manual"});
      case CamExposureTime:
        return isEmpty(value) || validateInt(value, (v) -> v > 0);
      case CamFocusMode:
        return validateStringInList(value, new String[]{"", "auto", "infinity"});
      case CamFormat:
        return validateStringInList(value, new String[]{"", "jpg", "jpeg", "raw"});
      case CamISO:
        return isEmpty(value) || validateInt(value, (v) -> v > 0);
      case CamMode:
        return validateStringInList(value, new String[]{"", "photo", "video"});
      case CamNoiseReductionMode:
        return validateStringInList(value, new String[]{"", "off", "fast", "quality"});
      case CamRepeatCount:
        return isEmpty(value) || validateInt(value, (v) -> v >= 0);
      case CamRepeatFPS:
        return isEmpty(value) || validateDouble(value, (v) -> v > 0.);
      case CamReso:
        return isEmpty(value) || validateInt(value, (v) -> v > 0);
      case CamTonemapMode:
        return validateStringInList(value, new String[]{"", "fast", "quality"});

      case DetectorInputAlignment:
        return isEmpty(value)
            || validateStringInList(value, new String[]{"start", "end", "center", "centre"});
      case DetectorMaxExposureTime:
        return isEmpty(value) || validateInt(value, (v) -> v > 0);

      // These settings must be valid time values or unset/empty.
      case LightsFrom:
      case LightsTo:
      case ScheduleFrom:
      case ScheduleTo:
        return isEmpty(value) || Helpers.parseTime(value, true) != null;

      case ScheduleInterval:
        return isEmpty(value) || validateInt(value, (v) -> v >= 0);
      case ScheduleReso:
        return isEmpty(value) || validateInt(value, (v) -> v > 0);
      case ScheduleTimesReso:
        return isEmpty(value) || validateInt(value, (v) -> v > 0);

      case UnknownKey:
        Log.d(TAG, "Unknown config key: ", key);
        return true; // Allow the value because there are no rules for it.

      case USBBaudRate:
        return isEmpty(value) || validateInt(value, (v) -> v > 0);
      case USBDataBits:
        return isEmpty(value) || validateInt(value, (v) -> v >= 5 && v <= 8);
      case USBParity:
        return validateStringInList(value,
            new String[]{"", "none", "odd", "even", "mark", "space"});
      case USBStopBits:
        return isEmpty(value) || validateInt(value, (v) -> v >= 1 && v <= 3);

      // Percentages.
      case NumberPlateBboxMinConfidence:
      case NumberPlateOCRMinConfidence:
      case VehicleBboxMinConfidence:
        return isEmpty(value) || validateDouble(value, (v) -> v >= 0 && v <= 100);

      case VehicleMotionFilterAngleTolerance:
        return isEmpty(value) || validateDouble(value, (v) -> v >= 0 && v <= 180);
      case VehicleMotionFilterAngles:
        return isEmpty(value) || validateDoubleList(value, 10, (v) -> v >= -180 && v <= 180);
      case VehicleMotionFilterMinMagnitude:
        return isEmpty(value) || validateDouble(value, (v) -> v >= 0 && v < 1.42); // Sqrt(2).

      case VideoCaptureFPS:
        return isEmpty(value) || validateDouble(value, (v) -> v > 0.);
      case VideoPlaybackFPS:
        return isEmpty(value) || validateInt(value, (v) -> v > 0);
      case VideoReso:
        return Pattern.matches("[1-9][0-9]{1,3}[xX][1-9][0-9]{1,3}", value);
    }

    return true;
  }

  /**
   * Returns true if v is either null or empty (after trimming whitespace).
   */
  private boolean isEmpty(@Nullable String v) {
    return v == null || v.trim().isEmpty();
  }

  /**
   * Returns true if v is neither null nor empty (after trimming whitespace).
   */
  private boolean notEmpty(@Nullable String v) {
    return v != null && !v.trim().isEmpty();
  }

  /**
   * Returns true if v is of type int with value 0 or 1.
   */
  private boolean isBool(@Nullable String v) {
    return validateInt(v, (v2) -> v2 == 0 || v2 == 1);
  }

  /**
   * Returns true if v is of type int and the validator returns true.
   */
  private boolean validateInt(@Nullable String v, @NonNull Function<Integer, Boolean> validator) {
    try {
      return v != null ? validator.apply(Integer.parseInt(v)) : false;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Returns true if v is of type double and the validator returns true.
   */
  private boolean validateDouble(@Nullable String v, @NonNull Function<Double, Boolean> validator) {
    try {
      return v != null ? validator.apply(Double.parseDouble(v)) : false;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Returns true if v is a comma-separated list of type double and the validator returns true for
   * all elements.
   * <p>
   * The limit parameter has the same meaning as its namesake in {@link Pattern#split}.
   */
  private boolean validateDoubleList(@Nullable String v, int limit,
                                     @NonNull Function<Double, Boolean> validator) {
    final String[] values = sListRegex.split(v, limit);

    boolean valid = true;
    for (int i = 0; i < values.length && valid; i++) {
      try {
        valid = validator.apply(Double.parseDouble(values[i]));
      } catch (NumberFormatException | NullPointerException e) {
        return false;
      }
    }
    return valid;
  }

  /**
   * Returns true if v is non-null and equals one of the list elements.
   */
  private boolean validateStringInList(@Nullable String v, @NonNull String[] list) {
    if (v == null) return false;
    for (String e : list) {
      if (v.equals(e)) return true;
    }
    return false;
  }

  /**
   * Checks whether an update to the value of this setting requires a reboot to apply.
   *
   * @param key The configuration key.
   * @return True if a reboot is required to apply the setting. False if the value can be changed
   * on the fly.
   */
  private boolean updateRequiresReboot(final ConfigKeys key) {
    // Keys NOT requiring a reboot.
    switch (key) {
      case AwsAppName:
      case AwsBucket:
      case AwsHealthMonitorName:
      case AwsImageMaskName:
      case AwsRekognitionRegion:
      case AwsToolsName:
      case AwsUpdaterName:
      case AwsUpgradeName:
      case AwsUserName:
      case BleActivations:
      case BleDelay:
      case BleDeviceAddresses:
      case BleDuration:
      case BleFlex:
      case BleMaxFrequency:
      case BleMinActivations:
      case BleMinActivationsDuration:
      case BleOn:
      case BleScanMode:
      case BleServiceUUIDs:
      case BleTaperMaxDuration:
      case BleTaperParamA:
      case BleTaperParamB:
      case BleTaperParamC:
      case BleTaperParamLinear:
      case BleTaperWindow:
      case CamAspectRatio:
      case CamEdgeMode:
      case CamExposureArea:
      case CamExposureMode:
      case CamExposureTime:
      case CamFocusArea:
      case CamFocusMode:
      case CamFormat:
      case CamISO:
      case CamMode:
      case CamNoiseReductionMode:
      case CamRepeatCount:
      case CamRepeatFPS:
      case CamRepeatLocked:
      case CamReso:
      case CamSchedule:
      case CamTonemapMode:
      case CamWhiteBalanceArea:
      case DCRawArgs:
      case DetectorInputAlignment:
      case DetectorMaxExposureTime:
      case DisplayDuration:
      case DisplayLineNumberPlate:
      case DisplayLineSpeed:
      case EnableDataUpload:
      case EnableNumberPlateDetection:
      case EnableNumberPlateRecognition:
      case EnableRadar:
      case EnableVideoUpload:
      case ImageCropRegion:
      case JPEGQuality:
      case LightAddresses:
      case LightsFrom:
      case LightsTo:
      case LiveViewCount:
      case LiveViewFPS:
      case LiveViewReso:
      case LocationUpdateInterval:
      case LogLevel:
      case MaxUploadSize:
      case NumberPlateBboxMinConfidence:
      case NumberPlateMaxFailedDetections:
      case NumberPlateOCRMinConfidence:
      case Pause:
      case PauseRadar:
      case RadioAlwaysOn:
      case RadarMaxFrequency:
      case RadarMinSpeed:
      case RetryUploadForTypes:
      case ScheduleFrom:
      case ScheduleInterval:
      case ScheduleReso:
      case ScheduleTimes:
      case ScheduleTimesReso:
      case ScheduleTo:
      case SensorModelCapacity:
      case SensorModelMinThreshold:
      case SensorModelSigmaFactor:
      case SmsCodeword:
      case SmsUserCodeword:
      case SoftwareEnv:
      case USBBaudRate:
      case USBCommand:
      case USBDataBits:
      case USBParity:
      case USBStopBits:
      case UseWifi:
      case UnknownKey:
      case VehicleBboxMinConfidence:
      case VehicleMotionFilterAngleTolerance:
      case VehicleMotionFilterAngles:
      case VehicleMotionFilterMinMagnitude:
      case VideoBitrate:
      case VideoCaptureDuration:
      case VideoCaptureFPS:
      case VideoPlaybackFPS:
      case VideoReso:
      case VideoStillReso:
      case VideoThumbnailReso:
      case WifiIsHidden:
      case WifiPSK:
      case WifiSSID:
        return false;
    }

    return true;
  }

  /**
   * Performs any actions immediately required after updating the given settings.
   *
   * @param updated A mapping from the keys of changed settings to their new values (which may be
   *                null to reset to the default value).
   */
  private void performUpdateActions(final Map<ConfigKeys, String> updated) {
    boolean downloadsInitiated = false;
    boolean wifiUpdated = false;
    boolean radarStartedOrStopped = false;

    for (Map.Entry<ConfigKeys, String> entry : updated.entrySet()) {
      final ConfigKeys key = entry.getKey();
      @Nullable final String value = entry.getValue();

      switch (key) {
        case AwsAppName: // Fall through.
        case AwsHealthMonitorName: // Fall through.
        case AwsImageMaskName: // Fall through.
        case AwsToolsName: // Fall through.
        case AwsUpdaterName: // Fall through.
        case AwsUpgradeName:
          // Trigger the download of updates. Only do this once per config changeset.
          if (!downloadsInitiated && notEmpty(value)) {
            Log.d(TAG, "Initiating download of updates");
            AwsClient.downloadUpdates(mContext, false);
            downloadsInitiated = true;
          }
          break;

        case AwsUserName:
          MqttClient.getInstance(mContext).reconnect();
          break;

        case EnableRadar: // Fall through.
        case USBBaudRate: // Fall through.
        case USBCommand: // Fall through.
        case USBDataBits: // Fall through.
        case USBParity: // Fall through.
        case USBStopBits:
          // (Re)start or stop the radar service once if any of these settings have changed.
          if (!radarStartedOrStopped) {
            radarStartedOrStopped = true;

            // Add commands to be sent to the radar if the command string has changed from the
            // previously applied string.
            final SharedPreferences state =
                mContext.getSharedPreferences(STATE_PREFS_NAME, Context.MODE_PRIVATE);
            String cmd = getString(ConfigKeys.USBCommand).trim();
            if (cmd.equals(state.getString(ConfigKeys.USBCommand.name(), ""))) {
              cmd = null;
            }

            boolean forceRestart = (updated.containsKey(ConfigKeys.USBBaudRate)
                || updated.containsKey(ConfigKeys.USBDataBits)
                || updated.containsKey(ConfigKeys.USBParity)
                || updated.containsKey(ConfigKeys.USBStopBits));
            RadarService.updateServiceState(mContext, forceRestart, cmd);

            if (cmd != null && !cmd.isEmpty()) {
              // Assume the commands have been applied if the service is running after a short
              // delay. Keep track of this command string in the local state.
              final String s = cmd;
              new Handler().postDelayed(() -> {
                if (RadarService.isRunning()) {
                  state.edit().putString(ConfigKeys.USBCommand.name(), s).apply();
                } else {
                  Log.w(TAG, "Warning: The radar service is not running, USB commands were"
                      + " probably not applied");
                }
              }, 2000);
            }
          }
          break;

        case BleDeviceAddresses:
          if (notEmpty(value)) {
            // The receiver will validate the device addresses.
            mContext.sendBroadcast(new Intent(mContext, BeaconScannerReceiver.class));
          }
          break;
        case BleOn:
          if (getBool(key)) Radio.turnOnBluetooth(mContext);
          else Radio.turnOffBluetooth(mContext);
          break;
        case LocationUpdateInterval:
          LocationUpdateReceiver.scheduleRecurringUpdates(mContext,
              getInt(ConfigKeys.LocationUpdateInterval));
          break;
        case LogLevel:
          Log.setLogLevel(Log.Level.from(getString(key)));
          break;
        case UseWifi:
          if (getBool(key)) Radio.turnOnWifi(mContext);
          else Radio.turnOffWifi(mContext);
          break;
        case ScheduleInterval:
          ScheduleReceiver.updateSchedule(mContext);
          break;
        case ScheduleTimes:
          // Skip this action if ScheduleInterval has also been updated to avoid unnecessarily
          // updating the scheduled alarm twice.
          if (!updated.containsKey(ConfigKeys.ScheduleInterval)) {
            ScheduleReceiver.updateSchedule(mContext);
          }
          break;

        case WifiIsHidden: // Fall through.
        case WifiPSK: // Fall through.
        case WifiSSID:
          if (!wifiUpdated) Radio.addOrUpdateWiFiNetwork(mContext);
          wifiUpdated = true;
          break;
      }
    }
  }

  /**
   * Returns a file name in the format used for AWS (a timestamp).
   *
   * @param timestampMillis The specific timestamp to use, in milliseconds since the epoch.
   * @param fileExtInclDot  The file type extension (e.g. ".log").
   * @return A file name based on the given timestamp.
   */
  public static String getTimestampFileName(long timestampMillis, String fileExtInclDot) {
    // Get the current timestamp and replace + chars that may cause problems in file systems.
    // Note: SimpleDateFormat instances are not safe for use by multiple threads.
    String timestamp = new SimpleDateFormat(AWS_FILE_NAME_FORMAT, Locale.ROOT)
        .format(new Date(timestampMillis));
    timestamp = timestamp.replace("+", "");
    return timestamp + fileExtInclDot;
  }

  /**
   * Returns the local config file if it exists, null otherwise.
   */
  @Nullable
  private File getConfigFile() {
    // Try the OTA folder.
    File confFile = getFileInAppDir(APP_DIR_OTA, CONFIG_FILE_NAME);
    if (!confFile.exists()) {
      // Try the root directory for the app.
      confFile = getFileInAppDir(null, CONFIG_FILE_NAME);
      if (!confFile.exists()) {
        return null;
      }
    }
    return confFile;
  }

  /**
   * Returns the specified file in a subfolder of the app's root directory.
   *
   * @param subfolderName Optional sub-folder name.
   * @param fileName      The name of the file.
   * @return The abstract file object.
   */
  public static File getFileInAppDir(@Nullable String subfolderName, @NonNull String fileName) {
    if (subfolderName != null && !subfolderName.isEmpty()) {
      fileName = subfolderName + "/" + fileName;
    }
    return new File(getAppFolderPath(), fileName);
  }

  /**
   * Returns the absolute app folder path with a trailing /.
   */
  public static String getAppFolderPath() {
    return APP_DIR_PATH;
  }

  @NonNull
  private static List<Integer> primitiveArrayToList(@NonNull int[] a) {
    final List<Integer> l = new ArrayList<>(a.length);
    for (int v : a) {
      l.add(v);
    }
    return l;
  }

  @NonNull
  private static List<Double> primitiveArrayToList(@NonNull double[] a) {
    final List<Double> l = new ArrayList<>(a.length);
    for (double v : a) {
      l.add(v);
    }
    return l;
  }


  /**
   * Exports the {@link ConfigKeys} and their values to a JSON-encoded file.
   */
  private static class ExportTask extends AsyncTask<Void, Void, Void> {
    @SuppressLint("StaticFieldLeak")
    private final Context mContext;
    private final Settings mSettings;
    private final String mFileName;
    private final boolean mRebootWhenDone;

    ExportTask(Context context, Settings settings, String fileName, boolean rebootWhenDone) {
      mContext = context.getApplicationContext();
      mSettings = settings;
      mFileName = fileName;
      mRebootWhenDone = rebootWhenDone;
    }

    @Override
    protected Void doInBackground(Void... voids) {
      final File out = getFileInAppDir(APP_DIR_TMP, mFileName);

      // Populate a JSON object with all non-confidential settings.
      final JSONObject json = new JSONObject();
      for (final ConfigKeys key : ConfigKeys.values()) {
        // Skip confidential entries and UnknownKey.
        if (key == ConfigKeys.AwsKeySecret || key == ConfigKeys.SmsCodeword
            || key == ConfigKeys.SmsUserCodeword || key == ConfigKeys.WifiPSK
            || key == ConfigKeys.UnknownKey) {
          continue;
        }

        // Write the key name and value (the default value if not explicitly set).
        try {
          final String name = key.name();
          if (key.valueType.equals(String.class)) {
            json.put(name, mSettings.getString(key));
          } else if (key.valueType.equals(Long.class)) {
            json.put(name, mSettings.getLong(key));
          } else if (key.valueType.equals(Integer.class)) {
            json.put(name, mSettings.getInt(key));
          } else if (key.valueType.equals(Boolean.class)) {
            json.put(name, mSettings.getBool(key) ? 1 : 0);
          } else if (key.valueType.equals(Double.class)) {
            json.put(name, mSettings.getDouble(key));
          } else if (key.valueType.equals(String[].class)) {
            json.put(name, TextUtils.join(",", mSettings.getStringList(key)));
          } else if (key.valueType.equals(int[].class)) {
            json.put(name, TextUtils.join(",", mSettings.getIntList(key)));
          } else if (key.valueType.equals(double[].class)) {
            json.put(name, TextUtils.join(",", mSettings.getDoubleList(key)));
          } else {
            Log.e(TAG, "Unknown value type for ", name);
          }
        } catch (Exception e) {
          Log.e(TAG, "Export failed: ", e.getMessage());
          return null;
        }
      }

      final String jsonString = json.toString();

      // Write JSON to file. Only one call to write, so no point using a BufferedWriter.
      try (Writer w = new OutputStreamWriter(new FileOutputStream(out), StandardCharsets.UTF_8)) {
        w.write(jsonString);
      } catch (Exception e) {
        Log.w(TAG, "Export failed: ", e.getMessage());
        //noinspection ResultOfMethodCallIgnored
        out.delete();
        return null;
      }

      // Move to the upload folder.
      File dest = getFileInAppDir(APP_DIR_NEW, mFileName);
      if (!out.renameTo(dest)) {
        Log.w(TAG, "Failed to move exported config file to new/", mFileName);
        return null;
      }

      Log.d(TAG, "Export successful");
      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      finish();
    }

    @Override
    protected void onCancelled() {
      finish();
    }

    private void finish() {
      if (!mRebootWhenDone) {
        // Only start transfers when not rebooting, as they should be performed shortly after app
        // start anyway and may not complete if the reboot is triggered here.
        AwsClient.uploadFiles(mContext, false);
      }

      if (mRebootWhenDone) {
        Log.i(TAG, "Rebooting to apply config update");
        Helpers.reboot(mContext, 500);
      }
    }
  } // ExportTask

}
