package io.hubsy.core;

import android.annotation.SuppressLint;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.AsyncTask;

import java.io.File;
import java.util.Arrays;

import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;

public class StorageService extends JobService {
  private static final String TAG = "StorageService";
  private static final String NS_PREFIX = "io.hubsy.core";

  /**
   * The unique ID for jobs of this type.
   */
  public static final int JOB_ID = 9342039;

  /**
   * The minimum amount of free storage on the filesystem in bytes. Type long.
   */
  public static final String EXTRA_MIN_FREE_BYTES = NS_PREFIX + ".extra.min_free_bytes";

  private Worker mWorker;

  @Override
  public boolean onStartJob(JobParameters params) {
    mWorker = new Worker(params);
    mWorker.execute();
    return true;
  }

  @Override
  public boolean onStopJob(JobParameters params) {
    if (mWorker != null) mWorker.cancel(true);
    return true;
  }

  /**
   * Worker deletes the oldest files from the archive folder until the desired amount of storage
   * space is available or no more files can be deleted.
   */
  @SuppressLint("StaticFieldLeak") // Will be cancelled if job gets cancelled.
  private class Worker extends AsyncTask<Void, Void, Void> {
    private final JobParameters mParams;

    private final long mMinFreeBytes; // The targeted minimum amount of free storage.
    private long mFreeBytes; // Free storage in bytes.
    private long mBytesToFree; // Total number of bytes needing to be freed.
    private long mBytesFreed; // Total number of bytes freed.
    private long mFilesDeleted; // Number of files deleted.

    private Worker(JobParameters params) {
      mParams = params;
      mMinFreeBytes = mParams.getExtras().getLong(EXTRA_MIN_FREE_BYTES);
    }

    @Override
    protected Void doInBackground(Void... args) {
      mFreeBytes = Helpers.getFreeMemoryInBytes();
      if (mFreeBytes >= mMinFreeBytes) {
        return null;
      }
      mBytesToFree = mMinFreeBytes - mFreeBytes;

      // Delete archived files first.
      deleteFromDir(new File(Settings.getAppFolderPath() + Settings.APP_DIR_ARCHIVED));

      // Continue by deleting failed uploads if not enough space has been recovered yet.
      if (mFreeBytes < mMinFreeBytes) {
        deleteFromDir(new File(Settings.getAppFolderPath() + Settings.APP_DIR_FAILED));
      }

      if (mBytesFreed < mBytesToFree) {
        Log.w(TAG, "Failed to free ", (mBytesToFree - mBytesFreed), " bytes, only ",
            mBytesFreed, " bytes were freed by deleting ", mFilesDeleted, " files");
      } else {
        Log.d(TAG, "Deleted ", mFilesDeleted, " files to free ", mBytesFreed, " bytes");
      }

      return null;
    }

    /**
     * Deletes files from a directory until {@link #mMinFreeBytes} has been satisfied or there are
     * no more files to delete.
     *
     * @param path The directory path.
     */
    private void deleteFromDir(final File path) {
      File[] files = path.listFiles();
      if (files == null) {
        return;
      }

      // Sort them by ascending last modified time. Cache the time first to avoid repeated file
      // attribute reads during sort.
      class IndexTime {
        private int idx;
        private long time;
      }
      final IndexTime[] modTimes = new IndexTime[files.length];
      for (int i = 0; i < files.length; i++) {
        modTimes[i] = new IndexTime();
        modTimes[i].idx = i;
        modTimes[i].time = files[i].lastModified();
      }
      Arrays.sort(modTimes, (lhs, rhs) -> {
        if (lhs.time < rhs.time) return -1;
        else if (lhs.time > rhs.time) return 1;
        return 0;
      });

      // Delete the oldest files until enough memory has been freed.
      for (int i = 0; i < files.length && mFreeBytes < mMinFreeBytes && !isCancelled(); ++i) {
        final File file = files[modTimes[i].idx];
        if (!file.isFile()) continue;

        final long size = file.length();
        if (file.delete()) {
          mBytesFreed += size;
          mFreeBytes += size;
          mFilesDeleted++;
        }
      }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      StorageService.this.jobFinished(mParams, false);
    }
  } // class Worker
}
