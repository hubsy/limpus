package io.hubsy.core;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Pair;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;
import io.hubsy.core.util.MD5;

/**
 * A singleton which registers listeners for OTA file downloaded events the first time
 * {@link #registerOnce} is called.
 */
public class UpdateInstaller {
  private static final String TAG = "UpdateInstaller";

  private static volatile UpdateInstaller sSingleton;

  private final Settings mSettings;

  public static void registerOnce(@NonNull Context context) {
    if (sSingleton == null) {
      synchronized (UpdateInstaller.class) {
        if (sSingleton == null) {
          sSingleton = new UpdateInstaller(context);
        }
      }
    }
  }

  private UpdateInstaller(final Context context) {
    mSettings = Settings.getInstance(context);

    // Register broadcast receivers listening for supported events.
    LocalBroadcastManager bm = LocalBroadcastManager.getInstance(context);

    // Config updates.
    bm.registerReceiver(new NewConfigFile(), new IntentFilter(Constants.ACTION_NEW_CONFIG_FILE));

    // App updates.
    registerTGZipExtractor(bm, Constants.ACTION_NEW_APP_UPDATE,
        Constants.ACTION_NEW_APP_UPDATE_HASH, ConfigKeys.AwsAppName);
    // Health monitor update.
    registerTGZipExtractor(bm, Constants.ACTION_NEW_HMON_UPDATE,
        Constants.ACTION_NEW_HMON_UPDATE_HASH, ConfigKeys.AwsHealthMonitorName);
    // System tools update.
    registerTGZipExtractor(bm, Constants.ACTION_NEW_TOOLS_UPDATE,
        Constants.ACTION_NEW_TOOLS_UPDATE_HASH, ConfigKeys.AwsToolsName);
    // Updater executable update.
    registerTGZipExtractor(bm, Constants.ACTION_NEW_UPDATER_UPDATE,
        Constants.ACTION_NEW_UPDATER_UPDATE_HASH, ConfigKeys.AwsUpdaterName);

    // Stand-alone upgrade script.
    IntentFilter upgradeScriptFilter = new IntentFilter(Constants.ACTION_NEW_UPGRADE_SCRIPT);
    upgradeScriptFilter.addAction(Constants.ACTION_NEW_UPGRADE_SCRIPT_HASH);
    FileUpdateReceiver upgradeScriptReceiver = new FileUpdateReceiver(
        Constants.ACTION_NEW_UPGRADE_SCRIPT, Constants.ACTION_NEW_UPGRADE_SCRIPT_HASH,
        ConfigKeys.AwsUpgradeName, Settings.APP_DIR_OTA, "upgrade.sh", false, true);
    bm.registerReceiver(upgradeScriptReceiver, upgradeScriptFilter);

    // Image mask updates.
    IntentFilter imageMaskFilter = new IntentFilter(Constants.ACTION_NEW_IMAGE_MASK);
    imageMaskFilter.addAction(Constants.ACTION_NEW_IMAGE_MASK_HASH);
    FileUpdateReceiver imageMaskReceiver = new FileUpdateReceiver(Constants.ACTION_NEW_IMAGE_MASK,
        Constants.ACTION_NEW_IMAGE_MASK_HASH, ConfigKeys.AwsImageMaskName, Settings.APP_DIR_DATA,
        "image-mask.png", false, false);
    bm.registerReceiver(imageMaskReceiver, imageMaskFilter);
  }

  /**
   * Registers a {@link ExternalUpdateReceiver} to extract a .tgz archive in the
   * {@link Settings#APP_DIR_OTA} folder.
   */
  private void registerTGZipExtractor(LocalBroadcastManager bm, String updateFileAction,
                                      String hashFileAction, ConfigKeys updateKey) {
    IntentFilter filter = new IntentFilter(updateFileAction);
    filter.addAction(hashFileAction);
    ExternalUpdateReceiver receiver = new ExternalUpdateReceiver(
        updateFileAction, hashFileAction, updateKey,
        new String[]{"tar", "-C", Settings.getAppFolderPath() + Settings.APP_DIR_OTA,
            "--no-same-permissions", "--no-same-owner", "-xzf"},
        ".tgz", true);
    bm.registerReceiver(receiver, filter);
  }

  /**
   * New config file downloaded event handler.
   */
  private class NewConfigFile extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      Log.d(TAG, "A new config file has been downloaded");
      mSettings.readConfigFile(true);
    }
  }

  /**
   * Handles file downloaded events for the installation of a new file version for a
   * {@link ConfigKeys} key, following the successful verification of its checksum against the
   * corresponding hash file.
   */
  private class FileUpdateReceiver extends BroadcastReceiver {
    private final String TAG = "FileUpdateReceiver";

    // NOTE: None of the member variables of this class are allowed to change once constructed.
    // Instances are shared by event invocations.
    private final String mUpdateFileAction;
    private final String mHashFileAction;
    private final ConfigKeys mUpdateKey;
    private final String mInstallDir;
    private final String mInstallName;
    private final boolean mAppendVersion;
    private final boolean mRebootOnSuccess;

    /**
     * @param updateFileAction The intent action identifying the update file.
     * @param hashFileAction   The intent action identifying the corresponding hash file.
     * @param updateKey        The identification key for the file to update.
     * @param installDir       The installation directory, relative to the app's root data dir.
     * @param installName      The installation file name.
     * @param appendVersion    Whether the version string should be appended to installName.
     * @param rebootOnSuccess  Whether to restart the device after successful installation.
     */
    public FileUpdateReceiver(String updateFileAction, String hashFileAction, ConfigKeys updateKey,
                              String installDir, String installName, boolean appendVersion,
                              boolean rebootOnSuccess) {
      mUpdateFileAction = updateFileAction;
      mHashFileAction = hashFileAction;
      mUpdateKey = updateKey;
      mInstallDir = installDir;
      mInstallName = installName;
      mAppendVersion = appendVersion;
      mRebootOnSuccess = rebootOnSuccess;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
      final String awsKey = intent.getStringExtra(Constants.EXTRA_AWS_KEY);
      final String path = intent.getStringExtra(Constants.EXTRA_FILE_PATH);
      if (awsKey == null || path == null) {
        Log.e(TAG, "Error missing intent extra ",
            (awsKey == null ? Constants.EXTRA_AWS_KEY : Constants.EXTRA_FILE_PATH));
        return;
      }

      // Get the file objects for both the update and hash files if available.
      Pair<File, File> files = getUpdateFiles(intent, mUpdateFileAction, mHashFileAction, path);
      if (files == null) return; // Still waiting for a file.

      // Check the integrity.
      if (MD5.checkMD5(files.first, files.second)) {
        boolean success =
            install(mUpdateKey, awsKey, files.first, mInstallDir, mInstallName, mAppendVersion);
        Helpers.deleteFile(files.second); // Delete the hash file.
        if (success && mRebootOnSuccess) {
          Log.i(TAG, "Rebooting to complete the update of ", mUpdateKey,
              " to v", mSettings.getVersionFromAwsKey(awsKey));
          Helpers.reboot(context, 1000);
        }
      } else {
        Helpers.deleteFile(files.first);
        Helpers.deleteFile(files.second);
        Log.w(TAG, "The update file is not valid, aborting the installation of ", mUpdateKey,
            " v", mSettings.getVersionFromAwsKey(awsKey));
      }

    }
  } // FileUpdateReceiver

  /**
   * Handles file downloaded events and passes the file to a system command following the successful
   * verification of its checksum against the corresponding hash file.
   */
  private class ExternalUpdateReceiver extends BroadcastReceiver {
    private final String TAG = "ExternalUpdateReceiver";

    // NOTE: Except for mInProgress, none of the member variables of this class are allowed to
    // change once constructed. Instances are shared by event invocations.
    private final String mUpdateFileAction;
    private final String mHashFileAction;
    private final ConfigKeys mUpdateKey;
    private final String[] mCmdAndArgs;
    private final String mUpdateFileSuffix;
    private final boolean mRebootOnSuccess;

    // Records the path of file currently being processed by the background task.
    private final HashSet<String> mInProgress = new HashSet<>();

    /**
     * @param updateFileAction The intent action identifying the update file.
     * @param hashFileAction   The intent action identifying the corresponding hash file.
     * @param updateKey        The identification key for the file to update.
     * @param cmdAndArgs       The command and its arguments. The file path of update file will be
     *                         appended as the final argument when the command is executed for it.
     * @param updateFileSuffix The expected file suffix (e.g. extension) for the update file. If
     *                         non-null, the update file name must end with this suffix.
     * @param rebootOnSuccess  Whether to restart the device after a successful command execution.
     */
    public ExternalUpdateReceiver(String updateFileAction, String hashFileAction,
                                  ConfigKeys updateKey, String[] cmdAndArgs,
                                  @Nullable String updateFileSuffix, boolean rebootOnSuccess) {
      mUpdateFileAction = updateFileAction;
      mHashFileAction = hashFileAction;
      mUpdateKey = updateKey;
      mCmdAndArgs = cmdAndArgs;
      mUpdateFileSuffix = updateFileSuffix;
      mRebootOnSuccess = rebootOnSuccess;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
      final String awsKey = intent.getStringExtra(Constants.EXTRA_AWS_KEY);
      final String path = intent.getStringExtra(Constants.EXTRA_FILE_PATH);
      if (awsKey == null || path == null) {
        Log.e(TAG, "Error missing intent extra ",
            (awsKey == null ? Constants.EXTRA_AWS_KEY : Constants.EXTRA_FILE_PATH));
        return;
      }

      // Get the file objects for both the update and hash files if available.
      Pair<File, File> files = getUpdateFiles(intent, mUpdateFileAction, mHashFileAction, path);
      if (files == null) return; // Still waiting for a file.

      if (mUpdateFileSuffix != null && !files.first.getName().endsWith(mUpdateFileSuffix)) {
        Log.w(TAG, "Only processing updates with suffix \"", mUpdateFileSuffix, "\"");
        return;
      }

      final String updatePath = files.first.getAbsolutePath();
      if (mInProgress.contains(updatePath)) {
        // This is the event for the second file (they may be received in any order), but both files
        // were already available and have been processed when the event for the first file was
        // received.
        return;
      }

      // Check that this is still the latest configured version. Compare the version strings, not
      // the AWS keys, as this method may be called in response to the hash file download event.
      final String newVersion = mSettings.getVersionFromAwsKey(awsKey);
      final String latestVersion = mSettings.getVersionFromAwsKey(mSettings.getString(mUpdateKey));
      if (newVersion.isEmpty() || !newVersion.equals(latestVersion)) {
        Log.i(TAG, "The update file is outdated, aborting the installation of ", mUpdateKey,
            " v", newVersion, "; the latest version is v", latestVersion);
        Helpers.deleteFile(files.first);
        Helpers.deleteFile(files.second);
        return;
      }

      // Check the integrity.
      if (MD5.checkMD5(files.first, files.second)) {
        // Asynchronously execute the specified system command.
        mInProgress.add(updatePath);
        new Worker(context, files.first, newVersion)
            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        Helpers.deleteFile(files.second); // Delete the hash file.
      } else {
        Helpers.deleteFile(files.first);
        Helpers.deleteFile(files.second);
        Log.w(TAG, "The update file is not valid, aborting the installation of ", mUpdateKey,
            " v", newVersion);
      }
    }

    /**
     * Runs the command in mCmdAndArgs with the updatePath appended in a background thread.
     */
    @SuppressLint("StaticFieldLeak")
    private class Worker extends AsyncTask<Void, Void, Void> {
      private final Context mContext;
      private final File mUpdateFile;
      private final String mUpdatePath;
      private final String mNewVersion;

      private boolean mSuccess = false;

      public Worker(Context context, File updateFile, String newVersion) {
        mContext = context.getApplicationContext();
        mUpdateFile = updateFile;
        mUpdatePath = updateFile.getAbsolutePath();
        mNewVersion = newVersion;
      }

      @Override
      protected Void doInBackground(Void... voids) {
        // Append the update path to the end of the command array.
        ArrayList<String> cmd = new ArrayList<>(mCmdAndArgs.length + 1);
        cmd.addAll(Arrays.asList(mCmdAndArgs));
        cmd.add(mUpdatePath);

        // Start the system process.
        Log.d(TAG, "Executing: ", cmd.toString());
        final Process process = Helpers.executeCommandAsync(cmd.toArray(new String[0]));
        int exitval = 1;
        if (process == null) {
          Log.w(TAG, "Failed to create the system process");
          return null;
        }

        // Wait for the process to finish.
        try {
          exitval = process.waitFor();
        } catch (InterruptedException e) {
          Log.w(TAG, "Interrupted while waiting for process: ", e.getMessage());
        }

        if (exitval == 0) {
          mSuccess = true;
        } else {
          Log.w(TAG, "The system cmd failed, the process exited with error code ", exitval);
        }

        return null;
      }

      @Override
      protected void onPostExecute(Void aVoid) {
        postProcess();
      }

      @Override
      protected void onCancelled() {
        postProcess();
      }

      private void postProcess() {
        Helpers.deleteFile(mUpdateFile);
        mInProgress.remove(mUpdatePath);

        if (mSuccess && mRebootOnSuccess) {
          Log.i(TAG, "Rebooting to update ", mUpdateKey, " to v", mNewVersion);
          Helpers.reboot(mContext, 1000);
        }
      }
    } // Worker

  } // ExternalUpdateReceiver

  /**
   * Checks if both the update and hash files have been downloaded. The intent must match one of the
   * provided action strings.
   *
   * @param intent           The event intent.
   * @param updateFileAction The update file intent action.
   * @param hashFileAction   The hash file intent action.
   * @param path             The path of the downloaded file for which the event was generated.
   * @return A pair with the update file as the first entry and the hash file as the second entry if
   * and only if both files exist, null otherwise.
   */
  private Pair<File, File> getUpdateFiles(Intent intent, String updateFileAction,
                                          String hashFileAction, String path) {
    // Get the file objects for both the update and hash files if available.
    Pair<File, File> files;
    final String action = intent.getAction();
    if (updateFileAction.equals(action)) {
      files = getUpdateFiles(path, null);
    } else if (hashFileAction.equals(action)) {
      files = getUpdateFiles(null, path);
    } else {
      Log.e(TAG, "Invalid action: ", action);
      files = null;
    }

    return files;
  }

  /**
   * Checks if both the update and hash files have been downloaded. Only one of the paths must be
   * provided, the other can be inferred.
   *
   * @param updatePath The path to the update file.
   * @param hashPath   The path to the corresponding hash file.
   * @return A pair with the update file as the first entry and the hash file as the second entry if
   * and only if both files exist, null otherwise.
   */
  private Pair<File, File> getUpdateFiles(@Nullable String updatePath, @Nullable String hashPath) {
    if (updatePath == null && hashPath == null) return null;

    // Expect the hash file to follow the naming convention: <updatePath>.md5
    if (updatePath != null && hashPath == null) {
      hashPath = updatePath + ".md5";
    } else if (updatePath == null) {
      // Strip the .md5 extension.
      if (!hashPath.endsWith(".md5")) {
        Log.w(TAG, "The hash file name does not end in .md5: ", hashPath);
        return null;
      }
      updatePath = hashPath.substring(0, hashPath.length() - 4);
    }

    // Check if both files exist.
    File updateFile = new File(updatePath);
    File hashFile = new File(hashPath);
    boolean haveUpdate = updateFile.exists();
    boolean haveHash = hashFile.exists();
    if (!haveUpdate && !haveHash) {
      return null; // The update may have already been installed when the first event was received.
    } else if (!haveUpdate) {
      Log.d(TAG, "Waiting for the update file (have hash) for ", updateFile.getName());
      return null;
    } else if (!haveHash) {
      Log.d(TAG, "Waiting for the hash file (have update) for ", updateFile.getName());
      return null;
    }

    Log.d(TAG, "Found the update and hash files for ", updateFile.getName());
    return new Pair<>(updateFile, hashFile);
  }

  /**
   * Installs an update for key.
   *
   * @param key           The identifying the type of file to update.
   * @param awsKey        The AWS key identifying the source of the file.
   * @param installFile   The file to install.
   * @param installDir    The installation directory, relative to the app's root data dir.
   * @param installName   The installation file name.
   * @param appendVersion Whether the version string should be appended to installName.
   * @return True on success, false if the update was not installed.
   */
  private boolean install(final ConfigKeys key, final String awsKey, final File installFile,
                          final String installDir, final String installName,
                          final boolean appendVersion) {
    // Check that this is still the latest configured version. Compare the version strings, not
    // the AWS keys, as this method may be called in response to the hash file download event.
    final String oldVersion = mSettings.getVersion(key);
    final String newVersion = mSettings.getVersionFromAwsKey(awsKey);
    final String latestVersion = mSettings.getVersionFromAwsKey(mSettings.getString(key));
    if (newVersion.isEmpty() || !newVersion.equals(latestVersion)) {
      Log.i(TAG, "The update file is outdated, aborting the installation of ", key,
          " v", newVersion, "; the latest version is v", latestVersion);
      Helpers.deleteFile(installFile);
      return false;
    }

    Log.d(TAG, "Installing ", key, " v", newVersion);

    // Move the file to the install dir.
    final String newFileName = appendVersion ? installName + ".v" + newVersion : installName;
    final File newFile = Helpers.moveFile(installFile, installDir, newFileName);
    if (newFile == null) {
      Log.w(TAG, "Failed to install ", installFile.getPath(), " as ", installDir, "/", newFileName);
      return false;
    }
    final File oldFile = oldVersion.isEmpty() ? null
        : appendVersion
        ? Settings.getFileInAppDir(installDir, installName + ".v" + oldVersion)
        : Settings.getFileInAppDir(installDir, installName);

    // Update the version database.
    SharedPreferences.Editor editor = mSettings.getVersions().edit();
    editor.putString(key.name(), newVersion);
    if (!editor.commit()) {
      Log.w(TAG, "Failed to commit changes to the versions database");
      // Delete the new version if it didn't overwrite the old file.
      if (oldFile == null || !oldFile.equals(newFile)) {
        Helpers.deleteFile(newFile);
      }
      return false;
    }

    // Delete the old version if it was not overwritten by the new version.
    if (oldFile != null && !oldFile.equals(newFile)) {
      Helpers.deleteFile(oldFile);
    }

    Log.i(TAG, "Successfully installed ", key, " v", newVersion);
    return true;
  }
}
