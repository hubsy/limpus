package io.hubsy.core.beaconscanner;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.List;

import io.hubsy.core.Settings;
import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.util.HealthCheck;
import io.hubsy.core.util.Log;
import io.hubsy.core.util.Radio;

/**
 * Starts the scanner service when it is triggered.
 */
public class BeaconScannerReceiver extends BroadcastReceiver {
  private static final String TAG = "BeaconScannerReceiver";

  // Service is disabled until the first explicit intent is received to prevent Bluetooth changes
  // starting the scanner before the app is initialised.
  private static boolean sEnableService = false;

  @Override
  public void onReceive(final Context context, Intent intent) {
    final String action = intent.getAction();
    if (action == null) {
      sEnableService = true;
    } else {
      Log.d(TAG, "Triggered by ", action);
      if (!sEnableService) {
        Log.d(TAG, "BLE service is disabled until first explicit intent");
        return;
      }
    }

    // Handle Bluetooth state changes and skip scan if BT is not in an end state.
    if (action != null && action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
      if (!handleBluetoothStateChanged(intent)) return;
    }

    final Context appContext = context.getApplicationContext();
    final Settings settings = Settings.getInstance(context);

    // Check if the service should be running.
    if (!settings.getBool(ConfigKeys.BleOn)) {
      Log.d(TAG, "Bluetooth is disabled in the settings");
      Radio.turnOffBluetooth(context);
      return;
    }

    // Load the service config.
    final long scanDuration = settings.getLong(ConfigKeys.BleDuration);
    final long scanDelay = settings.getLong(ConfigKeys.BleDelay);
    final long scanFlex = settings.getLong(ConfigKeys.BleFlex);
    final int scanMode = settings.getInt(ConfigKeys.BleScanMode);
    final List<String> deviceAddresses = settings.getStringList(ConfigKeys.BleDeviceAddresses);
    final List<String> serviceUUIDs = settings.getStringList(ConfigKeys.BleServiceUUIDs);

    // Check if we have at least one valid device filter, stop scanning otherwise.
    boolean haveDeviceFilter = false;
    for (String address : deviceAddresses) {
      if (BluetoothAdapter.checkBluetoothAddress(address)) {
        haveDeviceFilter = true;
        break;
      }
    }
    if (!haveDeviceFilter) {
      Log.d(TAG, "No valid device address filters configured, disabling the scanner");
      return;
    }

    // Start the service.
    final Intent scanner = new Intent(context, BeaconScannerService.class);
    scanner.putExtra(BeaconScannerService.EXTRA_SCAN_DURATION, scanDuration);
    scanner.putExtra(BeaconScannerService.EXTRA_DELAY_MILLIS, scanDelay);
    scanner.putExtra(BeaconScannerService.EXTRA_FLEX_MILLIS, scanFlex);
    scanner.putExtra(BeaconScannerService.EXTRA_SCAN_MODE, scanMode);
    if (deviceAddresses.size() > 0) {
      scanner.putExtra(BeaconScannerService.EXTRA_DEVICE_ADDRESSES,
          deviceAddresses.toArray(new String[0]));
    }
    if (serviceUUIDs.size() > 0) {
      scanner.putExtra(BeaconScannerService.EXTRA_SERVICE_UUIDS,
          serviceUUIDs.toArray(new String[0]));
    }
    BeaconScannerService.acquireWakelock(appContext, 3 * scanDuration);
    context.startService(scanner);
  }

  /**
   * Handles {@link BluetoothAdapter#ACTION_STATE_CHANGED}.
   *
   * @return True if in one of the end states: either {@link BluetoothAdapter#STATE_ON} or
   * {@link BluetoothAdapter#STATE_OFF}.
   */
  private boolean handleBluetoothStateChanged(Intent intent) {
    final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF);
    Log.d(TAG, "New bluetooth adapter state: ", state);
    switch (state) {
      case BluetoothAdapter.STATE_OFF:
        return true; // Let the service try to enable it if other conditions for BLE scans are met.
      case BluetoothAdapter.STATE_ON:
        HealthCheck.getInstance().updateTimestamp(HealthCheck.Timestamp.BluetoothOn);
        return true;
      default:
        return false; // Wait for one of the end states.
    }
  }
}
