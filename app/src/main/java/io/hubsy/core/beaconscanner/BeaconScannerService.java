package io.hubsy.core.beaconscanner;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.ParcelUuid;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.hubsy.core.Constants;
import io.hubsy.core.Settings;
import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.util.HealthCheck;
import io.hubsy.core.util.Log;
import io.hubsy.core.util.Radio;

public class BeaconScannerService extends Service {
  private static final String TAG = "BeaconScannerService";

  /**
   * Event action used when a Bluetooth LE device has been discovered. Intents include extras
   * {@link #EXTRA_DEVICE_ADDRESS}, {@link #EXTRA_DEVICE_NAME} {@link #EXTRA_DEVICE_RSSI}, and
   * {@link #EXTRA_SERVICE_UUIDS}.
   */
  public static final String ACTION_DEVICE_DISCOVERED =
      Constants.NS_PREFIX + ".action.device_discovered";

  /**
   * Device address. Type String.
   */
  public static final String EXTRA_DEVICE_ADDRESS = Constants.NS_PREFIX + ".extra.device_address";
  /**
   * Device addresses. Type String[].
   */
  public static final String EXTRA_DEVICE_ADDRESSES =
      Constants.NS_PREFIX + ".extra.device_addresses";
  /**
   * Device name. Type String.
   */
  public static final String EXTRA_DEVICE_NAME = Constants.NS_PREFIX + ".extra.device_name";
  /**
   * Device received signal strength indicator (unit: dBm). Type int.
   */
  public static final String EXTRA_DEVICE_RSSI = Constants.NS_PREFIX + ".extra.device_rssi";
  /**
   * Device service UUIDs. Type String[]. Optional.
   */
  public static final String EXTRA_SERVICE_UUIDS = Constants.NS_PREFIX + ".extra.service_uuids";

  /**
   * Job scan duration (unit: ms). Type long.
   */
  public static final String EXTRA_SCAN_DURATION = Constants.NS_PREFIX + ".extra.scan_duration";
  /**
   * Job scheduling delay (unit: ms). Type long.
   */
  public static final String EXTRA_DELAY_MILLIS = Constants.NS_PREFIX + ".extra.delay_millis";
  /**
   * Job allowed extra scheduling delay (unit: ms). Type long.
   */
  public static final String EXTRA_FLEX_MILLIS = Constants.NS_PREFIX + ".extra.flex_millis";
  /**
   * The BT LE scan mode. Type int. One of (in order of increasing power consumption):
   * {@link ScanSettings#SCAN_MODE_LOW_POWER},
   * {@link ScanSettings#SCAN_MODE_BALANCED},
   * {@link ScanSettings#SCAN_MODE_LOW_LATENCY}.
   */
  public static final String EXTRA_SCAN_MODE = Constants.NS_PREFIX + ".extra.scan_mode";

  /**
   * The types of the sensors having caused an {@link #ACTION_DEVICE_DISCOVERED} event. Bitmask of
   * type int.
   */
  public static final String EXTRA_SENSOR_TYPES = Constants.NS_PREFIX + ".extra.sensor_types";
  /**
   * The bit flag for acceleration sensors used by {@link #EXTRA_SENSOR_TYPES}.
   */
  public static final int EXTRA_SENSOR_TYPE_ACCELEROMETER = 0x1;
  /**
   * The bit flag for PIR sensors used by {@link #EXTRA_SENSOR_TYPES}.
   */
  public static final int EXTRA_SENSOR_TYPE_PIR = 0x2;

  // Must be acquired and released through {@link #acquireWakelock} and {@link #releaseWakelock},
  // respectively.
  private static PowerManager.WakeLock sWakeLock;
  private static int sWakeLockRefCounter = 0;

  private BluetoothLeScanner mScanner;
  private ScannerCallback mScanCallback;
  // Runnable that stops the ongoing scan and schedules the next job.
  private Runnable mStopScanAndRescheduleJob;
  private boolean mNextScanIsScheduled;

  // A mapping from hardware address to device object.
  private HashMap<String, BluetoothDevice> mDiscoveredDevices = new HashMap<>();

  // Enable verbose log only every x times service is started.
  private static final int VERBOSE_LOG_INTERVAL = 100;
  private static int sLogCounter = 0; // Number of service starts since last log output.

  // Maps sensors to their activation models and keeps them around across scan intervals.
  private static final HashMap<String, ChangeModel> mActivationModels = new HashMap<>();
  // Contains sensors whose activation model has been updated in this interval.
  private HashSet<String> mModelsUpdated = new HashSet<>();

  /**
   * Acquires a {@link PowerManager#PARTIAL_WAKE_LOCK}.
   * <p>
   * <b>Must</b> be called once before each {@link Context#startService(Intent)}.
   * <p>
   * <b>Must</b> be called from the main thread.
   */
  public static void acquireWakelock(Context context, long timeoutMillis) {
    if (sWakeLock == null) {
      PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
      sWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, Constants.NS_PREFIX + ":" + TAG);
    }
    sWakeLock.acquire(timeoutMillis);
    sWakeLockRefCounter++;
  }

  /**
   * Releases the wakelock.
   * <p>
   * <b>Must</b> be called exactly once per {@link Context#startService(Intent)}.
   * <p>
   * <b>Must</b> be called from the main thread.
   */
  private static void releaseWakelock() {
    if (sWakeLock == null) {
      Log.e(TAG, "Cannot release null WakeLock");
      return;
    } else if (!sWakeLock.isHeld()) {
      Log.e(TAG, "Cannot release unacquired WakeLock");
      return;
    }
    sWakeLock.release();
    sWakeLockRefCounter--;
    if (sWakeLockRefCounter != 0 && !sWakeLock.isHeld()) {
      Log.e(TAG, "Ref counter not in sync with WakeLock");
      sWakeLockRefCounter = 0;
    }
  }

  /**
   * Schedules the next scan.
   *
   * @param context     The context to use for scheduling.
   * @param delayMillis The minimum time to wait before the next scan job.
   * @param flexMillis  A flex period after the delay is up to allow for better scheduling.
   */
  private static void scheduleNextScan(Context context, long delayMillis, long flexMillis) {
    if (flexMillis >= delayMillis) flexMillis = delayMillis / 2;

    context = context.getApplicationContext();
    Intent scanIntent = new Intent(context, BeaconScannerReceiver.class);
    PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, scanIntent,
        PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT);
    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

    // NOTE: The system sets the delay to no less than 5000ms, no matter whether setWindow or
    // setExactAndAllowWhileIdle are used.
    alarmManager.setWindow(AlarmManager.ELAPSED_REALTIME_WAKEUP,
        SystemClock.elapsedRealtime() + delayMillis, flexMillis, alarmIntent);
  }

  @Override
  public void onDestroy() {
    // The service should automatically get re-created by the system.
    if (mStopScanAndRescheduleJob != null) {
      Log.d(TAG, "Service destroyed while a scan was in progress");
      mStopScanAndRescheduleJob.run();
    }
  }

  @Override
  public int onStartCommand(final Intent intent, int flags, final int startId) {
    // Enable/disable non-critical logging for this service invocation.
    final boolean verboseLog = sLogCounter % VERBOSE_LOG_INTERVAL == 0;
    sLogCounter++;
    if (verboseLog) Log.d(TAG, "Invocation #", sLogCounter);

    final long scanDuration = intent.getLongExtra(EXTRA_SCAN_DURATION, 5000);

    if (sWakeLock == null || !sWakeLock.isHeld()) {
      Log.w(TAG, "Not holding the WakeLock, acquiring it");
      acquireWakelock(this, 3 * scanDuration);
    }
    if (mStopScanAndRescheduleJob != null) {
      Log.d(TAG, "Previous job still running, stop and reschedule");
      mStopScanAndRescheduleJob.run();
      stopSelf(startId);
      releaseWakelock();
      return START_NOT_STICKY;
    }

    // Prepare for BLE scan.
    final BluetoothManager btManager =
        (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
    final BluetoothAdapter btAdapter = btManager.getAdapter();
    if (btAdapter == null || !btAdapter.isEnabled()) {
      Log.d(TAG, "Bluetooth is disabled, turning it on...");
      Radio.turnOnBluetooth(this);
      stopSelf(startId);
      releaseWakelock();
      return START_NOT_STICKY;
    }
    mScanner = btAdapter.getBluetoothLeScanner();

    // Scan filters.
    final List<ScanFilter> filters = new ArrayList<>();
    final String[] filterAddrs = intent.getStringArrayExtra(EXTRA_DEVICE_ADDRESSES);
    if (filterAddrs != null) {
      for (String addr : filterAddrs) {
        if (!BluetoothAdapter.checkBluetoothAddress(addr)) {
          if (verboseLog) Log.w(TAG, "Invalid device address format (skipping): ", addr);
          continue;
        }
        filters.add(new ScanFilter.Builder().setDeviceAddress(addr).build());
      }
    }
    final String filterAddr = intent.getStringExtra(EXTRA_DEVICE_ADDRESS);
    if (filterAddr != null) {
      if (BluetoothAdapter.checkBluetoothAddress(filterAddr)) {
        filters.add(new ScanFilter.Builder().setDeviceAddress(filterAddr).build());
      } else if (verboseLog) {
        Log.w(TAG, "Invalid device address format (skipping): ", filterAddr);
      }
    }
    final String[] filterUUIDs = intent.getStringArrayExtra(EXTRA_SERVICE_UUIDS);
    if (filterUUIDs != null) {
      for (String uuid : filterUUIDs) {
        filters.add(new ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString(uuid)).build());
      }
    }

    // Scan settings.
    // NOTE, within a 5 second scan period:
    // - SCAN_MODE_LOW_POWER often misses some or all beacons, rarely detects the light sensor
    // - SCAN_MODE_BALANCED usually detects accelerometers, sometimes light sensor
    // - SCAN_MODE_LOW_LATENCY usually detects accelerometers, frequently light sensor
    int scanMode = intent.getIntExtra(EXTRA_SCAN_MODE, ScanSettings.SCAN_MODE_BALANCED);
    if (scanMode != ScanSettings.SCAN_MODE_LOW_POWER
        && scanMode != ScanSettings.SCAN_MODE_BALANCED
        && scanMode != ScanSettings.SCAN_MODE_LOW_LATENCY) {
      if (verboseLog) Log.w(TAG, "Invalid scan mode, setting to low latency");
      scanMode = ScanSettings.SCAN_MODE_LOW_LATENCY;
    }
    ScanSettings.Builder settingsBuilder = new ScanSettings.Builder()
        .setScanMode(scanMode)
        .setReportDelay(0);
    settingsBuilder = settingsBuilder
        .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
        .setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE)
        .setNumOfMatches(ScanSettings.MATCH_NUM_MAX_ADVERTISEMENT);
    final ScanSettings settings = settingsBuilder.build();

    if (verboseLog) Log.d(TAG, "Starting device scan with ", filters.size(), " filters");
    mScanCallback = new ScannerCallback(verboseLog);
    try {
      mScanner.startScan(filters, settings, mScanCallback);
      HealthCheck.getInstance(this).updateTimestamp(HealthCheck.Timestamp.BleScan);
    } catch (IllegalStateException e) {
      Log.w(TAG, "Scan aborted due to Bluetooth state change: ", e.getMessage());
      mScanner = null;
      // Continue so another scan gets scheduled.
    }

    // Stop scanning after the scanning period.
    final Handler mainHandler = new Handler(Looper.getMainLooper());
    mStopScanAndRescheduleJob = () -> {
      // Remove this runnable from the message queue to prevent it from
      // running twice if it was triggered early.
      mainHandler.removeCallbacks(mStopScanAndRescheduleJob);
      mStopScanAndRescheduleJob = null;

      // Stop scanning.
      if (mScanner != null && mScanCallback != null) {
        try {
          mScanner.stopScan(mScanCallback);
        } catch (IllegalStateException e) {
          Log.w(TAG, "Cannot stop scan, Bluetooth adapter no longer enabled: ", e.getMessage());
        }
        mScanner = null;
        mScanCallback = null;
      }
      if (verboseLog) {
        Log.d(TAG, "Scan finished, discovered ", mDiscoveredDevices.size(), " devices");
      }
      mDiscoveredDevices.clear();

      // Send a heartbeat to activation models not otherwise updated this interval.
      for (Map.Entry<String, ChangeModel> entry : mActivationModels.entrySet()) {
        if (!mModelsUpdated.contains(entry.getKey())) {
          entry.getValue().heartbeatUpdate();
        }
      }
      mModelsUpdated.clear();

      // If the next scan interval hasn't been scheduled yet, do it now.
      if (!mNextScanIsScheduled) {
        BeaconScannerService.scheduleNextScan(BeaconScannerService.this,
            intent.getLongExtra(EXTRA_DELAY_MILLIS, 3000),
            intent.getLongExtra(EXTRA_FLEX_MILLIS, 200));
      }

      stopSelf(startId);
      releaseWakelock();
    };
    mainHandler.postDelayed(mStopScanAndRescheduleJob, scanDuration);

    // Schedule the next scan interval straight away so the scan time of this interval is part of
    // the minimum 5000ms delay imposed by the system.
    BeaconScannerService.scheduleNextScan(this,
        scanDuration + intent.getLongExtra(EXTRA_DELAY_MILLIS, 3000),
        intent.getLongExtra(EXTRA_FLEX_MILLIS, 200));
    mNextScanIsScheduled = true;

    return START_REDELIVER_INTENT;
  }

  @Override
  public IBinder onBind(Intent intent) {
    return null; // Disable binding to this service.
  }

  private class ScannerCallback extends ScanCallback {
    private final boolean verboseLog;

    // The current settings for ChangeModel models.
    @Nullable
    private Settings mSettings;
    private int mModelCapacity;
    private int mModelMinThreshold;
    private double mModelSigmaFactor;

    /**
     * @param verbose Enable/disable non-critical log output.
     */
    private ScannerCallback(boolean verbose) {
      verboseLog = verbose;
    }

    /**
     * Loads the latest {@link Settings} used by this class.
     */
    private void loadSettings() {
      // Load the latest analysis config.
      mSettings = Settings.getInstance(BeaconScannerService.this);
      mModelCapacity = mSettings.getInt(ConfigKeys.SensorModelCapacity);
      mModelMinThreshold = mSettings.getInt(ConfigKeys.SensorModelMinThreshold);
      mModelSigmaFactor = mSettings.getDouble(ConfigKeys.SensorModelSigmaFactor);
    }

    @Override
    public void onScanResult(int callbackType, ScanResult result) {
      // NOTE: The callback is always called on the main thread so this implementation is safe.
      final BluetoothDevice device = result.getDevice();
      final String address = device.getAddress();
      final ScanRecord record = result.getScanRecord();


      // Log the event. Include Ruuvi specific data if available.
      final byte[] ruuviPayload = record == null ? null : record.getManufacturerSpecificData(0);
      final RuuviData ruuvi = ruuviPayload == null ? null : new RuuviData(ruuviPayload);
      boolean isRuuvi = false;
      boolean broadcast = true;
      int sensorFlags = 0;
      if (ruuvi != null && ruuvi.format > 0) {
        isRuuvi = true;
        Pair<Boolean, Integer> v = analyseRuuviData(ruuvi, address, result);
        broadcast = v.first;
        sensorFlags = v.second;
      } else if (verboseLog) {
        Log.d(TAG, "Found device with address ", address, ", rssi ", result.getRssi(),
            "dBm, name ", device.getName(), ", and record: ", record);
      } else {
        Log.d(TAG, "Found device with address ", address, ", rssi ", result.getRssi(), "dBm");
      }

      // Send broadcast if not yet done so for this device/interval.
      if (broadcast && (isRuuvi || !mDiscoveredDevices.containsKey(address))) {
        mDiscoveredDevices.put(address, device);

        final String deviceName = device.getName();
//        String[] serviceUUIDs = null;
//        if (record != null) {
//          List<ParcelUuid> uuids = record.getServiceUuids();
//          if (uuids != null) {
//            serviceUUIDs = new String[uuids.size()];
//            for (int i = 0; i < uuids.size(); i++) {
//              serviceUUIDs[i] = uuids.get(i).toString();
//            }
//          }
//        }
        final Intent intent = new Intent(ACTION_DEVICE_DISCOVERED)
            .putExtra(EXTRA_DEVICE_ADDRESS, address)
            .putExtra(EXTRA_DEVICE_NAME, deviceName == null ? "" : deviceName)
            .putExtra(EXTRA_DEVICE_RSSI, result.getRssi())
            .putExtra(EXTRA_SENSOR_TYPES, sensorFlags);
//        if (serviceUUIDs != null) {
//          intent.putExtra(EXTRA_SERVICE_UUIDS, serviceUUIDs);
//        }
        sendBroadcast(intent);
        HealthCheck.getInstance().updateTimestamp(HealthCheck.Timestamp.BleBroadcast);
      }
    }

    @Override
    public void onBatchScanResults(List<ScanResult> results) {
      for (ScanResult result : results) {
        onScanResult(ScanSettings.CALLBACK_TYPE_ALL_MATCHES, result);
      }
    }

    @Override
    public void onScanFailed(int errorCode) {
      Log.e(TAG, "Scan failed with error code ", errorCode);
      if (mStopScanAndRescheduleJob != null) {
        mStopScanAndRescheduleJob.run();
      }
    }

    /**
     * Analyse a Ruuvi event.
     *
     * @param ruuvi   The event payload.
     * @param address The sensor address.
     * @param result  The scan result.
     * @return The first element is true if the event should be broadcast, false if it should be
     * dropped. The second element contains the {@link #EXTRA_SENSOR_TYPES} flags for the event.
     */
    private Pair<Boolean, Integer> analyseRuuviData(RuuviData ruuvi, String address,
                                                    ScanResult result) {
      if (mSettings == null) loadSettings();

      // Initialise the sensor's model if null or re-create/update it if the config has changed.
      ChangeModel model = mActivationModels.get(address);
      if (model == null) {
        model = new ChangeModel(address, mModelCapacity, mModelMinThreshold, mModelSigmaFactor);
        mActivationModels.put(address, model);
        Log.d(TAG, "Created an activation model for ", address);
      } else if (mModelCapacity != model.mCapacity) {
        model = new ChangeModel(address, mModelCapacity, mModelMinThreshold, mModelSigmaFactor);
        mActivationModels.put(address, model);
        Log.d(TAG, "Re-created the activation model for ", address);
      } else if (mModelMinThreshold != model.mMinThreshold
          || mModelSigmaFactor != model.mSigmaFactor) {
        model.mMinThreshold = mModelMinThreshold;
        model.mSigmaFactor = mModelSigmaFactor;
        Log.d(TAG, "Updated the activation model for ", address);
      }

      boolean keep;
      int sensorFlags = 0;
      if (ruuvi.sequenceNumber == model.mLastSeqNo) {
        keep = false; // Duplicate packet.
      } else if (ruuvi.eventCounterPIR == 0 && ruuvi.eventCounterAccel == 0) {
        // Heartbeat packet.
        keep = false;
        logEvent(ruuvi, address, result, " (heartbeat)");
      } else if (ruuvi.eventCounterPIR > 0 && ruuvi.eventCounterAccel == 0) {
        // PIR only event.
        keep = true;
        sensorFlags = EXTRA_SENSOR_TYPE_PIR;
        logEvent(ruuvi, address, result, " (PIR: keep)");
      } else if (ruuvi.eventCounterAccel > 0) {
        // Accelerometer event, possibly together with PIR.
        // Analyse the acceleration values and update the model.
        keep = model.analyseAndUpdate(ruuvi);
        mModelsUpdated.add(address);
        if (keep) sensorFlags = EXTRA_SENSOR_TYPE_ACCELEROMETER;

        // Check if the PIR also activated.
        if (ruuvi.eventCounterPIR > 0) {
          boolean axKeep = keep;
          keep = true; // PIR events are always kept.
          sensorFlags |= EXTRA_SENSOR_TYPE_PIR;
          logEvent(ruuvi, address, result, (axKeep ? " (XYZ+PIR: keep)" : " (PIR: keep)"));
        } else {
          logEvent(ruuvi, address, result, (keep ? " (XYZ: keep)" : " (XYZ: drop)"));
        }
      } else {
        Log.w(TAG, "Unexpected PIR & accelerometer counter values in Ruuvi packet: ",
            ruuvi.toString());
        keep = false;
      }

      model.mLastSeqNo = ruuvi.sequenceNumber;
      return new Pair<>(keep, sensorFlags);
    }

    private void logEvent(RuuviData ruuvi, String address, ScanResult result, String suffix) {
      Log.d(TAG, "Found Ruuvi with address ", address, ", rssi ", result.getRssi(), "dBm, ",
          ruuvi, suffix);
    }
  } // ScannerCallback

  /**
   * Tracks sensor measurements.
   */
  private static class ChangeModel {
    private static final String TAG = "ChangeModel";

    // Ring buffers for x, y and z values.
    private final int[] mBufX;
    private final int[] mBufY;
    private final int[] mBufZ;

    // The ring buffer metadata.
    private int mBufStart; // The index of the oldest value in the buffer.
    private int mBufSize; // The number of elements in the buffer.
    private final int mCapacity; // The buffer capacity.

    private int mMinThreshold;
    private double mSigmaFactor;

    private long mSumX; // Sum of x-values.
    private long mSumY;
    private long mSumZ;
    private long mSumX2; // Sum of squared x-values.
    private long mSumY2;
    private long mSumZ2;

    private String mSensorAddress;
    private int mLastSeqNo = -1; // The last seen measurement sequence number.

    /**
     * @param address      The MAC of the sensor being tracked by this instance.
     * @param capacity     The number of values to track. If set to zero the model is effectively a
     *                     no-op and the analysis methods always return true.
     * @param minThreshold The min. difference between measurements and the respective mean values
     *                     for a measurement to be considered an event.
     * @param sigmaFactor  The std dev factor for a measurement to be treated as an event.
     */
    public ChangeModel(String address, int capacity, int minThreshold, double sigmaFactor) {
      if (capacity < 0 || minThreshold < 0 || sigmaFactor <= 0.) {
        Log.w(TAG, "Invalid config capacity=", capacity, " minThreshold=", minThreshold,
            " sigmaFactor=", sigmaFactor);
        capacity = 0;
        minThreshold = 0;
        sigmaFactor = 1.;
      }

      mSensorAddress = address;
      mBufX = new int[capacity];
      mBufY = new int[capacity];
      mBufZ = new int[capacity];
      mCapacity = capacity;
      mMinThreshold = minThreshold;
      mSigmaFactor = sigmaFactor;
    }

    /**
     * Analyses the new data and then incorporates it into the model's state.
     *
     * @param data The new sensor data.
     * @return True if the data significantly diverges from the norm.
     */
    public boolean analyseAndUpdate(RuuviData data) {
      // Analyse the new values.
      boolean isSignificant = analyse(data.accelX, data.accelY, data.accelZ);

      // Update the model with the new values.
      update(data.accelX, data.accelY, data.accelZ);

      return isSignificant;
    }

    /**
     * Updates the model with the (truncated) integer mean values for each axis. This causes the
     * standard deviations to tend towards zero with every heartbeat.
     */
    public void heartbeatUpdate() {
      if (mBufSize == 0) return;

//      Log.d(TAG, mSensorAddress
//          + " heartbeat x=" + (mSumX / mBufSize) + " (" + Math.round(stddev(mSumX, mSumX2))
//          + ") y=" + (mSumY / mBufSize) + " (" + Math.round(stddev(mSumY, mSumY2))
//          + ") z=" + (mSumZ / mBufSize) + " (" + Math.round(stddev(mSumZ, mSumZ2))
//          + ") size=" + mBufSize);
      update((int) (mSumX / mBufSize), (int) (mSumY / mBufSize), (int) (mSumZ / mBufSize));
    }

    /**
     * Checks whether the total change in the measurements exceeds the threshold.
     *
     * @param ax X-acceleration.
     * @param ay Y-acceleration.
     * @param az Z-acceleration.
     * @return True if the combined change is >= the threshold.
     */
    private boolean analyse(long ax, long ay, long az) {
      if (mBufSize == 0) return mCapacity == 0;

      // Calculate the change per axis and check if the total change satisfies the min threshold.
      final double dx = delta(ax, mSumX);
      final double dy = delta(ay, mSumY);
      final double dz = delta(az, mSumZ);
      final double dSum = dx + dy + dz;
      if (dSum < mMinThreshold) {
        Log.d(TAG, Locale.ENGLISH, "%s dx=%.0f + dy=%.0f + dz=%.0f = %.0f < %d",
            mSensorAddress, dx, dy, dz, dSum, mMinThreshold);
        return false;
      }

      // Calculate the dynamic threshold.
      final double sigmaX = stddev(mSumX, mSumX2);
      final double sigmaY = stddev(mSumY, mSumY2);
      final double sigmaZ = stddev(mSumZ, mSumZ2);
      final double scaleX = scaleFactor(sigmaX);
      final double scaleY = scaleFactor(sigmaY);
      final double scaleZ = scaleFactor(sigmaZ);
      final double threshold = sigmaX * scaleX + sigmaY * scaleY + sigmaZ * scaleZ;

      Log.d(TAG, Locale.ENGLISH, "%s sigmaX=%.0f sigmaY=%.0f sigmaZ=%.0f dSum=%.0f threshold=%.0f",
          mSensorAddress, sigmaX, sigmaY, sigmaZ, dSum, threshold);
      return dSum >= threshold;
    }

    /**
     * Calculates the difference of v from the mean (sum / mBufSize).
     */
    private double delta(long v, long sum) {
      double mu = ((double) sum) / ((double) mBufSize);
      return Math.abs(((double) v) - mu);
    }

    /**
     * Calculates the sigma scale factor by linearly reducing mSigmaFactor, if > 1.0, so that it
     * reaches 1.0 when sigma is equal to (or larger than) 1000. This keeps large sigmas from
     * requiring unrealistically large inputs to reach the threshold.
     *
     * @param sigma The standard deviation.
     * @return The scale factor.
     */
    private double scaleFactor(double sigma) {
      double scale = mSigmaFactor;
      if (scale > 1.) {
        scale = Math.max(1., scale - (scale - 1.) * sigma / 1000.);
      }
      return scale;
    }

    /**
     * Calculates the population standard deviation.
     *
     * @param sum  The sum of measurements.
     * @param sum2 The sum of squared measurements.
     * @return The standard deviation.
     */
    private double stddev(long sum, long sum2) {
      if (mBufSize < 2) return 0.;

      double mu = ((double) sum) / ((double) mBufSize);
      double mu2 = ((double) sum2) / ((double) mBufSize);
      double var = mu2 - mu * mu;
      return var > 0. ? Math.sqrt(var) : 0.;
    }

    /**
     * Updates the model with the new values.
     *
     * @param ax X-acceleration.
     * @param ay Y-acceleration.
     * @param az Z-acceleration.
     */
    private void update(int ax, int ay, int az) {
      if (mCapacity == 0) return;

      if (mBufSize < mCapacity) {
        // Simply store the new values at the next index.
        mBufX[mBufSize] = ax;
        mBufY[mBufSize] = ay;
        mBufZ[mBufSize] = az;
        mBufSize++;
      } else {
        // Subtract the oldest values from the sums.
        final int i = mBufStart;
        mSumX -= mBufX[i];
        mSumY -= mBufY[i];
        mSumZ -= mBufZ[i];
        mSumX2 -= mBufX[i] * mBufX[i];
        mSumY2 -= mBufY[i] * mBufY[i];
        mSumZ2 -= mBufZ[i] * mBufZ[i];

        // Replace them with the new values and update the indices.
        mBufX[i] = ax;
        mBufY[i] = ay;
        mBufZ[i] = az;
        mBufStart = (i + 1) % mCapacity;
        mBufSize = mCapacity;
      }

      // Update the sums.
      mSumX += ax;
      mSumY += ay;
      mSumZ += az;
      mSumX2 += ax * ax;
      mSumY2 += ay * ay;
      mSumZ2 += az * az;
    }
  } // ChangeModel

  /**
   * Parses Ruuvi data format 5 manufacturer specific data.
   */
  private static class RuuviData {
    final int format;  // Data format version.
    final double temperature; // Temperature, C.
    final double humidity; // Humidity, %.
    //    final int pressure; // Pressure, Pa.
    final int eventCounterPIR; // PIR event counter since last idle period.
    final int accelX; // X-acceleration, +/-mG.
    final int accelY; // Y-acceleration, +/-mG.
    final int accelZ; // Z-acceleration, +/-mG.
    final double battery; // Battery, V.
    final int txPower; // Signal strength, dBm.
    final int eventCounterAccel; // Accelerometer event counter since last idle period.
    final int sequenceNumber; // Measurement sequence number.

    RuuviData(@NonNull byte[] payload) {
      if (payload.length < 24) {
        Log.w(TAG, "Failed to parse Ruuvi payload, it only has ", payload.length, " bytes");
        format = eventCounterPIR = accelX = accelY = accelZ = txPower = eventCounterAccel
            = sequenceNumber = 0;
        temperature = humidity = battery = 0;
        return;
      } else if (payload[0] != 5) {
        Log.w(TAG, "Failed to parse Ruuvi payload, expected data format 5, got ", payload[0]);
        format = eventCounterPIR = accelX = accelY = accelZ = txPower = eventCounterAccel
            = sequenceNumber = 0;
        temperature = humidity = battery = 0;
        return;
      }

      format = payload[0];
      temperature = ((payload[1] << 8) | (payload[2] & 0xff)) * 0.005;
      humidity = (((payload[3] & 0xff) << 8) | (payload[4] & 0xff)) * 0.0025;
//      pressure = (((payload[5] & 0xff) << 8) | (payload[6] & 0xff)) + 50000;
      eventCounterPIR = (((payload[5] & 0xff) << 8) | (payload[6] & 0xff));
      accelX = ((payload[7] << 8) | (payload[8] & 0xff));
      accelY = ((payload[9] << 8) | (payload[10] & 0xff));
      accelZ = ((payload[11] << 8) | (payload[12] & 0xff));
      battery = ((((payload[13] & 0xff) << 8) | (payload[14] & 0xff)) >> 5) / 1000. + 1.6;
      txPower = (payload[14] & 0x1f) * 2 - 40;
      eventCounterAccel = (payload[15] & 0xff);
      sequenceNumber = (((payload[16] & 0xff) << 8) | (payload[17] & 0xff));
    }

    @NonNull
    @Override
    public String toString() {
      return String.format(Locale.ROOT,
          "F:%d, T:%.1f, H:%.0f, P:%d, X:%d, Y:%d, Z:%d, V:%.3f, Tx:%d, C:%d, S:%d",
          format, temperature, humidity, eventCounterPIR, accelX, accelY, accelZ, battery, txPower,
          eventCounterAccel, sequenceNumber);
    }
  } // RuuviData
}
