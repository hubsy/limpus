package io.hubsy.core.beaconscanner;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;
import io.hubsy.core.util.Radio;

/**
 * Controls the state of a Bluetooth LE relay.
 * <p>
 * <b>This class must be used from the main thread.</b>
 */
public class RelayController {
  private static final String TAG = "RelayController";

  // Used to limit RelayController to one instance per device.
  private static final Map<String, RelayController> deviceControllers = new HashMap<>();

  /**
   * Returns the controller singleton for the given device address.
   *
   * @param context A context.
   * @param address The MAC address of the device.
   * @return The instance, or null if there was a problem. This may be a permanent problem for the
   * given address if it is invalid; or a temporary problem due to the current system state.
   */
  @Nullable
  public static RelayController get(@NonNull Context context, @NonNull String address) {
    RelayController controller = deviceControllers.get(address);
    if (controller == null) {
      // Make sure the address is well-formed.
      if (!BluetoothAdapter.checkBluetoothAddress(address)) {
        Log.e(TAG, "Invalid bluetooth device address: ", address);
        return null;
      }

      // Create the controller.
      controller = new RelayController(context, address);
      deviceControllers.put(address, controller);
    }

    return controller;
  }

  /**
   * Callback interface for listeners interested in relay state changes.
   */
  public interface StateCallback {
    /**
     * @param relayIsOn True if the relay has entered the "on" state; false if it has entered the
     *                  "off" state as previously requested or due to an error.
     */
    void stateChanged(boolean relayIsOn);
  }

  // List of listeners.
  private List<StateCallback> callbacks = new ArrayList<>();

  /**
   * States of the state machine. Persistent states wait for outside requests to transition, whereas
   * transient states wait for internal events instead.
   */
  private enum State {
    GATTClosed,         // Persistent. Next state is Connecting.
    Connecting,         // Transient. Next state is Connected or Aborting.
    Connected,          // Transient. Next state is RelayTurningOn or Reconnecting.
    RelayTurningOn,     // Transient. Next state is RelayOn, RelayTurningOff or Reconnecting.
    RelayOn,            // Persistent. Next state is RelayTurningOff or Reconnecting.
    RelayTurningOff,    // Transient. Next state is Aborting or Reconnecting.
    Reconnecting,       // Transient. Next state is Connected, RelayOn, RelayTurningOff or Aborting.
    Aborting,           // Transient. Next state must be GATTClosed.
  }

  private State state;
  private State stateBeforeReconnecting; // Used to resume a state after a reconnecting.

  private final Context context;

  private final String address;
  private BluetoothGatt gatt;

  // Handler for callbacks to communicate with this instance.
  private final Handler eventHandler;

  // Constants for the relay state change service and characteristic as advertised by the device.
  private static final UUID SERVICE_UUID = UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb");
  //  private static final int SERVICE_TYPE = BluetoothGattService.SERVICE_TYPE_PRIMARY;
  private static final UUID CHARACTERISTIC_UUID =
      UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb");
//  private static final int CHARACTERISTIC_PERMISSIONS = 0;
//  private static final int CHARACTERISTIC_PROPERTIES = 22;
//  private static final int CHARACTERISTIC_WRITE_TYPE =
//      BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE;

  // Get the software version. Returns two bytes, the first being the module ID, which is 15,
  // followed by the software version.
//  private static final byte[] cmdGetVersion = {0x5A};
  // Get relay states. Returns one byte with high bits indicating the corresponding relay is on.
//  private static final byte[] cmdGetStates = {0x5B};
  private static final byte[] cmdRelayOn = {0x65}; // Turn the relay on.
  private static final String cmdRelayOnHex = Helpers.bytesToHex(cmdRelayOn);
  private static final byte[] cmdRelayOff = {0x6F}; // Turn the relay off.
  private static final String cmdRelayOffHex = Helpers.bytesToHex(cmdRelayOff);
  // Get the battery voltage. Returns ASCII text followed by a carriage return character.
//  private static final byte[] cmdGetVoltage = {0x92};

  // The service and characteristic which determine the state of the relay.
  private BluetoothGattService relayStateService;
  private BluetoothGattCharacteristic relayState;

  private RelayController(Context context, String address) {
    this.context = context.getApplicationContext();
    this.address = address;
    this.state = State.GATTClosed;
    this.stateBeforeReconnecting = State.Aborting;
    eventHandler = new Handler(Looper.getMainLooper());
  }

  /**
   * Turns the relay on.
   *
   * @param callback Will be called once either when the request has been successful and the relay
   *                 is on, or when the request has failed.
   */
  public void turnOn(@Nullable StateCallback callback) {
    // Get the BT adapter and check if it is enabled.
    final BluetoothManager btManager =
        (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
    final BluetoothAdapter btAdapter = btManager != null ? btManager.getAdapter() : null;
    if (btAdapter == null || !btAdapter.isEnabled()) {
      Log.d(TAG, "Bluetooth is disabled, turning it on...");
      Radio.turnOnBluetooth(context);

      // Unable to turn on at the moment, notify immediately.
      if (callback != null) callback.stateChanged(false);
      return;
    }

    if (state == State.RelayOn) {
      // Already in the requested state, notify immediately.
      if (callback != null) callback.stateChanged(true);
      return;
    }

    // Return if in a transient state, but keep callback to notify when reaching a permanent state.
    if (callback != null) callbacks.add(callback);
    if (state != State.GATTClosed) return; // Already in progress. Cannot turn on while turning off.

    // Connect to the GATT server on the device.
    if (gatt != null) { // Should never happen.
      Log.e(TAG, "Error GATT should be null in state ", State.GATTClosed.name());
      gatt.close();
    }
    final BluetoothDevice device = btAdapter.getRemoteDevice(address);
    gatt = device.connectGatt(context, false, new GattCallback());
    if (gatt == null) {
      Log.d(TAG, "Failed to initiate GATT connection");
      notifyAndClearCallbacks(false); // Make sure the callback gets called and removed.
      return;
    }
    state = State.Connecting;
    Log.d(TAG, "Turning on ", address);

    eventHandler.postDelayed(abortIfNotInPersistentState, 1500);
  }

  /**
   * Turns the relay off.
   *
   * @param callback Will be called once when the relay has been turned off.
   */
  public void turnOff(@Nullable StateCallback callback) {
    if (state == State.GATTClosed || gatt == null) {
      if (state != State.GATTClosed) { // This should never be the case.
        Log.e(TAG, "Error trying to turn relay off but GATT is null");
        state = State.GATTClosed;
      }

      // Already in the requested state, notify immediately.
      if (callback != null) callback.stateChanged(false);
      return;
    }

    // Return if already turning off, but keep callback to notify when reaching a permanent state.
    if (callback != null) callbacks.add(callback);
    if (state == State.RelayTurningOff || state == State.Aborting) return; // Already in progress.

    // Perform the appropriate action based on the current state.
    Log.d(TAG, "Turning off ", address);
    switch (state) {
      case Connecting:
      case Connected:
      case Reconnecting:
        abort(false);
        break;
      case RelayTurningOn:
      case RelayOn:
        enterStateRelayTurningOff();
        break;
      default:
        Log.e(TAG, "Invalid state in turnOff: ", state.name());
        abort(false);
        break;
    }

    eventHandler.postDelayed(abortIfNotInPersistentState, 4000);
  }

  /**
   * Notifies {@link StateCallback} instances of the state change and then clears all callbacks.
   *
   * @param relayIsOn The value to pass to callbacks.
   */
  private void notifyAndClearCallbacks(boolean relayIsOn) {
    for (StateCallback c : callbacks) {
      c.stateChanged(relayIsOn);
    }
    callbacks.clear();
  }

  /**
   * Enter {@link State#Reconnecting} and attempt to reconnect to the device.
   */
  private void reconnect() {
    if (state != State.Connected && state != State.RelayTurningOn && state != State.RelayOn
        && state != State.RelayTurningOff && state != State.Reconnecting) {
      Log.e(TAG, "Invalid entry state for Reconnecting: ", state.name());
      abort(false);
      return;
    }

    if (state == State.Reconnecting) {
      // Wait a little while if already in this state to avoid thrashing it too much.
      eventHandler.postDelayed(() -> {
        if (state == State.Aborting) return; // May have been entered while this was delayed.
        if (!gatt.connect()) {
          Log.d(TAG, "Failed to reconnect to ", address);
          abort(true);
        }
      }, 500);
      return;
    }

    // Keep track of the old state so it can be re-entered when the connection is re-established.
    stateBeforeReconnecting = state;

    state = State.Reconnecting;
    if (gatt.connect()) {
      Log.d(TAG, "Reconnecting to ", address);
    } else {
      Log.d(TAG, "Failed to reconnect to ", address);
      abort(true);
    }
  }

  /**
   * Enters state {@link State#Aborting}.
   *
   * @param disconnected Skips disconnecting if already disconnected. This should only be true when
   *                     called from {@link #disconnectedEvent}.
   */
  private void abort(boolean disconnected) {
    if (state == State.GATTClosed || state == State.Aborting) {
      Log.e(TAG, "Invalid entry state for Aborting: ", state.name());
      disconnected = true; // Close immediately to get back to a clean state.
    }

    // Disconnect and schedule a check that the state actually transitions to GATTClosed.
    state = State.Aborting;
    if (!disconnected && gatt != null) {
      gatt.disconnect();
      eventHandler.postDelayed(close, 1000);
    } else {
      close.run();
    }
  }

  /**
   * Tests whether we are in a persistent state and aborts if not.
   * <p>
   * Clears any remaining entries of itself from the eventHandler.
   */
  private final Runnable abortIfNotInPersistentState = new Runnable() {
    @Override
    public void run() {
      if (state == State.GATTClosed || state == State.RelayOn) return; // Persistent states, good.

      // Clear any scheduled instances of this.
      eventHandler.removeCallbacks(this);

      if (state != State.Aborting) {
        // Either the system did not invoke a callback or it is simply taking too long.
        Log.d(TAG, "Aborting the connection attempt (timeout) to ", address);
        abort(false);

        // Forget the cached service to trigger a new discovery next time.
        relayStateService = null;
        // Remove the controller entirely to reinitialise it from scratch.
        deviceControllers.remove(address);
      }
    }
  };

  /**
   * Enters state {@link State#GATTClosed}.
   * <p>
   * Clears any remaining entries of itself from the eventHandler.
   */
  private final Runnable close = new Runnable() {
    @Override
    public void run() {
      if (state == State.GATTClosed) return;
      if (state != State.Aborting) {
        Log.e(TAG, "Invalid entry state for GATTClosed: ", state.name());
        // Close anyway to get back to a clean state.
      }

      if (gatt != null) gatt.close();
      gatt = null;
      relayState = null;
      stateBeforeReconnecting = State.Aborting;
      // Clear the event queue (instances of abortIfNotInPersistentState and everything else).
      eventHandler.removeCallbacksAndMessages(null);
      state = State.GATTClosed;

      notifyAndClearCallbacks(false);
    }
  };

  /**
   * Handles device connection established events and enters state {@link State#Connected}, or
   * state {@link #stateBeforeReconnecting} if the current state is {@link State#Reconnecting}.
   */
  private final Runnable connectedEvent = new Runnable() {
    @Override
    public void run() {
      // Aborting may have been entered concurrently with the event leading here being enqueued.
      if (state == State.Aborting) return;

      if (state == State.Reconnecting) {
        // Resume the previous state.
        switch (stateBeforeReconnecting) {
          case Connected: // Fall-through.
          case RelayTurningOn:
            enterStateConnected();
            break;
          case RelayOn:
            Log.d(TAG, "Reconnected to device ", address);
            state = State.RelayOn; // Nothing else to do, this is a persistent state.
            break;
          case RelayTurningOff:
            Log.d(TAG, "Reconnected to device ", address);
            enterStateRelayTurningOff();
            break;
          default:
            Log.e(TAG, "Invalid previous state after reconnect: ", stateBeforeReconnecting.name());
            abort(false);
        }
      } else {
        enterStateConnected();
      }
    }
  };

  /**
   * Enter {@link State#Connected} and perform its actions. May be entered multiple times before
   * transitioning to another state. Enters state {@link State#RelayTurningOn} when ready.
   */
  private void enterStateConnected() {
    if (state != State.Connecting && state != State.Connected && state != State.Reconnecting) {
      Log.e(TAG, "Invalid entry state for Connected: ", state.name());
      abort(false);
      return;
    }

    // Don't log multiple times when re-entering from the same state.
    if (state != State.Connected) Log.d(TAG, "Connected to ", address);
    state = State.Connected;

    // If the service has been cached, then try to reuse it.
    if (relayState == null && relayStateService != null && gatt.getService(SERVICE_UUID) == null) {
      try {
        // It is necessary to use reflection to add a service to the GATT client without running
        // service discovery.
        @SuppressWarnings("JavaReflectionMemberAccess")
        Field field = BluetoothGatt.class.getDeclaredField("mServices");
        field.setAccessible(true);
        //noinspection unchecked
        List<BluetoothGattService> list = (List<BluetoothGattService>) field.get(gatt);
        list.add(relayStateService);
        relayState = relayStateService.getCharacteristic(CHARACTERISTIC_UUID);
      } catch (Exception e) {
        // Fall back to service discovery.
        relayState = null;
        relayStateService = null;
        Log.d(TAG, "Failed to access BluetoothGatt.mServices: ", e.getMessage());
      }
    }

    if (relayState == null) {
      // Perform service discovery.
      if (!gatt.discoverServices()) {
        Log.d(TAG, "Failed to start service discovery for ", address);
        abort(false);
        return;
      }
      Log.d(TAG, "Service discovery started for ", address);
    } else {
      enterStateRelayTurningOn(); // Turn the relay on.
    }
  }

  /**
   * Handles device disconnected events.
   */
  private final Runnable disconnectedEvent = new Runnable() {
    @Override
    public void run() {
      Log.d(TAG, "Disconnected from ", address);

      // With callbacks enqueuing events on different threads it is possible for a disconnect to be
      // proceseed after, for example, a call to close has just completed.
      switch (state) {
        case GATTClosed:
          break; // Ignore.
        case Connecting:
          abort(true);
          break;
        case Connected:
          reconnect();
          break;
        case RelayTurningOn:
          reconnect();
          break;
        case RelayOn:
          reconnect();
          break;
        case RelayTurningOff:
          reconnect();
          break;
        case Reconnecting:
          reconnect();
          break;
        case Aborting:
          close.run();
          break;
        default:
          Log.e(TAG, "Invalid state for disconnect event: ", state.name());
          abort(true);
          break;
      }
    }
  };

  /**
   * Handles services discovered events.
   */
  private final Runnable servicesDiscoveredEvent = new Runnable() {
    @Override
    public void run() {
      // Aborting may have been entered concurrently with the event leading here being enqueued.
      if (state == State.Aborting) return;
      if (state != State.Connected) {
        Log.e(TAG, "Invalid entry state for servicesDiscoveredEvent: ", state.name());
        abort(false);
        return;
      }
      Log.d(TAG, "Services discovered for ", address, "\n", formatServiceData());

      // Get the service and characteristic we are interested in.
      relayStateService = gatt.getService(SERVICE_UUID);
      if (relayStateService == null) {
        Log.w(TAG, "Failed to discover the relay service on ", address);
      } else {
        relayState = relayStateService.getCharacteristic(CHARACTERISTIC_UUID);
        if (relayState == null) {
          relayStateService = null;
          Log.w(TAG, "Failed to discover the relay characteristic on ", address);
        }
      }

      enterStateConnected(); // Continue in state Connected.
    }
  };

  /**
   * Handles service discovery failed events.
   */
  private final Runnable serviceDiscoveryFailedEvent = new Runnable() {
    @Override
    public void run() {
      // Aborting may have been entered concurrently with the event leading here being enqueued.
      if (state == State.Aborting) return;

      Log.d(TAG, "Service discovery failed for ", address);
      enterStateConnected(); // Try again.
    }
  };

  /**
   * Changes the state of the relay.
   *
   * @param value The new state to send.
   * @return True if the write operation has been initiated successfully.
   */
  private boolean writeRelayState(final byte[] value) {
    if (gatt == null || value == null) return false;

    if (relayState == null) {
      Log.w(TAG, "Failed to send data, characteristic is null");
      return false;
    }
    if (!relayState.setValue(value)) {
      Log.w(TAG, "Failed to locally update characteristic value: ", Helpers.bytesToHex(value));
      return false;
    }
    if (!gatt.writeCharacteristic(relayState)) {
      Log.d(TAG, "Failed to initiate relay state update");
      return false;
    }
    return true;
  }

  /**
   * Enter {@link State#RelayTurningOn} and perform its actions.
   */
  private void enterStateRelayTurningOn() {
    if (state != State.Connected) {
      Log.e(TAG, "Invalid entry state for RelayTurningOn: ", state.name());
      abort(false);
      return;
    }

    state = State.RelayTurningOn;
    if (!writeRelayState(cmdRelayOn)) {
      abort(false);
    }
    Log.d(TAG, "Relay turning on ", address);
  }

  /**
   * Enter {@link State#RelayTurningOff} and perform its actions.
   */
  private void enterStateRelayTurningOff() {
    if (state != State.RelayTurningOn && state != State.RelayOn && state != State.Reconnecting) {
      Log.e(TAG, "Invalid entry state for RelayTurningOff: ", state.name());
      abort(false);
      return;
    }

    state = State.RelayTurningOff;
    if (!writeRelayState(cmdRelayOff)) {
      abort(false);
    }
  }

  /**
   * Each instance handles a specific characteristic write succeeded event.
   * <p>
   * If the event is for cmdRelayOn, then it enters state {@link State#RelayOn}.
   * If it is for cmdRelayOff, then it invokes {@link #abort(boolean)}.
   */
  private class CharacteristicWriteSuccess implements Runnable {
    final BluetoothGattCharacteristic characteristic;

    CharacteristicWriteSuccess(BluetoothGattCharacteristic c) {
      characteristic = c;
    }

    @Override
    public void run() {
      // Aborting may have been entered concurrently with the event leading here being enqueued.
      if (state == State.Aborting) return;

      if (relayState != null && relayState.getUuid().equals(characteristic.getUuid())) {
        final String value = Helpers.bytesToHex(characteristic.getValue());
        if (cmdRelayOnHex.equals(value)) {
          if (state != State.RelayTurningOn) {
            Log.e(TAG, "Invalid entry state for cmdRelayOn write event: ", state.name());
          }
          state = State.RelayOn;
          // Remove instances of abortIfNotInPersistentState, a persistent state has been reached.
          eventHandler.removeCallbacks(abortIfNotInPersistentState);
          Log.d(TAG, "Turned on ", address);
          notifyAndClearCallbacks(true);
        } else if (cmdRelayOffHex.equals(value)) {
          if (state != State.RelayTurningOff) {
            Log.e(TAG, "Invalid entry state for cmdRelayOff write event: ", state.name());
          }
          Log.d(TAG, "Turned off ", address);
          abort(false);
        } else {
          Log.e(TAG, "Error unknown relay state 0x", value, " for ", address);
          abort(false);
        }
      } else {
        Log.e(TAG, "Unexpected characteristic in CharacteristicWriteSuccess event in state: ",
            state.name(), ": ", (characteristic != null ? characteristic.getUuid() : "null"));
      }
    }
  }

  /**
   * Each instance handles a specific characteristic write failure event.
   */
  private class CharacteristicWriteFailure implements Runnable {
    final BluetoothGattCharacteristic characteristic;
    final int gattErrorStatus;

    CharacteristicWriteFailure(BluetoothGattCharacteristic c, int status) {
      characteristic = c;
      gattErrorStatus = status;
    }

    @Override
    public void run() {
      // Aborting may have been entered concurrently with the event leading here being enqueued.
      if (state == State.Aborting) return;

      if (relayState != null && relayState.getUuid().equals(characteristic.getUuid())) {
        final String value = Helpers.bytesToHex(characteristic.getValue());
        if (state == State.RelayTurningOn) {
          Log.d(TAG, "Failed to update relay state to 'on' (0x", value, "), GATT status ",
              gattErrorStatus, " for ", address);
        } else if (state == State.RelayTurningOff) {
          Log.d(TAG, "Failed to update relay state to 'off' (0x", value, "), GATT status ",
              gattErrorStatus, " for ", address);
        } else {
          Log.e(TAG, "Invalid entry state for CharacteristicWriteFailure event: ", state.name(),
              ", characteristic value is: 0x", value);
        }
        abort(false);
      } else {
        Log.e(TAG, "Unexpected characteristic in CharacteristicWriteFailure event in state: ",
            state.name(), ": ", (characteristic != null ? characteristic.getUuid() : "null"));
      }
    }
  }

  /**
   * Returns the service data as a formatted string meant for logging/debugging.
   */
  private String formatServiceData() {
    if (gatt == null) return "";

    final List<BluetoothGattService> services = gatt.getServices();
    final StringBuilder msg = new StringBuilder(128);
    for (final BluetoothGattService service : services) {
      msg.append("Service UUID=");
      msg.append(service.getUuid().toString());
      msg.append(" instance=");
      msg.append(service.getInstanceId());
      msg.append(" type=");
      msg.append(service.getType());

      msg.append(" characteristics:\n");
      final List<BluetoothGattCharacteristic> characts = service.getCharacteristics();
      for (final BluetoothGattCharacteristic c : characts) {
        msg.append("    "); // Indentation.
        msg.append("UUID=");
        msg.append(c.getUuid().toString());
        msg.append(" value=");
        msg.append(Helpers.bytesToHex(c.getValue()));
        msg.append(" permissions=");
        msg.append(c.getPermissions());
        msg.append(" properties=");
        msg.append(c.getProperties());
        msg.append(" write_type=");
        msg.append(c.getWriteType());

        msg.append(" descriptors=[");
        final List<BluetoothGattDescriptor> descriptors = c.getDescriptors();
        for (int i = 0; i < descriptors.size(); i++) {
          final BluetoothGattDescriptor desc = descriptors.get(i);
          msg.append("{UUID=");
          msg.append(desc.getUuid().toString());
          msg.append(" value=");
          msg.append(Helpers.bytesToHex(desc.getValue()));
          msg.append(" permissions=");
          msg.append(desc.getPermissions());
          msg.append("}");
          if (i < (descriptors.size() - 1)) msg.append(", ");
        }
        msg.append("]\n");
      }
      if (characts.size() == 0) msg.append("\n");
    }

    return msg.toString();
  }

  // NOTE that callbacks are not called on the main thread but using some thread-pool.
  private class GattCallback extends BluetoothGattCallback {
    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
      switch (newState) {
        case BluetoothProfile.STATE_CONNECTED:
          eventHandler.post(connectedEvent);
          break;
        case BluetoothProfile.STATE_DISCONNECTED:
          eventHandler.post(disconnectedEvent);
          break;
      }
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
      if (status == BluetoothGatt.GATT_SUCCESS) {
        eventHandler.post(servicesDiscoveredEvent);
      } else {
        eventHandler.post(serviceDiscoveryFailedEvent);
      }
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt,
                                     BluetoothGattCharacteristic characteristic, int status) {
    }

    @Override
    public void onCharacteristicWrite(BluetoothGatt gatt,
                                      BluetoothGattCharacteristic characteristic, int status) {
      if (status == BluetoothGatt.GATT_SUCCESS) {
        eventHandler.post(new CharacteristicWriteSuccess(characteristic));
      } else {
        eventHandler.post(new CharacteristicWriteFailure(characteristic, status));
      }
    }
  }
}
