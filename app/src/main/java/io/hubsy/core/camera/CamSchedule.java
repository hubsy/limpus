package io.hubsy.core.camera;

import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import io.hubsy.core.Settings.CamScheduleKeys;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;

/**
 * A camera schedule represents a collection of {@link CamScheduleEntry} and provides
 * functionality to retrieve the active entry based on the time of day.
 */
public class CamSchedule {
  private static final String TAG = "CamSchedule";

  @Nullable
  private final List<CamScheduleEntry> mEntries;

  private CamSchedule(@Nullable List<CamScheduleEntry> entries) {
    mEntries = entries;
  }

  /**
   * Returns the number of entries in this schedule.
   */
  public int size() {
    return mEntries == null ? 0 : mEntries.size();
  }

  /**
   * Returns the index of the currently active entry; or -1 if the schedule is empty or
   * inconsistent.
   */
  public int activeEntry() {
    if (mEntries != null) {
      for (int i = 0, len = size(); i < len; i++) {
        final int nextIdx = i < (len - 1) ? i + 1 : 0; // Wrap around when necessary.
        CamScheduleEntry current = mEntries.get(i);
        CamScheduleEntry next = mEntries.get(nextIdx);
        if (i == nextIdx || Helpers.isTimeInRange(current.time, next.time)) {
          return i; // This is the correct interval.
        }
      }
    }

    Log.w(TAG, "Failed to find the current schedule entry");
    return -1;
  }

  /**
   * Returns the entry at the given index; or null if idx is out of bounds.
   *
   * @param idx The index of the entry to retrieve.
   * @return The entry.
   */
  public CamScheduleEntry getEntry(int idx) {
    if (mEntries == null || idx < 0 || idx >= mEntries.size()) return null;
    return mEntries.get(idx);
  }

  /**
   * Parses the JSON encoded array of camera configuration schedules.
   *
   * @param json The encoded schedules.
   * @return The decoded schedule; or null on error.
   */
  @Nullable
  public static CamSchedule fromJSON(String json) {
    // Parse the JSON array.
    JSONArray scheduleItems;
    try {
      scheduleItems = new JSONArray(json);
    } catch (JSONException e) {
      Log.w(TAG, "Failed to parse schedule");
      return null;
    }

    final int len = scheduleItems.length();
    if (len == 0) { // Not an error, but nothing more to do.
      Log.d(TAG, "Empty schedule");
      return null;
    }

    // Parse the JSON array elements.
    final List<CamScheduleEntry> entries = new ArrayList<>(len);
    for (int i = 0; i < len; i++) {
      JSONObject obj = scheduleItems.optJSONObject(i);
      if (obj == null) {
        Log.w(TAG, "Failed to parse schedule, array element is not a JSON object");
        return null;
      }

      // The schedule time is the only required element.
      String time = obj.optString(CamScheduleKeys.Time.name());
      if (time.isEmpty()) {
        Log.w(TAG, "Failed to parse schedule, missing the schedule time");
        return null;
      }

      // Populate a new schedule config.
      final CamScheduleEntry schedule = new CamScheduleEntry(time);
      schedule.interpolate = obj.optBoolean(CamScheduleKeys.Interpolate.name(), false);

      // Background subtraction model settings.
      schedule.bgSubCleanFGMask =
          obj.optBoolean(CamScheduleKeys.BGSubCleanFGMask.name(), schedule.bgSubCleanFGMask);

      schedule.bgSubInvert =
          obj.optBoolean(CamScheduleKeys.BGSubInvert.name(), schedule.bgSubInvert);

      parseBGSubKernelParams(obj, schedule);

      schedule.bgSubThreshold = (float)
          obj.optDouble(CamScheduleKeys.BGSubThreshold.name(), schedule.bgSubThreshold);
      schedule.bgSubThreshold = Math.min(1.f, Math.max(0.f, schedule.bgSubThreshold));

      // Parse the exposure settings.
      final String exposureMode = obj.optString(CamScheduleKeys.CamExposureMode.name());
      if ("auto".equalsIgnoreCase(exposureMode)) {
        schedule.exposureMode = CameraController.ExposureMode.AUTO;
      } else if ("manual".equalsIgnoreCase(exposureMode)) {
        schedule.exposureMode = CameraController.ExposureMode.MANUAL;
        int exposureMillis = obj.optInt(CamScheduleKeys.CamExposureTime.name(), 0);
        if (exposureMillis > 0) schedule.exposureTime = exposureMillis * 1000L * 1000L;
        schedule.sensitivity = obj.optInt(CamScheduleKeys.CamISO.name(), schedule.sensitivity);
      }

      // Parse the sequence settings.
      schedule.sequenceInterval =
          obj.optLong(CamScheduleKeys.SequenceInterval.name(), schedule.sequenceInterval);
      schedule.sequenceLength =
          obj.optInt(CamScheduleKeys.SequenceLength.name(), schedule.sequenceLength);
      schedule.sequenceReso =
          obj.optInt(CamScheduleKeys.SequenceReso.name(), schedule.sequenceReso);

      // Limit the number of sequence images to prevent uncontrolled memory consumption.
      final double maxBytes = 32 * 1024 * 1024; // Max 32 MiB.
      final double imageSizeBytes = schedule.sequenceReso * schedule.sequenceReso * 0.75 * 3;
      if ((schedule.sequenceLength * imageSizeBytes) > maxBytes) {
        schedule.sequenceLength = (int) Math.floor(maxBytes / imageSizeBytes);
        Log.w(TAG, "Limiting the sequence length to ", schedule.sequenceLength,
            " to stay within 32 MiB for buffers");
      }

      entries.add(schedule);
    }

    Log.d(TAG, "Parsed camera parameter schedule with ", entries.size(), " entries");
    return new CamSchedule(entries);
  }

  /**
   * Extracts the BGSubKernel and its dimensions and updates schedule with the values or defaults
   * on error.
   */
  private static void parseBGSubKernelParams(JSONObject obj, CamScheduleEntry schedule) {
    schedule.bgSubKernelHeight =
        obj.optInt(CamScheduleKeys.BGSubKernelHeight.name(), schedule.bgSubKernelHeight);
    schedule.bgSubKernelWidth =
        obj.optInt(CamScheduleKeys.BGSubKernelWidth.name(), schedule.bgSubKernelWidth);

    // Make sure the height and width are valid and not excessively large.
    int size = schedule.bgSubKernelHeight * schedule.bgSubKernelWidth;
    if (size <= 0 || size > 10000) {
      Log.w(TAG, "Invalid kernel size");
      schedule.bgSubKernelHeight = 7;
      schedule.bgSubKernelWidth = 7;
      size = schedule.bgSubKernelHeight * schedule.bgSubKernelWidth;
      schedule.bgSubKernel = new float[size];
      Arrays.fill(schedule.bgSubKernel, 1.f / size);
      return;
    }

    float[] kernel = new float[size];
    schedule.bgSubKernel = kernel;
    final float defaultValue = 1.f / size;

    // Get the expressions array.
    JSONArray exprs = obj.optJSONArray(CamScheduleKeys.BGSubKernel.name());
    if (exprs == null) {
      Log.d(TAG, "No bgsub kernel specified, using default");
      Arrays.fill(kernel, defaultValue);
      return;
    }

    // Parse the array elements.
    int kernelIdx = 0;
    int i = 0;
    for (; i < exprs.length() && kernelIdx < kernel.length; i++) {
      String enc = exprs.optString(i, "");

      // Extract how often this value should be repeated.
      String[] countAndExpr = enc.split(" *: *", 2);
      int count = 1;
      String expr = "";
      if (countAndExpr.length == 1) { // The count is 1 if not specified.
        expr = countAndExpr[0];
      } else if (countAndExpr.length == 2) {
        try {
          count = Integer.parseInt(countAndExpr[0]);
          expr = countAndExpr[1];
        } catch (NumberFormatException e) {
          Log.w(TAG, "Invalid format at index ", i, " of ", CamScheduleKeys.BGSubKernel);
          Arrays.fill(kernel, defaultValue);
          return;
        }
      }

      // Parse the constant or compute the expression for the value.
      float value = 0.f;
      expr = expr.trim();
      if (count > 0 && !expr.isEmpty()) {
        // Only a single division is currently supported.
        String[] symbols = expr.split(" */ *", 2);
        if (symbols.length == 1) { // A constant.
          try {
            value = Float.parseFloat(symbols[0].trim());
          } catch (NumberFormatException e) {
            Log.w(TAG, "Invalid value at index ", i, " of ", CamScheduleKeys.BGSubKernel);
            Arrays.fill(kernel, defaultValue);
            return;
          }
        } else if (symbols.length == 2) { // A dividend and divisor.
          try {
            float v1 = Float.parseFloat(symbols[0].trim());
            float v2 = Float.parseFloat(symbols[1].trim());
            if (v2 == 0.f) {
              Log.w(TAG, "Invalid value: the divisor is zero at index ", i, " of ",
                  CamScheduleKeys.BGSubKernel);
              Arrays.fill(kernel, defaultValue);
              return;
            }

            value = v1 / v2;
          } catch (NumberFormatException e) {
            Log.w(TAG, "Invalid values at index ", i, "of ", CamScheduleKeys.BGSubKernel);
            Arrays.fill(kernel, defaultValue);
            return;
          }
        }
      }

      // Write the value count times to the array.
      int c = 0;
      for (; c < count && kernelIdx < kernel.length; c++) {
        kernel[kernelIdx++] = value;
      }
      if (c != count) {
        Log.w(TAG, "Unexpected number of values in ", CamScheduleKeys.BGSubKernel);
        Arrays.fill(kernel, defaultValue);
        return;
      }
    }

    // Check that all values were set. Also complain if there are too many values, as misconfigured
    // kernels likely won't do what is expected.
    if (i != exprs.length()) {
      Log.w(TAG, "Unexpected number of values in ", CamScheduleKeys.BGSubKernel);
      Arrays.fill(kernel, defaultValue);
      return;
    } else if (kernelIdx != kernel.length) {
      Log.w(TAG, "Not enough values in ", CamScheduleKeys.BGSubKernel, ", expected ", size);
      Arrays.fill(kernel, defaultValue);
      return;
    }

    // Log the kernel.
    StringBuilder buf = new StringBuilder();
    buf.append("[");
    for (int y = 0; y < schedule.bgSubKernelHeight; y++) {
      if (y == 0) buf.append("[");
      else buf.append(", [");

      for (int x = 0; x < schedule.bgSubKernelWidth; x++) {
        float v = kernel[y * schedule.bgSubKernelWidth + x];
        if (x < schedule.bgSubKernelWidth - 1) {
          buf.append(String.format(Locale.ROOT, "%.3f ", v));
        } else {
          buf.append(String.format(Locale.ROOT, "%.3f", v));
        }
      }

      buf.append("]");
    }
    buf.append("]");
    Log.d(TAG, "BGSub kernel: ", buf.toString());
  }
}
