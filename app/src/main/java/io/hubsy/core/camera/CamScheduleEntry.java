package io.hubsy.core.camera;

import android.support.annotation.NonNull;

import io.hubsy.core.Settings.CamScheduleKeys;

/**
 * Camera capture related settings for a specific time of day.
 */
public class CamScheduleEntry extends CameraController.Parameters {
  /**
   * See {@link CamScheduleKeys#BGSubCleanFGMask}.
   */
  public boolean bgSubCleanFGMask = true;
  /**
   * See {@link CamScheduleKeys#BGSubInvert}.
   */
  public boolean bgSubInvert = false;
  /**
   * See {@link CamScheduleKeys#BGSubKernel}.
   */
  public float[] bgSubKernel;
  /**
   * See {@link CamScheduleKeys#BGSubKernelHeight}.
   */
  public int bgSubKernelHeight = 7;
  /**
   * See {@link CamScheduleKeys#BGSubKernelWidth}.
   */
  public int bgSubKernelWidth = 7;
  /**
   * See {@link CamScheduleKeys#BGSubThreshold}.
   */
  public float bgSubThreshold = 0.7f;
  /**
   * Whether interpolation is desired. There may be other conditions that need to be satisfied
   * before values can actually be interpolated.
   */
  public boolean interpolate = false;
  /**
   * See {@link CamScheduleKeys#SequenceInterval}.
   */
  public long sequenceInterval = 1000;
  /**
   * See {@link CamScheduleKeys#SequenceLength}.
   */
  public int sequenceLength = 0;
  /**
   * See {@link CamScheduleKeys#SequenceReso}.
   */
  public int sequenceReso = 640;
  /**
   * The start time of this scheduled interval. Format: HH:MM.
   */
  @NonNull
  public final String time;

  CamScheduleEntry(@NonNull String time) {
    this.time = time;
  }
}

