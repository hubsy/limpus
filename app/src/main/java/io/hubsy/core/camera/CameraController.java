package io.hubsy.core.camera;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.MeteringRectangle;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Range;
import android.util.Size;
import android.util.SparseArray;
import android.view.Surface;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Semaphore;

import io.hubsy.core.Settings;
import io.hubsy.core.util.HealthCheck;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;
import io.hubsy.core.util.PhoneOrientation;

/**
 * Interacts with the camera to perform capture requests.
 * <p>
 * Instances of this class are not thread-safe, but different instances may be used concurrently by
 * different threads. However, when interacting with the same camera some or all of them will fail
 * their actions.
 */
public class CameraController {
  private static final String TAG = "CameraController";

  /**
   * Callback interface to notify clients of the class about the outcome of their requested action.
   */
  public interface ActionCallback {
    /**
     * Notifies about the success or failure of a requested action. If success is false, all other
     * arguments may be null.
     *
     * @param success  True if the action was successful, false if it failed.
     * @param images   The image data if {@code success} is true. To release resources,
     *                 {@link ImageWrapper#close()} must be called on each non-null element.
     *                 Closing the camera controller also closes all images obtained through it.
     * @param data     The capture data.
     * @param characts The characteristics of the camera used for the capture.
     */
    void onActionCompleted(boolean success, @Nullable ImageWrapper[] images,
                           @Nullable CaptureData data, @Nullable CameraCharacteristics characts);
  }

  /**
   * A wrapper around image data, which enables clients to both close the image and notify the
   * controller.
   */
  public static class ImageWrapper {
    /**
     * The image should never be closed directly, but by calling {@link #close()} instead.
     * instead.
     */
    @NonNull
    public final Image data;

    private final WeakReference<CameraController> mController;
    private final Semaphore mConfigImageBuffers; // Counter for available image buffers.
    private boolean mIsClosed;

    private ImageWrapper(@NonNull Image img, @NonNull CameraController controller,
                         @NonNull Semaphore configImageBuffers) {
      data = img;
      mController = new WeakReference<>(controller);
      mConfigImageBuffers = configImageBuffers;
    }

    /**
     * Close the image and notify the controller that the buffer is available again.
     */
    public void close() {
      if (mIsClosed) return;
      mIsClosed = true;

      try {
        data.close();
      } catch (IllegalStateException e) {
        Log.e(TAG, "Failed to close the wrapped image: ", e.getMessage());
      }
      mConfigImageBuffers.release();

      CameraController controller = mController.get();
      if (controller != null) controller.processNextRequestIfReady();
    }
  }

  /**
   * The focus modes supported by this class. If the device does not support the requested mode,
   * then it will fall back to one of the other modes, likely AUTO or FIXED.
   */
  public enum FocusMode {
    /**
     * Auto-focus.
     */
    AUTO,
    /**
     * Used for fixed-focus lenses. No focus settings will be applied whatsoever.
     */
    FIXED,
    /**
     * Focus to infinity.
     */
    INFINITY,
  }

  /**
   * Supported exposure modes.
   */
  public enum ExposureMode {
    /**
     * Auto-exposure.
     */
    AUTO,
    /**
     * Manual-exposure. {@link Parameters#exposureTime} and {@link Parameters#sensitivity} will be
     * used.
     */
    MANUAL,
  }

  /**
   * Supported capture modes.
   */
  public enum CaptureMode {
    /**
     * Still image capture.
     */
    Photo,
    /**
     * Video capture.
     */
    Video,
  }

  /**
   * Parameters defining the output image(s).
   */
  public static class ImageConfig implements Cloneable {
    /**
     * The aspect ratio.
     */
    public String aspectRatio = "4:3";
    /**
     * The mode to use for {@link CaptureRequest#EDGE_MODE}.
     */
    public int edgeMode = CaptureRequest.EDGE_MODE_FAST;
    /**
     * One of the image formats from {@link ImageFormat}. Will fall back to {@link ImageFormat#JPEG}
     * if an unsupported format is specified. The config is not used for photo taking if the format
     * is {@link ImageFormat#UNKNOWN}, but may be used for video.
     */
    public int format = ImageFormat.UNKNOWN;
    /**
     * The maximum number of images with this config to be processed concurrently. When all buffers
     * are in use, further capture requests will be delayed until an {@link ImageWrapper} returned
     * by {@link ActionCallback#onActionCompleted} is closed.
     */
    public int imageBuffers = 1;
    /**
     * The encoding quality to use when {@link #format} is JPEG.
     */
    public int jpegQuality = 80;
    /**
     * The size of the larger image dimension. The highest supported resolution where both
     * dimensions are <= this value is selected, or the lowest resolution if the value is too small.
     */
    public int maxDimension = 1920;
    /**
     * The mode to use for {@link CaptureRequest#NOISE_REDUCTION_MODE}.
     */
    public int noiseReductionMode = CaptureRequest.NOISE_REDUCTION_MODE_FAST;
    /**
     * The mode to use for {@link CaptureRequest#TONEMAP_MODE}.
     */
    public int tonemapMode = CaptureRequest.TONEMAP_MODE_FAST;

    @Override
    protected ImageConfig clone() throws CloneNotSupportedException {
      return (ImageConfig) super.clone();
    }

    /**
     * Like {@link #clone}, but never throws a {@link CloneNotSupportedException}.
     */
    public ImageConfig copy() {
      try {
        return clone();
      } catch (CloneNotSupportedException e) {
        Log.e(TAG, e.getMessage());
        return new ImageConfig();
      }
    }
  }

  /**
   * The parameters for captures made through a controller instance.
   */
  public static class Parameters implements Cloneable {
    /**
     * The metering area used for auto-exposure, or null to let the camera decide. Values are
     * specified as percentage offsets from the top-left corner of the final image.
     */
    @Nullable
    public Rect aeArea = null;
    /**
     * The metering area used for auto-focus, or null to let the camera decide. Values are
     * specified as percentage offsets from the top-left corner of the final image.
     */
    @Nullable
    public Rect afArea = null;
    /**
     * The metering area used for auto-whitebalance, or null to let the camera decide. Values are
     * specified as percentage offsets from the top-left corner of the final image.
     */
    @Nullable
    public Rect awbArea = null;
    /**
     * The capture mode to use.
     */
    public CaptureMode captureMode = CaptureMode.Photo;
    /**
     * The capture time in milliseconds since the epoch. This field is only meaningful for output
     * parameters.
     */
    public long captureTime;
    /**
     * The exposure mode.
     */
    public ExposureMode exposureMode = ExposureMode.AUTO;
    /**
     * The exposure time in nanoseconds. Only used with {@link ExposureMode#MANUAL}. See
     * {@link CaptureRequest#SENSOR_EXPOSURE_TIME} for more details.
     */
    public long exposureTime = 20 * 1000L * 1000L; // 20ms.
    /**
     * The focus mode. Will fall back to {@link FocusMode#FIXED} if necessary.
     */
    public FocusMode focusMode = FocusMode.AUTO;
    /**
     * One or more image configurations. The config to use for a particular capture is selected in
     * {@link #takePhoto} and related methods.
     */
    @NonNull
    public List<ImageConfig> imageConfigs = new ArrayList<>(2);
    /**
     * The output file. This field is only meaningful for output parameters and only used when
     * recording video.
     */
    @Nullable
    public File outFile;
    /**
     * The clockwise rotation, in degrees, required to display the image upright. This field is only
     * meaningful for output parameters.
     */
    public int rotation = 0;
    /**
     * The ISO sensitivity. Only used with {@link ExposureMode#MANUAL}. See
     * {@link CaptureRequest#SENSOR_SENSITIVITY} for more details.
     */
    public int sensitivity = 200;
    /**
     * The video encoding bitrate in bits/sec.
     */
    public int videoBitrate = 5000000;
    /**
     * The video capture duration in millis.
     */
    public int videoCaptureDuration = 10 * 1000;
    /**
     * The video capture rate in fps.
     */
    public double videoCaptureFPS = 1.;
    /**
     * The video config to use when recording video.
     */
    @Nullable
    public ImageConfig videoConfig;
    /**
     * The video height in pixels.
     */
    public int videoHeight = 720;
    /**
     * The video playback rate in fps.
     */
    public int videoPlaybackFPS = 5;
    /**
     * The video width in pixels.
     */
    public int videoWidth = 1280;

    @Override
    protected Parameters clone() throws CloneNotSupportedException {
      Parameters c = (Parameters) super.clone();
      c.afArea = afArea == null ? null : new Rect(afArea);
      c.aeArea = aeArea == null ? null : new Rect(aeArea);
      c.awbArea = awbArea == null ? null : new Rect(awbArea);

      c.imageConfigs = new ArrayList<>(imageConfigs.size());
      for (ImageConfig config : imageConfigs) {
        c.imageConfigs.add(config.copy());
      }
      c.videoConfig = videoConfig == null ? null : videoConfig.copy();

      return c;
    }

    /**
     * Like {@link #clone}, but never throws a {@link CloneNotSupportedException}.
     */
    public Parameters copy() {
      try {
        return clone();
      } catch (CloneNotSupportedException e) {
        Log.e(TAG, e.getMessage());
        return new Parameters();
      }
    }
  }

  /**
   * Returns a camera controller for the main rear-facing camera, or null if it is not available.
   * <p>
   * Must be called on a thread with an active {@link Looper}, and must be {@link #close()}ed when
   * done to free resources.
   *
   * @param context The context to use.
   * @param params  The capture parameters.
   * @return The camera controller instance or null.
   */
  @Nullable
  public static CameraController get(@NonNull Context context, @NonNull Parameters params) {
    CameraController controller = new CameraController(context, params);
    if (controller.mState == State.Closed) return null;
    return controller;
  }

  /**
   * States of the state machine. Persistent states wait for outside requests to transition, whereas
   * transient states wait for internal events instead.
   * <p>
   * All states except for Closed and Closing implicitly have Closing as a possible next state.
   */
  private enum State {
    Closed,       // Persistent. Camera is closed and not ready for use. Next state is Initialising.
    Initialising, // Transient.  Camera is being initialised. Next state is Waiting.
    Waiting,      // Persistent|Transient. Waiting for a new request. Next state is Preview or Capturing.
    Preview,      // Transient.  Camera is in preview mode. Next state is LensChanging.
    LensChanging, // Transient.  Waiting for lens changes. Next state is Exposure.
    Exposure,     // Transient.  Waiting for exposure settings. Next state is WhiteBalance.
    WhiteBalance, // Transient.  Waiting for white balance. Next state is Capturing.
    Capturing,    // Transient.  Camera is capturing an image. Next state is Waiting, Preview or Capturing.
    Closing,      // Transient.  Camera is being closed. Next state is Closed.
  }

  /**
   * A POD for data relevant to a particular capture request.
   */
  public static class CaptureData {
    /**
     * The listener to notify when the capture completes, whether successfully or not.
     */
    @NonNull
    final ActionCallback callback;
    /**
     * The capture index in a sequence of captures if this is part of a repeating request.
     */
    public long captureIdx = -1;
    /**
     * If true, AE and AWB do not wait until values have converged before proceeding.
     */
    final boolean skipWaitToConverge;
    /**
     * If the capture is unsuccessful, this indicates whether the frame was dropped due to some
     * performance reason rather than a more serious error.
     */
    public boolean frameDropped;
    /**
     * The image data after a successful image capture, stored at indices corresponding to
     * {@link #imageConfigIdx}.
     */
    @NonNull
    ImageWrapper[] images;
    /**
     * Indicates if an image buffer has been reserved in {@link #mConfigImageBuffers}.
     */
    final boolean[] imageBufferReserved;
    /**
     * The still image configs to use, specified as indices into {@link Parameters#imageConfigs}.
     * Does not include the video config if this is a video capture request.
     */
    @NonNull
    final int[] imageConfigIdx;
    /**
     * If in auto-mode, this flag specifies whether to wait until AE and AWB parameters are locked
     * rather than just converged before proceeding with the capture.
     */
    final boolean lockAEAndAWB;
    /**
     * The capture should only proceed if {@link SystemClock#elapsedRealtime()} is smaller than this
     * timestamp when the request is ready to be processed. Zero means the request does not expire.
     */
    long notAfter;
    /**
     * The actual parameters used during a successful capture. They may be different from the
     * requested values depending on the capabilities of the hardware.
     */
    @Nullable
    public Parameters params;
    /**
     * The capture metadata after a successful capture.
     */
    @Nullable
    public CaptureResult result;
    /**
     * When true and available, reuse previously locked AF, AE and/or AWB settings. Implies
     * lockAEAndAWB=true.
     */
    final boolean reuseLockedParams;

    private CaptureData(@NonNull ActionCallback c, @NonNull int[] imageConfigIdx_,
                        boolean lockAEAndAWB_, boolean reuseLockedParams_,
                        boolean skipWaitToConverge_) {
      callback = c;
      imageConfigIdx = imageConfigIdx_;
      images = new ImageWrapper[imageConfigIdx.length];
      imageBufferReserved = new boolean[imageConfigIdx.length];
      lockAEAndAWB = (reuseLockedParams_ || lockAEAndAWB_) && !skipWaitToConverge_;
      reuseLockedParams = reuseLockedParams_ && !skipWaitToConverge_;
      skipWaitToConverge = skipWaitToConverge_;
    }

    /**
     * Initialise the new capture data with the input values (only!) from rhs.
     */
    @SuppressWarnings("CopyConstructorMissesField")
    private CaptureData(@NonNull CaptureData rhs) {
      callback = rhs.callback;
      imageConfigIdx = rhs.imageConfigIdx.clone();
      images = new ImageWrapper[imageConfigIdx.length];
      imageBufferReserved = new boolean[imageConfigIdx.length];
      lockAEAndAWB = rhs.lockAEAndAWB;
      reuseLockedParams = rhs.reuseLockedParams;
      skipWaitToConverge = rhs.skipWaitToConverge;
    }

    /**
     * Performs a <b>shallow</b> copy, except that {@link #params} is cloned and elements of
     * {@link #imageBufferReserved} are always false. The returned copy should not be used for
     * further request processing by the CameraController.
     */
    public CaptureData makeSubordinateCopy() {
      CaptureData d = new CaptureData(this);
      d.captureIdx = captureIdx;
      d.frameDropped = frameDropped;
      d.images = images;
      d.notAfter = notAfter;
      d.params = params == null ? null : params.copy();
      d.result = result;
      return d;
    }
  }

  private State mState = State.Closed; // The state of the state machine.
  private Handler mEventHandler; // Handler used for callbacks.

  // The desired size if available, otherwise the nearest available size is chosen.
  private static final Size sConfigPreviewSize = new Size(640, 480);

  // The in-use settings. May be modified from the requested values to match the capabilities.
  private final Parameters mParams;
  // The number of image buffers not currently in use for each ImageConfig in mParams.
  private final List<Semaphore> mConfigImageBuffers;

  // Flags to indicate whether focus, exposure or white balance are locked. Only relevant where the
  // corresponding mode is set to auto.
  private boolean mIsAFLocked = false;
  private boolean mIsAELocked = false;
  private boolean mIsAWBLocked = false;

  private String mMainCameraID; // The ID of the main back-facing camera.
  private CameraDevice mCameraDevice;
  private CameraCharacteristics mCharacts;
  private CameraCaptureSession mCaptureSession;
  private CaptureRequest.Builder mPreviewBuilder;
  private CaptureRequest mPreviewRequest;
  private CaptureRequest mCaptureRequest;

  // Capabilities.
  private StreamConfigurationMap mStreamConfigs;
  private boolean mHasManualSensor;
  private boolean mHasVariableLens;
  @Nullable
  private Range<Integer> mSensitivityRange;
  @Nullable
  private Range<Long> mExposureTimeRange;
  private Rect mActiveArraySize;
  private int mMaxRegionsAf;
  private int mMaxRegionsAe;
  private int mMaxRegionsAwb;

  // Outputs.
  private List<ImageReader> mImageReaders;
  private List<Surface> mImageSurfaces;
  // Maps image surfaces to the index of their ImageConfig.
  private Map<Surface, Integer> mImageSurfaceToConfigIdx;
  private Surface mVideoSurface;
  private SurfaceTexture mPreviewSurfaceTex;
  private Surface mPreviewSurface;

  // Sequence requests.
  private long mCaptureSequenceLength = -1;
  private double mCaptureSequenceFPS = 1.;

  // Video.
  private MediaRecorder mRecorder;
  private boolean mVideoRequestReceived = false;
  private boolean mRecordingStopped = false;
  private File mVideoOut;
  private int mVideoSequenceId = -1;
  private TotalCaptureResult mVideoLatestCaptureResult;
  // Timestamp of the last HealthCheck update performed for video frames.
  private long mLastVideoFrameSuccessUpdate = Long.MIN_VALUE;

  // Each element is a request to capture a single photo.
  private final Queue<CaptureData> mCaptureRequests = new LinkedList<>();
  // Stores the request under its capture sequence ID while in flight.
  private final SparseArray<CaptureData> mInFlight = new SparseArray<>();

  protected CameraController(final Context context, final Parameters params) {
    mParams = params.copy();

    // There must be at least one image config unless this is a video request, in which case there
    // must be a video config.
    if (mParams.captureMode == CaptureMode.Video && mParams.videoConfig == null) {
      Log.d(TAG, "No video config specified, using defaults");
      mParams.videoConfig = new ImageConfig();
    } else if (mParams.captureMode != CaptureMode.Video && mParams.imageConfigs.size() == 0) {
      Log.d(TAG, "No image config specified, using defaults");
      ImageConfig config = new ImageConfig();
      config.format = ImageFormat.JPEG;
      mParams.imageConfigs.add(config);
    }

    // Allocate a free image buffer counter for each image config.
    mConfigImageBuffers = new ArrayList<>(mParams.imageConfigs.size());
    for (ImageConfig config : mParams.imageConfigs) {
      mConfigImageBuffers.add(new Semaphore(config.imageBuffers));
    }

    // Start tracking the camera initialisation so the health monitor can react to failures.
    HealthCheck.getInstance(context).updateTimestamp(HealthCheck.Timestamp.PhotoRequest);

    final CameraManager manager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);

    String[] camIDs;
    try {
      camIDs = manager.getCameraIdList();
    } catch (CameraAccessException e) {
      Log.w(TAG, "Failed to query the camera ID list: ", e.getMessage());
      return;
    }

    // Find the rear-facing camera.
    for (String id : camIDs) {
      try {
        mCharacts = manager.getCameraCharacteristics(id);
        Integer facing = mCharacts.get(CameraCharacteristics.LENS_FACING);
        if (facing != null && facing == CameraCharacteristics.LENS_FACING_BACK) {
          mMainCameraID = id;
          break;
        }
      } catch (CameraAccessException e) {
        Log.w(TAG, "Failed to query characteristics for camera ", id, ": ", e.getMessage());
      }
    }
    if (mMainCameraID == null) {
      Log.w(TAG, "Failed to find the requested camera");
      return;
    }

    queryCapabilities();

    // Use the looper of the calling thread.
    Looper looper = Looper.myLooper();
    if (looper == null) {
      Log.e(TAG, "Error: calling thread has no looper");
      return;
    }
    mEventHandler = new Handler(looper);

    try {
      manager.openCamera(mMainCameraID, new CameraStateCallback(), mEventHandler);
      mState = State.Initialising;
    } catch (CameraAccessException | SecurityException e) {
      Log.w(TAG, "Failed to open the camera: ", e.getMessage());
      return;
    }

    // Update the device orientation reading.
    new PhoneOrientation(context);
  }

  /**
   * Queries relevant capabilities of the device and initialises member fields.
   */
  private void queryCapabilities() {
    Integer level = mCharacts.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
    Log.d(TAG, "Supported camera feature level: ", level);

    mStreamConfigs = mCharacts.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

    mHasManualSensor = supports(CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES,
        CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_MANUAL_SENSOR);
    Float minFocusDist = mCharacts.get(CameraCharacteristics.LENS_INFO_MINIMUM_FOCUS_DISTANCE);
    mHasVariableLens = minFocusDist == null || minFocusDist > 0;
    mSensitivityRange = mCharacts.get(CameraCharacteristics.SENSOR_INFO_SENSITIVITY_RANGE);
    mExposureTimeRange = mCharacts.get(CameraCharacteristics.SENSOR_INFO_EXPOSURE_TIME_RANGE);

    // Max. AF, AE, AWB regions.
    mActiveArraySize = mCharacts.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
    Integer maxRegionsAf = mCharacts.get(CameraCharacteristics.CONTROL_MAX_REGIONS_AF);
    mMaxRegionsAf = maxRegionsAf == null ? 0 : maxRegionsAf;
    Integer maxRegionsAe = mCharacts.get(CameraCharacteristics.CONTROL_MAX_REGIONS_AE);
    mMaxRegionsAe = maxRegionsAe == null ? 0 : maxRegionsAe;
    Integer maxRegionsAwb = mCharacts.get(CameraCharacteristics.CONTROL_MAX_REGIONS_AWB);
    mMaxRegionsAwb = maxRegionsAwb == null ? 0 : maxRegionsAwb;

//    Log.d(TAG, "Supported scene modes: ", Arrays.toString(
//        mCharacts.get(CameraCharacteristics.CONTROL_AVAILABLE_SCENE_MODES)));
    Integer maxAnalogISO = mCharacts.get(CameraCharacteristics.SENSOR_MAX_ANALOG_SENSITIVITY);
    Log.d(TAG, "Supported ISO range ",
        mSensitivityRange == null ? "null" : mSensitivityRange.toString(),
        "; max analog sensitivity ", maxAnalogISO,
        "; exposure time range (ns) ",
        mExposureTimeRange == null ? "null" : mExposureTimeRange.toString());
    Log.d(TAG, "Supported regions AF=", mMaxRegionsAf, " AE=", mMaxRegionsAe,
        " AWB=", mMaxRegionsAwb);

//    Log.d(TAG, "Supported edge modes: ", Arrays.toString(
//        mCharacts.get(CameraCharacteristics.EDGE_AVAILABLE_EDGE_MODES)));
//    Log.d(TAG, "Supported tonemapping modes: ", Arrays.toString(
//        mCharacts.get(CameraCharacteristics.TONEMAP_AVAILABLE_TONE_MAP_MODES)));
//    Log.d(TAG, "Supported noise reduction modes: ", Arrays.toString(
//        mCharacts.get(CameraCharacteristics.NOISE_REDUCTION_AVAILABLE_NOISE_REDUCTION_MODES)));
  }

  /**
   * Submits a request to take a single photo.
   *
   * @param imageConfigIdx    The index of the image config to use for this request. Must be a
   *                          valid index into the {@link Parameters#imageConfigs} specified when
   *                          creating this controller.
   * @param reuseLockedParams Reuse locked AF, AE and/or AWB parameters from the previous capture.
   *                          Only meaningful if this is not the first capture through this
   *                          controller and for modes which are set to auto.
   * @param callback          Called to notify about the outcome of this request.
   */
  public void takePhoto(int imageConfigIdx, boolean reuseLockedParams,
                        @NonNull ActionCallback callback) {
    takePhotoSequence(imageConfigIdx, 1, 1., reuseLockedParams, callback);
  }

  /**
   * Submits a request to take a sequence of photos.
   *
   * @param imageConfigIdx    The index of the image config to use for this request. Must be a
   *                          valid index into the {@link Parameters#imageConfigs} specified when
   *                          creating this controller.
   * @param count             The number of capture requests to submit. 0 for infinite, the sequence
   *                          must be stopped explicitly.
   * @param fps               The target frame rate in seconds.
   * @param reuseLockedParams Reuse locked AF, AE and/or AWB parameters from the previous capture.
   *                          Only used for modes which are set to auto.
   * @param callback          Called to notify about the outcome of this request.
   */
  public void takePhotoSequence(int imageConfigIdx, long count, double fps,
                                boolean reuseLockedParams, @NonNull ActionCallback callback) {
    if (mState == State.Closed || mState == State.Closing) {
      Log.w(TAG, "Cannot take photo, controller is closed");
      callback.onActionCompleted(false, null, null, null);
      return;
    }

    if (mParams.captureMode != CaptureMode.Photo) {
      Log.e(TAG, "Cannot take a photo with an instance configured for video");
      callback.onActionCompleted(false, null, null, null);
      return;
    }

    if (imageConfigIdx < 0 || imageConfigIdx >= mParams.imageConfigs.size()) {
      Log.e(TAG, "Cannot take photo, invalid image config");
      callback.onActionCompleted(false, null, null, null);
      return;
    }

    // Check sequence params.
    final boolean isSequenceRequest = count == 0 || count > 1;
    if (count < 0) {
      Log.e(TAG, "The sequence count cannot be negative");
      callback.onActionCompleted(false, null, null, null);
      return;
    } else if (isSequenceRequest && mCaptureSequenceLength >= 0) {
      Log.i(TAG, "Cannot start a new capture sequence while another one is ongoing");
      callback.onActionCompleted(false, null, null, null);
      return;
    } else if (isSequenceRequest && fps <= 0.) {
      Log.e(TAG, "The repeating capture FPS cannot be <= 0");
      callback.onActionCompleted(false, null, null, null);
      return;
    }

    // Prepare the capture data.
    CaptureData data;
    if (isSequenceRequest) { // Initialise the capture sequence.
      data = new CaptureData(callback, new int[]{imageConfigIdx}, reuseLockedParams, reuseLockedParams, false);
      data.captureIdx = 0;
      mCaptureSequenceLength = count;
      mCaptureSequenceFPS = fps;
    } else {
      data = new CaptureData(callback, new int[]{imageConfigIdx}, true, reuseLockedParams, false);
    }

    mCaptureRequests.add(data);
    processNextRequestIfReady();
  }

  /**
   * Stops an ongoing capture sequence.
   */
  public void stopPhotoSequence() {
    if (mCaptureSequenceLength < 0) {
      Log.d(TAG, "No capture sequence to stop");
      return;
    }

    Log.d(TAG, "Stopping the capture sequence");
    mCaptureSequenceLength = -1;
  }

  /**
   * Submits a request to record a video. Only one request can be submitted to every controller
   * instance.
   *
   * @param imageConfigIdx     The indices of the still image configs to use for this request. May
   *                           be null or empty, but if not the elements must be unique, valid
   *                           indices into the {@link Parameters#imageConfigs} specified when
   *                           creating this controller.
   * @param skipWaitToConverge If true and when in auto-mode, AE and AWB do not wait until values
   *                           have converged before proceeding.
   * @param callback           Called to notify about the outcome of this request.
   */
  public void recordVideo(@Nullable int[] imageConfigIdx, boolean skipWaitToConverge,
                          @NonNull ActionCallback callback) {
    if (mState == State.Closed || mState == State.Closing) {
      Log.w(TAG, "Cannot record video, controller is closed");
      callback.onActionCompleted(false, null, null, null);
      return;
    }

    if (mParams.captureMode != CaptureMode.Video) {
      Log.e(TAG, "Cannot take a video with an instance configured for photos");
      callback.onActionCompleted(false, null, null, null);
      return;
    }

    // Only one video can be recorded per intance, because the media recorder has to be reset
    // between videos, which means a new surface needs to be retrieved from it and in turn a new
    // camera capture session must be created.
    if (mVideoRequestReceived) {
      Log.e(TAG, "Cannot record more than one video per instance");
      callback.onActionCompleted(false, null, null, null);
      return;
    }
    mVideoRequestReceived = true;

    if (imageConfigIdx == null) imageConfigIdx = new int[0];

    mCaptureRequests.add(
        new CaptureData(callback, imageConfigIdx, false, false, skipWaitToConverge));
    processNextRequestIfReady();
  }

  /**
   * Stops an ongoing video recording.
   */
  public void stopVideo() {
    if (!mVideoRequestReceived || mState != State.Capturing || mRecordingStopped) {
      if (!mRecordingStopped) Log.d(TAG, "No video to stop");
      return;
    }

    stopVideoCapture();
  }

  /**
   * Enters state {@link State#Closing} and frees all resources. All further capture requests on
   * this instance will fail.
   * <p>
   * This function is idempotent.
   */
  public void close() {
    if (mState == State.Closed || mState == State.Closing) return;

    mState = State.Closing;
    mRecordingStopped = true;

    if (mCameraDevice != null) {
      mCameraDevice.close();
      mCameraDevice = null;
    }
    mCaptureSession = null; // Closed with the camera.

    Log.d(TAG, "Closing the camera");
    int remainingListeners = mCaptureRequests.size() + mInFlight.size();
    if (remainingListeners > 0) {
      Log.d(TAG, "Remaining listeners to notify: ", remainingListeners);
    }

    // Notify any remaining listeners about their failed requests.
    for (CaptureData data : mCaptureRequests) {
      data.callback.onActionCompleted(false, null, null, null);
    }
    mCaptureRequests.clear();
    for (int i = 0; i < mInFlight.size(); i++) {
      CaptureData data = mInFlight.valueAt(i);
      data.callback.onActionCompleted(false, null, null, null);
      // Try closing images that have already been captured and not yet closed.
      for (int j = 0; j < data.images.length; j++) {
        ImageWrapper image = data.images[j];
        if (image != null) {
          image.close();
          data.images[j] = null;
        }
      }
    }
    mInFlight.clear();
  }

  /**
   * Checks if the camera supports the requested {@link ImageConfig#format} and falls back on
   * {@link ImageFormat#JPEG} if it doesn't.
   *
   * @return The selected format.
   */
  private int selectFormat(final ImageConfig config) {
    int requested = config.format;
    final int[] formats = mStreamConfigs.getOutputFormats();
    for (int f : formats) {
      if (f == requested) {
        return f;
      }
    }
    Log.w(TAG, "Requested format not supported, using JPEG");
    config.format = ImageFormat.JPEG;
    return ImageFormat.JPEG;
  }

  /**
   * Returns all supported sizes for format.
   */
  private List<Size> supportedSizes(final int format) {
    Size[] highFPS = mStreamConfigs.getOutputSizes(format);
    Size[] highRes = mStreamConfigs.getHighResolutionOutputSizes(format);
    final ArrayList<Size> sizes = new ArrayList<>(
        (highFPS == null ? 0 : highFPS.length) + (highRes == null ? 0 : highRes.length));
    if (highFPS != null) sizes.addAll(Arrays.asList(highFPS));
    if (highRes != null) sizes.addAll(Arrays.asList(highRes));
    return sizes;
  }

  /**
   * Returns the largest supported resolution for the given format with the requested
   * {@link ImageConfig#aspectRatio} and larger dimension smaller or equal to
   * {@link ImageConfig#maxDimension}.
   * <p>
   * The smallest supported size is returned if no resolution matches the requirements.
   *
   * @param format The format for which to select the size.
   * @param config The config.
   * @return The selected output dimensions.
   */
  private Size selectSize(final int format, final ImageConfig config) {
    // Sort the supported sizes in ascending order by pixel count.
    final List<Size> sizes = supportedSizes(format);
    Collections.sort(sizes, (l, r) -> {
      int lArea = l.getWidth() * l.getHeight();
      int rArea = r.getWidth() * r.getHeight();
      return Integer.compare(lArea, rArea);
    });

    // Use int rather than float comparisons for aspect ratio.
    final int ratio43 = 4 * 10 / 3;
    final int ratio169 = 16 * 10 / 9;
    int aspectRatio;
    if ("4:3".equals(config.aspectRatio)) {
      aspectRatio = ratio43;
    } else if ("16:9".equals(config.aspectRatio)) {
      aspectRatio = ratio169;
    } else {
      Log.w(TAG, "Invalid aspect ratio ", config.aspectRatio, ", falling back to 4:3");
      config.aspectRatio = "4:3";
      aspectRatio = ratio43;
    }

    // Select the largest size <= the requested size and with the desired ratio.
    Size selected = null;
    for (Size s : sizes) {
      int max = Math.max(s.getWidth(), s.getHeight());
      int min = Math.min(s.getWidth(), s.getHeight());
      int ratio = max * 10 / min;

      if (ratio == aspectRatio && max <= config.maxDimension) {
        selected = s;
      } else if (max > config.maxDimension) {
        break;
      }
    }

    if (selected == null) { // Use the smallest supported size.
      selected = sizes.get(0);
      Log.d(TAG, "Using smallest supported image size: ", selected);
    }
    int max = Math.max(selected.getWidth(), selected.getHeight());
    int min = Math.min(selected.getWidth(), selected.getHeight());
    int selectedRatio = max * 10 / min;
    config.maxDimension = max;
    config.aspectRatio = selectedRatio == ratio169 ? "16:9" : "4:3";

    return selected;
  }

  /**
   * Selects the largest preview resolution smaller than or equal to {@link #sConfigPreviewSize}.
   *
   * @return The selected size.
   */
  private Size selectPreviewSize() {
    final Size[] sizes = mStreamConfigs.getOutputSizes(SurfaceTexture.class);
    if (sizes == null || sizes.length == 0) {
      Log.e(TAG, "Cannot create preview for output type SurfaceTexture");
      close();
      return new Size(0, 0);
    }

    // Sort by resolution in descending order and pick the largest one <= the requested size.
    Arrays.sort(sizes, (l, r) ->
        Long.signum((long) r.getWidth() * r.getHeight() - (long) l.getWidth() * l.getHeight()));
    for (Size size : sizes) {
      if (size.getWidth() <= sConfigPreviewSize.getWidth()
          && size.getHeight() <= sConfigPreviewSize.getHeight()) {
        return size;
      }
    }
    return sizes[sizes.length - 1]; // Fallback to the smallest supported size.
  }

  /**
   * Determines session settings and creates the session.
   */
  private void createSession() {
    if (mState != State.Initialising) {
      Log.e(TAG, "Invalid entry state for createSession: ", mState.name());
      close();
      return;
    }
    if (mCameraDevice == null) {
      Log.e(TAG, "Camera is null in createSession");
      close();
      return;
    }

    // All surfaces available for use with the session.
    final int numImageConfigs = mParams.imageConfigs.size();
    final List<Surface> surfaces = new ArrayList<>(2 + numImageConfigs);

    // Create the preview surface.
    Size previewSize = selectPreviewSize();
    try {
      mPreviewSurfaceTex = new SurfaceTexture(1); // Dummy tex name, will not be bound.
      mPreviewSurfaceTex.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());
      mPreviewSurface = new Surface(mPreviewSurfaceTex);
      surfaces.add(mPreviewSurface);
    } catch (Surface.OutOfResourcesException e) {
      Log.w(TAG, "Cannot create the surface texture: ", e.getMessage());
      close();
      return;
    }

    // Create and configure the output surfaces.
    mImageSurfaces = new ArrayList<>(numImageConfigs);
    mImageSurfaceToConfigIdx = new HashMap<>(numImageConfigs);
    switch (mParams.captureMode) {
      case Photo:
        configurePhotoBuffers(surfaces);
        break;
      case Video:
        // Prepare video capture.
        if (!configureVideoBuffers(surfaces)) return;

        // Also configure image buffers if there are still image configs.
        if (!mParams.imageConfigs.isEmpty()) {
          configurePhotoBuffers(surfaces);
        }
        break;
    }

    try {
      mCameraDevice.createCaptureSession(surfaces, new SessionStateCallback(), mEventHandler);
    } catch (CameraAccessException | IllegalStateException e) {
      Log.w(TAG, "Failed to create the capture session: ", e.getMessage());
      close();
    }
  }

  /**
   * Configures photo buffers for a new session.
   *
   * @param surfaces New output surfaces will be added to this list.
   */
  private void configurePhotoBuffers(final List<Surface> surfaces) {
    mImageReaders = new ArrayList<>(mParams.imageConfigs.size());
    for (int i = 0; i < mParams.imageConfigs.size(); i++) {
      final ImageConfig cfg = mParams.imageConfigs.get(i);
      int captureFormat = selectFormat(cfg);
      Size captureSize = selectSize(captureFormat, cfg);

      ImageReader reader = ImageReader.newInstance(captureSize.getWidth(),
          captureSize.getHeight(), captureFormat, cfg.imageBuffers);
      reader.setOnImageAvailableListener(this::onImageAvailable, mEventHandler);
      mImageReaders.add(reader);

      Surface surface = reader.getSurface();
      mImageSurfaces.add(surface);
      mImageSurfaceToConfigIdx.put(surface, i);
      surfaces.add(surface);

      Log.d(TAG, "Configuring session for images of size ", captureSize.getWidth(), "x",
          captureSize.getHeight(), " and format ", captureFormat);
    }
  }

  /**
   * Configures video buffers and the {@link #mRecorder} for a new session.
   *
   * @param surfaces New output surfaces will be added to this list.
   * @return True on success, false on failure.
   */
  private boolean configureVideoBuffers(final List<Surface> surfaces) {
    // Validate params.
    boolean hasError = false;
    if (mParams.videoBitrate <= 0) {
      Log.w(TAG, "Invalid video bitrate: ", mParams.videoBitrate);
      hasError = true;
    }
    if (mParams.videoCaptureDuration <= 0 || mParams.videoPlaybackFPS <= 0) {
      Log.w(TAG, "Invalid video capture duration / playback fps: ",
          mParams.videoCaptureDuration, " / ", mParams.videoPlaybackFPS);
      hasError = true;
    }
    if (hasError) {
      close();
      return false;
    }

    // Configure the media recorder.
    mRecorder = new MediaRecorder();
    mRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);

    // Set the recording profile.
//        if (!CamcorderProfile.hasProfile(CamcorderProfile.QUALITY_TIME_LAPSE_1080P)) {
//          Log.w(TAG, "The camera does not support profile QUALITY_TIME_LAPSE_1080P");
//          close();
//          return false;
//        }
//        CamcorderProfile profile = CamcorderProfile.get(CamcorderProfile.QUALITY_TIME_LAPSE_1080P);
//        mRecorder.setProfile(profile);
//        Log.d(TAG, "Using camcorder profile with format=", profile.fileFormat,
//            " width=", profile.videoFrameWidth, " height=" + profile.videoFrameHeight,
//            " fps=", profile.videoFrameRate, " codec=", profile.videoCodec,
//            " bitrate=", profile.videoBitRate);
    mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
    mRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
    mRecorder.setVideoSize(mParams.videoWidth, mParams.videoHeight);
    mRecorder.setVideoEncodingBitRate(mParams.videoBitrate);

    String outPath = Settings.getAppFolderPath() + Settings.APP_DIR_TMP + "/timelapse.mp4";
    mVideoOut = new File(outPath);
    mRecorder.setOutputFile(outPath);

    // Set the recording and playback frame rates and duration.
    final int playbackMillis = (int)
        (mParams.videoCaptureDuration * mParams.videoCaptureFPS / mParams.videoPlaybackFPS);
    mRecorder.setVideoFrameRate(mParams.videoPlaybackFPS);
    mRecorder.setCaptureRate(mParams.videoCaptureFPS);
    mRecorder.setMaxDuration(playbackMillis);
    mRecorder.setMaxFileSize(10L * 1024L * 1024L);

    final int rotation = getRotation();
    mRecorder.setOrientationHint(rotation);

    mRecorder.setOnErrorListener((mr, what, extra) -> {
      Log.w(TAG, "The media recorder failed with error=", what, " and extra=", extra);
      close();
    });
    mRecorder.setOnInfoListener((mr, what, extra) -> {
      switch (what) {
        case MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED:
          if (!mRecordingStopped) {
            Log.d(TAG, "Max video duration reached");
            stopVideoCapture();
          }
          break;
        case MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED:
          if (!mRecordingStopped) {
            Log.d(TAG, "Max video file size reached");
            stopVideoCapture();
          }
          break;
      }
    });

    Log.d(TAG, "Preparing to record video for ", mParams.videoCaptureDuration, "ms with reso ",
        mParams.videoWidth, "x", mParams.videoHeight, "; bit rate ", mParams.videoBitrate,
        "; capture/playback fps ", mParams.videoCaptureFPS, "/", mParams.videoPlaybackFPS,
        "; orientation ", rotation);
    try {
      mRecorder.prepare();
    } catch (IOException e) {
      Log.w(TAG, "Failed to prepare the media recorder: ", e.getMessage());
      close();
      return false;
    }

    Surface surface = mRecorder.getSurface();
    surfaces.add(surface);
    mVideoSurface = surface;

    return true;
  }

  /**
   * Sets the focus, white-balance and exposure control modes based on the settings and device
   * capabilities.
   *
   * @param builder The request builder to update.
   */
  private void setControlMode(CaptureRequest.Builder builder) {
    builder.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_AUTO);
    setFocusMode(builder);
    setExposureMode(builder);
    builder.set(CaptureRequest.CONTROL_AWB_MODE, CaptureRequest.CONTROL_AWB_MODE_AUTO);
  }

  /**
   * Sets the focus mode based on the settings and device capabilities.
   *
   * @param builder The request builder to update.
   */
  private void setFocusMode(CaptureRequest.Builder builder) {
    // Cannot set a focus mode other than fixed if the lens is not variable.
    if (!mHasVariableLens) {
      if (mParams.focusMode != FocusMode.FIXED) {
        Log.w(TAG, "Lens is fixed, cannot use focus mode: ", mParams.focusMode);
        mParams.focusMode = FocusMode.FIXED;
      }
      return;
    }

    switch (mParams.focusMode) {
      case AUTO: {
        if (mParams.captureMode == CaptureMode.Photo) {
          // Set continuous AF if supported and basic AF otherwise.
          // NOTE: Continuous AF keeps ending up in state CONTROL_AF_STATE_NOT_FOCUSED_LOCKED. Regular
          // AF takes longer, but ends in CONTROL_AF_STATE_FOCUSED_LOCKED. This does not always appear
          // to be true, though, so there may be no advantage.
//        if (supports(CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES,
//            CameraCharacteristics.CONTROL_AF_MODE_CONTINUOUS_PICTURE)) {
//          builder.set(CaptureRequest.CONTROL_AF_MODE,
//              CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
//        } else {
          builder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO);
//          Log.w(TAG, "AF_MODE_CONTINUOUS_PICTURE not supported, using AF_MODE_AUTO");
//        }
        } else if (mParams.captureMode == CaptureMode.Video) {
          builder.set(CaptureRequest.CONTROL_AF_MODE,
              CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_VIDEO);
        }
        break;
      }
      case FIXED: {
        break; // Do nothing.
      }
      case INFINITY: {
        if (mHasManualSensor) {
          builder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_OFF);
          builder.set(CaptureRequest.LENS_FOCUS_DISTANCE, 0f);
        } else {
          Log.w(TAG, "The camera does not support manual sensor settings");
          mParams.focusMode = FocusMode.AUTO;
          setFocusMode(builder);
          return;
        }
        break;
      }
    }
  }

  /**
   * Sets the exposure mode based on the settings and device capabilities.
   *
   * @param builder The request builder to update.
   */
  private void setExposureMode(CaptureRequest.Builder builder) {
    switch (mParams.exposureMode) {
      case AUTO:
        builder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
        break;
      case MANUAL:
        if (mHasManualSensor && mSensitivityRange != null && mExposureTimeRange != null) {
          builder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_OFF);

          mParams.sensitivity = Math.max(
              Math.min(mParams.sensitivity, mSensitivityRange.getUpper()),
              mSensitivityRange.getLower());
          builder.set(CaptureRequest.SENSOR_SENSITIVITY, mParams.sensitivity);

          // Fix: The upper end of the exposure range is incorrect. From empirical tests it's either
          // 500ms or 688ms on the S7, depending on the sensor. Requesting a larger value than
          // supported leads to the capture results reporting 33ms, although the true exposure time
          // appears to be even shorter. Clamp it to 688ms max.
          mParams.exposureTime = Math.max(
              Math.min(mParams.exposureTime, 688 * 1000L * 1000L), //mExposureTimeRange.getUpper()),
              mExposureTimeRange.getLower());
          builder.set(CaptureRequest.SENSOR_EXPOSURE_TIME, mParams.exposureTime);
        } else {
          Log.w(TAG, "The camera does not support manual exposure");
          mParams.exposureMode = ExposureMode.AUTO;
          setExposureMode(builder);
          return;
        }
        break;
    }
  }

  /**
   * Sets the metering area of the given type in builder to the rect as defined by
   * {@link #imageRectToMeteringRect}.
   */
  private void setMeteringArea(@NonNull CaptureRequest.Builder builder,
                               @NonNull CaptureRequest.Key<MeteringRectangle[]> type,
                               @Nullable Rect rect) {
    MeteringRectangle r = imageRectToMeteringRect(rect, MeteringRectangle.METERING_WEIGHT_MAX);
    if (r != null) {
      builder.set(type, new MeteringRectangle[]{r});

      String typeStr = "Unknown";
      if (type == CaptureRequest.CONTROL_AF_REGIONS) typeStr = "AF";
      else if (type == CaptureRequest.CONTROL_AE_REGIONS) typeStr = "AE";
      else if (type == CaptureRequest.CONTROL_AWB_REGIONS) typeStr = "AWB";
      Log.d(TAG, typeStr, " metering area (sensor space): ", r);
    }
  }

  /**
   * Checks if a particular mode is in the list of supported characteristics returned for key.
   *
   * @param key  The key for the list of supported modes to consult.
   * @param mode The mode to look for.
   * @return True if the mode is supported.
   */
  private boolean supports(final CameraCharacteristics.Key<int[]> key, int mode) {
    if (mCharacts == null) return false;

    int[] modes = mCharacts.get(key);
    if (modes == null) return false;

    for (int v : modes) {
      if (v == mode) return true;
    }
    return false;
  }

  /**
   * Takes a rectangle of offsets relative to the top-left corner of the final, rotated image,
   * expressed as percentages of its width and height, and returns the corresponding
   * {@link MeteringRectangle} in the sensor's coordinate system.
   *
   * @param r      The input rectangle.
   * @param weight The weight assigned to the metering area.
   * @return The metering rectangle, or null if values are invalid.
   */
  @Nullable
  private MeteringRectangle imageRectToMeteringRect(@Nullable Rect r, int weight) {
    if (r == null || mActiveArraySize == null) return null;
    //Log.d(TAG, "Metering area (input): ", r.flattenToString());

    // Copy the rect and shift the values so that they are centred on (0,0), assuming they are
    // correctly set as percentage values in [0, 100].
    r = new Rect(r.left - 50, r.top - 50, r.right - 50, r.bottom - 50);

    // Transform the rect into the coordinate system of the sensor.
    final int orientation = getRotation();
    r = rotate(r, (360 - orientation) / 90);

    // Shift back and this time ensure the resulting range is in [0,100].
    r.left = clamp(r.left + 50, 0, 100);
    r.top = clamp(r.top + 50, 0, 100);
    r.right = clamp(r.right + 50, 0, 100);
    r.bottom = clamp(r.bottom + 50, 0, 100);

    if (r.isEmpty()) return null;
    //Log.d(TAG, "Metering area (rotated): ", r.flattenToString());

    // Treat the rect as percentages into the active array size.
    float width = mActiveArraySize.width();
    float height = mActiveArraySize.height();
    return new MeteringRectangle(
        (int) (r.left / 100.f * width), (int) (r.top / 100.f * height),
        (int) (r.width() / 100.f * width), (int) (r.height() / 100.f * height),
        clamp(weight,
            MeteringRectangle.METERING_WEIGHT_MIN, MeteringRectangle.METERING_WEIGHT_MAX));
  }

  /**
   * Rotates rectangle r clockwise by the requested number of quadrants.
   *
   * @param r         The rectangle.
   * @param quadrants The number of quadrants to rotate clockwise.
   * @return The rotated rectangle, or a reference to r if quadrants is zero or a multiple of 4.
   */
  private Rect rotate(@NonNull final Rect r, final int quadrants) {
    if (quadrants % 4 == 0) return r;

    // Rotate by quadrants x 90 degrees.
    final Matrix mat = new Matrix();
    mat.setRotate(quadrants * 90.f);
    final RectF rf = new RectF(r);
    mat.mapRect(rf);
    return new Rect(Math.round(rf.left), Math.round(rf.top),
        Math.round(rf.right), Math.round(rf.bottom));
  }

  /**
   * Clamps val into range [min, max].
   *
   * @return The clamped value.
   */
  private int clamp(int val, int min, int max) {
    val = val < min ? min : val;
    val = val > max ? max : val;
    return val;
  }

  /**
   * Prepares preview and enters {@link State#Waiting} when ready.
   */
  private void preparePreview() {
    if (mState != State.Initialising) {
      Log.e(TAG, "Invalid entry state to preview: ", mState.name());
      close();
      return;
    }

    try {
      // Create the request builder and set the target surface.
      mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
      mPreviewBuilder.addTarget(mPreviewSurface);

      // Configure the preview request.
      setControlMode(mPreviewBuilder);

      // Set the metering areas for AF, AE and AWB if given and supported.
      if (mParams.focusMode == FocusMode.AUTO && mMaxRegionsAf > 0) {
        setMeteringArea(mPreviewBuilder, CaptureRequest.CONTROL_AF_REGIONS, mParams.afArea);
      }
      if (mParams.exposureMode == ExposureMode.AUTO && mMaxRegionsAe > 0) {
        setMeteringArea(mPreviewBuilder, CaptureRequest.CONTROL_AE_REGIONS, mParams.aeArea);
      }
      if (mMaxRegionsAwb > 0) {
        setMeteringArea(mPreviewBuilder, CaptureRequest.CONTROL_AWB_REGIONS, mParams.awbArea);
      }

      mPreviewRequest = mPreviewBuilder.build();
      mState = State.Waiting;
      Log.d(TAG, "Preview prepared");
    } catch (CameraAccessException | IllegalStateException | IllegalArgumentException e) {
      Log.w(TAG, "Failed to prepare preview: ", e.getMessage());
      close();
    }
  }

  /**
   * Schedules the follow-up frame to previousFrame in a capture sequence.
   *
   * @param previousFrame           The frame on which to base the newly schedule frame.
   * @param fps                     The frame rate at which frames are captured.
   * @param referenceRealtimeMillis The reference time in {@link SystemClock#elapsedRealtime()}
   *                                when the capture sequence started.
   */
  private void scheduleNextFrame(@NonNull CaptureData previousFrame, final double fps,
                                 final long referenceRealtimeMillis) {
    if (mCaptureSequenceLength < 0) {
      Log.d(TAG, "Not scheduling any more frames, the repeating capture has been stopped");
      return;
    } else if (previousFrame.captureIdx < 0) {
      Log.e(TAG, "The previous frame is not part of a capture sequence");
      return;
    }

    final CaptureData data = new CaptureData(previousFrame);
    data.captureIdx = previousFrame.captureIdx + 1;

    // Work out when the frame should be scheduled according to the reference time and frame count.
    double interval = 1000. / fps;
    long targetTime = referenceRealtimeMillis + (long) (data.captureIdx * interval);
    data.notAfter = referenceRealtimeMillis + (long) ((data.captureIdx + 1) * interval);

    long now = SystemClock.elapsedRealtime();
    if (targetTime < now) {
      // The timer did not go off in time and we missed a frame. Drop it.
      Log.d(TAG, "Not scheduling frame ", data.captureIdx, ", it was due to be captured ",
          (now - targetTime), "ms ago");
      data.frameDropped = true;
      data.callback.onActionCompleted(false, null, data, null);

      // Schedule the next frame instead.
      if (mCaptureSequenceLength == 0 || (data.captureIdx + 1) < mCaptureSequenceLength) {
        scheduleNextFrame(data, fps, referenceRealtimeMillis);
      } else {
        mCaptureSequenceLength = -1; // Sequence finished.
      }
      return;
    }

    // Schedule the frame.
    long delay = targetTime - now;
    mEventHandler.postDelayed(() -> {
      if (mCaptureSequenceLength < 0) return;

      mCaptureRequests.add(data);
      processNextRequestIfReady();
      if (mCaptureSequenceLength == 0 || (data.captureIdx + 1) < mCaptureSequenceLength) {
        scheduleNextFrame(data, fps, referenceRealtimeMillis);
      } else {
        mCaptureSequenceLength = -1; // Sequence finished.
      }
    }, delay);
    Log.d(TAG, "Next frame scheduled for ", delay, "ms from now");
  }

  /**
   * Does one of the following things in order of preference:
   * <p>
   * a) If the next request has expired, it is dropped.
   * <p>
   * b) If ready to process the next request, it resumes preview (see {@link #resumePreview()} or
   * immediately starts capture, depending on the request config.
   * <p>
   * c) Enters {@link State#Waiting} if the transition is possible.
   * <p>
   * d) Nothing.
   */
  private void processNextRequestIfReady() {
    if (mState != State.Waiting && mState != State.Capturing) {
      // Ignore. Other states should eventually lead to another invokation or close the controller.
      return;
    }
    if (mRecordingStopped) {
      return; // Only one video per instance supported.
    }

    // Check if a new capture request is ready to be processed.
    final CaptureData data = mCaptureRequests.peek();
    if (data != null) {
      // Check if the request has expired.
      long now;
      if (data.notAfter > 0 && (now = SystemClock.elapsedRealtime()) > data.notAfter) {
        Log.d(TAG, "Dropping request, it expired ", (now - data.notAfter), "ms ago");
        mCaptureRequests.poll();
        data.frameDropped = true;
        data.callback.onActionCompleted(false, null, data, null);
        processNextRequestIfReady();
        return;
      }

      // Determine if this is the second or later request in a sequence.
      final boolean isSequenceRequest = data.captureIdx > 0;

      // Check if all image buffers required to proceed are available and abort if any are not.
      boolean buffersReserved = true;
      for (int i = 0; i < data.imageConfigIdx.length; i++) {
        data.imageBufferReserved[i] = mConfigImageBuffers.get(data.imageConfigIdx[i]).tryAcquire();
        buffersReserved = buffersReserved && data.imageBufferReserved[i];
        if (!data.imageBufferReserved[i]) {
          // Release all already reserved buffers.
          for (int j = i - 1; j >= 0; j--) {
            mConfigImageBuffers.get(data.imageConfigIdx[j]).release();
            data.imageBufferReserved[j] = false;
          }
          break;
        }
      }

      if (buffersReserved) {
        // Update the request timestamp.
        if (mParams.captureMode == CaptureMode.Photo
            || mParams.captureMode == CaptureMode.Video && mParams.videoCaptureDuration <= 20000) {
          // TODO The hmon must be made more flexible to deal with video requests of longer
          // durations, as the current implementation uses a fixed 30 sec timeout. The expected
          // request duration should be configurable.
          HealthCheck.getInstance().updateTimestamp(HealthCheck.Timestamp.PhotoRequest);
        }

        if (isSequenceRequest) {
          mCaptureRequests.poll();
          capturePhoto(data); // Only photo requests can be sequence requests.
        } else {
          resumePreview();
        }
      } else { // No free image buffers.
        Log.d(TAG, "Deferring request, no free buffers");
        if (mState == State.Capturing && mInFlight.size() == 0) {
          mState = State.Waiting;
        }
      }
    } else if (mState == State.Capturing && mInFlight.size() == 0) {
      mState = State.Waiting;
      Log.d(TAG, "Waiting for request");
    }
  }

  /**
   * Enters state {@link State#Preview}. {@link #preparePreview()} must have already been called.
   */
  private void resumePreview() {
    if (mState != State.Waiting && mState != State.Capturing) {
      Log.e(TAG, "Invalid entry state for resumePreview: ", mState.name());
      close();
      return;
    }

    try {
      mCaptureSession.setRepeatingRequest(mPreviewRequest, mPreviewCaptureCallback, mEventHandler);
      mState = State.Preview;
      Log.d(TAG, "Starting preview");
    } catch (CameraAccessException | IllegalStateException | IllegalArgumentException e) {
      Log.w(TAG, "Failed to start preview: ", e.getMessage());
      close();
    }
  }

  /**
   * Initiates the auto-focus routine and enters {@link State#LensChanging}.
   */
  private void startAutoFocus() {
    if (mState != State.Preview) {
      Log.e(TAG, "Invalid entry state for startAutoFocus: ", mState.name());
      close();
      return;
    }

    try {
      mPreviewBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
          CaptureRequest.CONTROL_AF_TRIGGER_START);
      mCaptureSession.capture(mPreviewBuilder.build(), mPreviewCaptureCallback, mEventHandler);
      mState = State.LensChanging;
      Log.d(TAG, "AF trigger start (locking)");

      // Reset the builder's value to avoid restarting AF in the next request.
      mPreviewBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
          CaptureRequest.CONTROL_AF_TRIGGER_IDLE);
    } catch (CameraAccessException | IllegalStateException | IllegalArgumentException e) {
      Log.w(TAG, "Failed to submit AF lock request: ", e.getMessage());
      close();
    }
  }

  /**
   * Starts the exposure precapture sequences and enters state {@link State#Exposure}.
   */
  private void runPrecaptureSequence() {
    if (mState != State.Preview && mState != State.LensChanging) {
      Log.e(TAG, "Invalid entry state for runPrecaptureSequence: ", mState.name());
      close();
      return;
    }

    try {
      mPreviewBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
          CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
      mCaptureSession.capture(mPreviewBuilder.build(), mPreviewCaptureCallback, mEventHandler);
      mState = State.Exposure;
      Log.d(TAG, "Starting AE precapture");

      // Reset the builder's value to avoid restarting AE in the next request.
      mPreviewBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
          CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_IDLE);
    } catch (CameraAccessException | IllegalStateException | IllegalArgumentException e) {
      Log.w(TAG, "Failed to submit precapture request: ", e.getMessage());
      close();
    }
  }

  /**
   * Locks the current exposure settings. Does not perform any state transitions.
   */
  private void lockExposure() {
    if (mState != State.Exposure) {
      Log.e(TAG, "Invalid entry state for lockExposure: ", mState.name());
      close();
      return;
    }

    try {
      // The lock must remain true for preview requests until it is meant to be unlocked.
      mPreviewBuilder.set(CaptureRequest.CONTROL_AE_LOCK, true);
      mPreviewRequest = mPreviewBuilder.build();
      mCaptureSession.setRepeatingRequest(mPreviewRequest, mPreviewCaptureCallback, mEventHandler);
      Log.d(TAG, "Locking AE");
    } catch (CameraAccessException | IllegalStateException | IllegalArgumentException e) {
      Log.w(TAG, "Failed to submit AE lock request: ", e.getMessage());
      close();
    }
  }

  /**
   * Locks the current white balance settings. Does not perform any state transitions.
   */
  private void lockWhiteBalance() {
    if (mState != State.WhiteBalance) {
      Log.e(TAG, "Invalid entry state for lockWhiteBalance: ", mState.name());
      close();
      return;
    }

    try {
      // The lock must remain true for preview requests until it is meant to be unlocked.
      mPreviewBuilder.set(CaptureRequest.CONTROL_AWB_LOCK, true);
      mPreviewRequest = mPreviewBuilder.build();
      mCaptureSession.setRepeatingRequest(mPreviewRequest, mPreviewCaptureCallback, mEventHandler);
      Log.d(TAG, "Locking AWB");
    } catch (CameraAccessException | IllegalStateException | IllegalArgumentException e) {
      Log.w(TAG, "Failed to submit AWB lock request: ", e.getMessage());
      close();
    }
  }

  /**
   * Unlocks focus, exposure and white balance. Does not perform any state transitions.
   */
  private void unlockAfAeAwb() {
    if (mState != State.Preview) {
      Log.e(TAG, "Invalid entry state for unlockAfAeAwb: ", mState.name());
      close();
      return;
    }

    try {
      // Unlock the locks and cancel the triggers.
      mPreviewBuilder.set(CaptureRequest.CONTROL_AE_LOCK, false);
      mPreviewBuilder.set(CaptureRequest.CONTROL_AWB_LOCK, false);
      mPreviewBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
          CaptureRequest.CONTROL_AF_TRIGGER_CANCEL);
      mPreviewBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
          CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_CANCEL);
      mCaptureSession.capture(mPreviewBuilder.build(), mPreviewCaptureCallback, mEventHandler);
      mIsAFLocked = mIsAELocked = mIsAWBLocked = false;
      Log.d(TAG, "Unlocking AF, AE and AWB");

      // Change triggers to idle.
      mPreviewBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
          CaptureRequest.CONTROL_AF_TRIGGER_IDLE);
      mPreviewBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
          CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_IDLE);

      // The locks must remain false for preview requests until they are meant to be locked again.
      mPreviewRequest = mPreviewBuilder.build();
      mCaptureSession.setRepeatingRequest(mPreviewRequest, mPreviewCaptureCallback, mEventHandler);
    } catch (CameraAccessException | IllegalStateException | IllegalArgumentException e) {
      Log.w(TAG, "Failed to unlock AF, AE and AWB: ", e.getMessage());
      close();
    }
  }

  /**
   * Returns the clockwise rotation, in degrees, required to display images upright, taking both
   * device and sensor orientations into account.
   */
  private int getRotation() {
    //noinspection ConstantConditions
    int sensorOrientation = mCharacts.get(CameraCharacteristics.SENSOR_ORIENTATION);
    int deviceOrientation = PhoneOrientation.Rotation_0_90_180_270;
    return (deviceOrientation + sensorOrientation) % 360;
  }

  /**
   * Creates and initialises a capture request builder according to the config in data.
   *
   * @param data         The capture data for this request.
   * @param templateType The basic request template to start with, e.g.
   *                     {@link CameraDevice#TEMPLATE_STILL_CAPTURE}.
   * @return The pre-configured request builder, or null on error.
   */
  @Nullable
  private CaptureRequest.Builder initCaptureRequest(@NonNull final CaptureData data,
                                                    final int templateType) {
    // Create the request builder and set the target surface.
    CaptureRequest.Builder builder;
    try {
      builder = mCameraDevice.createCaptureRequest(templateType);
      if (mVideoSurface != null) builder.addTarget(mVideoSurface);
      for (int i = 0; i < data.imageConfigIdx.length; i++) {
        builder.addTarget(mImageSurfaces.get(data.imageConfigIdx[i]));
      }
    } catch (IllegalArgumentException | CameraAccessException | IllegalStateException e) {
      Log.w(TAG, "Failed to create capture request: ", e.getMessage());
      return null;
    }

    // Configure the request with the same modes as used during preview.
    setControlMode(builder);
    if (data.lockAEAndAWB) { // Set the AF and AWB lock flags.
      if (mParams.exposureMode == ExposureMode.AUTO) {
        builder.set(CaptureRequest.CONTROL_AE_LOCK, true);
      }
      builder.set(CaptureRequest.CONTROL_AWB_LOCK, true); // Always using AWB.
    }

    // Quality settings.
    ImageConfig config = mParams.captureMode == CaptureMode.Video && mParams.videoConfig != null
        ? mParams.videoConfig
        : mParams.imageConfigs.get(data.imageConfigIdx[0]);
    builder.set(CaptureRequest.EDGE_MODE, config.edgeMode);
    builder.set(CaptureRequest.NOISE_REDUCTION_MODE, config.noiseReductionMode);
    builder.set(CaptureRequest.TONEMAP_MODE, config.tonemapMode);

    // Format specific settings.
    switch (config.format) {
      case ImageFormat.JPEG:
        builder.set(CaptureRequest.JPEG_ORIENTATION, getRotation());
        builder.set(CaptureRequest.JPEG_QUALITY, (byte) config.jpegQuality);
        break;
      case ImageFormat.RAW_SENSOR:
        // See the DngCreator constructor documentation.
        builder.set(CaptureRequest.STATISTICS_LENS_SHADING_MAP_MODE,
            CaptureRequest.STATISTICS_LENS_SHADING_MAP_MODE_ON);
        break;
    }

    return builder;
  }

  /**
   * Submits the next pending capture request to the capture session and enters state
   * {@link State#Capturing}.
   *
   * @param data The capture data for this request.
   */
  private void capturePhoto(@NonNull final CaptureData data) {
    if (mState != State.Waiting && mState != State.WhiteBalance && mState != State.Capturing) {
      Log.e(TAG, "Invalid entry state for capturePhoto: ", mState.name());
      close();
      return;
    }

    // Create the request. Only repeating captures after the second in the sequence can reuse the
    // previous request.
    if (mCaptureRequest == null || data.captureIdx < 2) {
      CaptureRequest.Builder builder =
          initCaptureRequest(data, CameraDevice.TEMPLATE_STILL_CAPTURE);
      if (builder == null) {
        close();
        return;
      }

      mCaptureRequest = builder.build();
    }

    try { // Submit the request.
      int id = mCaptureSession.capture(mCaptureRequest, mCaptureCallback, mEventHandler);
      mState = State.Capturing;
      mInFlight.put(id, data);
      Log.d(TAG, "Starting capture");
    } catch (CameraAccessException | IllegalStateException | IllegalArgumentException e) {
      Log.w(TAG, "Failed to submit capture request: ", e.getMessage());
      close();
    }
  }

  /**
   * Submits the next pending capture request to the capture session and enters state
   * {@link State#Capturing}.
   *
   * @param data The capture data for this request.
   */
  private void captureVideo(@NonNull final CaptureData data) {
    if (mState != State.WhiteBalance) {
      Log.e(TAG, "Invalid entry state for captureVideo: ", mState.name());
      close();
      return;
    }

    // Create the request.
    CaptureRequest.Builder builder = initCaptureRequest(data, CameraDevice.TEMPLATE_RECORD);
    if (builder == null) {
      close();
      return;
    }

    try { // Start recording.
      mVideoSequenceId = mCaptureSession.setRepeatingRequest(
          builder.build(), mVideoCaptureCallback, mEventHandler);
      mState = State.Capturing;
      mInFlight.put(mVideoSequenceId, data);
      mRecorder.start();
      Log.d(TAG, "Starting video capture");
    } catch (CameraAccessException | IllegalStateException | IllegalArgumentException e) {
      Log.w(TAG, "Failed to submit capture request: ", e.getMessage());
      close();
    }

    // Set up a fallback timer to stop the video in case the MediaRecorder max duration callback
    // is not invoked, which sometimes happens.
    mEventHandler.postDelayed(() -> {
      if (!mRecordingStopped) {
        Log.d(TAG, "Max video duration exceeded");
        stopVideoCapture();
      }
    }, mParams.videoCaptureDuration);
  }

  /**
   * Stops video capture.
   */
  private void stopVideoCapture() {
    if (mState != State.Capturing) {
      Log.e(TAG, "Invalid entry state for stopVideoCapture: ", mState.name());
      close();
      return;
    }
    if (mRecorder == null) {
      Log.e(TAG, "Error: The recorder is null in stopVideoCapture");
      close();
      return;
    }

    // Stop recording.
    Log.d(TAG, "Stopping video capture");
    boolean success = false;
    try {
      mRecorder.stop();
      success = true;
    } catch (RuntimeException e) {
      if (mVideoOut != null && mVideoOut.exists()) {
        // Stop sometimes fails. Just clean up the video file.
        Log.d(TAG, "The video recorder may not have stopped cleanly");
        Helpers.deleteFile(mVideoOut);
      }
      mVideoOut = null;
    }
    mRecordingStopped = true;

    // Stop capturing.
    try {
      mCaptureSession.stopRepeating();
    } catch (CameraAccessException | IllegalStateException e) {
      Log.w(TAG, "Failed to stop capturing: ", e.getMessage());
      close();
      return;
    }

    CaptureData data = mInFlight.get(mVideoSequenceId);
    if (data == null) {
      Log.e(TAG, "Failed to retrieve capture request data in stopVideoCapture");
      close();
      return;
    }

    if (success && mVideoLatestCaptureResult != null) {
      data.params = cloneAndAugmentRequestParams(mParams, mVideoLatestCaptureResult, true);
      data.params.outFile = mVideoOut;
    }
    success = success && data.params != null;

    finishCaptureAndNotifyClient(mVideoSequenceId, data, success, false, false);
  }

  /**
   * Capture callback for preview and precapture sequences.
   */
  private final CameraCaptureSession.CaptureCallback mPreviewCaptureCallback
      = new CameraCaptureSession.CaptureCallback() {
    private CaptureData mCaptureData;
    private boolean mIsAELocking;
    private boolean mIsAWBLocking;

    @Override
    public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                    @NonNull CaptureRequest request,
                                    @NonNull CaptureResult partialResult) {
      process(partialResult);
    }

    @Override
    public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                   @NonNull CaptureRequest request,
                                   @NonNull TotalCaptureResult result) {
      process(result);
    }

    @Override
    public void onCaptureFailed(@NonNull CameraCaptureSession session,
                                @NonNull CaptureRequest request,
                                @NonNull CaptureFailure failure) {
      int reason = failure.getReason();
      Log.w(TAG, "Preview failed with error code ", reason);
      close();
    }

    @Override
    public void onCaptureSequenceAborted(@NonNull CameraCaptureSession session, int sequenceId) {
      if (mState != State.Capturing) {
        Log.w(TAG, "Preview sequence aborted");
        close();
      } else {
        Log.d(TAG, "Preview sequence aborted during capture");
      }
    }

    /**
     * Preview capture. Includes transitioning of (auto-)focus, (auto-)exposure and white balance
     * sequences in preparation for capture.
     */
    private void process(CaptureResult result) {
      switch (mState) {
        case Preview:
          processStatePreview(result);
          break;
        case LensChanging:
          processStateLensChanging(result);
          break;
        case Exposure: // A.k.a. precapture.
          processStateExposure(result);
          break;
        case WhiteBalance:
          processStateWhiteBalance(result);
          break;
      }
    }

    /**
     * Process state {@link State#Preview} and advance to {@link State#LensChanging} when a new
     * capture request is available.
     */
    private void processStatePreview(final CaptureResult result) {
      // Get the capture request data.
      mCaptureData = mCaptureRequests.poll();
      if (mCaptureData == null) {
        Log.e(TAG, "No capture request found in processStatePreview");
        close();
        return;
      }
      Log.d(TAG, "Processing capture request");

      // Make sure the is*Locking flags have been cleared.
      mIsAELocking = false;
      mIsAWBLocking = false;

      if (!mCaptureData.reuseLockedParams && (mIsAFLocked || mIsAELocked || mIsAWBLocked)) {
        // Unlock any previously locked AF/AE/AWB settings.
        unlockAfAeAwb();

        // TODO add a new state UNLOCKING that waits for AF / AE / AWB to unlock before proceeding
        // with the remaining code in this method. This should avoid running through all the next
        // steps before the unlocks have actually taken hold and treating the old "locked" state as
        // the new locked state in the respective checks in processStateLensChanging/Exposure/WB.
      }

      switch (mParams.focusMode) {
        case AUTO:
          Integer afMode = result.get(CaptureResult.CONTROL_AF_MODE);
          if (mIsAFLocked
              || afMode == null
              || afMode == CaptureResult.CONTROL_AF_MODE_CONTINUOUS_PICTURE
              || afMode == CaptureResult.CONTROL_AF_MODE_CONTINUOUS_VIDEO) {
            mState = State.LensChanging; // Skip the AF trigger.
            process(result);
          } else {
            startAutoFocus(); // Start the capture sequence with an active AF scan.
          }
          break;
        case FIXED: // Fall through.
        case INFINITY:
          mState = State.LensChanging; // Nothing to do, process the next state.
          process(result);
          break;
        default:
          Log.e(TAG, "Error unsupported focusMode: ", mParams.focusMode);
          close();
          break;
      }
    }

    /**
     * Verify if all conditions of state {@link State#LensChanging} are fulfilled and proceed to
     * {@link State#Exposure} when ready.
     */
    private void processStateLensChanging(final CaptureResult result) {
      switch (mParams.focusMode) {
        case AUTO: {
          Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
          Integer afMode = result.get(CaptureResult.CONTROL_AF_MODE);
          if (afState == null) {
            Log.w(TAG, "Skipping AF, state is null");
            transitionToExposure(result);
          } else if (afMode == null) {
            Log.w(TAG, "Skipping AF, mode is null");
            transitionToExposure(result);
          } else if (afMode == CaptureResult.CONTROL_AF_MODE_CONTINUOUS_PICTURE
              || afMode == CaptureResult.CONTROL_AF_MODE_CONTINUOUS_VIDEO) {
            if (afState == CaptureResult.CONTROL_AF_STATE_PASSIVE_FOCUSED
                || afState == CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED) {
              Log.d(TAG, "Continuous AF focused");
              transitionToExposure(result);
            } else if (afState == CaptureResult.CONTROL_AF_STATE_PASSIVE_UNFOCUSED
                || afState == CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED) {
              Log.d(TAG, "Continuous AF unfocused");
              transitionToExposure(result);
            }
          } else {
            if (afState == CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED) {
              Log.d(TAG, "AF locked and focused");
              mIsAFLocked = true;
              transitionToExposure(result);
            } else if (afState == CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED) {
              Log.d(TAG, "AF locked and unfocused");
              mIsAFLocked = true;
              transitionToExposure(result);
            }
          }
          break;
        }
        case FIXED: {
          transitionToExposure(result); // Nothing to do, process the next state.
          break;
        }
        case INFINITY: {
          Integer lensState = result.get(CaptureResult.LENS_STATE);
          if (lensState == null) Log.w(TAG, "Cannot determine lens state (null)");
          if (lensState == null || lensState == CaptureResult.LENS_STATE_STATIONARY) {
            // The lens has stopped moving.
            Log.d(TAG, "Infinity focus, lens stationary");
            transitionToExposure(result);
          }
          break;
        }
        default: {
          Log.e(TAG, "Error unsupported focusMode: ", mParams.focusMode);
          close();
          break;
        }
      }
    }

    /**
     * Transitions to state {@link State#Exposure}.
     */
    private void transitionToExposure(final CaptureResult result) {
      // Check the current AE state to see if precapture is needed.
      Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
      if (mParams.exposureMode == ExposureMode.MANUAL || mIsAELocked
          || aeState == null || aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
        // AE is off, already locked, converged or not going anywhere. No need to run precapture.
        mState = State.Exposure;
        process(result); // Check if this result already satisfies the next state too.
      } else {
        runPrecaptureSequence(); // Trigger precapture for AE.
      }
    }

    /**
     * Verify if all conditions of state {@link State#Exposure} are fulfilled and proceed to
     * {@link State#WhiteBalance} when ready.
     */
    private void processStateExposure(final CaptureResult result) {
      boolean done = mParams.exposureMode == ExposureMode.MANUAL;
      if (!done) {
        Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
        if (aeState == null) {
          Log.w(TAG, "Skipping AE, state is null");
          done = true;
        } else if (!mIsAELocking
            && (aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED
            || aeState == CaptureResult.CONTROL_AE_STATE_FLASH_REQUIRED
            || mCaptureData.skipWaitToConverge)) {
          if (!mCaptureData.skipWaitToConverge) Log.d(TAG, "AE has converged");
          if (mCaptureData.lockAEAndAWB) {
            mIsAELocking = true;
            lockExposure();
          } else {
            done = true;
          }
        } else if (aeState == CaptureResult.CONTROL_AE_STATE_LOCKED) {
          Log.d(TAG, "AE locked");
          mIsAELocked = true;
          mIsAELocking = false;
          done = true;
        }
      }

      if (done) {
        mState = State.WhiteBalance;
        process(result); // Check if this result already satisfies the next state too.
      }
    }

    /**
     * Verify if all conditions of state {@link State#WhiteBalance} are fulfilled and proceed to
     * {@link State#Capturing} when ready.
     */
    private void processStateWhiteBalance(final CaptureResult result) {
      boolean done = false;
      Integer awbState = result.get(CaptureResult.CONTROL_AWB_STATE);
      if (awbState == null) {
        Log.w(TAG, "Skipping AWB, state is null");
        done = true;
      } else if (!mIsAWBLocking
          && (awbState == CaptureResult.CONTROL_AWB_STATE_CONVERGED
          || mCaptureData.skipWaitToConverge)) {
        if (!mCaptureData.skipWaitToConverge) Log.d(TAG, "AWB has converged");
        if (mCaptureData.lockAEAndAWB) {
          mIsAWBLocking = true;
          lockWhiteBalance();
        } else {
          done = true;
        }
      } else if (awbState == CaptureResult.CONTROL_AWB_STATE_LOCKED) {
        Log.d(TAG, "AWB locked");
        mIsAWBLocked = true;
        mIsAWBLocking = false;
        done = true;
      }

      if (done) {
        transitionToCapture();
      }
    }

    /**
     * Transitions to state {@link State#Capturing}.
     */
    private void transitionToCapture() {
      // Stop preview.
      try {
        mCaptureSession.stopRepeating();
      } catch (CameraAccessException | IllegalStateException e) {
        Log.w(TAG, "Failed to stop preview: ", e.getMessage());
        close();
        return;
      }

      // Start capture.
      if (mParams.captureMode == CaptureMode.Photo) {
        capturePhoto(mCaptureData);
      } else if (mParams.captureMode == CaptureMode.Video) {
        captureVideo(mCaptureData);
      }


      if (mCaptureData.captureIdx == 0 && mCaptureSequenceLength > 1) {
        // This is the first request in a sequence. Schedule the first follow-up frame now that
        // actual capture has commenced.
        CaptureData previous = new CaptureData(mCaptureData.callback, mCaptureData.imageConfigIdx,
            mCaptureData.lockAEAndAWB, mCaptureData.lockAEAndAWB, mCaptureData.skipWaitToConverge);
        previous.captureIdx = 0;
        scheduleNextFrame(previous, mCaptureSequenceFPS, SystemClock.elapsedRealtime());
      }

      mCaptureData = null;
    }
  }; // mPreviewCaptureCallback

  /**
   * Capture callback for still image captures.
   */
  private final CameraCaptureSession.CaptureCallback mCaptureCallback
      = new CameraCaptureSession.CaptureCallback() {
    @Override
    public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                   @NonNull CaptureRequest request,
                                   @NonNull TotalCaptureResult result) {
      // Attach the result to the in flight data of the current request.
      int sequenceId = result.getSequenceId();
      CaptureData data = mInFlight.get(sequenceId);
      if (data == null) {
        Log.e(TAG, "Failed to retrieve capture request data in onCaptureCompleted (image)");
        close();
        return;
      }

      // Make a copy of the params used for this capture before the next capture starts.
      data.params = cloneAndAugmentRequestParams(mParams, result, true);
      data.result = result;

      // Notify the client if both the image and capture result are available.
      if (allImagesAvailable(data.images)) {
        finishCaptureAndNotifyClient(sequenceId, data, true, true, false);
      }
    }

    @Override
    public void onCaptureFailed(@NonNull CameraCaptureSession session,
                                @NonNull CaptureRequest request,
                                @NonNull CaptureFailure failure) {
      int reason = failure.getReason();
      Log.w(TAG, "Capture failed with error code ", reason);
      finishCaptureAndNotifyClient(failure.getSequenceId(), null, false, true, false);
    }
  }; // mCaptureCallback

  /**
   * Capture callback for video captures.
   */
  private final CameraCaptureSession.CaptureCallback mVideoCaptureCallback
      = new CameraCaptureSession.CaptureCallback() {
    @Override
    public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                   @NonNull CaptureRequest request,
                                   @NonNull TotalCaptureResult result) {
      if (mRecordingStopped) return;

      int sequenceId = result.getSequenceId();
      CaptureData data = mInFlight.get(sequenceId);
      if (data == null) {
        Log.e(TAG, "Failed to retrieve capture request data in onCaptureCompleted (video)");
        close();
        return;
      }

      mVideoLatestCaptureResult = result;
      data.result = result;
      data.params = cloneAndAugmentRequestParams(mParams, result, false);

      // Finalise any image captures being done along with the video recording.
      if (data.images.length > 0 && allImagesAvailable(data.images)) {
        finishCaptureAndNotifyClient(sequenceId, data, true, false, true);
      }
    }

    @Override
    public void onCaptureSequenceCompleted(@NonNull CameraCaptureSession session, int sequenceId,
                                           long frameNumber) {
      Log.d(TAG, "Video capture sequence completed");
    }

    @Override
    public void onCaptureSequenceAborted(@NonNull CameraCaptureSession session, int sequenceId) {
      Log.w(TAG, "Video capture sequence aborted");
      close(); // Ensure clients are notified even if stopVideoCapture was not executed.
    }
  }; // mVideoCaptureCallback

  /**
   * Returns true if neither the array nor any of its elements are null.
   */
  private boolean allImagesAvailable(ImageWrapper[] images) {
    if (images == null) return false;

    boolean notNull = true;
    for (ImageWrapper image : images) {
      notNull = notNull && image != null;
    }

    return notNull;
  }

  /**
   * Clones the request parameters and updates them with the actual capture settings used by the
   * camera for the capture of result.
   *
   * @param requestParams The request parameters to clone.
   * @param result        The capture result.
   * @param verbose       Log details about the capture when true.
   * @return The cloned and augmented parameters.
   */
  private Parameters cloneAndAugmentRequestParams(@NonNull Parameters requestParams,
                                                  @NonNull TotalCaptureResult result,
                                                  boolean verbose) {
    final Parameters params = requestParams.copy();

    // Set the capture time.
    params.captureTime = System.currentTimeMillis();

    // Set the in-use capture settings, which may be different from the requested ones.
    Integer iso = result.get(CaptureResult.SENSOR_SENSITIVITY);
    Long exposure = result.get(CaptureResult.SENSOR_EXPOSURE_TIME);
    params.sensitivity = iso != null ? iso : params.sensitivity;
    params.exposureTime = exposure != null ? exposure : params.exposureTime;

    if (verbose) {
      Log.d(TAG, "Capture completed with focus mode ", params.focusMode,
          "; exposure mode ", params.exposureMode, "; ISO ", iso,
          "; exposure ", (params.exposureTime / 1000L), "us",
          (params.exposureMode == ExposureMode.MANUAL
              ? " (requested " + (requestParams.exposureTime / 1000L) + "us)"
              : ""));
    }

    // Fix: Reset the exposure time to the requested value instead of the reported value so it is
    // recorded correctly in the EXIF data, because, when the format is JPG, all exposures above
    // 33ms are reported as 33ms, even though the actual exposure appears to be correct up to the
    // supported maximum exposure time (500 or 688ms on S7). This is not the case for RAW image
    // capture results, which correctly report the time up the maximum.
    if (params.exposureMode == ExposureMode.MANUAL) {
      params.exposureTime = requestParams.exposureTime;
    }

    return params;
  }

  /**
   * Acquires the latest image from the reader and asynchronously writes it to storage. Not being
   * able to acquire an image is treated as an error.
   *
   * @param reader The image source.
   */
  private void onImageAvailable(final ImageReader reader) {
    if (mState != State.Capturing) {
      if (!mRecordingStopped) {
        Log.e(TAG, "Invalid entry state for onImageAvailable: ", mState.name());
      } // Silently ignore if this was just another in-flight frame after recording has stopped.
      close();
      return;
    }

    Image image;
    try {
      image = reader.acquireLatestImage();
    } catch (IllegalStateException e) {
      Log.e(TAG, "Error trying to acquire image from reader: ", e.getMessage());
      close();
      return;
    }
    if (image == null && mParams.captureMode == CaptureMode.Video) {
      // acquireLatestImage must have dropped one or more images; ignore if it was only a still
      // taken while recording video.
//      Log.d(TAG, "No image acquired, frames have been dropped");
      return;
    } else if (image == null) {
      Log.e(TAG, "Error no image available in reader");
      close();
      return;
    }

    if (mParams.captureMode == CaptureMode.Video && mRecordingStopped) {
      // It is okay if image data becomes available after video recording has stopped. Discard it.
      image.close();
      return;
    }

    if (mParams.captureMode != CaptureMode.Video) {
      // Logging this when capturing stills during video recording is too spammy.
      Log.d(TAG, "Image data available");
    }

    // Get the request data. Sequence IDs monotonically increase and regular (non-reprocess)
    // requests are processed in order. Further, the mInFlight map is also in ascending order, so
    // the first entry should correspond to this request.
    if (mInFlight.size() == 0) {
      Log.e(TAG, "Failed to retrieve capture request data in onImageAvailable");
      close();
      return;
    }
    int sequenceId = mInFlight.keyAt(0);
    CaptureData data = mInFlight.valueAt(0);

    // Get the config index for the image surface.
    Integer cfgIdx = mImageSurfaceToConfigIdx.get(reader.getSurface());
    if (cfgIdx == null) {
      Log.e(TAG, "Missing surface to image config index mapping");
      close();
      return;
    }

    // Search the index into data.imageConfigIdx that contains cfgIdx.
    int dataCfgIdx = -1;
    for (int i = 0; i < data.imageConfigIdx.length; i++) {
      if (data.imageConfigIdx[i] == cfgIdx) {
        dataCfgIdx = i;
        break;
      }
    }
    if (dataCfgIdx < 0) {
      Log.e(TAG, "Failed to find the image config index in the capture data");
      close();
      return;
    }


    data.images[dataCfgIdx] = new ImageWrapper(image, this, mConfigImageBuffers.get(cfgIdx));

    // Notify the client if both the image and capture result are available.
    if (data.result != null && allImagesAvailable(data.images)) {
      boolean retainInFlight = data.params != null && data.params.captureMode == CaptureMode.Video;
      finishCaptureAndNotifyClient(sequenceId, data, true, true, retainInFlight);
    }
  }

  /**
   * Retrieves the {@link ActionCallback} for sequenceId and notifies the listener about the
   * outcome of their request.
   *
   * @param sequenceId  The unique ID returned by one of the capture calls.
   * @param data        The data for the request identified by sequenceId. This is merely an
   *                    optimisation, avoiding the need to look the data up from {@link #mInFlight}.
   * @param success     The request outcome, true if successful, false if not.
   * @param processNext If true, {@link #processNextRequestIfReady()} is also called.
   * @param retainData  By default, the data associated with sequenceId is deleted from
   *                    {@link #mInFlight}, but if this is true, the in flight list is not modified.
   */
  private void finishCaptureAndNotifyClient(int sequenceId, @Nullable CaptureData data,
                                            boolean success, boolean processNext,
                                            boolean retainData) {
    if (data == null) {
      data = mInFlight.get(sequenceId);
      if (data == null) {
        Log.e(TAG, "Failed to retrieve capture request data");
        return;
      }
    }
    if (!retainData) mInFlight.delete(sequenceId);

    // Fill in missing data.
    if (success && data.params != null) {
      data.params.rotation = getRotation();
    }

    // Return the image buffer permits to the semaphore on failure.
    if (!success) {
      for (int i = 0; i < data.imageBufferReserved.length; i++) {
        if (data.imageBufferReserved[i]) {
          if (data.images[i] != null) {
            Log.d(TAG, "Discarding image data in unsuccessful capture");
            data.images[i].close(); // The semaphore permit is released during this call.
            data.images[i] = null;
          } else {
            mConfigImageBuffers.get(data.imageConfigIdx[i]).release();
          }
          data.imageBufferReserved[i] = false;
        }
      }
    }

    // This must be called after the current request has been deleted from mInFlight.
    if (processNext) processNextRequestIfReady();

    // Pass on a copy of the data when retaining the original instance.
    if (retainData) {
      CaptureData copy = data.makeSubordinateCopy();
      data.images = new ImageWrapper[data.images.length];
      data.result = null;
      data = copy;
    }

    if (success) {
      // Rate-limit the health check update for video frames, but always record it for photos or the
      // finished video.
      long now;
      if (mParams.captureMode != CaptureMode.Video || !retainData) {
        HealthCheck.getInstance().updateTimestamp(HealthCheck.Timestamp.PhotoSuccess);
      } else if (mLastVideoFrameSuccessUpdate + 3000 <= (now = SystemClock.elapsedRealtime())) {
        HealthCheck.getInstance().updateTimestamp(HealthCheck.Timestamp.PhotoSuccess);
        mLastVideoFrameSuccessUpdate = now;
      }
    }

    // Notify the listener. The image is null if the capture failed or when it is a video recording.
    data.callback.onActionCompleted(success, data.images, data, mCharacts);
  }

  private class CameraStateCallback extends CameraDevice.StateCallback {
    /**
     * Enters state {@link State#Closed}.
     */
    @Override
    public void onClosed(@NonNull CameraDevice camera) {
      if (mImageSurfaces != null) {
        for (Surface surface : mImageSurfaces) {
          surface.release();
        }
        mImageSurfaces = null;
        mImageSurfaceToConfigIdx = null;
      }
      if (mVideoSurface != null) {
        mVideoSurface.release();
        mVideoSurface = null;
      }
      if (mImageReaders != null) {
        for (ImageReader reader : mImageReaders) {
          reader.close();
        }
        mImageReaders = null;
      }

      if (mRecorder != null) {
        mRecorder.reset();
        mRecorder.release();
        mRecorder = null;

        if (mVideoOut != null && mVideoOut.exists()) {
          Log.d(TAG, "Cleaning up incomplete video file");
          Helpers.deleteFile(mVideoOut);
        }
        mVideoOut = null;
      }

      if (mPreviewSurface != null) {
        mPreviewSurface.release();
        mPreviewSurface = null;
      }
      if (mPreviewSurfaceTex != null) {
        mPreviewSurfaceTex.release();
        mPreviewSurfaceTex = null;
      }

      mState = State.Closed;
      Log.d(TAG, "Camera closed");
    }

    @Override
    public void onOpened(@NonNull CameraDevice camera) {
      Log.d(TAG, "Camera opened");
      mCameraDevice = camera;
      createSession();
    }

    @Override
    public void onDisconnected(@NonNull CameraDevice camera) {
      Log.w(TAG, "Camera disconnected");
      close();
    }

    @Override
    public void onError(@NonNull CameraDevice camera, int i) {
      Log.w(TAG, "Camera failed with error code ", i);
      close();
    }
  }

  private class SessionStateCallback extends CameraCaptureSession.StateCallback {
    @Override
    public void onConfigured(@NonNull CameraCaptureSession session) {
      if (mState != State.Initialising) {
        Log.e(TAG, "Invalid entry state for capture session onConfigured: ", mState.name());
        close();
        return;
      }

      // Record the successful initialisation before processing individual requests.
      HealthCheck.getInstance().updateTimestamp(HealthCheck.Timestamp.PhotoSuccess);

      mCaptureSession = session;
      Log.d(TAG, "Capture session initialised");
      preparePreview();
      processNextRequestIfReady();
    }

    @Override
    public void onConfigureFailed(@NonNull CameraCaptureSession session) {
      Log.w(TAG, "Capture session configuration failed");
      close();
    }
  }
}
