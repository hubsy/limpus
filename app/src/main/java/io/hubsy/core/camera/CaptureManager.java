package io.hubsy.core.camera;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.camera2.CameraCharacteristics;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import io.hubsy.core.Constants;
import io.hubsy.core.Settings;
import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.camera.postprocess.Postprocessor;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;

/**
 * The CaptureManager is the base class for photo or video capture management. Call {@link #create}
 * to get an instance able to handle the request.
 */
public abstract class CaptureManager {
  private static final String TAG = "CaptureManager";

  /**
   * The exception type used by subclasses to indicate that the configuration is invalid and capture
   * cannot proceed. Appropriate logging of the cause should happen at the source as this exception
   * is only used as a failure signal by {@link #create}.
   */
  protected static class ConfigurationException extends RuntimeException {
  }

  /**
   * Creates a concrete instance able to handle the request according to the application
   * {@link Settings} and request parameters.
   *
   * @param context A context.
   * @param extras  The request parameters.
   * @param done    Callback to run when the capture completes, whether successful or not.
   * @return The instance, or null if no suitable instance could be created for the given request.
   */
  @Nullable
  public static CaptureManager create(@NonNull Context context, @NonNull Bundle extras,
                                      @NonNull Runnable done) {
    final Settings settings = Settings.getInstance(context);

    final boolean isEvent = extras.containsKey(Constants.EXTRA_EVENT_TIMESTAMP);
    final boolean isSpeedEvent =
        extras.getInt(Constants.EXTRA_SPEED, Integer.MIN_VALUE) != Integer.MIN_VALUE;

    final String camMode = extras.getString(ConfigKeys.CamMode.extraName(),
        settings.getString(ConfigKeys.CamMode));

    // Create the correct manager.
    CaptureManager manager = null;
    try {
      if (isSpeedEvent) {
        Log.d(TAG, "New vehicle detection request");
        manager = new VehicleCaptureLocal(context,
            new VehicleCaptureLocal.Config(context, settings, extras), done);
      } else if ("video".equalsIgnoreCase(camMode)) {
        Log.d(TAG, "New video request");
        manager = new VideoCapture(context, new VideoCapture.Config(settings, extras), done);
      } else { // Photo
        // Determine if it is a repeating capture.
        int repeatCount = extras.getInt(ConfigKeys.CamRepeatCount.extraName(),
            settings.getInt(ConfigKeys.CamRepeatCount));
        boolean isRepeating = !isEvent && repeatCount >= 0 && repeatCount != 1;

        if (isRepeating) {
          Log.d(TAG, "New photo sequence request");
          manager = new PhotoSequenceCapture(context,
              new PhotoSequenceCapture.Config(settings, extras), done);
        } else {
          Log.d(TAG, "New photo request");
          manager = new PhotoCapture(context, new PhotoCapture.Config(settings, extras), done);
        }
      }
    } catch (ConfigurationException e) {
      // Should be logged with the appropriate log level at the source. Ignore.
    } catch (Exception e) {
      Log.e(TAG, e.getMessage());
    }

    return manager;
  }

  private final Config mCfg;

  protected final Runnable mDone; // Callback to invoke when the capture is done.
  protected CameraController mCameraController;

  protected CaptureManager(@NonNull Config config, @NonNull Runnable done) {
    mCfg = config;
    mDone = done;
  }

  /**
   * Initialises the CaptureManager. This must be called before {@link #capture()} is invoked.
   *
   * @param context A context.
   * @return True if initialisation was started successfully. Note that some of the initialisation
   * process runs asynchronously and may still fail later on.
   */
  public boolean initialise(@NonNull Context context) {
    mCameraController = CameraController.get(context, mCfg.params);
    return mCameraController != null;
  }

  /**
   * Starts the capture.
   */
  public abstract void capture();

  /**
   * Releases resources held by this CaptureManager. This must be called before another
   * CaptureManager is initialised or either this or the other capture may fail.
   */
  public void release() {
    if (mCameraController != null) {
      mCameraController.close();
      mCameraController = null;
    }
  }

  /**
   * Update changeable request parameters. Only select parameters can be modified.
   *
   * @param extras Request parameters to update.
   */
  public void updateConfig(@NonNull Bundle extras) {
  }

  /**
   * Returns true if the array itself or any of its first len elements are null, or if the array's
   * length is less than len. Elements at indices >= len are not tested.
   */
  protected <T> boolean isNull(T[] a, int len) {
    if (a == null || a.length < len) return true;

    boolean notNull = true;
    for (int i = 0; i < len && notNull; i++) {
      notNull = a[i] != null;
    }

    return !notNull;
  }


  /**
   * The base class for capture configuration.
   */
  protected static class Config {
    // Cached camera parameter schedule.
    private static String sCamScheduleJSON;
    private static CamSchedule sCamSchedule;
    @Nullable
    protected CamScheduleEntry currentSchedule; // The current camera schedule, if any.

    protected boolean isEvent; // The request is in response to an event.
    @Nullable
    protected String eventTrigger; // An optional string identifying the event source.
    @Nullable
    protected String[] eventSensors; // An optional list of MAC addresses for the event triggers.

    @Nullable
    protected RectF cropRegion; // The normalised crop region.

    protected long timestamp; // The event timestamp.
    protected String timestampFmt; // The formatted event timestamp.
    protected long refTimestamp; // The reference event timestamp.
    protected String fnameSuffix; // An optional filename suffix.
    protected String outPathNoExt; // The full output file path without file extension.

    // Data that may be shared by multiple postprocessors.
    protected Postprocessor.SharedData sharedData = new Postprocessor.SharedData();

    // The camera params.
    protected CameraController.Parameters params = new CameraController.Parameters();
    // The primary image config.
    protected CameraController.ImageConfig imageConfig = new CameraController.ImageConfig();


    /**
     * Parses the basic configuration relevant to all captures. Subclass constructors should call
     * super first and then modify and add to the configuration as necessary.
     *
     * @param settings The settings.
     * @param extras   The request parameters.
     */
    protected Config(@NonNull Settings settings, @NonNull Bundle extras) {
      isEvent = extras.containsKey(Constants.EXTRA_EVENT_TIMESTAMP);
      if (isEvent) {
        eventTrigger = extras.getString(Constants.EXTRA_EVENT_TRIGGER);
        eventSensors = extras.getStringArray(Constants.EXTRA_EVENT_SENSORS);
      }

      // Load the camera schedule.
      final int scheduleIdx = loadCamSchedule(settings);
      if (scheduleIdx >= 0) currentSchedule = sCamSchedule.getEntry(scheduleIdx);

      cropRegion = getImageCropRegion(settings, extras);

      // Construct the output path excluding the file extension from the event timestamp.
      timestamp = extras.getLong(Constants.EXTRA_EVENT_TIMESTAMP, 0);
      if (timestamp == 0) {
        Log.d(TAG, "No event timestamp in intent, using current time");
        timestamp = System.currentTimeMillis();
      }
      timestampFmt = Settings.getTimestampFileName(timestamp, "");
      outPathNoExt = Settings.getAppFolderPath() + Settings.APP_DIR_TMP + "/" + timestampFmt;

      // Append the reference event timestamp to the filename if given.
      refTimestamp = extras.getLong(Constants.EXTRA_REFERENCE_EVENT_TIMESTAMP, 0);
      if (refTimestamp > 0) {
        outPathNoExt += "_" + Settings.getTimestampFileName(refTimestamp, "");
      }

      // Append the optional suffix.
      fnameSuffix = extras.getString(Constants.EXTRA_FILENAME_SUFFIX);
      if (fnameSuffix != null) {
        outPathNoExt += "_" + fnameSuffix;
      }

      // Configure image quality.
      final String edgeMode = settings.getString(ConfigKeys.CamEdgeMode);
      if ("off".equalsIgnoreCase(edgeMode)) {
        imageConfig.edgeMode = CameraCharacteristics.EDGE_MODE_OFF;
      } else if ("fast".equalsIgnoreCase(edgeMode)) {
        imageConfig.edgeMode = CameraCharacteristics.EDGE_MODE_FAST;
      } else if ("quality".equalsIgnoreCase(edgeMode)) {
        imageConfig.edgeMode = CameraCharacteristics.EDGE_MODE_HIGH_QUALITY;
      }

      final String noiseMode = settings.getString(ConfigKeys.CamNoiseReductionMode);
      if ("off".equalsIgnoreCase(noiseMode)) {
        imageConfig.noiseReductionMode = CameraCharacteristics.NOISE_REDUCTION_MODE_OFF;
      } else if ("fast".equalsIgnoreCase(noiseMode)) {
        imageConfig.noiseReductionMode = CameraCharacteristics.NOISE_REDUCTION_MODE_FAST;
      } else if ("quality".equalsIgnoreCase(noiseMode)) {
        imageConfig.noiseReductionMode = CameraCharacteristics.NOISE_REDUCTION_MODE_HIGH_QUALITY;
      }

      final String tonemapMode = settings.getString(ConfigKeys.CamTonemapMode);
      if ("fast".equalsIgnoreCase(tonemapMode)) {
        imageConfig.tonemapMode = CameraCharacteristics.TONEMAP_MODE_FAST;
      } else if ("quality".equalsIgnoreCase(tonemapMode)) {
        imageConfig.tonemapMode = CameraCharacteristics.TONEMAP_MODE_HIGH_QUALITY;
      }

      // Set the focus mode.
      final String focusMode = settings.getString(ConfigKeys.CamFocusMode);
      if ("auto".equalsIgnoreCase(focusMode)) {
        params.focusMode = CameraController.FocusMode.AUTO;
      } else if ("infinity".equalsIgnoreCase(focusMode)) {
        params.focusMode = CameraController.FocusMode.INFINITY;
      }

      // Set the AF, AE and AWB metering areas.
      final List<Integer> afArea = settings.getIntList(ConfigKeys.CamFocusArea);
      if (afArea.size() == 4) {
        params.afArea = new Rect(afArea.get(0), afArea.get(1), afArea.get(2), afArea.get(3));
      }
      final List<Integer> aeArea = settings.getIntList(ConfigKeys.CamExposureArea);
      if (aeArea.size() == 4) {
        params.aeArea = new Rect(aeArea.get(0), aeArea.get(1), aeArea.get(2), aeArea.get(3));
      }
      final List<Integer> awbArea = settings.getIntList(ConfigKeys.CamWhiteBalanceArea);
      if (awbArea.size() == 4) {
        params.awbArea = new Rect(awbArea.get(0), awbArea.get(1), awbArea.get(2), awbArea.get(3));
      }

      // Set exposure from schedule or settings.
      if (scheduleIdx < 0 || !setExposureFromSchedule(scheduleIdx)) {
        setExposure(settings);
      }
    }

    /**
     * Reads setting {@link ConfigKeys#ImageCropRegion} and validates that none of its values are
     * outside [0.0, 1.0].
     *
     * @return The Rect for the specified region, or null if unset or invalid.
     */
    @Nullable
    private RectF getImageCropRegion(Settings settings, Bundle extras) {
      // Get the crop region from the extras or the config.
      List<Double> roi = null;
      double[] a = extras.getDoubleArray(ConfigKeys.ImageCropRegion.extraName());
      if (a != null && a.length == 4) {
        roi = new ArrayList<>(a.length);
        for (double v : a) {
          roi.add(v);
        }
      }

      if (roi == null) roi = settings.getDoubleList(ConfigKeys.ImageCropRegion);
      if (roi.size() != 4) return null;

      float x = roi.get(0).floatValue();
      float y = roi.get(1).floatValue();
      float w = roi.get(2).floatValue();
      float h = roi.get(3).floatValue();
      if (x < 0 || y < 0 || w < 0 || h < 0 || x >= 1.f || y >= 1.f || w > 1.f || h > 1.f) {
        Log.w(TAG, "Invalid crop region: ", TextUtils.join(", ", roi));
        return null;
      }

      return new RectF(x, y, x + w, y + h);
    }

    /**
     * Updates {@link #sCamSchedule} if necessary and returns the index for the current schedule
     * interval.
     *
     * @return The index for the current interval or -1 if no schedule is configured.
     */
    private int loadCamSchedule(Settings settings) {
      // Set the exposure values according to the schedule if one is given.
      final String jsonSchedule = settings.getString(ConfigKeys.CamSchedule);
      if (jsonSchedule.isEmpty()) return -1;

      // Use the cached schedule if it has not changed or parse the new one.
      CamSchedule schedule;
      if (sCamSchedule != null && jsonSchedule.equals(sCamScheduleJSON)) {
        Log.d(TAG, "Using cached exposure schedule");
        schedule = sCamSchedule;
      } else {
        schedule = CamSchedule.fromJSON(jsonSchedule);
        sCamSchedule = schedule;
        sCamScheduleJSON = jsonSchedule;
      }

      return schedule == null ? -1 : schedule.activeEntry();
    }

    /**
     * Sets exposure values from general settings instead of the current schedule.
     */
    private void setExposure(final Settings settings) {
      final String exposureMode = settings.getString(ConfigKeys.CamExposureMode);
      if ("auto".equalsIgnoreCase(exposureMode)) {
        params.exposureMode = CameraController.ExposureMode.AUTO;
      } else if ("manual".equalsIgnoreCase(exposureMode)) {
        params.exposureMode = CameraController.ExposureMode.MANUAL;
        int exposureMillis = settings.getInt(ConfigKeys.CamExposureTime);
        if (exposureMillis > 0) params.exposureTime = exposureMillis * 1000L * 1000L;
        params.sensitivity = settings.getInt(ConfigKeys.CamISO);
      }
    }

    /**
     * Applies the exposure settings of the specified schedule entry to params.
     *
     * @param scheduleIdx The index of the current schedule.
     * @return True if params has been updated, false if not.
     */
    private boolean setExposureFromSchedule(final int scheduleIdx) {
      final int len = sCamSchedule == null ? 0 : sCamSchedule.size();
      if (scheduleIdx < 0 || scheduleIdx >= len) return false;

      // Determine the index of the next schedule, if any.
      final int nextIdx = scheduleIdx < (len - 1) ? scheduleIdx + 1 : 0;
      final CamScheduleEntry current = sCamSchedule.getEntry(scheduleIdx);
      final CamScheduleEntry next = sCamSchedule.getEntry(nextIdx);

      // Check if values should be interpolated.
      if (scheduleIdx != nextIdx && current.interpolate
          && current.exposureMode == CameraController.ExposureMode.MANUAL
          && next.exposureMode == CameraController.ExposureMode.MANUAL) {
        // Calculate interpolation factor t for the current time between the two schedules.
        float t = interpolationFactor(current.time, next.time);

        // Interpolate the settings.
        params.exposureMode = CameraController.ExposureMode.MANUAL;
        params.exposureTime = interpolate(t, current.exposureTime, next.exposureTime);
        params.sensitivity = (int) interpolate(t, current.sensitivity, next.sensitivity);
        Log.d(TAG, "Interpolating between camera parameter schedules ", current.time,
            " and ", next.time, " with factor ", t);
      } else { // Do not interpolate.
        params.exposureMode = current.exposureMode;
        params.exposureTime = current.exposureTime;
        params.sensitivity = current.sensitivity;
        Log.d(TAG, "On camera parameter schedule ", current.time);
      }

      return true;
    }

    /**
     * Linearly interpolates with factor t [0, 1] between from and to.
     */
    private long interpolate(float t, long from, long to) {
      return from + (long) (t * (to - from));
    }

    /**
     * Calculates the linear interpolation factor t for the current time given interval [from, to].
     *
     * @return Factor t, clamped to [0, 1].
     */
    private float interpolationFactor(String from, String to) {
      final Calendar now = Calendar.getInstance();
      final Pair<Calendar, Calendar> range = Helpers.parseTimeRange(from, to, false, now);
      if (range == null) return 0f;

      long startMillis = range.first.getTimeInMillis();
      long endMillis = range.second.getTimeInMillis();
      long nowMillis = now.getTimeInMillis();
      long durationMillis = endMillis - startMillis;
      float t = durationMillis > 0
          ? (float) (((double) (nowMillis - startMillis)) / ((double) durationMillis))
          : 0f;
      return Math.min(Math.max(t, 0f), 1f);
    }
  } // Config

  /**
   * Prepares to ship the logs and kicks off file transfers on run().
   */
  protected static class RotateLogsAndTransferFiles implements Runnable {
    private final Context mContext;
    private final AtomicInteger mCounter;
    private final boolean mCheckForUpdates;

    /**
     * @param context         A context.
     * @param counter         An optional counter. When provided, the log rotation and file
     *                        transfers only take place if the counter equals zero after being
     *                        decremented by 1.
     * @param checkForUpdates Whether file transfers should also check for updates (i.e. downloads).
     */
    public RotateLogsAndTransferFiles(@NonNull Context context, @Nullable AtomicInteger counter,
                                      boolean checkForUpdates) {
      mContext = context.getApplicationContext();
      mCounter = counter;
      mCheckForUpdates = checkForUpdates;
    }

    @Override
    public void run() {
      // Decrement the counter if provided and check if it has reached zero.
      if (mCounter == null || mCounter.decrementAndGet() == 0) {
        Helpers.prepareAndShipLogs(mContext, mCheckForUpdates);
      }
    }
  } // RotateLogsAndTransferFiles
}
