package io.hubsy.core.camera;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.media.ExifInterface;
import android.text.format.DateFormat;

import java.io.File;
import java.io.IOException;

import io.hubsy.core.util.Log;

/**
 * Writes EXIF data to JPEG image files.
 */
public class ExifWriter extends AsyncTask<Void, Void, Void> {
  private static final String TAG = "ExifWriter";

  private final File mJpeg;
  private final CameraController.Parameters mCaptureParams;
  private final String mUserComment;
  private final Runnable mCallback;
  private final Handler mHandler;

  /**
   * Writes EXIF attributes in a background thread.
   *
   * @param jpeg          The JPEG image to modify.
   * @param captureParams The optional capture parameters to write.
   * @param userComment   The optional EXIF UserComment attribute.
   * @param callback      The callback to invoke when done.
   * @param handler       The handler used to schedule callback.
   */
  public ExifWriter(@NonNull File jpeg, @Nullable CameraController.Parameters captureParams,
                    @Nullable String userComment, @NonNull Runnable callback,
                    @NonNull Handler handler) {
    mJpeg = jpeg;
    mCaptureParams = captureParams;
    mUserComment = userComment;
    mCallback = callback;
    mHandler = handler;
  }

  @Override
  protected Void doInBackground(Void... voids) {
    ExifInterface exif;
    try {
      exif = new ExifInterface(mJpeg.getAbsolutePath());
    } catch (IOException e) {
      Log.w(TAG, e.getMessage());
      return null;
    }

    exif.setAttribute(ExifInterface.TAG_MODEL, "Hubsy");

    // Set capture attributes.
    if (mCaptureParams != null) {
      final String dateTime =
          DateFormat.format("yyyy:MM:dd HH:mm:ss", mCaptureParams.captureTime).toString();
      exif.setAttribute(ExifInterface.TAG_DATETIME, dateTime);
      exif.setAttribute(ExifInterface.TAG_DATETIME_DIGITIZED, dateTime);
      exif.setAttribute(ExifInterface.TAG_DATETIME_ORIGINAL, dateTime);

      switch (mCaptureParams.exposureMode) {
        case AUTO:
          exif.setAttribute(ExifInterface.TAG_EXPOSURE_PROGRAM,
              Short.toString(ExifInterface.EXPOSURE_PROGRAM_NORMAL));
          exif.setAttribute(ExifInterface.TAG_EXPOSURE_MODE,
              Short.toString(ExifInterface.EXPOSURE_MODE_AUTO));
          break;
        case MANUAL:
          exif.setAttribute(ExifInterface.TAG_EXPOSURE_PROGRAM,
              Short.toString(ExifInterface.EXPOSURE_PROGRAM_MANUAL));
          exif.setAttribute(ExifInterface.TAG_EXPOSURE_MODE,
              Short.toString(ExifInterface.EXPOSURE_MODE_MANUAL));
          break;
      }
      exif.setAttribute(ExifInterface.TAG_EXPOSURE_TIME,
          Double.toString(mCaptureParams.exposureTime / 1000000000.));

      exif.setAttribute(ExifInterface.TAG_SENSITIVITY_TYPE,
          Short.toString(ExifInterface.SENSITIVITY_TYPE_ISO_SPEED));
      exif.setAttribute(ExifInterface.TAG_PHOTOGRAPHIC_SENSITIVITY,
          Integer.toString(mCaptureParams.sensitivity));

      exif.setAttribute(ExifInterface.TAG_WHITE_BALANCE,
          Short.toString(ExifInterface.WHITE_BALANCE_AUTO));

      switch (mCaptureParams.rotation) {
        case 90:
          exif.setAttribute(ExifInterface.TAG_ORIENTATION,
              Integer.toString(ExifInterface.ORIENTATION_ROTATE_90));
          break;
        case 180:
          exif.setAttribute(ExifInterface.TAG_ORIENTATION,
              Integer.toString(ExifInterface.ORIENTATION_ROTATE_180));
          break;
        case 270:
          exif.setAttribute(ExifInterface.TAG_ORIENTATION,
              Integer.toString(ExifInterface.ORIENTATION_ROTATE_270));
          break;
      }
    }

    // Set the UserComment.
    if (mUserComment != null) exif.setAttribute(ExifInterface.TAG_USER_COMMENT, mUserComment);

    // Write to file.
    try {
      exif.saveAttributes();
    } catch (IOException e) {
      Log.w(TAG, "Failed to save EXIF attributes: ", e.getMessage());
      return null;
    }

    Log.d(TAG, "EXIF written successfully");
    return null;
  }

  @Override
  protected void onPostExecute(Void aVoid) {
    finish();
  }

  @Override
  protected void onCancelled() {
    finish();
  }

  private void finish() {
    mHandler.post(mCallback);
  }
}
