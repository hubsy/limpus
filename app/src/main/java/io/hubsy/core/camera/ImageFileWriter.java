package io.hubsy.core.camera;

import android.graphics.ImageFormat;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.DngCreator;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.media.ExifInterface;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import io.hubsy.core.util.Log;

/**
 * An AsyncTask to write image data to a file.
 */
public class ImageFileWriter extends AsyncTask<Void, Void, Void> {
  private static final String TAG = "ImageFileWriter";

  private final Image mImage;
  private final File mImageFile;
  private final CameraCharacteristics mCharacts;
  private final CaptureResult mMetadata;
  private final int mOrientation;

  private final Runnable mCallback;
  private final Handler mHandler;

  /**
   * This constructor can be used to store all supported image file formats. Currently
   * {@link ImageFormat#JPEG} and {@link ImageFormat#RAW_SENSOR} are supported. For RAW images,
   * the characteristics and metadata must not be null.
   *
   * @param img      The image to save.
   * @param file     The output file.
   * @param ch       The camera characteristics.
   * @param meta     Information about the actual settings used for image capture.
   * @param rotation The cw rotation needed to display the image upright. Only needed for DNGs.
   * @param callback The callback to invoke when done.
   * @param handler  The handler used to schedule callback.
   */
  public ImageFileWriter(@NonNull Image img, @NonNull File file,
                         @Nullable CameraCharacteristics ch, @Nullable CaptureResult meta,
                         int rotation, @NonNull Runnable callback, @NonNull Handler handler) {
    mImage = img;
    mImageFile = file;
    mCharacts = ch;
    mMetadata = meta;
    mOrientation = rotation;
    mCallback = callback;
    mHandler = handler;
  }

  /**
   * Writes the image data to the file.
   */
  @Override
  protected Void doInBackground(Void... voids) {
    // Create the parent directory if necessary.
    File dir = mImageFile.getParentFile();
    if (dir != null && !dir.exists() && !dir.mkdirs()) {
      Log.w(TAG, "Failed to create the output directory");
      return null;
    }

    // Write the image.
    try (FileOutputStream out = new FileOutputStream(mImageFile)) {
      switch (mImage.getFormat()) {
        case ImageFormat.JPEG:
          writeJPEG(out);
          break;
        case ImageFormat.RAW_SENSOR:
          writeRawSensor(out);
          break;
        default:
          Log.e(TAG, "Unsupported image format: ", mImage.getFormat());
          return null;
      }
    } catch (IOException | IllegalStateException e) {
      Log.w(TAG, "Failed to write image file: ", e.getMessage());
      //noinspection ResultOfMethodCallIgnored
      mImageFile.delete();
      return null;
    }

    Log.d(TAG, "Image saved");
    return null;
  }

  private void writeJPEG(FileOutputStream stream) throws IOException {
    ByteBuffer buffer = mImage.getPlanes()[0].getBuffer();
    buffer.rewind();
    stream.getChannel().write(buffer);
  }

  private void writeRawSensor(FileOutputStream stream) throws IOException, IllegalStateException {
    if (mCharacts == null || mMetadata == null) {
      throw new IllegalStateException("Cannot write DNG image without the capture metadata");
    }

    try (DngCreator dng = new DngCreator(mCharacts, mMetadata)) {
      switch (mOrientation) {
        case 90:
          dng.setOrientation(ExifInterface.ORIENTATION_ROTATE_90);
          break;
        case 180:
          dng.setOrientation(ExifInterface.ORIENTATION_ROTATE_180);
          break;
        case 270:
          dng.setOrientation(ExifInterface.ORIENTATION_ROTATE_270);
          break;
      }
      dng.writeImage(stream, mImage);
    }
  }

  @Override
  protected void onPostExecute(Void v) {
    finish();
  }

  @Override
  protected void onCancelled() {
    finish();
  }

  private void finish() {
    mHandler.post(mCallback);
  }
}
