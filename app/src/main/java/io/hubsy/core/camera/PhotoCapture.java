package io.hubsy.core.camera;

import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraCharacteristics;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.concurrent.atomic.AtomicInteger;

import io.hubsy.core.Settings;
import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.camera.postprocess.BackgroundSubtractionStage;
import io.hubsy.core.camera.postprocess.CropJPEGStage;
import io.hubsy.core.camera.postprocess.DngConverterStage;
import io.hubsy.core.camera.postprocess.EventDecisionStage;
import io.hubsy.core.camera.postprocess.EventWaiterStage;
import io.hubsy.core.camera.postprocess.ExifWriterStage;
import io.hubsy.core.camera.postprocess.ImageCloserStage;
import io.hubsy.core.camera.postprocess.ImageFileWriterStage;
import io.hubsy.core.camera.postprocess.MoveJPEGStage;
import io.hubsy.core.camera.postprocess.Postprocessor;
import io.hubsy.core.imgproc.BGSubtraction;
import io.hubsy.core.util.Log;

/**
 * A CaptureManager for single photo captures. Additional computational images may be captured to
 * perform image analysis, such as background subtraction, but the general result is a single photo.
 */
class PhotoCapture extends PhotoCaptureBase {
  private static final String TAG = "PhotoCapture";

  private final Context mContext;
  private final Config mCfg;

  protected PhotoCapture(@NonNull Context context, @NonNull Config config,
                         @NonNull Runnable done) {
    super(config, done);
    mCfg = config;
    mContext = context.getApplicationContext();

    // Drop this request if it references a previous event that was dropped after image analysis.
    if (config.refTimestamp > 0 && config.refTimestamp == sTimestampOfLastDroppedEvent) {
      Log.d(TAG, "Dropping follow-up request to filtered out event image");
      throw new ConfigurationException(); // Signal that this capture should be aborted.
    }
  }

  @Override
  public void capture() {
    if (mCameraController == null) {
      mDone.run();
      return;
    }

    // Schedule the main photo.
    mCameraController.takePhoto(0, false,
        new CaptureCallback(0, 0));

    if (mCfg.doBGSub && mCfg.numImages > 1) {
      // Schedule the sequence images.
      double fps = mCfg.currentSchedule == null || mCfg.currentSchedule.sequenceInterval <= 0
          ? 1. : 1000. / mCfg.currentSchedule.sequenceInterval;
      mCameraController.takePhotoSequence(1, mCfg.numImages - 1, fps, true,
          new CaptureCallback(1, 1));
    }
  }

  @Override
  public void release() {
    super.release();

    // Release the model if that hasn't happened yet.
    if (mCfg.bgModelReleaser != null) {
      mCfg.bgModelReleaser.run();
      mCfg.bgModelReleaser = null;
    }
  }

  private class CaptureCallback implements CameraController.ActionCallback {
    private int mRequestIdx;

    private final int mImageConfigIdx;
    private final String mDataDir;

    /**
     * @param requestIdx     The index of this request in the sequence of related photos. Index 0
     *                       is considered the main photo in the sequence.
     * @param imageConfigIdx The index into {@link CameraController.Parameters#imageConfigs} for
     *                       this single or sequence capture request.
     */
    CaptureCallback(int requestIdx, int imageConfigIdx) {
      mRequestIdx = requestIdx;
      mImageConfigIdx = imageConfigIdx;

      mDataDir = mCfg.dataUploadEnabled ? Settings.APP_DIR_NEW : Settings.APP_DIR_ARCHIVED;
    }

    @Override
    public void onActionCompleted(boolean success,
                                  @Nullable CameraController.ImageWrapper[] images,
                                  @Nullable CameraController.CaptureData captureData,
                                  @Nullable CameraCharacteristics characts) {
      final CameraController.Parameters params = captureData == null ? null : captureData.params;

      if (!success || params == null || images == null || isNull(images, 1)) {
        Log.w(TAG, "Taking photo failed");
        mDone.run();
        new RotateLogsAndTransferFiles(mContext, null, !mCfg.isEvent).run();
        return;
      }

      Log.d(TAG, "Photo taken successfully");

      // Initialise the general data needed for postprocessing.
      final Postprocessor.Data data = new Postprocessor.Data(mCfg.sharedData);
      data.image = images[0];
      data.captureParams = params;
      data.imageConfig = params.imageConfigs.get(mImageConfigIdx);
      data.captureResult = captureData.result;
      data.cameraCharacteristics = characts;
      final int maxImageSize = (mRequestIdx == 0 || mCfg.currentSchedule == null)
          ? mCfg.maxImageSize : mCfg.currentSchedule.sequenceReso;

      // Generate a new timestamp for sequence images and reference the event timestamp.
      // Append a final underscore to mark these images as computational extras.
      String outPathNoExt = mCfg.outPathNoExt;
      if (mRequestIdx > 0) {
        String timestamp = Settings.getTimestampFileName(params.captureTime, "");
        outPathNoExt = Settings.getAppFolderPath() + Settings.APP_DIR_TMP + "/" + timestamp
            + "_" + mCfg.timestampFmt + "_";
      }

      // Prepare the callback to run when postprocessing is complete.
      Runnable cb =
          new RotateLogsAndTransferFiles(mContext, mCfg.numImagesToProcess, !mCfg.isEvent);


      // Start image postprocessing.
      postprocess(data, mRequestIdx, outPathNoExt, Settings.APP_DIR_NEW, mDataDir, maxImageSize,
          true, cb);

      mRequestIdx++;
    }
  } // CaptureCallback


  /**
   * Configure and execute the postprocessing pipeline.
   *
   * @param data         The data to be passed into the pipeline.
   * @param requestIdx   The image index in the sequence, with zero being the original photo.
   * @param outPathNoExt The output path without file type extension.
   * @param jpegDestDir  The directory path, relative to the app's root dir, where to move the JPEG
   *                     after processing.
   * @param dataDestDir  The directory path, relative to the app's root dir, where to move data
   *                     files (like sequence images).
   * @param maxImageSize The target size of the larger dimension of the final image.
   * @param writeExif    Write EXIF data if true <b>and</b> requestIdx == 0.
   * @param callback     If non-null, callback will be run when postprocessing finishes.
   */
  private void postprocess(@NonNull final Postprocessor.Data data, final int requestIdx,
                           @NonNull final String outPathNoExt, @NonNull final String jpegDestDir,
                           @NonNull final String dataDestDir, int maxImageSize, boolean writeExif,
                           @Nullable Runnable callback) {
    final boolean isSequenceImage = requestIdx > 0;
    final boolean isMainImage = !isSequenceImage;
    if (data.image == null) {
      Log.e(TAG, "Cannot do postprocessing, the image is null");
      return;
    } else if (data.imageConfig == null) {
      Log.e(TAG, "Cannot do postprocessing, the image config is null");
      return;
    }

    // Prepare the postprocess pipeline.
    final ArrayList<Postprocessor.Stage> stages = new ArrayList<>(10);

    boolean jpegMoved = false;
    if (isMainImage) {
      // Save the image. Sequence images are passed on for processing and are not written here.
      stages.add(new ImageFileWriterStage(outPathNoExt, mCfg.numImagesToClose, mDone));

      if (data.image.data.getFormat() == ImageFormat.RAW_SENSOR) {
        // Add a DNG to JPEG conversion stage if needed. This also applies the crop.
        float scaleFactor = (float) maxImageSize / (float) data.imageConfig.maxDimension;
        stages.add(new DngConverterStage(mContext, scaleFactor, mCfg.cropRegion));
      } else if (mCfg.cropRegion != null) {
        // Crop the image.
        stages.add(new CropJPEGStage(mCfg.cropRegion, data.imageConfig.jpegQuality));
      }

      if (writeExif) { // Add the EXIF writer stage.
        // Set a custom comment if an event trigger is given.
        String comment = null;
        if (mCfg.eventTrigger != null && !mCfg.eventTrigger.isEmpty()) {
          comment = "Trigger: " + mCfg.eventTrigger;
        }
        stages.add(new ExifWriterStage(comment));
      }

      if (mCfg.isEvent && mCfg.doBGSub) {
        // When doing background subtraction on sequence images the event photo has to wait for the
        // outcome.
        EventWaiterStage waiter = new EventWaiterStage();
        mCfg.bgSubCallbacks.add(waiter); // Will trigger when bg sub is done.
        stages.add(waiter);
      } else if (mCfg.isEvent) {
        // Not analysing the image locally, mark it as being relevant so the event is uploaded.
        data.shared.hasForegroundChanges = true;
      }

      // Write the event to the DB if the outcome indicates the photo is relevant.
      if (mCfg.isEvent) {
        if (mCfg.eventSensors != null) {
          // Also move event images to jpegDestDir and archive non-event images.
          Postprocessor.Stage moveIfEvent = new MoveJPEGStage(jpegDestDir);
          Postprocessor.Stage moveIfNotEvent = new MoveJPEGStage(Settings.APP_DIR_ARCHIVED);
          stages.add(new EventDecisionStage(mContext, mCfg.timestamp,
              new LinkedHashSet<>(Arrays.asList(mCfg.eventSensors)), moveIfEvent, moveIfNotEvent));
          jpegMoved = true;
        } else {
          Log.e(TAG, "Missing list of event sensors, cannot upload event");
        }
      }
    }

    if (isSequenceImage && mCfg.doBGSub && mCfg.currentSchedule != null) {
      // Do background subtraction. Set the output path for the image so that it will be uploaded.
      // Note that this stage synchronises (joins) postprocessing of all sequence images.
      // The expected index counts sequence images only, subtract 1 for the event image.
      mCfg.bgSubImagePathsNoExt.set(requestIdx - 1, outPathNoExt);
      stages.add(new BackgroundSubtractionStage(mCfg.bgModel, requestIdx - 1, mCfg.bgSubImages,
          mCfg.cropRegion, mCfg.bgSubImagePathsNoExt, dataDestDir, mCfg.bgSubImagesToProcess,
          mCfg.bgSubCallbacks, mCfg.currentSchedule.bgSubThreshold,
          mCfg.currentSchedule.bgSubCleanFGMask, mCfg.currentSchedule.bgSubInvert));
      if (requestIdx == (mCfg.numImages - 1)) {
        // No further images in the sequence. Set the model to null so cleanUpAndFinish can verify
        // that the responsibility to release its resources has been passed on.
        mCfg.bgModel = null;
      }
    }

    if (isSequenceImage) {
      // Close the image after it has been processed.
      stages.add(new ImageCloserStage(mCfg.numImagesToClose, mDone));
    }

    // Add a stage to move the JPEG to its target directory.
    if (!jpegMoved) {
      if (isMainImage) stages.add(new MoveJPEGStage(jpegDestDir));
      else stages.add(new MoveJPEGStage(dataDestDir));
    }

    if (isMainImage && mCfg.isEvent && mCfg.doBGSub) {
      // Remember the event timestamp if the image was filtered out.
      stages.add((d, done) -> {
        if (!d.shared.hasForegroundChanges) sTimestampOfLastDroppedEvent = mCfg.timestamp;
        done.run();
      });
    }

    // Execute the pipeline.
    Postprocessor processor = new Postprocessor(mContext, stages, data, callback);
    processor.execute();
  }


  protected static class Config extends PhotoCaptureBase.Config {
    protected boolean doBGSub; // Flag to enable background subtraction.
    // Data shared between BackgroundSubtractionStage instances.
    protected BGSubtraction bgModel; // The model instance.
    private BGModelReleaser bgModelReleaser; // Must be called to release the bgModel instance.
    // Sequence images. Null where the BackgroundSubtractionStage has not yet set the image.
    protected ArrayList<CameraController.ImageWrapper> bgSubImages;
    // Background subtraction sequence image paths without file extension. Null where not yet set.
    protected ArrayList<String> bgSubImagePathsNoExt;
    protected AtomicInteger bgSubImagesToProcess; // Number of outstanding images.
    protected ArrayList<Runnable> bgSubCallbacks; // Callbacks to trigger when BG subtraction is done.

    protected boolean dataUploadEnabled; // Toggles uploading of computational images.


    protected Config(@NonNull Settings settings, @NonNull Bundle extras) {
      super(settings, extras);

      numImages = 1;

      // Get the settings for image analysis if this request is for an event.
      if (isEvent && currentSchedule != null
          && currentSchedule.sequenceLength > 0 && currentSchedule.bgSubKernel != null) {
        // Add an image config for the images to analyse.
        CameraController.ImageConfig sequenceConfig = imageConfig.copy();
        sequenceConfig.format = ImageFormat.YUV_420_888;
        // Sequence images are used to update the background subtraction model in reverse order, so
        // first all must be captured and queued up. Enough buffers must be used to avoid a stall.
        sequenceConfig.imageBuffers = currentSchedule.sequenceLength;
        sequenceConfig.maxDimension = currentSchedule.sequenceReso;
        sequenceConfig.aspectRatio = "4:3";
        params.imageConfigs.add(sequenceConfig);

        numImages = 1 + currentSchedule.sequenceLength;

        if (BGSubtraction.isAvailable) {
          // Check if the use of an image mask is enabled and if it is installed.
          final boolean maskDisabled = settings.getString(ConfigKeys.AwsImageMaskName).isEmpty();
          final String maskVersion = settings.getVersion(ConfigKeys.AwsImageMaskName);
          final String maskPath = maskDisabled || maskVersion.isEmpty() ? null
              : Settings.getFileInAppDir(Settings.APP_DIR_DATA, "image-mask.png").getAbsolutePath();

          // Initialise the model and prepare related data structs if initialisation is successful.
          try {
            bgModel = BGSubtraction.getInstance(currentSchedule.bgSubKernel,
                currentSchedule.bgSubKernelWidth, currentSchedule.bgSubKernelHeight,
                maskPath, maskVersion);
            bgModelReleaser = new BGModelReleaser(bgModel);

            // Prepare shared data for BackgroundSubtractionStage instances.
            bgSubImages = new ArrayList<>(currentSchedule.sequenceLength);
            bgSubImagePathsNoExt = new ArrayList<>(currentSchedule.sequenceLength);
            for (int i = 0; i < currentSchedule.sequenceLength; i++) {
              bgSubImages.add(null);
              bgSubImagePathsNoExt.add(null);
            }
            bgSubImagesToProcess = new AtomicInteger(currentSchedule.sequenceLength);
            bgSubCallbacks = new ArrayList<>(2 + currentSchedule.sequenceLength);

            // Make sure the model is released when postprocessing stages using it are done.
            bgSubCallbacks.add(bgModelReleaser);

            doBGSub = true;
          } catch (Exception e) {
            Log.e(TAG, "Failed to initialise background subtraction: " + e.getMessage());
          }
        } else {
          Log.w(TAG, "Background subtraction is unavailable");
        }
      }

      numImagesToClose = new AtomicInteger(numImages);
      numImagesToProcess = new AtomicInteger(numImages);

      dataUploadEnabled = settings.getBool(ConfigKeys.EnableDataUpload);
    }
  } // Config

  /**
   * Calls {@link BGSubtraction#releaseForReuse()} when {@link #run} is called.
   */
  private static class BGModelReleaser implements Runnable {
    private final BGSubtraction mModel;
    private boolean mReleased;

    public BGModelReleaser(@NonNull BGSubtraction model) {
      mModel = model;
    }

    @Override
    public void run() {
      Log.d(TAG, "Releasing the background model");
      if (!mReleased) {
        try {
          mModel.releaseForReuse();
        } catch (Exception e) {
          Log.e(TAG, "Failed to release the model: " + e.getMessage());
        }
        mReleased = true;
      }
    }
  } // BGModelReleaser
}

