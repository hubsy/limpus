package io.hubsy.core.camera;

import android.graphics.ImageFormat;
import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.concurrent.atomic.AtomicInteger;

import io.hubsy.core.Settings;
import io.hubsy.core.Settings.ConfigKeys;

/**
 * A CaptureManager for photo captures.
 */
abstract class PhotoCaptureBase extends CaptureManager {

  // The timestamp of the most recent request that was filtered out by image analysis.
  protected static long sTimestampOfLastDroppedEvent;

  protected PhotoCaptureBase(@NonNull Config config, @NonNull Runnable done) {
    super(config, done);
  }


  protected static class Config extends CaptureManager.Config {
    protected int numImages; // The number of images to be captured by this instance.
    // Does not really have to be atomic, this is just a convenient int type that can be passed by
    // reference. Indicates the number of remaining images to write to disk.
    protected AtomicInteger numImagesToClose;
    // Like mNumImagesToClose, but decremented when all processing for an image has been completed.
    protected AtomicInteger numImagesToProcess;

    protected int maxImageSize;


    protected Config(@NonNull Settings settings, @NonNull Bundle extras) {
      super(settings, extras);

      // Configure the mode and set the primary image config.
      params.captureMode = CameraController.CaptureMode.Photo;
      params.imageConfigs.add(imageConfig);

      numImages = 1;

      // Set image format.
      final String format = settings.getString(ConfigKeys.CamFormat);
      if ("jpg".equalsIgnoreCase(format) || "jpeg".equalsIgnoreCase(format)) {
        imageConfig.format = ImageFormat.JPEG;
      } else if ("raw".equalsIgnoreCase(format)) {
        imageConfig.format = ImageFormat.RAW_SENSOR;
      }

      // Set image quality.
      maxImageSize = extras.getInt(ConfigKeys.CamReso.extraName(), Settings.DEFAULT_RESO);
      imageConfig.maxDimension = maxImageSize;
      imageConfig.aspectRatio = settings.getString(ConfigKeys.CamAspectRatio);
      imageConfig.jpegQuality = settings.getInt(ConfigKeys.JPEGQuality);
    }
  } // Config
}
