package io.hubsy.core.camera;

import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraCharacteristics;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import io.hubsy.core.AwsClient;
import io.hubsy.core.Settings;
import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.camera.postprocess.CropJPEGStage;
import io.hubsy.core.camera.postprocess.DngConverterStage;
import io.hubsy.core.camera.postprocess.ExifWriterStage;
import io.hubsy.core.camera.postprocess.ImageFileWriterStage;
import io.hubsy.core.camera.postprocess.MoveJPEGStage;
import io.hubsy.core.camera.postprocess.Postprocessor;
import io.hubsy.core.util.Log;

/**
 * A CaptureManager for a sequence of N photos taken at some interval and using the same settings.
 */
class PhotoSequenceCapture extends PhotoCaptureBase {
  private static final String TAG = "PhotoSequenceCapture";

  private final Context mContext;
  private final Config mCfg;

  protected PhotoSequenceCapture(@NonNull Context context, @NonNull Config config,
                                 @NonNull Runnable done) {
    super(config, done);
    mCfg = config;
    mContext = context.getApplicationContext();
  }

  @Override
  public void capture() {
    if (mCameraController == null) {
      mDone.run();
      return;
    }

    mCameraController.takePhotoSequence(0, mCfg.numImages, mCfg.repeatFPS, mCfg.repeatLocked,
        new CaptureCallback(0, mCfg.fnameSuffix));
  }

  private class CaptureCallback implements CameraController.ActionCallback {
    private final int mImageConfigIdx;
    private final String mFilenameSuffix;

    private long mSuccessCount;
    private long mFailureCount;
    private boolean mCameraIsClosed;

    CaptureCallback(int imageConfigIdx, @Nullable String filenameSuffix) {
      mImageConfigIdx = imageConfigIdx;
      mFilenameSuffix = filenameSuffix;
    }

    @Override
    public void onActionCompleted(boolean success,
                                  @Nullable CameraController.ImageWrapper[] images,
                                  @Nullable CameraController.CaptureData captureData,
                                  @Nullable CameraCharacteristics characts) {
      final CameraController.Parameters params = captureData == null ? null : captureData.params;

      if (success) mSuccessCount++;
      else mFailureCount++;

      boolean isLast = false;
      if (mSuccessCount + mFailureCount == mCfg.numImages) {
        isLast = true;
        Log.d(TAG, "Repeating capture completed after ", mSuccessCount, " successful and ",
            mFailureCount, " dropped frames");
      }

      if (!success && (captureData == null || !captureData.frameDropped)) {
        Log.w(TAG, "Repeating capture failed after ", mSuccessCount, " successful and ",
            mFailureCount, " dropped frames");
        if (!mCameraIsClosed) {
          mDone.run();
          mCameraIsClosed = true;
        }
        new RotateLogsAndTransferFiles(mContext, null, !mCfg.isEvent).run();
        return;
      } else if (!success || params == null || images == null || isNull(images, 1)) {
        // Frame dropped. Run clean-up code if this was the last frame, but ignore otherwise.
        if (mCfg.numImagesToClose.decrementAndGet() == 0 && !mCameraIsClosed) {
          mDone.run();
          mCameraIsClosed = true;
        }
        new RotateLogsAndTransferFiles(mContext, mCfg.numImagesToProcess, !mCfg.isEvent).run();
        return;
      }

      // Initialise the general data needed for postprocessing.
      final Postprocessor.Data data = new Postprocessor.Data(new Postprocessor.SharedData());
      data.image = images[0];
      data.captureParams = params;
      data.imageConfig = params.imageConfigs.get(mImageConfigIdx);
      data.captureResult = captureData.result;
      data.cameraCharacteristics = characts;

      // Generate a new file timestamp. Append a final underscore to mark these images as
      // computational extras unless an explicit suffix is given.
      String timestamp = Settings.getTimestampFileName(params.captureTime, "");
      String outPathNoExt =
          Settings.getAppFolderPath() + Settings.APP_DIR_TMP + "/" + timestamp + "_";
      if (mFilenameSuffix != null) outPathNoExt += mFilenameSuffix;

      // Prepare the callback to run when postprocessing is complete.
      Runnable cb = isLast
          ? new RotateLogsAndTransferFiles(mContext, null, !mCfg.isEvent)
          : () -> AwsClient.uploadFiles(mContext, false);

      // Start image postprocessing.
      postprocess(data, outPathNoExt, Settings.APP_DIR_NEW, mCfg.maxImageSize, false, cb);
    }
  } // CaptureCallback

  /**
   * Configure and execute the postprocessing pipeline.
   *
   * @param data         The data to be passed into the pipeline.
   * @param outPathNoExt The output path without file type extension.
   * @param jpegDestDir  The directory path, relative to the app's root dir, where to move the JPEG
   *                     after processing.
   * @param maxImageSize The target size of the larger dimension of the final image.
   * @param writeExif    Write EXIF data if true <b>and</b> requestIdx == 0.
   * @param callback     If non-null, callback will be run when postprocessing finishes.
   */
  private void postprocess(@NonNull final Postprocessor.Data data,
                           @NonNull final String outPathNoExt, @NonNull final String jpegDestDir,
                           int maxImageSize, boolean writeExif, @Nullable Runnable callback) {
    if (data.image == null) {
      Log.e(TAG, "Cannot do postprocessing, the image is null");
      return;
    } else if (data.imageConfig == null) {
      Log.e(TAG, "Cannot do postprocessing, the image config is null");
      return;
    }

    // Prepare the postprocess pipeline.
    final ArrayList<Postprocessor.Stage> stages = new ArrayList<>(10);

    // Save the image.
    stages.add(new ImageFileWriterStage(outPathNoExt, mCfg.numImagesToClose, mDone));

    if (data.image.data.getFormat() == ImageFormat.RAW_SENSOR) {
      // Add a DNG to JPEG conversion stage if needed. This also applies the crop.
      float scaleFactor = (float) maxImageSize / (float) data.imageConfig.maxDimension;
      stages.add(new DngConverterStage(mContext, scaleFactor, mCfg.cropRegion));
    } else if (mCfg.cropRegion != null) {
      // Crop the image.
      stages.add(new CropJPEGStage(mCfg.cropRegion, data.imageConfig.jpegQuality));
    }

    // Add the EXIF writer stage.
    if (writeExif) stages.add(new ExifWriterStage(null));

    // Add a stage to move the JPEG to its target directory.
    stages.add(new MoveJPEGStage(jpegDestDir));

    // Execute the pipeline.
    Postprocessor processor = new Postprocessor(mContext, stages, data, callback);
    processor.execute();
  }


  protected static class Config extends PhotoCaptureBase.Config {
    protected double repeatFPS; // The frame rate of repeating captures.
    protected boolean repeatLocked; // Wheter repeating photos all use the same capture settings.

    protected Config(@NonNull Settings settings, @NonNull Bundle extras) {
      super(settings, extras);

      numImages = extras.getInt(ConfigKeys.CamRepeatCount.extraName(),
          settings.getInt(ConfigKeys.CamRepeatCount));
      if (numImages < 0) numImages = 1;

      repeatFPS = extras.getDouble(ConfigKeys.CamRepeatFPS.extraName(),
          settings.getDouble(ConfigKeys.CamRepeatFPS));
      if (repeatFPS <= 0.) repeatFPS = 1.;
      repeatLocked = extras.getBoolean(ConfigKeys.CamRepeatLocked.extraName(),
          settings.getBool(ConfigKeys.CamRepeatLocked));
      imageConfig.imageBuffers = numImages == 0 ? 16 : Math.min(16, numImages);


      if (numImages == 0) {
        numImagesToClose = new AtomicInteger(Integer.MAX_VALUE);
        numImagesToProcess = new AtomicInteger(Integer.MAX_VALUE);
      } else {
        numImagesToClose = new AtomicInteger(numImages);
        numImagesToProcess = new AtomicInteger(numImages);
      }

      Log.d(TAG, "Preparing repeating capture of ",
          (numImages == 0 ? "unlimited" : numImages), " images at ", repeatFPS, " fps");
    }
  } // Config
}

