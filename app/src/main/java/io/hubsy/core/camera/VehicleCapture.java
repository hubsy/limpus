package io.hubsy.core.camera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.RectF;
import android.hardware.camera2.CameraCharacteristics;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.hubsy.core.AwsClient;
import io.hubsy.core.Constants;
import io.hubsy.core.Settings;
import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.camera.postprocess.MapUtils;
import io.hubsy.core.camera.postprocess.NumberPlateOCRCallback;
import io.hubsy.core.camera.postprocess.ObjectDetectionCallback;
import io.hubsy.core.camera.postprocess.Postprocessor;
import io.hubsy.core.display.DeviceManager;
import io.hubsy.core.display.DisplayTask;
import io.hubsy.core.display.RS232Session;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;
import io.hubsy.core.util.WriteBufferToFileTask;

/**
 * A CaptureManager to perform number plate recognition. It captures videos, still images for
 * recognition and thumbnails.
 */
abstract class VehicleCapture extends VideoCaptureBase {
  private static final String TAG = "VehicleCapture";

  // Created the first time it is used and then kept around for further use.
  @SuppressLint("StaticFieldLeak") // It only stores the app context.
  private static DeviceManager sDeviceManager;

  private final Context mContext;
  private final Config mCfg;

  // Cannot be declared static in the inner class where it is used, so do it here. Not thread-safe!
  private static final SimpleDateFormat sFrameIDFormatter =
      new SimpleDateFormat("HHmmss.SSS", Locale.ROOT);

  // Timestamp in SystemClock.elapsedRealtime before the next capture should be started.
  private static long sPauseCaptureUntil;

  protected VehicleCapture(@NonNull Context context, @NonNull Config config,
                           @NonNull Runnable done) {
    super(config, done);
    mCfg = config;
    mContext = context.getApplicationContext();
  }

  @Override
  public void capture() {
    if (mCameraController == null) {
      mDone.run();
      return;
    }

    int[] configIdxs = new int[]{0, 1};
    mCameraController.recordVideo(configIdxs, true, newCaptureCallback());
  }

  @Override
  public void updateConfig(@NonNull Bundle extras) {
    super.updateConfig(extras);

    // Update the speed reading and timestamp of the ongoing capture.
    int speed = extras.getInt(Constants.EXTRA_SPEED, Integer.MIN_VALUE);
    mCfg.maxSpeedReading = Math.max(speed, mCfg.maxSpeedReading);
    if (speed != Integer.MIN_VALUE) mCfg.latestSpeedReading = speed;
  }

  /**
   * Must return a new instance of the callback.
   */
  protected abstract CaptureCallback newCaptureCallback();


  protected abstract class CaptureCallback implements CameraController.ActionCallback,
      ObjectDetectionCallback, NumberPlateOCRCallback {

    private final Handler mMainHandler;

    private boolean mRecordingStopRequested;
    private boolean mRecordingStopped;
    private boolean mRecordingAborted;

    private long mLastFrameTime;
    private int mNumFramesInFlight;
    private final int mMaxFramesInFlight;

    private CameraController.Parameters mVideoParams;

    private int mNumVehiclesDetected;
    private Map<Long, RectF> mVehicleBboxes;
    private long mLastVehicleBboxCaptureTime;

    private Map<Long, RectF> mNPBboxes;
    private long mLastNPBboxCaptureTime;

    private String mNPText;
    private float mNPTextConfidence;
    private Postprocessor.Data mNPTextFrameData;
    private ByteBuffer mNPCropJPEG; // The cropped number plate image; JPEG encoded.

    private long mLastDetectionTime; // Timestamp of the last successful object detection.
    private long mLastOCRTime; // Timestamp of the last successful OCR result.
    private int mNumIgnoredOCRResults; // Number of OCR results ignored since the last update.

    private CameraController.ImageWrapper mThumbnail;
    private CameraController.Parameters mThumbnailParams;
    private boolean mThumbnailFullyProcessed; // True when the frame has finished postprocessing.
    private int mThumbnailSpeed;

    // Identifies the sequence of display updates done by this instance.
    private final long mDisplaySequenceID = SystemClock.elapsedRealtime();
    // The latest strings sent to update the lines of the display.
    private String mDisplayLatestLine0;
    private String mDisplayLatestLine1;

    private static final String sMapKeySpeed = "speed"; // Type int.
    private static final String sMapKeyThumbnail = "thumbnail"; // Type ImageWrapper.
    protected static final String sMapKeyNumberPlate = "number_plate"; // Type ByteBuffer - JPEG.

    protected static final int sObjectTypeIDVehicle = 0; // Type ID for vehicle detections.
    protected static final int sObjectTypeIDNumberPlate = 1; // Type ID for number plate detections.

    /**
     * @param maxFramesInFlight The maximum number of concurrent calls to
     *                          {@link #analyseFrame(Postprocessor.Data, Postprocessor.Stage)}.
     *                          Frames beyond this limit will be dropped and are a sign of the app
     *                          not keeping up.
     */
    protected CaptureCallback(int maxFramesInFlight) {
      mMaxFramesInFlight = maxFramesInFlight;
      mMainHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void onActionCompleted(boolean success,
                                  @Nullable CameraController.ImageWrapper[] images,
                                  @Nullable CameraController.CaptureData captureData,
                                  @Nullable CameraCharacteristics characts) {
      final CameraController.Parameters params = captureData == null ? null : captureData.params;
      if (!success || params == null) {
        if (!mRecordingAborted) Log.w(TAG, "Recording video failed");
        cleanup();
        return;
      }

      if (params.outFile != null) {
        mRecordingStopped = true;
        processVideo(params, params.outFile);
      } else {
        processImages(images, captureData, characts);
      }
    }

    private void processVideo(@NonNull CameraController.Parameters params, @NonNull File video) {
      if (mRecordingAborted) {
        // Delete the video file.
        if (!video.delete()) {
          Log.w(TAG, "Failed to delete the aborted video file: ", video.getName());
        }
        params.outFile = null;
      } else {
        Log.d(TAG, "Video recorded successfully, frames in postprocessing: ", mNumFramesInFlight);

        // Rename the video file within the temp folder so it is not overwritten by a new video
        // recording and to add event/start and end timestamps to the file name.
        String endTime = Settings.getTimestampFileName(params.captureTime, "");
        String tmpName = mCfg.timestampFmt + "_" + endTime + ".mp4";
        File tmpFile = Settings.getFileInAppDir(Settings.APP_DIR_TMP, tmpName);
        if (!video.renameTo(tmpFile)) {
          Log.w(TAG, "Failed to rename the video file: ", video.getName());
          cleanup();
          return;
        }
        params.outFile = tmpFile;
      }

      mVideoParams = params;
      if (mNumFramesInFlight == 0) finaliseRecording();
    }

    private void processImages(@Nullable CameraController.ImageWrapper[] images,
                               @NonNull CameraController.CaptureData captureData,
                               @Nullable CameraCharacteristics characts) {
      final CameraController.Parameters params = captureData.params;
      if (params == null || images == null || isNull(images, 2)) {
        Log.w(TAG, "Taking still image failed");
        cleanup();
        return;
      }

      // Drop frames received after recording has stopped or is in the process of stopping.
      if (mRecordingStopped || mRecordingStopRequested) {
        images[0].close();
        images[1].close();
        return;
      }

      final long now = SystemClock.elapsedRealtime();

      // If this is the first frame, check if the exposure time exceeds the threshold.
      if (mLastFrameTime == 0 && params.exposureTime > mCfg.maxExposureTimeNanos) {
        Log.i(TAG, "The exposure time is too long: ", (params.exposureTime / 1000L), "us");
        images[0].close();
        images[1].close();
        sPauseCaptureUntil = now + 15 * 60000L; // 15 mins.
        mRecordingAborted = true;
        mRecordingStopRequested = true;
        if (mCameraController != null) mCameraController.stopVideo();
        return;
      }

      // Drop this frame if the rate is higher than desired.
      if (now < (mLastFrameTime + mCfg.frameInterval)) {
        Log.v(TAG, "Dropping frame, frame rate too high");
        images[0].close();
        images[1].close();
        return;
      }

      // Drop frames if we're not keeping up with processing.
      if (mNumFramesInFlight >= mMaxFramesInFlight) {
        Log.d(TAG, "Dropping frame, too many already in flight");
        images[0].close();
        images[1].close();
        return;
      }

      final String frameID = sFrameIDFormatter.format(new Date(params.captureTime));
      Log.d(TAG, "Processing video frame ", frameID);
      mLastFrameTime = now;
      mNumFramesInFlight++;

      // Initialise the general data needed for postprocessing.
      final Postprocessor.Data data = new Postprocessor.Data(mCfg.sharedData);
      data.id = frameID;
      data.image = images[0];
      data.captureParams = params;
      data.imageConfig = params.imageConfigs.get(0);
      data.captureResult = captureData.result;
      data.cameraCharacteristics = characts;
      data.attributes = new HashMap<>(32);
      data.attributes.put(sMapKeySpeed, mCfg.latestSpeedReading);
      data.attributes.put(sMapKeyThumbnail, images[1]);

      if (mThumbnail == null) replaceThumbnail(data); // Until a better one is found.

      analyseFrame(data, mFrameProcessedStage);
    }

    /**
     * Analyses the frame.
     *
     * @param data The frame data.
     * @param last Must be processed as the last stage of the pipeline.
     */
    protected abstract void analyseFrame(@NonNull final Postprocessor.Data data,
                                         @NonNull final Postprocessor.Stage last);

    /**
     * Called as the last stage of the frame processing pipeline.
     */
    private final Postprocessor.Stage mFrameProcessedStage = (data, done) -> {
      // Close the thumbnail for this frame if it is not the currently selected thumbnail.
      Object thumb = data.attributes.remove(sMapKeyThumbnail);
      if (thumb == mThumbnail && thumb != null) {
        mThumbnailFullyProcessed = true;
      } else {
        closeImageIfNotNull(thumb);
      }
      done.run();

      if (!mRecordingStopped && !mRecordingStopRequested) {
        final long now = SystemClock.elapsedRealtime();
        final long maxElapsedTime = (1 + mCfg.maxFailedDetections) * mCfg.frameInterval;
        boolean stopCapture = false;

        // Check if no more objects have been detected for too long after at least one was detected
        // previously. The assumption is that the object is no longer visible.
        if (mLastDetectionTime > 0 && now > mLastDetectionTime + maxElapsedTime) {
          Log.d(TAG, "Too many frames with no detection, stopping the recording");
          stopCapture = true;
        }

        // Check if no number plate has been read for too long after the last successful read.
        if (mLastOCRTime > 0 && now > mLastOCRTime + maxElapsedTime && !stopCapture) {
          Log.d(TAG, "Too many frames with no number plate text, stopping the recording");
          stopCapture = true;
        }

        if (stopCapture) {
          mRecordingStopRequested = true;
          if (mCameraController != null) mCameraController.stopVideo();
        }
      }

      mNumFramesInFlight--;
      Log.d(TAG, (mRecordingStopped ? "Recording stopped" : "Recording in progress"),
          ", frames in postprocessing: ", mNumFramesInFlight);
      if (mNumFramesInFlight == 0 && mRecordingStopped) finaliseRecording();
    };

    /**
     * Report the number of vehicles detected during the analysis of a single frame. This should be
     * the number of valid objects after filtering, but before any top-n selection to limit results.
     */
    protected void reportValidVehiclesInFrame(int count) {
      mNumVehiclesDetected += count;
    }

    /**
     * Invoked when an object has been detected. See {@link ObjectDetectionCallback} for details.
     */
    @SuppressLint("UseSparseArrays")
    @Override
    public void objectDetected(@NonNull Postprocessor.Data data, int id, @NonNull RectF bbox,
                               float confidence) {
      mLastDetectionTime = SystemClock.elapsedRealtime();

      final long captureTime = data.captureParams.captureTime;
      if (id == sObjectTypeIDVehicle) {
        if (mVehicleBboxes == null) mVehicleBboxes = new HashMap<>();
        mVehicleBboxes.put(captureTime, bbox);

        if (captureTime > mLastVehicleBboxCaptureTime) {
          mLastVehicleBboxCaptureTime = captureTime;

          // Replace the thumbnail if no number plate has been detected yet.
          if (mLastNPBboxCaptureTime == 0) replaceThumbnail(data);
        }
      } else if (id == sObjectTypeIDNumberPlate) {
        if (mNPBboxes == null) mNPBboxes = new HashMap<>();
        mNPBboxes.put(captureTime, bbox);

        if (captureTime > mLastNPBboxCaptureTime) {
          mLastNPBboxCaptureTime = captureTime;

          // Replace the thumbnail and number plate crop if no NP has been recognised (read) yet.
          if (mNPText == null) {
            replaceNumberPlateCrop(data);
            replaceThumbnail(data);
          }
        }
      }
    }

    /**
     * Invoked when the number plate OCR stage has processed a frame.
     * See {@link NumberPlateOCRCallback#numberPlate} for more details.
     */
    @Override
    public void numberPlate(@NonNull Postprocessor.Data data, @Nullable String text,
                            float confidence) {
      if (text == null || confidence < mCfg.minNPOCRConfidence) return;

      // Number plate successfully read.
      mLastOCRTime = mLastDetectionTime = SystemClock.elapsedRealtime();

      if (mNPText == null) {
        replaceNumberPlate(data, text, confidence);
        displayNumberPlate();
        Log.d(TAG, "Setting the number plate to ", text);
        return;
      }

      // Replace the previous result if this frame was captured later and still has the same or a
      // similar (hopefully corrected) number plate text or a significantly higher confidence.
      final boolean isIdentical = mNPText.equals(text);
      if (confidence >= mNPTextConfidence) {
        if (isIdentical || confidence >= mNPTextConfidence * 1.05f || similar(mNPText, text, 2)) {
          final String oldText = mNPText;
          replaceNumberPlate(data, text, confidence);

          if (isIdentical) {
            Log.d(TAG, data.id, ": Replacing number plate frame data for ", text);
            if (mCfg.displayEnabled && mCfg.displayDuration > 0) {
              // Reset the time before the display is cleared if it is in use.
              mMainHandler.removeCallbacks(mDisplayClearanceCallback);
              mMainHandler.postDelayed(mDisplayClearanceCallback, mCfg.displayDuration);
            }
          } else {
            Log.d(TAG, data.id, ": Replacing number plate ", oldText, " with ", text);
            displayNumberPlate(); // Update the display if in use.
          }
        } else {
          Log.d(TAG, data.id, ": Keeping number plate ", mNPText, ", ", text, " differs too much");
          mNumIgnoredOCRResults++;
        }
      } else { // Lower confidence.
        if (isIdentical) {
          Log.d(TAG, data.id, ": Keeping earlier frame with higher confidence for number plate ",
              text);
        } else {
          Log.d(TAG, data.id, ": Keeping earlier frame with higher confidence and number plate ",
              mNPText, " instead of ", text);
          mNumIgnoredOCRResults++;
        }
      }

      if (mNumIgnoredOCRResults > mCfg.maxFailedDetections && !mRecordingStopRequested) {
        // Assume the tracked vehicle has moved out of view for good.
        Log.d(TAG, data.id, "Too many ignored OCR results, stopping the recording");
        mRecordingStopRequested = true;
        if (mCameraController != null) mCameraController.stopVideo();
      }
    }

    /**
     * Replaces the current number plate text and associated frame data with the given data.
     */
    private void replaceNumberPlate(@NonNull Postprocessor.Data data, @NonNull String text,
                                    float confidence) {
      mNumIgnoredOCRResults = 0;

      mNPText = text;
      mNPTextFrameData = data;
      mNPTextConfidence = confidence;

      // The thumbnail and NP crop for this frame have already been set in objectDetected if this is
      // the first time this method is called, but it is possible that they were already replaced
      // again by the next frame finishing the NP object detection stage while mNPText is still
      // null, and so they need to be set again to be sure consistent frame data is used.
      replaceNumberPlateCrop(data);
      replaceThumbnail(data);
    }

    /**
     * Replaces the current number plate crop buffer with a copy of the one stored under
     * {@link #sMapKeyNumberPlate}.
     */
    private void replaceNumberPlateCrop(@NonNull Postprocessor.Data data) {
      ByteBuffer jpg = MapUtils.getDirectByteBuffer(data.attributes, sMapKeyNumberPlate, TAG, true);
      if (jpg != null) {
        // The jpg buffer must be copied, as it is allocated in native code and gets freed before
        // postprocessing of the frame finishes.
        if (mNPCropJPEG == null || mNPCropJPEG.capacity() < jpg.limit()) {
          Log.d(TAG, data.id, ": Copy NP JPG into new buffer");
          // Allocate slightly more than required to reduce re-allocations in future replacements.
          mNPCropJPEG = ByteBuffer.allocateDirect((int) (jpg.limit() * 1.33));
        } else {
          Log.d(TAG, data.id, ": Copy NP JPG into existing buffer");
          mNPCropJPEG.clear();
        }
        jpg.rewind();
        mNPCropJPEG.put(jpg);
        mNPCropJPEG.flip();
      }
    }

    /**
     * Retrieves the thumbnail image and other required information for the frame from the data and
     * replaces the current thumbnail. The old thumbnail image is closed as long as it is not equal
     * to the new one. Does not hold on to the data or attributes to avoid keeping other buffers
     * stored in there alive for longer than necessary.
     */
    private void replaceThumbnail(@NonNull Postprocessor.Data data) {
      Object image = data.attributes.get(sMapKeyThumbnail);
      Object speed = data.attributes.get(sMapKeySpeed);

      // Close the old thumbnail if its frame has already been fully processed. If it hasn't, then
      // it will be closed when it finishes processing instead.
      if (mThumbnailFullyProcessed && mThumbnail != null && mThumbnail != image) {
        mThumbnail.close();
      }

      mThumbnailFullyProcessed = false;
      mThumbnail = (CameraController.ImageWrapper) image;
      mThumbnailSpeed = speed != null ? (int) speed : mCfg.maxSpeedReading;
      mThumbnailParams = data.captureParams;
    }

    /**
     * Convenience function, which assumes the given image is either null or of type
     * {@link io.hubsy.core.camera.CameraController.ImageWrapper}, in which case it is closed.
     */
    private void closeImageIfNotNull(@Nullable Object image) {
      if (image != null) ((CameraController.ImageWrapper) image).close();
    }

    /**
     * Decides if strings a and b are similar.
     *
     * @param a       The first string.
     * @param b       The second string.
     * @param maxDiff The max. number of characters allowed to differ (includes additional chars).
     * @return True if the strings are similar.
     */
    private boolean similar(@NonNull String a, @NonNull String b, int maxDiff) {
      if (a.equals(b)) return true;

      // Similar enough if all but maxDiff characters match, counting additional characters the
      // same as a mismatched char.
      int matching = 0;
      for (int i = 0, j = 0; i < a.length() && j < b.length(); ) {
        if (a.charAt(i) == b.charAt(j)) {
          matching++;
          i++;
          j++;
        } else {
          int remainingA = a.length() - i - 1;
          int remainingB = b.length() - j - 1;
          // Increment the one with more chars left, or both if they have an equal number left.
          if (remainingA >= remainingB) i++;
          if (remainingB >= remainingA) j++;
        }
      }
      return (Math.max(a.length(), b.length()) - matching) <= maxDiff;
    }

    /**
     * Displays the number plate and speed value that was current when the frame was taken if an
     * output device is available.
     */
    private void displayNumberPlate() {
      if (!mCfg.displayEnabled) return;

      // Get the speed value that was current when the selected frame was captured.
      Object speedObj = mNPTextFrameData == null || mNPTextFrameData.attributes == null
          ? null : mNPTextFrameData.attributes.get(sMapKeySpeed);
      if (speedObj == null || mNPText == null) return;

      // Decide what to display on which output line.
      String speedVal = String.format(Locale.ROOT, "%d kmh", (int) speedObj);
      String line0 = mCfg.displayLineSpeed == 0 ? speedVal
          : mCfg.displayLineNumberPlate == 0 ? mNPText : null;
      String line1 = mCfg.displayLineSpeed == 1 ? speedVal
          : mCfg.displayLineNumberPlate == 1 ? mNPText : null;

      display(line0, line1);
    }

    /**
     * Displays the given lines of text on the output device, if any. The text is cleared after
     * mDisplayDuration millis.
     * <p>
     * A null value means the respective line will not be updated, it does not clear it. To clear a
     * line, send a space.
     */
    private void display(@Nullable String line0, @Nullable String line1) {
      if (!mCfg.displayEnabled || (line0 == null && line1 == null)) return;

      if (sDeviceManager == null) {
        sDeviceManager = new DeviceManager(mContext);
      }

      final RS232Session session = sDeviceManager.getSession();
      if (session == null || !session.isReady()) {
        Log.d(TAG, "The display session is not ready yet");
        return;
      }

      // Check if the lines have actually changed.
      line0 = mDisplayLatestLine0 == null || !mDisplayLatestLine0.equals(line0) ? line0 : null;
      line1 = mDisplayLatestLine1 == null || !mDisplayLatestLine1.equals(line1) ? line1 : null;
      mDisplayLatestLine0 = line0 == null ? mDisplayLatestLine0 : line0;
      mDisplayLatestLine1 = line1 == null ? mDisplayLatestLine1 : line1;

      if (line0 != null || line1 != null) { // At least one line must have changed.
        new DisplayTask(session, line0, line1, mDisplaySequenceID)
            .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
      }

      // Replace any previous clearance task with a new one.
      if (mCfg.displayDuration > 0) {
        mMainHandler.removeCallbacks(mDisplayClearanceCallback);
        mMainHandler.postDelayed(mDisplayClearanceCallback, mCfg.displayDuration);
      }
    }

    private final Runnable mDisplayClearanceCallback = () -> display(" ", " ");

    /**
     * Renames the finished video file, writes the thumbnail and finalises the recording process.
     */
    private void finaliseRecording() {
      if (mRecordingAborted || mVideoParams == null || mVideoParams.outFile == null) {
        if (mRecordingAborted) Log.d(TAG, "Finalising the aborted recording");
        else Log.e(TAG, "Finalising the failed recording");
        cleanup();
        return;
      }
      Log.d(TAG, "Finalising the recording");

      final boolean haveThumbnail = mThumbnail != null && mThumbnailParams != null;
      final long thumbCaptureTime = haveThumbnail ? mThumbnailParams.captureTime : 0;
      final RectF vehicleBbox = haveThumbnail && mVehicleBboxes != null
          ? mVehicleBboxes.get(thumbCaptureTime) : null;
      final RectF npBbox = haveThumbnail && mNPBboxes != null
          ? mNPBboxes.get(thumbCaptureTime) : null;
      final boolean haveNPText = mNPText != null && mNPTextFrameData != null;

      // The recording was successful. Rename the video file to include data from image analysis.
      String fname = mVideoParams.outFile.getName();

      // Remove the file extension.
      final String videoExt = fname.substring(fname.length() - 4);
      fname = fname.substring(0, fname.length() - 4);

      // Record the video capture fps and the speed reading that was current when the thumbnail
      // frame was captured in the file name.
      fname = String.format(Locale.ROOT, "%s_fps%d_sp%03d", fname,
          (int) mCfg.params.videoCaptureFPS,
          haveThumbnail ? mThumbnailSpeed : mCfg.maxSpeedReading);

      // Append the normalised vehicle and number plate bbox coords (left, top, width, height).
      // Adjust them for the crop region if necessary.
      final RectF crop = mCfg.cropRegion != null ? mCfg.cropRegion : new RectF(0, 0, 1, 1);
      if (vehicleBbox != null) {
        fname = String.format(Locale.ROOT, "%s_vb%.4f-%.4f-%.4f-%.4f", fname,
            crop.left + vehicleBbox.left * crop.width(),
            crop.top + vehicleBbox.top * crop.height(),
            vehicleBbox.width() * crop.width(),
            vehicleBbox.height() * crop.height());
      }
      if (npBbox != null) {
        fname = String.format(Locale.ROOT, "%s_bb%.4f-%.4f-%.4f-%.4f", fname,
            crop.left + npBbox.left * crop.width(),
            crop.top + npBbox.top * crop.height(),
            npBbox.width() * crop.width(),
            npBbox.height() * crop.height());
      }

      if (haveNPText) { // Append the number plate with spaces replaced by hyphens.
        fname += "_np" + mNPText.replace(" ", "-");
      }

      if (haveThumbnail) { //  Append the timestamp of the thumbnail frame.
        fname += "_rk" + Settings.getTimestampFileName(thumbCaptureTime, "");
      }

      Log.d(TAG, "Filtered vehicle count: ", mNumVehiclesDetected);
      if (haveNPText) {
        Log.d(TAG, "Using number plate from frame ", mNPTextFrameData.id,
            " confidence=", mNPTextConfidence);
      } else {
        Log.d(TAG, "No number plate read for video");
      }

      // One or both of the bounding boxes must have been detected to count a success.
      final boolean isSuccess = vehicleBbox != null || npBbox != null;

      final String videoName = fname + videoExt;
      final String videoDirTo = !mCfg.videoUploadEnabled || !isSuccess
          ? Settings.APP_DIR_ARCHIVED : Settings.APP_DIR_NEW;
      Helpers.moveFile(mVideoParams.outFile, videoDirTo, videoName);

      if (haveThumbnail) {
        // Save the thumbnail, write its EXIF data (mainly for the orientation) and save the number
        // plate crop, if available, before cleaning up.
        Log.d(TAG, "Saving thumbnail");
        finaliseThumbnailAndNPCrop(fname, isSuccess);
      } else {
        cleanup(); // Clean up now.
      }
    }

    /**
     * Write the thumbnail and, if available, the cropped number plate image to files and archive
     * them locally or queue them for uploading, depending on success and config.
     * <p>
     * Calls {@link #cleanup()} when the async. tasks have completed.
     *
     * @param fnameNoExt The file name without file extension.
     * @param isSuccess  True if the capture is considered successful.
     */
    private void finaliseThumbnailAndNPCrop(String fnameNoExt, boolean isSuccess) {
      final String thumbName = fnameNoExt + (isSuccess ? ".jpg" : "_.jpg");
      final File thumbTmp = Settings.getFileInAppDir(Settings.APP_DIR_TMP, thumbName);
      final String thumbDirTo = isSuccess || mCfg.dataUploadEnabled
          ? Settings.APP_DIR_NEW : Settings.APP_DIR_ARCHIVED;

      // Final callback to move the thumbnail and clean up.
      final Runnable moveThumbnailAndCleanup = () -> {
        Helpers.moveFile(thumbTmp, thumbDirTo);
        cleanup();
      };

      // Second to last callback to write and move the number plate crop.
      final Runnable whenThumbnailWritten;
      if (isSuccess && mNPCropJPEG != null) {
        // Write the number plate crop and then queue it for uploading.
        final String npCropName = fnameNoExt + AwsClient.NUMBER_PLATE_CROP_SUFFIX;
        final File npCropTmp = Settings.getFileInAppDir(Settings.APP_DIR_TMP, npCropName);
        whenThumbnailWritten = () -> new WriteBufferToFileTask(mNPCropJPEG, npCropTmp, () -> {
          Helpers.moveFile(npCropTmp, Settings.APP_DIR_NEW);
          moveThumbnailAndCleanup.run();
        }, mMainHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
      } else {
        whenThumbnailWritten = moveThumbnailAndCleanup;
      }

      // Save the thumbnail and write its EXIF data in chained async tasks.
      new ImageFileWriter(mThumbnail.data, thumbTmp, null, null, 0,
          () -> new ExifWriter(thumbTmp, mThumbnailParams, null, whenThumbnailWritten,
              mMainHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR),
          mMainHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * Clean up resources used for video and still image processing, whether they completed
     * successfully or not.
     */
    protected void cleanup() {
      Log.d(TAG, "Cleaning up after video capture");
      closeImageIfNotNull(mThumbnail);
      mDone.run();
      new RotateLogsAndTransferFiles(mContext, null, false).run();
    }
  } // CaptureCallback


  protected static class Config extends VideoCaptureBase.Config {
    protected boolean dataUploadEnabled; // Toggles uploading of computational images.
    protected boolean numberPlateDetectionEnabled; // Togles number plate detection.
    protected boolean numberPlateRecognitionEnabled; // Togles number plate recognition.

    protected int maxSpeedReading; // The max. speed reading so far.
    protected int latestSpeedReading; // The latest speed reading if available.

    private long maxExposureTimeNanos; // The longest allowed exposure time in nanoseconds.
    protected long frameInterval; // The interval between consecutive frames to be processed, in ms.
    protected int maxFailedDetections;

    protected float minVehicleBboxConfidence; // The min. confidence for vehicle localisation.
    protected float minNPBboxConfidence; // The min. confidence for number plate localisation.
    protected float minNPOCRConfidence; // The min. confidence for number plate OCR.

    protected int displayDuration; // The display duration for messages.
    protected int displayLineSpeed; // The display line on which to show the speed measurement.
    protected int displayLineNumberPlate; // The display line on which to show the number plate.
    protected boolean displayEnabled; // Toggles the output to the display on/off.

    protected Config(@NonNull Settings settings, @NonNull Bundle extras) {
      super(settings, extras);

      // Check if capture is paused and abort before any further resources are initialised.
      final long now = SystemClock.elapsedRealtime();
      if (now < sPauseCaptureUntil) {
        Log.d(TAG, "Capture paused for another ", ((sPauseCaptureUntil - now) / 1000L), "s");
        throw new ConfigurationException();
      }

      // Boolean flags.
      dataUploadEnabled = settings.getBool(ConfigKeys.EnableDataUpload);
      numberPlateDetectionEnabled = settings.getBool(ConfigKeys.EnableNumberPlateDetection);
      numberPlateRecognitionEnabled = settings.getBool(ConfigKeys.EnableNumberPlateRecognition);

      // Set the initial speed reading.
      maxSpeedReading = extras.getInt(Constants.EXTRA_SPEED, Integer.MIN_VALUE);
      if (maxSpeedReading != Integer.MIN_VALUE) {
        latestSpeedReading = maxSpeedReading;
      }

      // Add an image config for YUV still image captures while recording.
      CameraController.ImageConfig stillConfig = new CameraController.ImageConfig();
      params.imageConfigs.add(stillConfig);
      stillConfig.format = ImageFormat.YUV_420_888;
      stillConfig.imageBuffers = 10;
      stillConfig.maxDimension = settings.getInt(ConfigKeys.VideoStillReso);
      stillConfig.aspectRatio = settings.getString(ConfigKeys.CamAspectRatio);

      // And a second still image config for low resolution JPEG thumbnails.
      CameraController.ImageConfig thumbConfig = new CameraController.ImageConfig();
      params.imageConfigs.add(thumbConfig);
      thumbConfig.format = ImageFormat.JPEG;
      thumbConfig.imageBuffers = 10;
      thumbConfig.jpegQuality = settings.getInt(ConfigKeys.JPEGQuality);
      if (thumbnailReso <= 0) thumbnailReso = 640; // Not optional at the moment.
      thumbConfig.maxDimension = thumbnailReso;
      thumbConfig.aspectRatio = "4:3";

      maxExposureTimeNanos = settings.getInt(ConfigKeys.DetectorMaxExposureTime) * 1000000L;
//      frameInterval = params.videoCaptureFPS > 0 ? (long) (1000 / params.videoCaptureFPS) : 333;
      frameInterval = 333; // Target 3 fps for processing, independent from the video capture fps.

      minVehicleBboxConfidence = (float) settings.getDouble(ConfigKeys.VehicleBboxMinConfidence);
      minNPBboxConfidence = (float) settings.getDouble(ConfigKeys.NumberPlateBboxMinConfidence);
      minNPOCRConfidence = (float) settings.getDouble(ConfigKeys.NumberPlateOCRMinConfidence);

      maxFailedDetections = settings.getInt(ConfigKeys.NumberPlateMaxFailedDetections);

      displayDuration = settings.getInt(ConfigKeys.DisplayDuration);
      displayLineNumberPlate = settings.getInt(ConfigKeys.DisplayLineNumberPlate);
      displayLineSpeed = settings.getInt(ConfigKeys.DisplayLineSpeed);
      displayEnabled = displayLineNumberPlate >= 0 || displayLineSpeed >= 0;
    }
  } // Config
}
