package io.hubsy.core.camera;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.services.rekognition.AmazonRekognitionClient;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import io.hubsy.core.AwsClient;
import io.hubsy.core.Settings;
import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.camera.postprocess.AWSNumberPlateOCRStage;
import io.hubsy.core.camera.postprocess.BufferCloserStage;
import io.hubsy.core.camera.postprocess.ImageCloserStage;
import io.hubsy.core.camera.postprocess.MoveJPEGStage;
import io.hubsy.core.camera.postprocess.Postprocessor;
import io.hubsy.core.camera.postprocess.YUVToJPEGStage;
import io.hubsy.core.util.Log;

class VehicleCaptureAWS extends VehicleCapture {
  private static final String TAG = "VehicleCaptureAWS";

  private final Context mContext;
  private final Config mCfg;

  protected VehicleCaptureAWS(@NonNull Context context, @NonNull Config config,
                              @NonNull Runnable done) {
    super(context, config, done);
    mCfg = config;
    mContext = context.getApplicationContext();
  }

  @Override
  protected CaptureCallback newCaptureCallback() {
    return new CaptureCallback();
  }


  private class CaptureCallback extends VehicleCapture.CaptureCallback {
    private final ExecutorService mRecognitionStageExecutor;
    private final Pattern mNumberPlateRegex = Pattern.compile("[A-Z0-9 ]{5,6}");

    private static final int MAX_FRAMES_IN_FLIGHT = 6;

    CaptureCallback() {
      super(MAX_FRAMES_IN_FLIGHT);

      mRecognitionStageExecutor = new ThreadPoolExecutor(0, 8,
          20L, TimeUnit.SECONDS, new SynchronousQueue<>());
    }

    @Override
    protected void analyseFrame(@NonNull final Postprocessor.Data data,
                                @NonNull final Postprocessor.Stage last) {
      // Configure the pipeline.
      final ArrayList<Postprocessor.Stage> stages = new ArrayList<>(6);
      stages.add(new YUVToJPEGStage(mCfg.cropRegion, mCfg.jpegQuality));
      stages.add(new ImageCloserStage(null, null));
      stages.add(new AWSNumberPlateOCRStage(null, mCfg.rekognitionClient,
          mRecognitionStageExecutor, mCfg.minNPOCRConfidence, mNumberPlateRegex,
          sObjectTypeIDNumberPlate, this, this));
      stages.add(new BufferCloserStage(null));
      // If the image output path was configured, then it needs to be moved for uploading.
      if (data.imageJpeg != null) stages.add(new MoveJPEGStage(Settings.APP_DIR_NEW));
      stages.add(last);

      // Execute the pipeline.
      Postprocessor processor = new Postprocessor(mContext, stages, data, null);
      processor.execute();
    }

    @Override
    protected void cleanup() {
      if (!mRecognitionStageExecutor.isShutdown()) mRecognitionStageExecutor.shutdownNow();
      super.cleanup();
    }
  } // CaptureCallback


  protected static class Config extends VehicleCapture.Config {

    protected int jpegQuality; // The JPEG quality for images sent to AWS.
    protected AmazonRekognitionClient rekognitionClient; // The client for text recognition.

    protected Config(@NonNull Settings settings, @NonNull Bundle extras) {
      super(settings, extras);

      // Get AWS settings.
      int quality = settings.getInt(ConfigKeys.JPEGQuality);
      jpegQuality = quality < 0 || quality > 100 ? 85 : quality;

      AWSCredentials creds = AwsClient.getAwsCreds(settings);
      if (creds == null) throw new ConfigurationException();

      // The region defaults to Sydney.
      String regionName = settings.getString(ConfigKeys.AwsRekognitionRegion);
      Region region = Region.getRegion(regionName);
      if (region == null) {
        Log.w(TAG, "Invalid AWS region for Rekognition: ", regionName);
        throw new ConfigurationException();
      }

      // Create the Rekognition client.
      final ClientConfiguration awsConfig = new ClientConfiguration();
      awsConfig.setConnectionTimeout(5000);
      awsConfig.setMaxConnections(CaptureCallback.MAX_FRAMES_IN_FLIGHT);
      awsConfig.setMaxErrorRetry(1);
      awsConfig.setSocketTimeout(3000);
      try {
        rekognitionClient = new AmazonRekognitionClient(creds, awsConfig);
        rekognitionClient.setRegion(region);
      } catch (Exception e) {
        Log.w(TAG, "Failed to set AWS Rekognition region: ", e.getMessage());
        throw new ConfigurationException();
      }
    }
  } // Config
}
