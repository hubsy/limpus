package io.hubsy.core.camera;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;

import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.util.ArrayList;
import java.util.List;

import io.hubsy.core.Settings;
import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.camera.postprocess.BufferCloserStage;
import io.hubsy.core.camera.postprocess.EncodeJPEGStage;
import io.hubsy.core.camera.postprocess.EventWaiterStage;
import io.hubsy.core.camera.postprocess.ImageCloserStage;
import io.hubsy.core.camera.postprocess.ListIteratorStage;
import io.hubsy.core.camera.postprocess.MapUtils;
import io.hubsy.core.camera.postprocess.MotionFilterStage;
import io.hubsy.core.camera.postprocess.NumberPlateOCRStage;
import io.hubsy.core.camera.postprocess.ObjectDetectionCallbackStage;
import io.hubsy.core.camera.postprocess.ObjectDetectionStage;
import io.hubsy.core.camera.postprocess.ObjectFilterStage;
import io.hubsy.core.camera.postprocess.Postprocessor;
import io.hubsy.core.camera.postprocess.RGBToGreyscaleStage;
import io.hubsy.core.camera.postprocess.ResizeBufferStage;
import io.hubsy.core.camera.postprocess.TransformRectStage;
import io.hubsy.core.camera.postprocess.YUVToRGBStage;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;

class VehicleCaptureLocal extends VehicleCapture {
  private static final String TAG = "VehicleCaptureLocal";

  private final Context mContext;
  private final Config mCfg;

  protected VehicleCaptureLocal(@NonNull Context context, @NonNull Config config,
                                @NonNull Runnable done) {
    super(context, config, done);
    mCfg = config;
    mContext = context.getApplicationContext();
  }

  @Override
  protected CaptureCallback newCaptureCallback() {
    return new CaptureCallback();
  }


  private class CaptureCallback extends VehicleCapture.CaptureCallback {

    private static final String sMapKeyFrameResizeOutput = "frame.resize.output";
    private static final String sMapKeyFrameGreyscaleOutput = "frame.greyscale.output";
    private static final String sMapKeyPreviousFrameGreyscaleOutput = "prev.frame.greyscale.output";
    private static final String sMapKeyVehicleDetectorOutput = "vehicle.detector.output";
    private static final String sMapKeyVehicleFilterOutput = "vehicle.filter.output";
    private static final String sMapKeyVehicleIteratorOutput = "vehicle.iter.output";
    private static final String sMapKeyVehicleROIScaleOutput = "vehicle.roi.scale.output";
    private static final String sMapKeyVehicleResizeOutput = "vehicle.resize.output";
    private static final String sMapKeyVehicleResizeMatrixOutput = "vehicle.resize.matrix.output";
    private static final String sMapKeyNumberPlateDetectorOutput = "np.detector.output";
    private static final String sMapKeyNumberPlateFilterOutput = "np.filter.output";
    private static final String sMapKeyNumberPlateIteratorOutput = "np.iter.output";
    private static final String sMapKeyNumberPlateROIScaleOutput = "np.roi.scale.output";
    private static final String sMapKeyNumberPlateCropOutput = "np.crop.output";
    private static final String sMapKeyNumberPlateCropSizeOutput = "np.crop.size.output";
    private static final String sMapKeyNumberPlateJPEGOutput = sMapKeyNumberPlate;//"np.crop.jpg.output";
    private static final String sMapKeyNumberPlateResizeOutput = "np.resize.output";
    //    private static final String sMapKeyNumberPlateResizeMatrixOutput = "np.resize.matrix.output";

    private static final int MAX_FRAMES_IN_FLIGHT = 3;
    private static final int DETECTOR_DIM = 300; // The input dimensions.
    private static final int OCR_INPUT_WIDTH = 112;
    private static final int OCR_INPUT_HEIGHT = OCR_INPUT_WIDTH / 2;

    private Point mOffsets; // The frame offsets (in view coordinates) of the receptive field.

    private FrameSyncer mPreviousFrameGreyscaleWaiter;

    private ObjectDetectionStage mVehicleDetector;
    private ObjectDetectionStage mNumberPlateDetector;
    private NumberPlateOCRStage mNumberPlateReader;
    private MotionFilterStage mMotionFilter;

    CaptureCallback() {
      super(MAX_FRAMES_IN_FLIGHT);
    }

    @Override
    protected void analyseFrame(@NonNull final Postprocessor.Data data,
                                @NonNull final Postprocessor.Stage last) {
      // Configure the pipeline.
      final ArrayList<Postprocessor.Stage> stages = new ArrayList<>(32);

      String input;
      String output;
      final int frameWidth = data.image.data.getWidth();
      final int frameHeight = data.image.data.getHeight();
      final int bufferDim = Math.min(frameWidth, frameHeight);
      if (mOffsets == null) {
        calculateOffsets(frameWidth, frameHeight, bufferDim, data.captureParams.rotation);
      }

      // NOTE: This stage is only for testing! It saves the frame's thumbnail locally.
//      stages.add((d, done) -> {
//        CameraController.ImageWrapper img =
//            (CameraController.ImageWrapper) d.attributes.get("thumbnail");
//        File path = Settings.getFileInAppDir(Settings.APP_DIR_ARCHIVED, String.format(Locale.ROOT,
//            "%s_%s_sp%03d_.jpg", Settings.getTimestampFileName(d.captureParams.captureTime, ""),
//            mCfg.timestampFmt, mCfg.latestSpeedReading));
//        Handler handler = new Handler();
//        new ImageFileWriter(img.data, path, null, null, 0,
//            () -> new ExifWriter(path, d.captureParams, null, done, handler)
//                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR), handler)
//            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//      });

      // Convert a square region from the original image to RGB.
      stages.add(new YUVToRGBStage(new Rect(
          mOffsets.x, mOffsets.y, mOffsets.x + bufferDim, mOffsets.y + bufferDim)));
      stages.add(new ImageCloserStage(null, null));


      // === Vehicle detection. ===
      // Resize the buffer. The aspect ratio of the input is already correct (i.e. 1:1).
      output = sMapKeyFrameResizeOutput;
      stages.add(new ResizeBufferStage(bufferDim, bufferDim, 3, null, null,
          DETECTOR_DIM, DETECTOR_DIM, 0, true, false, false,
          output, null, null));

      if (mCfg.vehicleMotionFilterEnabled) {
        // Convert the resized image to greyscale for motion detection.
        input = output;
        output = sMapKeyFrameGreyscaleOutput;
        stages.add(new RGBToGreyscaleStage(input, DETECTOR_DIM, DETECTOR_DIM, output));

        // Add a notifier stage with an event waiter for the next frame's pipeline and trigger the
        // event when the greyscale conversion of this frame is complete.
        final FrameSyncer greyscaleWaiter = new FrameSyncer(new EventWaiterStage());
        stages.add((d, done) -> {
          greyscaleWaiter.data =
              MapUtils.getDirectByteBuffer(d.attributes, sMapKeyFrameGreyscaleOutput, TAG, true);
          greyscaleWaiter.dataEvent.run();
          done.run();
        });
        if (mPreviousFrameGreyscaleWaiter == null) {
          // This is the first frame. Execute the partial pipeline. No further processing is done.
          mPreviousFrameGreyscaleWaiter = greyscaleWaiter;
          stages.add(last);
          Postprocessor processor = new Postprocessor(mContext, stages, data, null);
          processor.execute();
          return;
        } else {
          // This is not the first frame. Wait for the previous frame's greyscale conversion and
          // then add the data to this frame's attributes.
          final FrameSyncer prevFrame = mPreviousFrameGreyscaleWaiter; // Capture the reference.
          stages.add(prevFrame.dataEvent);
          stages.add((d, done) -> {
            d.attributes.put(sMapKeyPreviousFrameGreyscaleOutput, prevFrame.data);
            done.run();
          });
          mPreviousFrameGreyscaleWaiter = greyscaleWaiter;
        }
      }

      // Run the vehicle detector.
      input = sMapKeyFrameResizeOutput;
      output = sMapKeyVehicleDetectorOutput;
      if (mVehicleDetector == null) { // Cache and reuse instances of the detector.
        mVehicleDetector = new ObjectDetectionStage(mCfg.vehicleDetectorModel, input, DETECTOR_DIM,
            DETECTOR_DIM, mCfg.vehicleDetectorLabels, AsyncTask.THREAD_POOL_EXECUTOR, 1,
            10, 0.2f, output);
      }
      stages.add(mVehicleDetector);

      // Filter detections.
      input = output;
      output = sMapKeyVehicleFilterOutput;
      stages.add(new ObjectFilterStage(input, mCfg.minVehicleBboxConfidence / 100f,
          25 * 15, 280 * 280, 0.7f, 3.5f,
          ObjectFilterStage.Orientation.ANY, mCfg.vehicleLabelsFilter, output));

      if (mCfg.vehicleMotionFilterEnabled) {
        // Apply motion filtering.
        input = output;
        // Output to the same key.
        if (mMotionFilter == null) {
          // Cache and reuse instances of the motion filter.
          mMotionFilter = new MotionFilterStage(input, sMapKeyPreviousFrameGreyscaleOutput,
              sMapKeyFrameGreyscaleOutput, DETECTOR_DIM, DETECTOR_DIM,
              mCfg.vehicleMotionFilterAngles, mCfg.vehicleMotionFilterAngleTolerance,
              mCfg.vehicleMotionFilterMinMagnitude, output);
        }
        stages.add(mMotionFilter);
      }

      // Report the number of vehicles after filtering.
      stages.add((d, done) -> {
        final List l = MapUtils.getList(d.attributes, sMapKeyVehicleFilterOutput, TAG, false);
        if (l != null) reportValidVehiclesInFrame(l.size());
        done.run();
      });


      // === Per vehicle processing. ===
      // These stages are repeated for each detected vehicle, up to the maximum allowed.
      input = output;
      output = sMapKeyVehicleIteratorOutput;
      final List<Postprocessor.Stage> vehicleStages = new ArrayList<>(12);
      stages.add(new ListIteratorStage(input, vehicleStages, 1, output));

      // Scale the vehicle bbox to the buffer dimensions.
      input = output;
      output = sMapKeyVehicleROIScaleOutput;
      final Matrix vehicleBboxToBuffer = new Matrix();
      vehicleBboxToBuffer.preScale(bufferDim, bufferDim);
      vehicleStages.add(new TransformRectStage(input, vehicleBboxToBuffer, null, null,
          false, false, output));

      // Invoke the vehicle detection callback.
      vehicleStages.add(new ObjectDetectionCallbackStage(sMapKeyVehicleROIScaleOutput,
          sMapKeyVehicleIteratorOutput, frameWidth, frameHeight, mOffsets.x, mOffsets.y,
          sObjectTypeIDVehicle, this));

      if (mCfg.numberPlateDetectionEnabled) {
        addNumberPlateDetection(vehicleStages, bufferDim, frameWidth, frameHeight);
      }


      // === Execute the pipeline. ===
      stages.add(last);
//      Postprocessor processor = new Postprocessor(mContext, stages, data, () -> {
//        ByteBuffer buf = (ByteBuffer) data.attributes.get(sMapKeyVehicleResizeOutput);
//        Log.d(TAG, "Got vehicle buffer: " + (buf != null));
//        if (buf != null) {
//          File f = Settings.getFileInAppDir(Settings.APP_DIR_ARCHIVED,
//              Settings.getTimestampFileName(System.currentTimeMillis(), "_vehicle_.jpg"));
//          Log.d(TAG, "Saving ", f.getName());
//          YUVToRGBStage.debugWriteRGBBufferToFile(f, buf, DETECTOR_DIM, DETECTOR_DIM);
//          Recognition r = (Recognition) data.attributes.get(sMapKeyVehicleIteratorOutput);
//          if (r != null) Log.d(TAG, "Selected vehicle: ", r.toString());
//        }
//
//        buf = (ByteBuffer) data.attributes.get(sMapKeyNumberPlateResizeOutput);
//        Log.d(TAG, "Got number plate buffer " + (buf != null));
//        if (buf != null) {
//          File f = Settings.getFileInAppDir(Settings.APP_DIR_ARCHIVED,
//              Settings.getTimestampFileName(System.currentTimeMillis(), "_np_.jpg"));
//          Log.d(TAG, "Saving ", f.getName());
//          YUVToRGBStage.debugWriteRGBBufferToFile(f, buf, DETECTOR_DIM, DETECTOR_DIM);
//          Recognition r = (Recognition) data.attributes.get(sMapKeyNumberPlateIteratorOutput);
//          if (r != null) Log.d(TAG, "Selected number plate: ", r.toString());
//        }
//      });
      Postprocessor processor = new Postprocessor(mContext, stages, data, null);
      processor.execute();
    }

    private void addNumberPlateDetection(final List<Postprocessor.Stage> vehicleStages,
                                         int bufferDim, int frameWidth, int frameHeight) {
      // Crop and resize the vehicle region.
      String input = sMapKeyVehicleROIScaleOutput;
      String output = sMapKeyVehicleResizeOutput;
      vehicleStages.add(new ResizeBufferStage(bufferDim, bufferDim, 3, null, input,
          DETECTOR_DIM, DETECTOR_DIM, 0, true, true, false,
          output, sMapKeyVehicleResizeMatrixOutput, null));

      // Run the number plate detector.
      input = output;
      output = sMapKeyNumberPlateDetectorOutput;
      if (mNumberPlateDetector == null) { // Cache and reuse instances of the detector.
        mNumberPlateDetector = new ObjectDetectionStage(mCfg.numberPlateDetectorModel, input,
            DETECTOR_DIM, DETECTOR_DIM, mCfg.numberPlateDetectorLabels,
            AsyncTask.THREAD_POOL_EXECUTOR, 1, 10, 0.2f, output);
      }
      vehicleStages.add(mNumberPlateDetector);

      input = output;
      output = sMapKeyNumberPlateFilterOutput;
      vehicleStages.add(new ObjectFilterStage(input, mCfg.minNPBboxConfidence / 100f,
          24 * 8, 100 * 40, 1.5f, 4.5f,
          ObjectFilterStage.Orientation.LANDSCAPE, null, output));


      // === Per number plate processing. ===
      // These stages are run for the top number plate match per vehicle.
      input = output;
      output = sMapKeyNumberPlateIteratorOutput;
      final List<Postprocessor.Stage> npStages = new ArrayList<>(12);
      vehicleStages.add(new ListIteratorStage(input, npStages, 1, output));

      // Transform the number plate bbox to the buffer dimensions and vehicle bbox offsets by first
      // scaling it to the detector input and then applying the inverse of the vehicle crop and
      // resize.
      input = output;
      output = sMapKeyNumberPlateROIScaleOutput;
      npStages.add(new TransformRectStage(input, null, null, (bbox) -> {
        Matrix m = new Matrix();
        // Scale the bbox to the detector input dimensions.
        m.preScale(DETECTOR_DIM, DETECTOR_DIM);
        // Scale up by an extra safety margin in both dimensions.
        float centerX = bbox.centerX();
        float centerY = bbox.centerY();
        m.preTranslate(centerX, centerY);
        m.preScale(1.15f, 1.15f);
        m.preTranslate(-centerX, -centerY);
        return m;
      }, false, true, output));
      npStages.add(new TransformRectStage(output, null, sMapKeyVehicleResizeMatrixOutput,
          null, true, false, output));

      // Crop and if necessary downsample (but not upsample) the NP region for JPEG encoding.
      input = output;
      output = sMapKeyNumberPlateCropOutput;
      npStages.add(new ResizeBufferStage(bufferDim, bufferDim, 3, null, input,
          ResizeBufferStage.MATCH_ROI_SIZE, ResizeBufferStage.MATCH_ROI_SIZE, 100, false,
          false, false, output, null, sMapKeyNumberPlateCropSizeOutput));

      // Encode as greyscale JPEG.
      input = output;
      output = sMapKeyNumberPlateJPEGOutput;
      npStages.add(new EncodeJPEGStage(input, 0, 0, sMapKeyNumberPlateCropSizeOutput,
          3, true, true, 75, output));

      // Invoke the number plate detection callback.
      npStages.add(new ObjectDetectionCallbackStage(sMapKeyNumberPlateROIScaleOutput,
          sMapKeyNumberPlateIteratorOutput, frameWidth, frameHeight, mOffsets.x, mOffsets.y,
          sObjectTypeIDNumberPlate, this));

      if (mCfg.numberPlateRecognitionEnabled) {
        addNumberPlateOCR(npStages, bufferDim);
      }

      // Free the native JPEG buffer.
      npStages.add(new BufferCloserStage(sMapKeyNumberPlateJPEGOutput));
    }

    private void addNumberPlateOCR(final List<Postprocessor.Stage> npStages, int bufferDim) {
      // Crop and resize the number plate region for OCR.
      String input = sMapKeyNumberPlateROIScaleOutput;
      String output = sMapKeyNumberPlateResizeOutput;
      npStages.add(new ResizeBufferStage(bufferDim, bufferDim, 3, null, input,
          OCR_INPUT_WIDTH, OCR_INPUT_HEIGHT, 0, true,
          true, false, output, null, null));

      // Run OCR.
      input = output;
      if (mNumberPlateReader == null) { // Cache and reuse instances of the reader.
        mNumberPlateReader = new NumberPlateOCRStage(mCfg.numberPlateOCRModel, input,
            OCR_INPUT_WIDTH, OCR_INPUT_HEIGHT, AsyncTask.THREAD_POOL_EXECUTOR, 1,
            null, this);
      }
      npStages.add(mNumberPlateReader);
    }

    @Override
    protected void cleanup() {
      if (mVehicleDetector != null) {
        mVehicleDetector.close();
        mVehicleDetector = null;
      }
      if (mNumberPlateDetector != null) {
        mNumberPlateDetector.close();
        mNumberPlateDetector = null;
      }
      if (mNumberPlateReader != null) {
        mNumberPlateReader.close();
        mNumberPlateReader = null;
      }
      mMotionFilter = null;

      super.cleanup();
    }

    /**
     * Calculates and sets {@link #mOffsets} in view coordinates.
     *
     * @param frameWidth  The frame width in data coordinates.
     * @param frameHeight The frame height in data coordinates.
     * @param bufferDim   The side length of the square receptive field. Must be no larger than
     *                    min(frameWidth, frameHeight).
     * @param rotation    The clockwise rotation from data to view coordinates.
     */
    private void calculateOffsets(int frameWidth, int frameHeight, int bufferDim, int rotation) {
      if (rotation == 90 || rotation == 270) {
        // Swap the height and width to convert them into view coordinates.
        int tmp = frameWidth;
        //noinspection SuspiciousNameCombination
        frameWidth = frameHeight;
        frameHeight = tmp;
      }

      switch (mCfg.detectorInputAlignment) {
        case "start":
          mOffsets = new Point(0, 0);
          break;

        case "end":
          mOffsets = frameWidth > frameHeight
              ? new Point(frameWidth - bufferDim, 0)
              : new Point(0, frameHeight - bufferDim);
          break;

        default: // "centre"
          mOffsets = new Point((frameWidth - bufferDim) / 2, (frameHeight - bufferDim) / 2);
          break;
      }
      Log.d(TAG, "Offsets in view coords: ", mOffsets.x, ",", mOffsets.y);
    }
  } // CaptureCallback

  /**
   * A data container with an associated Runnable to run once when the data becomes available.
   */
  private static class FrameSyncer {
    final EventWaiterStage dataEvent;
    ByteBuffer data;

    FrameSyncer(@NonNull EventWaiterStage event) {
      dataEvent = event;
    }
  }


  protected static class Config extends VehicleCapture.Config {
    protected String detectorInputAlignment;

    protected MappedByteBuffer vehicleDetectorModel;
    protected List<String> vehicleDetectorLabels;
    protected List<String> vehicleLabelsFilter;

    protected boolean vehicleMotionFilterEnabled;
    protected List<Double> vehicleMotionFilterAngles;
    protected double vehicleMotionFilterAngleTolerance;
    protected double vehicleMotionFilterMinMagnitude;

    protected MappedByteBuffer numberPlateDetectorModel;
    protected List<String> numberPlateDetectorLabels;

    protected MappedByteBuffer numberPlateOCRModel;

    protected Config(@NonNull Context context, @NonNull Settings settings, @NonNull Bundle extras) {
      super(settings, extras);

      // The crop region from the settings is not used. ROIs are adjusted for the hard-coded crop
      // region before being reported to the parent.
      cropRegion = null;

      detectorInputAlignment = settings.getString(ConfigKeys.DetectorInputAlignment);

      // Load the vehicle detector data.
      vehicleDetectorModel = Helpers.mapFile(context.getAssets(), "vehicle_detector.tflite");
      if (vehicleDetectorModel == null) throw new ConfigurationException();
      vehicleDetectorLabels = Helpers.readLines(context.getAssets(), "vehicle_detector_labels.txt");
      vehicleLabelsFilter = null; // Arrays.asList("Car", "Truck", "Van", "Bus");

      // Load the number plate detector data.
      numberPlateDetectorModel =
          Helpers.mapFile(context.getAssets(), "number_plate_detector.tflite");
      if (numberPlateDetectorModel == null) throw new ConfigurationException();
      numberPlateDetectorLabels =
          Helpers.readLines(context.getAssets(), "number_plate_detector_labels.txt");

      // Load the number plate OCR data.
      numberPlateOCRModel = Helpers.mapFile(context.getAssets(), "number_plate_ocr.tflite");
      if (numberPlateOCRModel == null) throw new ConfigurationException();

      vehicleMotionFilterAngles = settings.getDoubleList(ConfigKeys.VehicleMotionFilterAngles);
      vehicleMotionFilterEnabled = !vehicleMotionFilterAngles.isEmpty();
      if (vehicleMotionFilterEnabled) {
        vehicleMotionFilterAngleTolerance =
            settings.getDouble(ConfigKeys.VehicleMotionFilterAngleTolerance);
        vehicleMotionFilterMinMagnitude =
            settings.getDouble(ConfigKeys.VehicleMotionFilterMinMagnitude);
      }
    }
  } // Config
}
