package io.hubsy.core.camera;

import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraCharacteristics;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import io.hubsy.core.Constants;
import io.hubsy.core.Settings;
import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.camera.postprocess.ExifWriterStage;
import io.hubsy.core.camera.postprocess.ImageFileWriterStage;
import io.hubsy.core.camera.postprocess.MoveJPEGStage;
import io.hubsy.core.camera.postprocess.Postprocessor;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;

/**
 * A CaptureManager to record video and, optionally, a thumbnail.
 */
class VideoCapture extends VideoCaptureBase {
  private static final String TAG = "VideoCapture";

  private final Context mContext;
  private final Config mCfg;

  protected VideoCapture(@NonNull Context context, @NonNull Config config, @NonNull Runnable done) {
    super(config, done);
    mCfg = config;
    mContext = context.getApplicationContext();
  }

  @Override
  public void capture() {
    if (mCameraController == null) {
      mDone.run();
      return;
    }

    int[] configIdxs = mCfg.captureThumbnail ? new int[]{0} : null;
    mCameraController.recordVideo(configIdxs, false, new CaptureCallback());
  }

  @Override
  public void updateConfig(@NonNull Bundle extras) {
    super.updateConfig(extras);

    // Update the max speed. TODO temporary.
    int speed = extras.getInt(Constants.EXTRA_SPEED, Integer.MIN_VALUE);
    mCfg.maxSpeedReading = Math.max(speed, mCfg.maxSpeedReading);
  }


  private class CaptureCallback implements CameraController.ActionCallback {
    private boolean mRecordingStopped;
    private boolean mThumbnailCaptured;
    private int mNumFramesInFlight;

    @Override
    public void onActionCompleted(boolean success,
                                  @Nullable CameraController.ImageWrapper[] images,
                                  @Nullable CameraController.CaptureData captureData,
                                  @Nullable CameraCharacteristics characts) {
      final CameraController.Parameters params = captureData == null ? null : captureData.params;
      if (!success || params == null) {
        Log.w(TAG, "Recording video failed");
        cleanup();
        return;
      }

      if (params.outFile != null) {
        mRecordingStopped = true;
        processVideo(params, params.outFile);
      } else {
        processImages(images, captureData, characts);
      }
    }

    private void processVideo(@NonNull CameraController.Parameters params, @NonNull File video) {
      Log.d(TAG, "Video recorded successfully, frames in postprocessing: ", mNumFramesInFlight);

      // Add event/start and end timestamps to the file name and move it out of tmp.
      String endTime = Settings.getTimestampFileName(params.captureTime, "");
      String tmpName = mCfg.timestampFmt + "_" + endTime;
      // TODO temporary addition of speed value to video filename.
      if (mCfg.maxSpeedReading != Integer.MIN_VALUE) {
        tmpName += String.format(Locale.ROOT, "_sp%03d", mCfg.maxSpeedReading);
      }
      tmpName += ".mp4";

      String dirTo = mCfg.videoUploadEnabled ? Settings.APP_DIR_NEW : Settings.APP_DIR_ARCHIVED;
      Helpers.moveFile(video, dirTo, tmpName);

      if (mNumFramesInFlight == 0) cleanup();
    }

    private void processImages(@Nullable CameraController.ImageWrapper[] images,
                               @NonNull CameraController.CaptureData captureData,
                               @Nullable CameraCharacteristics characts) {
      final CameraController.Parameters params = captureData.params;
      if (params == null || images == null || isNull(images, 1)) {
        Log.w(TAG, "Taking still image failed");
        cleanup();
        return;
      }

      // Drop further frames once the thumbnail has been captured.
      if (mThumbnailCaptured || !mCfg.captureThumbnail) {
        images[0].close();
        return;
      }
      mThumbnailCaptured = true;

      Log.d(TAG, "Processing video frame ", params.captureTime);
      mNumFramesInFlight++;

      // Initialise the general data needed for postprocessing.
      final Postprocessor.Data data = new Postprocessor.Data(mCfg.sharedData);
      data.image = images[0];
      data.captureParams = params;
      data.imageConfig = params.imageConfigs.get(0);
      data.captureResult = captureData.result;
      data.cameraCharacteristics = characts;

      // Construct the output file name.
      String timestamp = Settings.getTimestampFileName(params.captureTime, "");
      String outPathNoExt = Settings.getAppFolderPath() + Settings.APP_DIR_TMP + "/" + timestamp
          + "_" + mCfg.timestampFmt;
      // TODO temporary addition of speed value to video filename.
      if (mCfg.maxSpeedReading != Integer.MIN_VALUE) {
        outPathNoExt += String.format(Locale.ROOT, "_sp%03d", mCfg.maxSpeedReading);
      }

      // Configure the postprocessing pipeline.
      final ArrayList<Postprocessor.Stage> stages = new ArrayList<>(3);
      stages.add(new ImageFileWriterStage(outPathNoExt, null, null));
      stages.add(new ExifWriterStage(null));
      stages.add(new MoveJPEGStage(Settings.APP_DIR_NEW));

      // Execute the pipeline.
      Postprocessor processor = new Postprocessor(mContext, stages, data, mFrameProcessedCallback);
      processor.execute();
    }

    // Invoked at the end of processing each frame.
    private Runnable mFrameProcessedCallback = () -> {
      mNumFramesInFlight--;
      if (mNumFramesInFlight == 0 && mRecordingStopped) cleanup();
    };

    private void cleanup() {
      mDone.run();
      new RotateLogsAndTransferFiles(mContext, null, !mCfg.isEvent).run();
    }
  } // CaptureCallback


  protected static class Config extends VideoCaptureBase.Config {
    protected int maxSpeedReading; // The max. speed reading so far. TODO temporary.
    protected boolean captureThumbnail; // Toggles thumbnail capture.

    protected Config(@NonNull Settings settings, @NonNull Bundle extras) {
      super(settings, extras);

      maxSpeedReading = extras.getInt(Constants.EXTRA_SPEED, Integer.MIN_VALUE);

      captureThumbnail = thumbnailReso > 0;
      if (captureThumbnail) {
        // Add a config for JPEG thumbnails.
        CameraController.ImageConfig thumbConfig = new CameraController.ImageConfig();
        params.imageConfigs.add(thumbConfig);
        thumbConfig.format = ImageFormat.JPEG;
        // The first frame is used as the thumbnail. Allow 2 more image buffers to be in use to
        // prevent any delays caused by images not being closed quickly enough after the thumbnail.
        thumbConfig.imageBuffers = 3;
        thumbConfig.jpegQuality = settings.getInt(ConfigKeys.JPEGQuality);
        thumbConfig.maxDimension = thumbnailReso;
        thumbConfig.aspectRatio = "4:3";
      }
    }
  } // Config
}
