package io.hubsy.core.camera;

import android.os.Bundle;
import android.support.annotation.NonNull;

import io.hubsy.core.Settings;
import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.util.Log;

/**
 * A CaptureManager for video captures.
 */
abstract class VideoCaptureBase extends CaptureManager {
  private static final String TAG = "VideoCaptureBase";

  protected VideoCaptureBase(@NonNull Config config, @NonNull Runnable done) {
    super(config, done);
  }


  protected static class Config extends CaptureManager.Config {

    protected int thumbnailReso; // The resolution of thumbnails.
    protected boolean videoUploadEnabled; // Toggles uploading of videos.

    protected Config(@NonNull Settings settings, @NonNull Bundle extras) {
      super(settings, extras);

      // Configure the mode and set the video image config.
      params.captureMode = CameraController.CaptureMode.Video;
      params.videoConfig = imageConfig;

      params.videoBitrate = settings.getInt(ConfigKeys.VideoBitrate);
      params.videoCaptureDuration = settings.getInt(ConfigKeys.VideoCaptureDuration);
      params.videoCaptureFPS = settings.getDouble(ConfigKeys.VideoCaptureFPS);
      params.videoPlaybackFPS = settings.getInt(ConfigKeys.VideoPlaybackFPS);

      // Set the resolution.
      String reso = settings.getString(ConfigKeys.VideoReso);
      String[] widthAndHeight = reso.split("[xX]", 2);
      try {
        params.videoWidth = Integer.parseInt(widthAndHeight[0]);
        params.videoHeight = Integer.parseInt(widthAndHeight[1]);
      } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
        Log.w(TAG, "Invalid video resolution '", reso, "': ", e.getMessage());
        throw new ConfigurationException();
      }

      thumbnailReso = settings.getInt(ConfigKeys.VideoThumbnailReso);
      videoUploadEnabled = settings.getBool(ConfigKeys.EnableVideoUpload);
    }
  } // Config
}
