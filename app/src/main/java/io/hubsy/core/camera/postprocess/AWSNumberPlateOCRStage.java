package io.hubsy.core.camera.postprocess;

import android.annotation.SuppressLint;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Trace;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.rekognition.AmazonRekognitionClient;
import com.amazonaws.services.rekognition.model.BoundingBox;
import com.amazonaws.services.rekognition.model.DetectTextRequest;
import com.amazonaws.services.rekognition.model.DetectTextResult;
import com.amazonaws.services.rekognition.model.Image;
import com.amazonaws.services.rekognition.model.TextDetection;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.regex.Pattern;

import io.hubsy.core.util.Log;

/**
 * A postprocessor stage that recognises number plates in images. The image is expected to be a JPEG
 * stored either in {@link Postprocessor.Data#buffer} or in the
 * {@link Postprocessor.Data#attributes}.
 * <p>
 * If a number is successfully read and {@link Postprocessor.Data#imageJpeg} is not null, then the
 * image stored under that path is renamed to contain the plate number.
 */
public class AWSNumberPlateOCRStage implements Postprocessor.Stage {
  private static final String TAG = "AWSNumberPlateOCRStage";

  private final String mJPEGKey;
  private final AmazonRekognitionClient mClient;
  private final Executor mExecutor;
  private final float mMinConfidence;
  private final Pattern mNumberPlateRegex;
  private final int mBboxCallbackID;
  private final ObjectDetectionCallback mBboxCallback;
  private final NumberPlateOCRCallback mTextCallback;

  /**
   * @param jpegKey          The key under which the JPEG {@link ByteBuffer} is stored in the
   *                         attributes. If null, the image is expected to be stored in
   *                         {@link Postprocessor.Data#buffer}.
   * @param client           The AWS service client.
   * @param executor         The executor on which to run background tasks.
   * @param minConfidence    The min. confidence to consider a recognition as valid. Range [0, 100].
   * @param numberPlateRegex An optional regex against which to match detected text for validation.
   * @param bboxCallbackID   The ID to pass to the bboxCallback method.
   * @param bboxCallback     An optional cb to invoke for the normalised number plate bounding box.
   * @param textCallback     An optional cb to invoke for the number plate text.
   */
  public AWSNumberPlateOCRStage(@Nullable String jpegKey,
                                @NonNull AmazonRekognitionClient client,
                                @NonNull Executor executor, float minConfidence,
                                @Nullable Pattern numberPlateRegex, int bboxCallbackID,
                                @Nullable ObjectDetectionCallback bboxCallback,
                                @Nullable NumberPlateOCRCallback textCallback) {
    mJPEGKey = jpegKey;
    mClient = client;
    mExecutor = executor;
    mMinConfidence = minConfidence;
    mNumberPlateRegex = numberPlateRegex;
    mBboxCallbackID = bboxCallbackID;
    mBboxCallback = bboxCallback;
    mTextCallback = textCallback;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    ByteBuffer image;
    if (mJPEGKey != null) {
      image = MapUtils.getDirectByteBuffer(data.attributes, mJPEGKey, TAG, true);
    } else {
      image = data.buffer;
      if (image == null) Log.e(TAG, "Error: The input buffer is null");
    }

    if (image == null) {
      if (mTextCallback != null) mTextCallback.numberPlate(data, null, 0.f);
      done.run();
      return;
    }

    try {
      new Worker(data, image, done).executeOnExecutor(mExecutor);
      Log.d(TAG, data.id, ": Executing");
    } catch (RejectedExecutionException e) {
      Log.d(TAG, "Skipping stage, the executor has been shut down");
      if (mTextCallback != null) mTextCallback.numberPlate(data, null, 0.f);
      done.run();
    }
  }

  // Not a leak, the outer class will not be collected until this completes anyway.
  @SuppressLint("StaticFieldLeak")
  private class Worker extends AsyncTask<Void, Void, Void> {
    private final Postprocessor.Data mData;
    private final ByteBuffer mImage;
    private final Runnable mDone;

    private List<TextDetection> mDetections;
    private TextDetection mSelection;
    private RectF mSelectionBbox;
    private String mPlateID;

    public Worker(Postprocessor.Data data, ByteBuffer image, Runnable done) {
      mData = data;
      mImage = image;
      mDone = done;
    }

    @Override
    protected Void doInBackground(Void... voids) {
      // Detect all text in the image.
      try {
        Trace.beginSection("awsDetectText");
        mImage.rewind();
        Image awsImage = new Image().withBytes(mImage);
        DetectTextRequest request = new DetectTextRequest().withImage(awsImage);
        DetectTextResult result = mClient.detectText(request);
        mDetections = result.getTextDetections();
      } catch (AmazonClientException e) {
        Log.w(TAG, mData.id, ": detectText failed: ", e.getMessage());
        return null;
      } finally {
        Trace.endSection(); // awsDetectText
      }

      // Check if the task was cancelled or the thread interrupted by the executor being shutdown.
      if (isCancelled() || Thread.currentThread().isInterrupted()) return null;

      if (mDetections != null) {
        // Select the result with the highest confidence value that matches the regex if any.
        final StringBuilder logBuilder = new StringBuilder(64);
        logBuilder.append(mData.id).append(": Detections:");
        float highestConfidence = 0.f;
        for (int i = 0; i < mDetections.size(); i++) {
          TextDetection text = mDetections.get(i);
          float confidence = text.getConfidence();
          String s = text.getDetectedText();
          logBuilder.append(" ").append(s).append(" [").append(text.getType()).append(": ")
              .append(confidence).append("]");
          if (confidence >= mMinConfidence && confidence > highestConfidence
              && "LINE".equals(text.getType())) {
            if (mNumberPlateRegex == null || mNumberPlateRegex.matcher(s).matches()) {
              highestConfidence = confidence;
              mSelection = text;
            }
          }
        }
        Log.d(TAG, logBuilder.toString());

        if (mSelection != null) {
          // Replace spaces with hyphens.
          mPlateID = mSelection.getDetectedText().replace(" ", "-");

          // Create the bounding box in absolute image coordinates.
          BoundingBox bb = mSelection.getGeometry().getBoundingBox();
          mSelectionBbox = new RectF(bb.getLeft(), bb.getTop(),
              bb.getLeft() + bb.getWidth(), bb.getTop() + bb.getHeight());
        }
      }

      if (mPlateID != null && !mPlateID.isEmpty() && mData.imageJpeg != null) {
        // Add the number to the file name.
        File oldFile = mData.imageJpeg;
        String path = oldFile.getAbsolutePath();
        int suffixLen = path.endsWith("_.jpg") ? 5 : 4;
        File newFile = path.length() > suffixLen
            ? new File(path.substring(0, path.length() - suffixLen) + "_np" + mPlateID + "_.jpg")
            : mData.imageJpeg;
        if (oldFile.renameTo(newFile)) {
          mData.imageJpeg = newFile;
        }
      }

      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      finish();
    }

    @Override
    protected void onCancelled() {
      finish();
    }

    private void finish() {
      if (mSelection != null) {
        String s = mSelection.getDetectedText();
        Log.d(TAG, Locale.ENGLISH,
            "%s: Number plate \"%s\" [%.2f,%.2f][%.2f,%.2f] (%.1f%%)%s",
            mData.id, s, mSelectionBbox.left, mSelectionBbox.top, mSelectionBbox.right,
            mSelectionBbox.bottom, mSelection.getConfidence(),
            (mData.imageJpeg == null ? "" : " in " + mData.imageJpeg.getName()));
      } else {
        Log.d(TAG, mData.id, ": No number plate");
      }

      // Notify the listeners.
      if (mSelection != null) {
        float confidence = mSelection.getConfidence();
        if (mBboxCallback != null) {
          mBboxCallback.objectDetected(mData, mBboxCallbackID, mSelectionBbox, confidence);
        }
        if (mTextCallback != null) {
          mTextCallback.numberPlate(mData, mSelection.getDetectedText(), confidence);
        }
      } else if (mTextCallback != null) {
        mTextCallback.numberPlate(mData, null, 0.f);
      }

      mDone.run();
    }
  }
}
