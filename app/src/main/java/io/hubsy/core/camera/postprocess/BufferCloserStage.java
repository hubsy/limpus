package io.hubsy.core.camera.postprocess;

import android.support.annotation.Nullable;

import java.nio.ByteBuffer;

import io.hubsy.core.imgproc.Utils;
import io.hubsy.core.util.Log;

/**
 * Frees the underlying memory of a direct {@link java.nio.ByteBuffer} that was allocated in native
 * code. The buffer is either retrieved from the {@link Postprocessor.Data#attributes} or from
 * {@link Postprocessor.Data#buffer} and removed or set to null afterwards.
 */
public class BufferCloserStage implements Postprocessor.Stage {
  private static final String TAG = "BufferCloserStage";

  private final String mBufferKey;

  /**
   * @param bufferKey The key to retrieve the buffer from the attributes. If null,
   *                  {@link Postprocessor.Data#buffer} is closed instead.
   */
  public BufferCloserStage(@Nullable String bufferKey) {
    mBufferKey = bufferKey;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    ByteBuffer buf;
    if (mBufferKey != null) {
      buf = MapUtils.getDirectByteBuffer(data.attributes, mBufferKey, TAG, true);
    } else {
      buf = data.buffer;
      if (buf == null) Log.e(TAG, "Error: The buffer is null");
    }

    if (buf != null) {
      try {
        Utils.freeBuffer(buf);
        if (mBufferKey != null) data.attributes.remove(mBufferKey);
        else data.buffer = null;
      } catch (Exception e) {
        Log.e(TAG, "Failed to free the native ByteBuffer: " + e.getMessage());
      }
    }

    done.run();
  }
}
