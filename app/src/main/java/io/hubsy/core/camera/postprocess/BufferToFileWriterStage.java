package io.hubsy.core.camera.postprocess;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;
import java.nio.ByteBuffer;

import io.hubsy.core.util.Log;
import io.hubsy.core.util.WriteBufferToFileTask;

/**
 * A postprocessor stage that writes a {@link ByteBuffer} to a file.
 */
public class BufferToFileWriterStage implements Postprocessor.Stage {
  private static final String TAG = "BufferToFileWriterStage";

  private final String mBufferKey;
  private final File mPath;

  /**
   * @param bufferKey If non-null, the {@link ByteBuffer} to be written is retrieved from the
   *                  {@link Postprocessor.Data#attributes} using this key. Otherwise,
   *                  {@link Postprocessor.Data#buffer} is used and must be non-null.
   * @param path      The output path.
   */
  public BufferToFileWriterStage(@Nullable String bufferKey, @NonNull File path) {
    mBufferKey = bufferKey;
    mPath = path;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    ByteBuffer buf;
    if (mBufferKey != null) {
      buf = MapUtils.getDirectByteBuffer(data.attributes, mBufferKey, TAG, true);
    } else {
      buf = data.buffer;
      if (buf == null) Log.e(TAG, "Error: The input buffer is null");
    }
    if (buf == null) {
      done.run();
      return;
    }

    new WriteBufferToFileTask(buf, mPath, done, null)
        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }
}
