package io.hubsy.core.camera.postprocess;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;

import io.hubsy.core.imgproc.Utils;
import io.hubsy.core.util.Log;

/**
 * A postprocessing stage to crop and replace the JPEG stored at
 * {@link Postprocessor.Data#imageJpeg}.
 */
public class CropJPEGStage implements Postprocessor.Stage {
  private static final String TAG = "CropJPEGStage";

  private final RectF mROI;
  private final int mQuality;

  /**
   * @param roi     The crop region-of-interest.
   * @param quality The encoding quality in [0,100].
   */
  public CropJPEGStage(@NonNull RectF roi, int quality) {
    mROI = roi;
    mQuality = quality;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    if (data.imageJpeg == null) {
      Log.e(TAG, "JPEG filepath is null");
      done.run();
      return;
    }

    new Worker(data, done, mROI, mQuality).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }

  private static class Worker extends AsyncTask<Void, Void, Void> {
    private final Postprocessor.Data mData;
    private final Runnable mDone;
    private final RectF mROI;
    private final int mQuality;

    public Worker(Postprocessor.Data data, Runnable done, RectF roi, int quality) {
      mData = data;
      mDone = done;
      mROI = roi;
      mQuality = quality;
    }

    @Override
    protected Void doInBackground(Void... voids) {
      // Load the image.
      Bitmap bmp = BitmapFactory.decodeFile(mData.imageJpeg.getAbsolutePath());
      if (bmp == null) {
        Log.e(TAG, "Failed to decode the image at: ", mData.imageJpeg.getPath());
        return null;
      }

      int orientation = mData.captureParams == null ? 0 : mData.captureParams.rotation;
      Rect region = Utils.absoluteRegion(mROI, bmp.getWidth(), bmp.getHeight());
      Bitmap cropped = Utils.crop(bmp, orientation, region);

      // Clip the crop region to the image size.
      if (cropped == bmp) {
        bmp.recycle();
        Log.d(TAG, "Nothing to crop");
        return null;
      }

      // Save it.
      try (BufferedOutputStream bos =
               new BufferedOutputStream(new FileOutputStream(mData.imageJpeg))) {
        if (cropped.compress(Bitmap.CompressFormat.JPEG, mQuality, bos)) {
          Log.d(TAG, "Image cropped");
        } else {
          Log.e(TAG, "Failed to compress the cropped image");
        }
      } catch (Exception e) {
        Log.e(TAG, e.getMessage());
      }

      bmp.recycle();
      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      finish();
    }

    @Override
    protected void onCancelled() {
      finish();
    }

    private void finish() {
      mDone.run();
    }
  }
}
