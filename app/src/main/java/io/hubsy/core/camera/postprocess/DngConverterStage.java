package io.hubsy.core.camera.postprocess;

import android.content.Context;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Handler;

import java.io.File;

import io.hubsy.core.Settings;
import io.hubsy.core.imgproc.DngConverter;
import io.hubsy.core.util.Log;

/**
 * A postprocessing stage for DNG to JPEG conversion with {@link DngConverter}.
 * <p>
 * Sets {@link Postprocessor.Data#imageJpeg} when done.
 */
public class DngConverterStage implements Postprocessor.Stage {
  private static final String TAG = "DngConverterStage";

  private Context mContext;
  private float mScaleFactor;
  private RectF mROI;

  public DngConverterStage(Context context, float scaleFactor, RectF roi) {
    mContext = context.getApplicationContext();
    mScaleFactor = scaleFactor;
    mROI = roi;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    if (data.imageDng == null) {
      Log.e(TAG, "DNG image is null, cannot convert to JPEG");
      done.run();
      return;
    }

    // Convert the DNG to a JPG for uploading and just archive the DNG.
    Log.d(TAG, "Converting DNG to JPG");
    int iso = data.captureParams == null ? 100 : data.captureParams.sensitivity;
    int quality = data.imageConfig == null ? 90 : data.imageConfig.jpegQuality;
    int orientation = data.captureParams == null ? 0 : data.captureParams.rotation;
    DngConverter converter = new DngConverter(mContext, data.imageDng,
        new File(Settings.getAppFolderPath(), Settings.APP_DIR_ARCHIVED),
        new File(Settings.getAppFolderPath(), Settings.APP_DIR_TMP),
        quality, iso, mScaleFactor, mROI, orientation, done, new Handler());
    data.imageJpeg = converter.getJPEG();
    // Use a serial background task to prevent multiple concurrent conversions, which may cause
    // problems due to the amount of memory required.
    converter.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
  }
}
