package io.hubsy.core.camera.postprocess;

import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Trace;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Size;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.HashMap;

import io.hubsy.core.imgproc.Utils;
import io.hubsy.core.util.Log;

/**
 * A postprocessor stage that encodes an 8-bit 1- or 3-channel (greyscale, RGB or BGR) image as
 * JPEG. Both the input and output image buffers are retrieved from and added to the
 * {@link Postprocessor.Data#attributes}.
 * <p>
 * <b>NOTE:</b> The output buffer must be released using {@link Utils#freeBuffer} or indirectly via
 * {@link BufferCloserStage}.
 */
public class EncodeJPEGStage implements Postprocessor.Stage {
  private static final String TAG = "EncodeJPEGStage";

  private final String mInputKey;
  private final int mImageWidth;
  private final int mImageHeight;
  private final String mImageSizeKey;
  private final int mChannels;
  private final boolean mIsRGB;
  private final boolean mToGrey;
  private final int mQuality;
  private final String mResultsKey;

  /**
   * @param inputKey     The key under which the input {@link ByteBuffer} is stored in the
   *                     attributes.
   * @param imageWidth   The image width. This value will be ignored if imageSizeKey is provided.
   * @param imageHeight  The image height. This value will be ignored if imageSizeKey is provided.
   * @param imageSizeKey An optional key to retrieve the input image {@link Size} from the
   *                     attributes.
   * @param channels     The number of channels in the image buffer (1 or 3).
   * @param isRGB        For 3-channel images, set this to true if the image is in RGB channel order
   *                     rather than BGR.
   * @param toGrey       For 3-channel images, set this to true to convert the output to greyscale.
   * @param quality      The JPEG encoding quality in [0,100].
   * @param resultsKey   The key under which to store the JPEG {@link ByteBuffer} in the
   *                     {@link Postprocessor.Data#attributes}. If null, the reference is assigned
   *                     to {@link Postprocessor.Data#buffer} instead. <b>NOTE: The output buffer
   *                     must be released explicitly!</b>
   */
  public EncodeJPEGStage(@NonNull String inputKey, int imageWidth, int imageHeight,
                         @Nullable String imageSizeKey, int channels, boolean isRGB, boolean toGrey,
                         int quality, @Nullable String resultsKey) {
    mInputKey = inputKey;
    mImageWidth = imageWidth;
    mImageHeight = imageHeight;
    mImageSizeKey = imageSizeKey;
    mChannels = channels;
    mIsRGB = isRGB;
    mToGrey = toGrey;
    mQuality = quality;
    mResultsKey = resultsKey;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    if (!Utils.isAvailable) {
      Log.w(TAG, "Library not available");
      done.run();
      return;
    }

    final ByteBuffer input = MapUtils.getDirectByteBuffer(data.attributes, mInputKey, TAG, true);
    if (input == null) {
      done.run();
      return;
    }

    int width = mImageWidth;
    int height = mImageHeight;
    if (mImageSizeKey != null) {
      Size size = MapUtils.getSize(data.attributes, mImageSizeKey, TAG, true);
      if (size == null) {
        done.run();
        return;
      }
      width = size.getWidth();
      height = size.getHeight();
    }

    final int rowStride = width * mChannels;
    final int len = height * rowStride;
    if (input.limit() < len) {
      Log.e(TAG, "The input buffer is too small");
      done.run();
      return;
    }

    new Worker(data, input, width, height, mChannels, mIsRGB, mToGrey, rowStride, 0,
        null, mQuality, mResultsKey, data.imageJpeg, done)
        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }

  static class Worker extends AsyncTask<Void, Void, Void> {
    private final Postprocessor.Data mData;
    private final ByteBuffer mImage;
    private final int mImageWidth;
    private final int mImageHeight;
    private final int mChannels;
    private final boolean mIsRGB;
    private final boolean mToGrey;
    private final int mRowStride;
    private final int mRotation;
    private final RectF mROI;
    private final int mQuality;
    private final String mResultsKey;
    private final File mOutFile;
    private final Runnable mDone;

    /**
     * Encodes the image as a JPG.
     *
     * @param data       The postprocessor data.
     * @param image      The image buffer.
     * @param width      The image width.
     * @param height     The image height.
     * @param channels   The image channel count.
     * @param isRGB      For 3-channel images, set this to true if the image is in RGB channel order
     *                   rather than BGR.
     * @param toGrey     For 3-channel images, set this to true to convert the output to greyscale.
     * @param rowStride  The row stride in bytes.
     * @param rotation   The rotation (must be a multiple of 90).
     * @param roi        The region of interest to encode. Null encodes the entire image.
     * @param quality    The JPEG encoding quality.
     * @param resultsKey The key under which to store the JPEG output buffer in the
     *                   {@link Postprocessor.Data#attributes}. If null, the reference is assigned
     *                   to {@link Postprocessor.Data#buffer} instead.
     * @param outFile    Optional output file path to which to write the JPEG.
     * @param done       Called when processing has finished.
     */
    Worker(@NonNull Postprocessor.Data data, @NonNull ByteBuffer image, int width, int height,
           int channels, boolean isRGB, boolean toGrey, int rowStride, int rotation,
           @Nullable RectF roi, int quality, @Nullable String resultsKey, @Nullable File outFile,
           @NonNull Runnable done) {
      mData = data;
      mImage = image;
      mImageWidth = width;
      mImageHeight = height;
      mChannels = channels;
      mIsRGB = isRGB;
      mToGrey = toGrey;
      mRowStride = rowStride;
      mRotation = rotation;
      mROI = roi == null ? new RectF(0, 0, 1, 1) : roi;
      mQuality = quality;
      mResultsKey = resultsKey;
      mOutFile = outFile;
      mDone = done;
    }

    @Override
    protected Void doInBackground(Void... voids) {
      // Determine the absolute crop region. The data will be rotated before cropping and the roi is
      // specified in the rotated coordinate system, but the scaling here happens before rotation,
      // so the absolute offsets must be adjusted accordingly.
      final Rect roi = mRotation == 90 || mRotation == 270
          ? Utils.absoluteRegion(mROI, mImageHeight, mImageWidth)
          : Utils.absoluteRegion(mROI, mImageWidth, mImageHeight);

      ByteBuffer jpeg;
      try {
        Trace.beginSection("encodeJPEG");
        jpeg = Utils.encodeJPEG(mImage, mImageWidth, mImageHeight, mChannels, mIsRGB, mToGrey,
            mRowStride, mRotation, roi.left, roi.top, roi.right, roi.bottom, mQuality);
      } catch (Exception e) {
        Log.e(TAG, "Failed to encode: ", e.getMessage());
        return null;
      } finally {
        Trace.endSection(); // encodeJPEG
      }

      // Store the result reference.
      if (mResultsKey == null) {
        mData.buffer = jpeg;
      } else {
        if (mData.attributes == null) mData.attributes = new HashMap<>();
        mData.attributes.put(mResultsKey, jpeg);
      }

      // Write the JPEG to a file if an output path was specified.
      if (jpeg != null && mOutFile != null) {
        jpeg.rewind();
        try (FileChannel fc = new FileOutputStream(mOutFile).getChannel()) {
          fc.write(jpeg);
          Log.d(TAG, mData.id, ": Completed with output bytes = ", jpeg.limit());
        } catch (IOException e) {
          Log.w(TAG, "Failed to write to ", mOutFile.getName(), ": ", e.getMessage());
        }
      }

      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      finish();
    }

    @Override
    protected void onCancelled() {
      finish();
    }

    private void finish() {
      mDone.run();
    }
  }
}
