package io.hubsy.core.camera.postprocess;

import android.content.Context;
import android.support.annotation.Nullable;

import java.util.Set;

import io.hubsy.core.AwsClient;
import io.hubsy.core.util.Log;

/**
 * This postprocessing stage calls {@link AwsClient#uploadSensorEvent} and optionally executes one
 * of two child stages depending on whether this is considered an event. The decision is based on
 * certain conditions being fulfilled. Currently, only
 * {@link Postprocessor.SharedData#hasForegroundChanges} is considered.
 */
public class EventDecisionStage implements Postprocessor.Stage {
  private static final String TAG = "EventDecisionStage";

  private final Context mContext;
  private final long mTimestamp;
  private final Set<String> mSensors;
  private final Postprocessor.Stage mEventStage;
  private final Postprocessor.Stage mNonEventStage;

  /**
   * @param ctx           A context.
   * @param timestamp     The event timestamp in Unix time.
   * @param sensors       The sensors to specify as having triggered the event.
   * @param eventStage    Optional child stage that is processed if the decision is positive.
   * @param nonEventStage Optional child stage that is processed if the decision is negative.
   */
  public EventDecisionStage(Context ctx, long timestamp, Set<String> sensors,
                            @Nullable Postprocessor.Stage eventStage,
                            @Nullable Postprocessor.Stage nonEventStage) {
    mContext = ctx.getApplicationContext();
    mTimestamp = timestamp;
    mSensors = sensors;
    mEventStage = eventStage;
    mNonEventStage = nonEventStage;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    Postprocessor.Stage child;
    if (data.shared.hasForegroundChanges) {
      AwsClient.uploadSensorEvent(mContext, mTimestamp, mSensors);
      child = mEventStage;
    } else {
      Log.d(TAG, "Image analysis outcome: no event");
      child = mNonEventStage;
    }

    if (child != null) child.process(data, done);
    else done.run();
  }
}
