package io.hubsy.core.camera.postprocess;

import io.hubsy.core.util.Log;

/**
 * This stage does nothing other than wait until both {@link EventWaiterStage#process} and
 * {@link EventWaiterStage#run} have been called, in any order, before it marks itself done. Both
 * methods must be invoked on the same thread.
 */
public class EventWaiterStage implements Postprocessor.Stage, Runnable {
  private static final String TAG = "EventWaiterStage";

  private Runnable mDone;
  private boolean mEventReceived;

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    if (mEventReceived) {
      done.run();
    } else {
      mDone = done;
      Log.d(TAG, "Waiting for event");
    }
  }

  @Override
  public void run() {
    if (mDone != null) {
      mDone.run();
    } else {
      mEventReceived = true;
      Log.d(TAG, "Event received");
    }
  }
}
