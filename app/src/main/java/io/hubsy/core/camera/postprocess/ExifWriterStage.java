package io.hubsy.core.camera.postprocess;

import android.os.Handler;
import android.support.annotation.Nullable;

import io.hubsy.core.camera.ExifWriter;
import io.hubsy.core.util.Log;

/**
 * A postprocessing stage to write EXIF data to {@link Postprocessor.Data#imageJpeg} using
 * {@link ExifWriter}.
 */
public class ExifWriterStage implements Postprocessor.Stage {
  private static final String TAG = "ExifWriterStage";

  private String mComment;

  public ExifWriterStage(@Nullable String exifComment) {
    mComment = exifComment;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    if (data.imageJpeg == null) {
      Log.e(TAG, "JPEG image is null");
      done.run();
      return;
    }

    new ExifWriter(data.imageJpeg, data.captureParams, mComment, done, new Handler()).execute();
  }
}
