package io.hubsy.core.camera.postprocess;

import android.support.annotation.Nullable;

import java.util.concurrent.atomic.AtomicInteger;

import io.hubsy.core.util.Log;

/**
 * A postprocessing stage which closes {@link Postprocessor.Data#image} and sets it to null.
 * <p>
 * Note that this should not be used together with {@link ImageFileWriterStage}, as that stage
 * already closes the image once written.
 */
public class ImageCloserStage implements Postprocessor.Stage {
  private static final String TAG = "ImageCloserStage";

  private AtomicInteger mNumImagesToClose;
  private Runnable mLastImageClosedCallback;

  /**
   * @param numImagesToClose        If non-null, this counter will be decremented when the image has
   *                                been closed.
   * @param lastImageClosedCallback If non-null, this is called when numImagesToClose reaches zero.
   */
  public ImageCloserStage(@Nullable AtomicInteger numImagesToClose,
                          @Nullable Runnable lastImageClosedCallback) {
    mNumImagesToClose = numImagesToClose;
    mLastImageClosedCallback = lastImageClosedCallback;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    if (data.image == null) {
      Log.e(TAG, "Cannot close a null image");
      done.run();
      return;
    }

    // Close the image.
    try {
      data.image.close();
    } catch (IllegalStateException e) {
      Log.e(TAG, "Failed to close image: " + e.getMessage());
    }
    data.image = null;

    // Decrement the outstanding images counter and, if it has reached zero, invoke the callback.
    if (mNumImagesToClose != null) {
      int remaining = mNumImagesToClose.decrementAndGet();
      if (remaining == 0 && mLastImageClosedCallback != null) {
        mLastImageClosedCallback.run();
      }
    }
    mLastImageClosedCallback = null; // Do not keep the reference alive any longer than necessary.

    done.run();
  }
}
