package io.hubsy.core.camera.postprocess;

import android.graphics.ImageFormat;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

import io.hubsy.core.camera.CameraController;
import io.hubsy.core.camera.ImageFileWriter;
import io.hubsy.core.util.Log;

/**
 * A postprocessing stage to write the image to disk with {@link ImageFileWriter}.
 * <p>
 * Sets {@link Postprocessor.Data#imageJpeg} or {@link Postprocessor.Data#imageDng}, depending on
 * the image format, when done and closes the image.
 */
public class ImageFileWriterStage implements Postprocessor.Stage {
  private static final String TAG = "ImageFileWriterStage";

  private final String mPathNoExt;

  // The callback reference needs to be kept alive while the task runs.
  @SuppressWarnings("FieldCanBeLocal")
  private Runnable mCallback;

  private AtomicInteger mNumImagesToClose;
  private Runnable mLastImageClosedCallback;

  /**
   * @param pathNoExt               The output file path without the file extension.
   * @param numImagesToClose        If non-null, the counter will be decremented when the image has
   *                                been closed.
   * @param lastImageClosedCallback If non-null, this is called when numImagesToClose reaches zero.
   */
  public ImageFileWriterStage(@NonNull String pathNoExt, @Nullable AtomicInteger numImagesToClose,
                              @Nullable Runnable lastImageClosedCallback) {
    mPathNoExt = pathNoExt;
    mNumImagesToClose = numImagesToClose;
    mLastImageClosedCallback = lastImageClosedCallback;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    final CameraController.ImageWrapper image = data.image;
    if (image == null || data.captureResult == null || data.captureParams == null
        || data.cameraCharacteristics == null) {
      Log.e(TAG, "Failed to save image, not all required data is available");
      done.run();
      return;
    }

    // Construct the image file name from the given path and the correct extension.
    File file;
    switch (image.data.getFormat()) {
      case ImageFormat.JPEG:
        file = new File(mPathNoExt + ".jpg");
        data.imageJpeg = file;
        break;
      case ImageFormat.RAW_SENSOR:
        file = new File(mPathNoExt + ".dng");
        data.imageDng = file;
        break;
      default:
        file = new File(mPathNoExt + ".dat");
        Log.e(TAG, "Unknown image format");
    }

    // Callback to clean-up resources after the image has been written. Calls done when finished.
    mCallback = () -> {
      // Close the image.
      try {
        data.image.close();
      } catch (IllegalStateException e) {
        Log.e(TAG, "Failed to close image: " + e.getMessage());
      }
      data.image = null;

      // Decrement the outstanding images counter and, if it has reached zero, invoke the callback.
      if (mNumImagesToClose != null) {
        int remaining = mNumImagesToClose.decrementAndGet();
        if (remaining == 0 && mLastImageClosedCallback != null) {
          mLastImageClosedCallback.run();
        }
      }
      mLastImageClosedCallback = null; // Do not keep the reference alive any longer than necessary.

      done.run();
    };

    // Save the image in an async task. Run on a thread pool so this stage is not held up by slow
    // stages such as DngConverterStage.
    new ImageFileWriter(image.data, file, data.cameraCharacteristics, data.captureResult,
        data.captureParams.rotation, mCallback, new Handler())
        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    // The orientation of raw images is written to the DNG metadata, which ends up producing an
    // already rotated JPG.
    if (image.data.getFormat() == ImageFormat.RAW_SENSOR) data.captureParams.rotation = 0;
  }
}
