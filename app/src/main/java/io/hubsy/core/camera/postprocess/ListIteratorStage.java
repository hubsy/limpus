package io.hubsy.core.camera.postprocess;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import io.hubsy.core.util.Log;

/**
 * A postprocessor stage to iterate over a list of outputs from a previous stage. For each element,
 * one or more consumer stages are inserted into the postprocessing pipeline and passed the current
 * element as the value of the given outputKey in {@link Postprocessor.Data#attributes}.
 */
public class ListIteratorStage implements Postprocessor.Stage {
  private static final String TAG = "ListIteratorStage";

  private final String mInputKey;
  private final List<Postprocessor.Stage> mConsumers;
  private final int mMaxIter;
  private final String mOutputKey;

  /**
   * @param inputKey  The key to retrieve the input from {@link Postprocessor.Data#attributes}.
   *                  This is usually a {@link List}, in which case each list element is passed
   *                  to the consumers, one at a time, starting from index 0. If it is not a list,
   *                  then the consumers are invoked once with that element.
   * @param consumers The consumers to invoke for each input element, in the given order. The
   *                  consumers must all be repeatable, i.e. support having their
   *                  {@link Postprocessor.Stage#process} method called more than once.
   * @param maxIter   Limits the number of iterations and thus list elements to be processed.
   * @param outputKey The key to use for the element that is being forwarded to the consumers.
   */
  public ListIteratorStage(@NonNull String inputKey, @NonNull List<Postprocessor.Stage> consumers,
                           int maxIter, @NonNull String outputKey) {
    mInputKey = inputKey;
    mConsumers = consumers;
    mMaxIter = maxIter;
    mOutputKey = outputKey;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    Object attr;
    if (data.attributes == null || (attr = data.attributes.get(mInputKey)) == null) {
      // A missing input is treated as empty and not as an error.
      done.run();
      return;
    }

    if (!(attr instanceof List)) {
      // Not a list, pass the attr as is to the consumers.
      data.postprocessor.insertAfterCurrent(mConsumers);
      data.attributes.put(mOutputKey, attr);
      done.run();
      return;
    }

    final List args = (List) attr;
    if (args.isEmpty()) {
      done.run();
      return;
    }

    // Create the iterator and run its first iteration.
    new Iterator(args).process(data, done);
  }

  /**
   * Inserts the consumers mArgs.size() times, setting the outputKey in the attributes to the next
   * element of the input list for each invocation.
   */
  private class Iterator implements Postprocessor.Stage {
    private final List mArgs;
    private final int mNumIters;
    private int mIdx;

    private List<Postprocessor.Stage> mConsumersPlusThis;

    Iterator(List args) {
      mArgs = args;
      mNumIters = mMaxIter < 0 ? mArgs.size() : Math.min(mMaxIter, mArgs.size());

      if (mNumIters > 1) {
        mConsumersPlusThis = new ArrayList<>(mConsumers.size() + 1);
        mConsumersPlusThis.addAll(mConsumers);
        mConsumersPlusThis.add(this);
      }
    }

    @Override
    public void process(Postprocessor.Data data, Runnable done) {
      if (mIdx >= mNumIters) {
        Log.e(TAG, "Invalid invocation, the iterator has exceeded its limit");
        done.run();
        return;
      }

      // Insert the consumers into the postprocessor. Note that instead of doing this here for every
      // iteration, all iterations could be unrolled and added to the postprocessor in one go.
      // However, doing it one iteration at a time makes it possible to easily add dynamic iteration
      // limits in the future, in particular time based limits.
      if (mIdx + 1 >= mNumIters) { // This is the last iteration.
        data.postprocessor.insertAfterCurrent(mConsumers);
      } else { // This is not the last iteration.
        data.postprocessor.insertAfterCurrent(mConsumersPlusThis);
      }

      // Update the attributes with the current value.
      data.attributes.put(mOutputKey, mArgs.get(mIdx));
      mIdx++;

      done.run();
    }
  }
}
