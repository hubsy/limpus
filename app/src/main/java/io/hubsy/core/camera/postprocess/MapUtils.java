package io.hubsy.core.camera.postprocess;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Size;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.hubsy.core.util.Log;

/**
 * Utilities for extracting more specifically typed data from {@link Map Maps} with {@link Object}
 * values.
 * <p></p>
 * All functions in this class behave as follows:
 * <ul>
 * <li>If the given map or the value retrieved from the map using the given key are null, the return
 * value will be null.
 * <li>If the value is not of the expected type, the return value will be null.
 * <li>If any of the conditions above is met so that the return value will be null and the logTag
 * is not null, a message is logged for that tag. The log message will be at debug level only if
 * it is due to to a null map or value AND logNullAsError is false; otherwise an error will be
 * logged.
 * </ul>
 */
public abstract class MapUtils {

  @Nullable
  private static ByteBuffer getByteBuffer(@Nullable Map<String, Object> m, @NonNull String key,
                                          @Nullable String logTag, boolean logNullAsError) {
    Object obj;
    if (m == null || (obj = m.get(key)) == null) {
      logNull(logTag, key, logNullAsError);
      return null;
    }

    if (!(obj instanceof ByteBuffer)) {
      logInvalidType(logTag, key, "ByteBuffer", obj);
      return null;
    }

    return (ByteBuffer) obj;
  }

  @Nullable
  public static ByteBuffer getDirectByteBuffer(@Nullable Map<String, Object> m,
                                               @NonNull String key, @Nullable String logTag,
                                               boolean logNullAsError) {
    ByteBuffer buf = getByteBuffer(m, key, logTag, logNullAsError);
    if (buf == null) {
      return null;
    }

    if (!buf.isDirect()) {
      log(logTag, true, "Invalid data for " + key + ": not a direct ByteBuffer");
      return null;
    }

    return buf;
  }

  @Nullable
  public static List getList(@Nullable Map<String, Object> m, @NonNull String key,
                             @Nullable String logTag, boolean logNullAsError) {
    Object obj;
    if (m == null || (obj = m.get(key)) == null) {
      logNull(logTag, key, logNullAsError);
      return null;
    }

    if (!(obj instanceof List)) {
      logInvalidType(logTag, key, "List", obj);
      return null;
    }

    return (List) obj;
  }

  @Nullable
  public static Rect getRect(@Nullable Map<String, Object> m, @NonNull String key,
                             @Nullable String logTag, boolean logNullAsError) {
    Object obj;
    if (m == null || (obj = m.get(key)) == null) {
      logNull(logTag, key, logNullAsError);
      return null;
    }

    if (!(obj instanceof Rect)) {
      logInvalidType(logTag, key, "Rect", obj);
      return null;
    }

    return (Rect) obj;
  }

  @Nullable
  public static Size getSize(@Nullable Map<String, Object> m, @NonNull String key,
                             @Nullable String logTag, boolean logNullAsError) {
    Object obj;
    if (m == null || (obj = m.get(key)) == null) {
      logNull(logTag, key, logNullAsError);
      return null;
    }

    if (!(obj instanceof Size)) {
      logInvalidType(logTag, key, "Size", obj);
      return null;
    }

    return (Size) obj;
  }

  @Nullable
  public static Recognition getRecognition(@Nullable Map<String, Object> m, @NonNull String key,
                                           @Nullable String logTag, boolean logNullAsError) {
    Object obj;
    if (m == null || (obj = m.get(key)) == null) {
      logNull(logTag, key, logNullAsError);
      return null;
    }

    if (!(obj instanceof Recognition)) {
      logInvalidType(logTag, key, "Recognition", obj);
      return null;
    }

    return (Recognition) obj;
  }

  private static void logNull(@Nullable String tag, @NonNull String key, boolean isError) {
    log(tag, isError,
        isError
            ? "Invalid data for \"" + key + "\": null"
            : "The data for \"" + key + "\" is null");
  }

  private static void logInvalidType(@Nullable String tag, @NonNull String key,
                                     @NonNull String expectedType, @NonNull Object obj) {
    log(tag, true, String.format(Locale.ENGLISH, "Invalid data for \"%s\": expected a %s, got a %s",
        key, expectedType, obj.getClass().getName()));
  }

  private static void log(@Nullable String tag, boolean isError, @NonNull String msg) {
    if (tag == null) return;

    if (isError) Log.e(tag, msg);
    else Log.d(tag, msg);
  }
}
