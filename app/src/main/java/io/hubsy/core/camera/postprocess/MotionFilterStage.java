package io.hubsy.core.camera.postprocess;

import android.annotation.SuppressLint;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Trace;
import android.support.annotation.NonNull;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;

import io.hubsy.core.imgproc.Utils;
import io.hubsy.core.util.Log;

/**
 * A postprocessor stage to filter results from {@link ObjectDetectionStage}. The input / output are
 * retrieved from / written to {@link Postprocessor.Data#attributes}.
 * <p>
 * The stage can be reused once a given call to {@link #process(Postprocessor.Data, Runnable)} has
 * returned. This allows temporary buffers allocated during the first use to be reused as well.
 */
public class MotionFilterStage implements Postprocessor.Stage {
  private static final String TAG = "MotionFilterStage";

  private final String mRecognitionsKey;
  private final String mPrevFrameKey;
  private final String mNextFrameKey;
  private final int mFrameWidth;
  private final int mFrameHeight;
  private final List<Double> mMotionAngles;
  private final double mToleranceAngle;
  private final double mNormMinMagnitude;
  private final String mResultsKey;

  // Caches temporary buffers for reuse in non-overlapping invocations.
  private final AtomicReference<Cache> mCache = new AtomicReference<>();

  /**
   * @param recognitionsKey  The key to retrieve the input of type List<{@link Recognition}> from
   *                         the attributes.
   * @param prevFrameKey     The key to retrieve the 8-bit greyscale {@link ByteBuffer} for frame0.
   * @param nextFrameKey     The key to retrieve the 8-bit greyscale {@link ByteBuffer} for frame1.
   * @param frameWidth       The frame width.
   * @param frameHeight      The frame height.
   * @param motionAngles     The allowed motion angles in counterclockwise degrees between
   *                         [-180,180] from the positive y-axis.
   * @param toleranceAngle   The tolerance angle in degrees from the motionAngles in [0,180].
   * @param normMinMagnitude High-pass filter for the normalised magnitude of the motion.
   * @param resultsKey       The key to use when storing the filtered list in the attributes.
   */
  public MotionFilterStage(@NonNull String recognitionsKey, @NonNull String prevFrameKey,
                           @NonNull String nextFrameKey, int frameWidth, int frameHeight,
                           @NonNull List<Double> motionAngles, double toleranceAngle,
                           double normMinMagnitude, @NonNull String resultsKey) {
    mRecognitionsKey = recognitionsKey;
    mPrevFrameKey = prevFrameKey;
    mNextFrameKey = nextFrameKey;
    mFrameWidth = frameWidth;
    mFrameHeight = frameHeight;
    mMotionAngles = motionAngles;
    mToleranceAngle = toleranceAngle;
    mNormMinMagnitude = normMinMagnitude;
    mResultsKey = resultsKey;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    // A missing input is treated as empty and not as an error.
    final List objects = MapUtils.getList(data.attributes, mRecognitionsKey, TAG, false);
    if (objects == null) {
      done.run();
      return;
    }
    if (objects.isEmpty()) {
      Log.d(TAG, "The input is empty");
      done.run();
      return;
    }

    // Get the frame data.
    final ByteBuffer prev = MapUtils.getDirectByteBuffer(data.attributes, mPrevFrameKey, TAG, true);
    final ByteBuffer next = MapUtils.getDirectByteBuffer(data.attributes, mNextFrameKey, TAG, true);
    if (prev == null || next == null) {
      done.run();
      return;
    }

    // Verify the size of the input buffers.
    final int minSize = mFrameWidth * mFrameHeight;
    if (prev.limit() < minSize || next.limit() < minSize) {
      Log.e(TAG, "Error: The input buffer size is insufficient for the image dimensions");
      done.run();
      return;
    }

    new Worker(data, done, objects, prev, next).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }

  private static class Cache {
    final int frameWidth;
    final int frameHeight;
    final ByteBuffer flow;

    Cache(int width, int height, ByteBuffer flow) {
      this.frameWidth = width;
      this.frameHeight = height;
      this.flow = flow;
    }
  }

  // Not a leak, the outer class will not be collected until this completes anyway.
  @SuppressLint("StaticFieldLeak")
  private class Worker extends AsyncTask<Void, Void, Void> {
    private final Postprocessor.Data mData;
    private final Runnable mDone;
    private final List mObjects;
    private final ByteBuffer mPrevFrame;
    private final ByteBuffer mNextFrame;
    private final List<Recognition> mFiltered;

    Worker(Postprocessor.Data data, Runnable done, List objects, ByteBuffer prev, ByteBuffer next) {
      mData = data;
      mDone = done;
      mObjects = objects;
      mPrevFrame = prev;
      mNextFrame = next;
      mFiltered = new ArrayList<>(objects.size());
    }

    @Override
    protected Void doInBackground(Void... voids) {
      try {
        Trace.beginSection("motionFilter");
        process();
      } finally {
        Trace.endSection(); // motionFilter
      }
      return null;
    }

    private void process() {
      // Get the cached buffers or allocate new ones.
      Cache cache = mCache.getAndSet(null);
      if (cache == null) {
        int flowBufSize = mFrameWidth * mFrameHeight * 2 * 4;// 2-channel float32.
        Trace.beginSection("malloc");
        cache = new Cache(mFrameWidth, mFrameHeight, ByteBuffer.allocateDirect(flowBufSize));
        Trace.endSection(); // malloc
      }

      try {
        Trace.beginSection("calcOpticalFlow");
        Utils.calcOpticalFlow(mPrevFrame, mNextFrame, mFrameWidth, mFrameHeight, 1, cache.flow);
      } catch (Exception e) {
        Log.e(TAG, "Error in calcOpticalFlow: ", e.getMessage());
        return;
      } finally {
        Trace.endSection(); // calcOpticalFlow
      }

      final double minMag2 = mNormMinMagnitude * mNormMinMagnitude;

      // Filter objects, maintaining their order.
      for (final Object val : mObjects) {
        // Validate its type.
        if (!(val instanceof Recognition)) {
          Log.e(TAG, "Invalid object type, expected Recognition, got ",
              (val == null ? "null" : val.getClass().getName()));
          continue;
        }
        final Recognition object = (Recognition) val;

        // Ensure the frame sizes match.
        if (mFrameWidth != object.getInputWidth() || mFrameHeight != object.getInputHeight()) {
          Log.e(TAG, Locale.ENGLISH, "The frame dimensions of %dx%d do not"
                  + " match the object detector input dimensions of %dx%d",
              mFrameWidth, mFrameHeight, object.getInputWidth(), object.getInputHeight());
          continue;
        }

        // Slightly shrink the object's bbox and, thus, reduce the number of flow vectors not
        // actually on the object's path, especially for objects at oblique angles, then scale it to
        // the frame dimensions.
        final RectF normalised = object.getBbox();
        if (normalised == null) {
          Log.e(TAG, "The object's bounding box is null");
          continue;
        }
        final Rect bbox = scaleBbox(normalised, 0.75f);

        // Calculate the mean flow  vector for the bbox.
        float[] meanVec;
        try {
          Trace.beginSection("calcMeanFlowVector");
          meanVec = Utils.calcMeanFlowVector(cache.flow, mFrameWidth, mFrameHeight, bbox.left,
              bbox.top, bbox.right, bbox.bottom);
        } catch (Exception e) {
          Log.e(TAG, "Error in calcMeanFlowVector: ", e.getMessage());
          return;
        } finally {
          Trace.endSection(); // calcMeanFlowVector
        }
        if (meanVec == null || meanVec.length < 2) {
          Log.e(TAG, "Invalid vector length");
          continue;
        }

        // Filter by the magnitude of the motion vector normalised to the image dimensions.
        final double normDx = meanVec[0] / mFrameWidth;
        final double normDy = meanVec[1] / mFrameHeight;
        final double mag2 = normDx * normDx + normDy * normDy;
        if (mag2 < minMag2) {
          Log.d(TAG, Locale.ENGLISH, "%s: Drop [%d] v=(%.2f,%.2f) m=%.4f",
              mData.id, object.getID(), normDx, normDy, Math.sqrt(mag2));
          continue;
        }

        // Filter by the angle of the motion vector (from the positive y-axis) when compared to the
        // allowed motion angles.
        final double angle = Math.toDegrees(Math.atan2(meanVec[0], meanVec[1]));
        boolean angleIsValid = mMotionAngles.isEmpty();
        double diff = 360.;
        for (final double allowedAngle : mMotionAngles) {
          diff = angle - allowedAngle;
          diff += diff > 180. ? -360. : diff < -180. ? 360. : 0.; // Move diff into [-180,180].
          if (Math.abs(diff) <= mToleranceAngle) {
            angleIsValid = true;
            break;
          }
        }
        if (!angleIsValid) {
          Log.d(TAG, Locale.ENGLISH, "%s: Drop [%d] v=(%.2f,%.2f) m=%.4f a=%.1f d=%.1f",
              mData.id, object.getID(), normDx, normDy, Math.sqrt(mag2), angle, diff);
          continue;
        }

        Log.d(TAG, Locale.ENGLISH, "%s: Keep [%d] v=(%.2f,%.2f) m=%.4f a=%.1f d=%.1f",
            mData.id, object.getID(), normDx, normDy, Math.sqrt(mag2), angle, diff);
        mFiltered.add(object);
      }

      mCache.getAndSet(cache);
      Log.d(TAG, mData.id, ": Passed ", mFiltered.size());
    }

    /**
     * Scales the bbox first by scaleFactor around its own centre and then by the frame dimensions.
     */
    private Rect scaleBbox(final RectF normalised, float scaleFactor) {
      final float centerX = normalised.centerX();
      final float centerY = normalised.centerY();
      final Matrix mat = new Matrix();
      mat.preScale(mFrameWidth, mFrameHeight);
      mat.preTranslate(centerX, centerY);
      mat.preScale(scaleFactor, scaleFactor);
      mat.preTranslate(-centerX, -centerY);

      final RectF scaled = new RectF();
      mat.mapRect(scaled, normalised);

      final Rect bbox = new Rect();
      scaled.round(bbox);

      return bbox;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      finish();
    }

    @Override
    protected void onCancelled() {
      finish();
    }

    private void finish() {
      mData.attributes.put(mResultsKey, mFiltered);

      mDone.run();
    }
  }
}
