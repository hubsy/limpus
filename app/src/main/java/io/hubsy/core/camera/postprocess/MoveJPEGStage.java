package io.hubsy.core.camera.postprocess;

import android.support.annotation.NonNull;

import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;

/**
 * A postprocessing stage to move {@link Postprocessor.Data#imageJpeg} to a different folder.
 */
public class MoveJPEGStage implements Postprocessor.Stage {
  private static final String TAG = "MoveJPEGStage";

  private final String mDirTo;

  /**
   * @param dirTo A subdirectory path, relative to the app's root data directory.
   */
  public MoveJPEGStage(@NonNull String dirTo) {
    mDirTo = dirTo;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    if (data.imageJpeg == null) {
      Log.e(TAG, "JPEG filepath is null");
      done.run();
      return;
    }

    Helpers.moveFile(data.imageJpeg, mDirTo);
    done.run();
  }
}
