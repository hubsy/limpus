package io.hubsy.core.camera.postprocess;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * A callback used by number plate OCR stages to signal the outcome.
 */
public interface NumberPlateOCRCallback {
  /**
   * Number plate recognition has completed. Must be invoked on the main thread.
   *
   * @param data       The postprocessing data of the corresponding frame.
   * @param text       The number plate text if it was recognised, or null otherwise.
   * @param confidence The confidence in the text and bbox results. Range [0, 100].
   */
  void numberPlate(@NonNull Postprocessor.Data data, @Nullable String text, float confidence);
}
