package io.hubsy.core.camera.postprocess;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Trace;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.tensorflow.lite.Interpreter;

import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executor;

import io.hubsy.core.imgproc.Utils;
import io.hubsy.core.util.Log;
import io.hubsy.core.util.SerialExecutor;

/**
 * A postprocessor stage for number plate OCR. The input / output are retrieved from / written to
 * {@link Postprocessor.Data#attributes}.
 * <p>
 * An instance can be used repeatedly for inference with the same model. Call {@link #close()} to
 * release the resources held by the instance when it is no longer needed.
 */
public class NumberPlateOCRStage implements Postprocessor.Stage, AutoCloseable {
  private static final String TAG = "NumberPlateOCRStage";

  private static final int NUM_CHANNELS = 3;
  private static final int NUM_BYTES_PER_CHANNEL = 1;

  private static final int MAX_CHARS = 6;
  private static final char[] CHAR_TABLE = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
      'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' '};

  private final String mInputKey;
  private final int mInputWidth;
  private final int mInputHeight;
  private final Executor mExecutor;
  private final String mResultKey;
  private final NumberPlateOCRCallback mCallback;

  private Interpreter mTFLite;

  /**
   * @param model       The mapped model file.
   * @param inputKey    The key to retrieve the input image byte buffer from
   *                    {@link Postprocessor.Data#attributes}.
   * @param inputWidth  The input image width.
   * @param inputHeight The input image height.
   * @param executor    The executor on which to run tasks. When null, tasks will be executed on a
   *                    background thread.
   * @param numThreads  The number of threads to use for inference.
   * @param resultKey   If non-null and OCR is successful, the key is used to store the result of
   *                    type {@link Recognition} in {@link Postprocessor.Data#attributes}, with the
   *                    detected text as the label.
   * @param callback    An optional cb to invoke for the number plate text.
   */
  public NumberPlateOCRStage(@NonNull MappedByteBuffer model, @NonNull String inputKey,
                             int inputWidth, int inputHeight, @Nullable Executor executor,
                             int numThreads, @Nullable String resultKey,
                             @Nullable NumberPlateOCRCallback callback) {
    mInputKey = inputKey;
    mInputWidth = inputWidth;
    mInputHeight = inputHeight;
    // Ensure the Interpreter is not used concurrently.
    mExecutor = executor == null ? AsyncTask.SERIAL_EXECUTOR : new SerialExecutor(executor, 2);
    mResultKey = resultKey;
    mCallback = callback;

    init(model, numThreads);
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    final ByteBuffer input = MapUtils.getDirectByteBuffer(data.attributes, mInputKey, TAG, true);
    if (input == null) {
      done.run();
      return;
    }

    final int len = mInputHeight * mInputWidth * NUM_CHANNELS * NUM_BYTES_PER_CHANNEL;
    if (input.limit() != len) {
      Log.e(TAG, "The input buffer is too ", (input.limit() < len ? "small" : "large"));
      done.run();
      return;
    }

    // The execution of this worker must be serialised with the worker used for init to ensure the
    // latter completes first.
    new Worker(data, done, input).executeOnExecutor(mExecutor);
  }

  /**
   * Initialises the interpreter asynchronously.
   */
  @SuppressLint("StaticFieldLeak")
  private void init(MappedByteBuffer model, int numThreads) {
    new AsyncTask<Void, Void, Void>() {
      @Override
      protected Void doInBackground(Void... voids) {
        Trace.beginSection("loadOCRModel");
        try {
          Interpreter.Options options = new Interpreter.Options();
          options.setNumThreads(numThreads);
          mTFLite = new Interpreter(model, options);
          Log.d(TAG, "Interpreter initialised");
        } catch (Exception | UnsatisfiedLinkError e) {
          Log.e(TAG, "Failed to initialise: ", e.getMessage());
        } finally {
          Trace.endSection(); // loadOCRModel
        }
        return null;
      }
    }.executeOnExecutor(mExecutor);
  }

  @Override
  @SuppressLint("StaticFieldLeak")
  public void close() {
    if (mTFLite != null) {
      // Close on the serial executor to ensure it does not clash with inference.
      new AsyncTask<Void, Void, Void>() {
        @Override
        protected Void doInBackground(Void... voids) {
          if (mTFLite != null) {
            try {
              mTFLite.close();
            } catch (Exception e) {
              Log.e(TAG, "Error while closing the Interpreter: ", e.getMessage());
            }
            mTFLite = null;
          }
          return null;
        }
      }.executeOnExecutor(mExecutor);
    }
  }

  // Not a leak, the outer class will not be collected until this completes anyway.
  @SuppressLint("StaticFieldLeak")
  private class Worker extends AsyncTask<Void, Void, Void> {
    private final Postprocessor.Data mData;
    private final Runnable mDone;
    private final ByteBuffer mInput;

    private String mText;
    private float mConfidence;

    Worker(Postprocessor.Data data, Runnable done, ByteBuffer input) {
      mData = data;
      mDone = done;
      mInput = input;
    }

    @Override
    protected Void doInBackground(Void... voids) {
      if (mTFLite == null) {
        Log.e(TAG, mData.id, ": Cannot run inference, the interpreter is not ready");
        return null;
      }

      try {
        Trace.beginSection("numberPlateOCR");
        process();
      } finally {
        Trace.endSection(); // numberPlateOCR
      }
      return null;
    }

    @SuppressLint("UseSparseArrays")
    private void process() {
      // Get the cached image buffer or allocate a new one.
      ByteBuffer floatImage;
      Trace.beginSection("malloc");
      try {
        int capacity = mInputHeight * mInputWidth * NUM_CHANNELS * 4; // float32.
        floatImage = ByteBuffer.allocateDirect(capacity);
      } catch (Throwable e) {
        Log.e(TAG, mData.id, ": ", e.getMessage());
        return;
      } finally {
        Trace.endSection(); // malloc
      }

      // Convert the image to float in range [-1, 1].
      Trace.beginSection("convertToFloat");
      try {
        Utils.convertImageToFloat(mInput, mInputWidth, mInputHeight, NUM_CHANNELS, 1f / 127.5f,
            -1f, floatImage);
      } catch (Exception e) {
        Log.e(TAG, mData.id, ": ", e.getMessage());
        return;
      } finally {
        Trace.endSection(); // convertToFloat
      }

      // NOTE: For some reason it is not safe to share these between invocations, even though worker
      // execution is serialised.
      // Output arrays for the char predictions. Shape [CHAR_INDEX, BATCH_SIZE, CHAR_TABLE_SIZE]
      final float[][][] outputChars = new float[MAX_CHARS][1][CHAR_TABLE.length];
      // Output array for the char count predictions. Shape [BATCH_SIZE, MAX_CHARS]
      final float[][] outputCharCount = new float[1][MAX_CHARS];

      // Prepare the output tensor mappings.
      final Map<Integer, Object> outputMap = new HashMap<>(MAX_CHARS + 1);
      for (int i = 0; i < MAX_CHARS; i++) {
        outputMap.put(i, outputChars[i]);
      }
      outputMap.put(MAX_CHARS, outputCharCount);

      Trace.beginSection("inference");
      try {
        mTFLite.runForMultipleInputsOutputs(new Object[]{floatImage}, outputMap);
      } catch (Exception e) {
        Log.e(TAG, mData.id, ": ", e.getMessage());
        return;
      } finally {
        Trace.endSection(); // inference
      }

      // Get the predictions.
      int[] charIndices = new int[MAX_CHARS];
      char[] chars = new char[MAX_CHARS];
      for (int i = 0; i < MAX_CHARS; i++) {
        charIndices[i] = argmax(outputChars[i][0]);
        chars[i] = CHAR_TABLE[charIndices[i]];
      }
      int charCountIdx = argmax(outputCharCount[0]);
      float charCountConfidence = outputCharCount[0][charCountIdx];
      int charCount = charCountIdx + 1;

      // Construct the text and calculate the prediction confidence.
      StringBuilder text = new StringBuilder(charCount);
      mConfidence = charCountConfidence;
      for (int i = 0; i < charCount; i++) {
        text.append(chars[i]);
        mConfidence *= outputChars[i][0][charIndices[i]];
      }
      // Trim to drop spaces at the end if the char # includes a char that believes it is a space.
      mText = text.toString().trim();

      if (mResultKey != null) { // Store the result.
        Recognition result = new Recognition(0, mText, 0, mConfidence, null,
            mInputWidth, mInputHeight);
        mData.attributes.put(mResultKey, result);
      }

      // Log the results.
      NumberFormat fmt = NumberFormat.getInstance(Locale.ENGLISH);
      fmt.setMinimumIntegerDigits(0);
      fmt.setMinimumFractionDigits(3);
      fmt.setMaximumFractionDigits(3);
      StringBuilder msg = new StringBuilder(100);
      msg.append(mData.id).append(": Read ").append(mText).append(":");
      msg.append(fmt.format(mConfidence));
      msg.append(" [char# ").append(charCount).append(":");
      msg.append(fmt.format(charCountConfidence)).append(" -");
      for (int i = 0; i < MAX_CHARS; i++) {
        msg.append(" ").append(chars[i]).append(":");
        msg.append(fmt.format(outputChars[i][0][charIndices[i]]));
      }
      msg.append(']');
      Log.d(TAG, msg.toString());
    }

    /**
     * Returns the index for the largest value in v. Only reports the first index for that value.
     */
    private int argmax(float[] v) {
      float max = Float.NEGATIVE_INFINITY;
      int idx = -1;
      for (int i = 0; i < v.length; i++) {
        if (v[i] > max) {
          max = v[i];
          idx = i;
        }
      }
      return idx;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      finish();
    }

    @Override
    protected void onCancelled() {
      finish();
    }

    private void finish() {
      if (mCallback != null) {
        mCallback.numberPlate(mData, mText, mConfidence * 100f);
      }

      mDone.run();
    }
  }
}
