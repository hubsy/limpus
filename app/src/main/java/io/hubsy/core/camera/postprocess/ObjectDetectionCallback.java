package io.hubsy.core.camera.postprocess;

import android.graphics.RectF;
import android.support.annotation.NonNull;

/**
 * A callback used to signal object detections.
 */
public interface ObjectDetectionCallback {
  /**
   * An object has been detected. Must be invoked on the main thread.
   *
   * @param data       The postprocessing data of the corresponding frame.
   * @param id         An ID with implementation defined meaning (e.g. the object type) or -1 if
   *                   unused.
   * @param bbox       The normalised bounding box for the object.
   * @param confidence The detection confidence. Range [0, 100].
   */
  void objectDetected(@NonNull Postprocessor.Data data, int id, @NonNull RectF bbox,
                      float confidence);
}
