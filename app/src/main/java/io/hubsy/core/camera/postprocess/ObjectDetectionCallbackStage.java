package io.hubsy.core.camera.postprocess;

import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.NonNull;

/**
 * A postprocessor stage to send {@link ObjectDetectionCallback#objectDetected} notifications with
 * the correctly adjusted bounding box and confidence parameters.
 */
public class ObjectDetectionCallbackStage implements Postprocessor.Stage {
  private static final String TAG = "ObjectDetectionCallbackStage";

  private final String mBboxKey;
  private final String mRecognitionKey;
  private final int mFrameWidth;
  private final int mFrameHeight;
  private final int mOffsetX;
  private final int mOffsetY;
  private final int mObjectTypeID;
  private final ObjectDetectionCallback mCallback;

  /**
   * @param bboxKey        The key to retrieve the bounding box in {@link Postprocessor.Data#buffer}
   *                       coordinates from the attributes.
   * @param recognitionKey The key to retrieve the {@link Recognition} from the attributes.
   * @param frameWidth     The frame width.
   * @param frameHeight    The frame height.
   * @param offsetX        The horizontal buffer offset in the frame.
   * @param offsetY        The vertical buffer offset in the frame.
   * @param objectTypeID   The ID to pass to the callback method.
   * @param callback       The callback to invoke.
   */
  public ObjectDetectionCallbackStage(@NonNull String bboxKey, @NonNull String recognitionKey,
                                      int frameWidth, int frameHeight, int offsetX, int offsetY,
                                      int objectTypeID, @NonNull ObjectDetectionCallback callback) {
    mBboxKey = bboxKey;
    mRecognitionKey = recognitionKey;
    mFrameWidth = frameWidth;
    mFrameHeight = frameHeight;
    mOffsetX = offsetX;
    mOffsetY = offsetY;
    mObjectTypeID = objectTypeID;
    mCallback = callback;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    Rect bbox = MapUtils.getRect(data.attributes, mBboxKey, TAG, true);
    Recognition r = MapUtils.getRecognition(data.attributes, mRecognitionKey, TAG, true);
    if (bbox != null && r != null) {
      // Add the offsets to the bbox and normalise it to the frame dimensions.
      RectF norm = new RectF(
          (bbox.left + mOffsetX) / (float) mFrameWidth,
          (bbox.top + mOffsetY) / (float) mFrameHeight,
          (bbox.right + mOffsetX) / (float) mFrameWidth,
          (bbox.bottom + mOffsetY) / (float) mFrameHeight);

      mCallback.objectDetected(data, mObjectTypeID, norm, r.getConfidence() * 100f);
    }

    done.run();
  }
}
