package io.hubsy.core.camera.postprocess;

import android.annotation.SuppressLint;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Trace;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.tensorflow.lite.Interpreter;

import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

import io.hubsy.core.util.Log;
import io.hubsy.core.util.SerialExecutor;

/**
 * A postprocessor stage for object detection. The input / output are retrieved from / written to
 * {@link Postprocessor.Data#attributes}.
 * <p>
 * An instance can be used repeatedly for inference with the same model. Call {@link #close()} to
 * release the resources held by the instance when it is no longer needed.
 */
public class ObjectDetectionStage implements Postprocessor.Stage, AutoCloseable {
  private static final String TAG = "ObjectDetectionStage";

  private static final int NUM_CHANNELS = 3;
  private static final int NUM_BYTES_PER_CHANNEL = 1;

  // Only return this many results.
  private static final int NUM_DETECTIONS = 10;

  private final String mInputKey;
  private final int mInputWidth;
  private final int mInputHeight;
  @Nullable
  private final List<String> mLabels;
  private final Executor mExecutor;
  private final int mLogTopN;
  private final float mLogConfidence;
  private final String mResultsKey;

  private Interpreter mTFLite;

  /**
   * @param model         The mapped model file.
   * @param inputKey      The key to retrieve the input image byte buffer from
   *                      {@link Postprocessor.Data#attributes}.
   * @param inputWidth    The input image width.
   * @param inputHeight   The input image height.
   * @param labels        The optional string labels.
   * @param executor      The executor on which to run tasks. When null, tasks will be executed on a
   *                      background thread.
   * @param numThreads    The number of threads to use for inference.
   * @param logTopN       The maximum number of results to log. Set to zero to disable.
   * @param logConfidence The minimum confidence for results to be logged. Range [0,1].
   * @param resultsKey    The key to use when storing the results of type List<{@link Recognition}>
   *                      in {@link Postprocessor.Data#attributes}.
   */
  public ObjectDetectionStage(@NonNull MappedByteBuffer model, @NonNull String inputKey,
                              int inputWidth, int inputHeight, @Nullable List<String> labels,
                              @Nullable Executor executor, int numThreads, int logTopN,
                              float logConfidence, @NonNull String resultsKey) {
    mInputKey = inputKey;
    mInputWidth = inputWidth;
    mInputHeight = inputHeight;
    mLabels = labels;
    // Ensure the Interpreter is not used concurrently.
    mExecutor = executor == null ? AsyncTask.SERIAL_EXECUTOR : new SerialExecutor(executor, 2);
    mLogTopN = logTopN;
    mLogConfidence = logConfidence;
    mResultsKey = resultsKey;

    init(model, numThreads);
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    final ByteBuffer input = MapUtils.getDirectByteBuffer(data.attributes, mInputKey, TAG, true);
    if (input == null) {
      done.run();
      return;
    }

    final int len = mInputHeight * mInputWidth * NUM_CHANNELS * NUM_BYTES_PER_CHANNEL;
    if (input.limit() != len) {
      Log.e(TAG, "The input buffer is too ", (input.limit() < len ? "small" : "large"));
      done.run();
      return;
    }

    // The execution of this worker must be serialised with the worker used for init to ensure the
    // latter completes first.
    new Worker(data, done, input).executeOnExecutor(mExecutor);
  }

  /**
   * Initialises the interpreter asynchronously.
   */
  @SuppressLint("StaticFieldLeak")
  private void init(MappedByteBuffer model, int numThreads) {
    new AsyncTask<Void, Void, Void>() {
      @Override
      protected Void doInBackground(Void... voids) {
        Trace.beginSection("loadDetectionModel");
        try {
          Interpreter.Options options = new Interpreter.Options();
          options.setNumThreads(numThreads);
          mTFLite = new Interpreter(model, options);
          Log.d(TAG, "Interpreter initialised");
        } catch (Exception | UnsatisfiedLinkError e) {
          Log.e(TAG, "Failed to initialise: ", e.getMessage());
        } finally {
          Trace.endSection(); // loadDetectionModel
        }
        return null;
      }
    }.executeOnExecutor(mExecutor);
  }

  @Override
  @SuppressLint("StaticFieldLeak")
  public void close() {
    if (mTFLite != null) {
      // Close on the serial executor to ensure it does not clash with inference.
      new AsyncTask<Void, Void, Void>() {
        @Override
        protected Void doInBackground(Void... voids) {
          if (mTFLite != null) {
            try {
              mTFLite.close();
            } catch (Exception e) {
              Log.e(TAG, "Error while closing the Interpreter: ", e.getMessage());
            }
            mTFLite = null;
          }
          return null;
        }
      }.executeOnExecutor(mExecutor);
    }
  }

  // Not a leak, the outer class will not be collected until this completes anyway.
  @SuppressLint("StaticFieldLeak")
  private class Worker extends AsyncTask<Void, Void, Void> {
    private final Postprocessor.Data mData;
    private final Runnable mDone;
    private final ByteBuffer mInput;

    Worker(Postprocessor.Data data, Runnable done, ByteBuffer input) {
      mData = data;
      mDone = done;
      mInput = input;
    }

    @Override
    protected Void doInBackground(Void... voids) {
      if (mTFLite == null) {
        Log.e(TAG, mData.id, ": Cannot run inference, the interpreter is not ready");
        return null;
      }

      // Contains the location of detected boxes. Shape [BATCH_SIZE, NUM_DETECTIONS, 4]
      final float[][][] outputLocations = new float[1][NUM_DETECTIONS][4];
      // Contains the classes of detected boxes. Shape [BATCH_SIZE, NUM_DETECTIONS]
      final float[][] outputClasses = new float[1][NUM_DETECTIONS];
      // Contains the scores of detected boxes. Shape [BATCH_SIZE, NUM_DETECTIONS]
      final float[][] outputScores = new float[1][NUM_DETECTIONS];
      // Contains the number of detected boxes. Shape [BATCH_SIZE]
      final float[] numDetections = new float[1];


      // Prepare the output tensor mappings. The indices must correspond to those passed to toco
      // during model conversion.
      @SuppressLint("UseSparseArrays") final Map<Integer, Object> outputMap = new HashMap<>();
      outputMap.put(0, outputLocations);
      outputMap.put(1, outputClasses);
      outputMap.put(2, outputScores);
      outputMap.put(3, numDetections);

      Trace.beginSection("objectDetection");
      try {
        mTFLite.runForMultipleInputsOutputs(new Object[]{mInput}, outputMap);
      } catch (Exception e) {
        Log.e(TAG, mData.id, ": ", e.getMessage());
        return null;
      } finally {
        Trace.endSection(); // objectDetection
      }

      // Prepare the results.
      final int numResults = Math.min(NUM_DETECTIONS, (int) numDetections[0]);
      final List<Recognition> results = new ArrayList<>(numResults);
      for (int i = 0; i < numResults; i++) {
        // Ensure the bbox is in range [0,1].
        final RectF bbox = new RectF(
            Math.max(0f, Math.min(1f, outputLocations[0][i][1])),
            Math.max(0f, Math.min(1f, outputLocations[0][i][0])),
            Math.max(0f, Math.min(1f, outputLocations[0][i][3])),
            Math.max(0f, Math.min(1f, outputLocations[0][i][2])));

        // Mobilenet SSD assumes the entry at line 0 in the label file is the background class,
        // but outputClasses numbers foreground classes from 0.
        int labelIdx = (int) outputClasses[0][i] + 1;
        String label = mLabels == null || labelIdx >= mLabels.size() ? null : mLabels.get(labelIdx);

        results.add(new Recognition(
            i, label, labelIdx, outputScores[0][i], bbox, mInputWidth, mInputHeight));
      }

      // Sort by descending confidence.
      Collections.sort(results, (l, r) -> Float.compare(r.getConfidence(), l.getConfidence()));

      mData.attributes.put(mResultsKey, results);
      Log.d(TAG, mData.id, ": Completed with objects = ", numResults);

      // Verbose logging.
      for (int i = 0; i < mLogTopN && i < numResults; i++) {
        Recognition r = results.get(i);
        if (r.getConfidence() < mLogConfidence) break;
        Log.d(TAG, r.toString());
      }

      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      finish();
    }

    @Override
    protected void onCancelled() {
      finish();
    }

    private void finish() {
      mDone.run();
    }
  }
}
