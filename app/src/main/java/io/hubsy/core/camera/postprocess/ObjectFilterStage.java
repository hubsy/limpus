package io.hubsy.core.camera.postprocess;

import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import io.hubsy.core.util.Log;

/**
 * A postprocessor stage to filter results from {@link ObjectDetectionStage}. The input / output are
 * retrieved from / written to {@link Postprocessor.Data#attributes}.
 */
public class ObjectFilterStage implements Postprocessor.Stage {
  private static final String TAG = "ObjectFilterStage";

  /**
   * The allowed orientation of the bounding box.
   */
  public enum Orientation {
    /**
     * Indicates that both landscape and portrait orientations are allowed.
     */
    ANY,
    /**
     * Only landscape orientation is allowed (width >= height).
     */
    LANDSCAPE,
    /**
     * Only portrait orientation is allowed (width <= height).
     */
    PORTRAIT,
  }

  private final String mInputKey;
  private final float mMinConfidence;
  private final int mMinArea;
  private final int mMaxArea;
  private final float mMinAspectRatio;
  private final float mMaxAspectRatio;
  private final Orientation mOrientation;
  private final List<String> mLabels;
  private final String mResultsKey;

  /**
   * Creates a filter stage for the given conditions.
   *
   * @param inputKey       The key to retrieve the input of type List<{@link Recognition}> from
   *                       {@link Postprocessor.Data#attributes}.
   * @param minConfidence  The minimum confidence in range [0,1].
   * @param minArea        The minimum object bbox area.
   * @param maxArea        The maximum object bbox area.
   * @param minAspectRatio The minimum aspect ratio in the specified orientation.
   * @param maxAspectRatio The maximum aspect ratio in the specified orientation.
   * @param orientation    The expected orientation. {@link Orientation#ANY} can be used to allow
   *                       both landscape or portrait bounding boxes.
   * @param labels         Optional list of labels to filter for.
   * @param resultsKey     The key to use when storing the filtered list in
   *                       {@link Postprocessor.Data#attributes}.
   */
  public ObjectFilterStage(@NonNull String inputKey, float minConfidence, int minArea, int maxArea,
                           float minAspectRatio, float maxAspectRatio, Orientation orientation,
                           @Nullable List<String> labels, @NonNull String resultsKey) {
    mInputKey = inputKey;
    mMinConfidence = minConfidence;
    mMinArea = minArea;
    mMaxArea = maxArea;
    mMinAspectRatio = minAspectRatio;
    mMaxAspectRatio = maxAspectRatio;
    mOrientation = orientation;
    mLabels = labels;
    mResultsKey = resultsKey;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    // A missing input is treated as empty and not as an error.
    final List objects = MapUtils.getList(data.attributes, mInputKey, TAG, false);
    if (objects == null) {
      done.run();
      return;
    }

    // Filter the objects.
    final List<Recognition> filtered = new ArrayList<>(objects.size());
    for (int i = 0, size = objects.size(); i < size; i++) {
      final Object val = objects.get(i);

      // Validate its type.
      if (!(val instanceof Recognition)) {
        Log.e(TAG, "Invalid object type, expected Recognition, got ",
            (val == null ? "null" : val.getClass().getName()));
        continue;
      }
      final Recognition object = (Recognition) val;

      if (object.getConfidence() < mMinConfidence) continue;
      if (!validateBbox(i, object)) continue;
      if (!validateLabel(i, object)) continue;

      filtered.add(object);
    }

    Log.d(TAG, data.id, ": Passed ", filtered.size());
    data.attributes.put(mResultsKey, filtered);

    done.run();
  }

  private boolean validateBbox(final int index, final Recognition object) {
    final RectF bbox = object.getBbox();
    final int imageWidth = object.getInputWidth();
    final int imageHeight = object.getInputHeight();
    if (bbox == null || bbox.isEmpty() || imageWidth <= 0 || imageHeight <= 0) {
      Log.w(TAG, "Filtered out [", index, "] due to invalid bbox data");
      return false;
    }

    // Scale the bbox to the input image dimensions.
    final long width = Math.round(bbox.width() * imageWidth);
    final long height = Math.round(bbox.height() * imageHeight);
    final long area = width * height;
    if (area < mMinArea || area > mMaxArea) {
      Log.d(TAG, "Filtered out [", index, "] by area: ", area);
      return false;
    }

    return validateAspectRatio(index, width, height);
  }

  private boolean validateAspectRatio(final int index, final float width, final float height) {
    // The actual orientation.
    final Orientation orientation = width == height
        ? Orientation.ANY // In this case any stands for both equally.
        : width > height
        ? Orientation.LANDSCAPE
        : Orientation.PORTRAIT;

    // If either actual or desired orientation are ANY, then that one satisfies the other.
    if (mOrientation != Orientation.ANY && orientation != Orientation.ANY
        && mOrientation != orientation) {
      Log.d(TAG, "Filtered out [", index, "] by orientation: ", orientation);
      return false;
    }

    // Calculate the ratio based on the larger side in the numerator (i.e. it will be >= 1.0).
    final float ratio = orientation == Orientation.LANDSCAPE ? width / height : height / width;
    final boolean valid = ratio >= mMinAspectRatio && ratio <= mMaxAspectRatio;
    if (!valid) Log.d(TAG, "Filtered out [", index, "] by aspect ratio: ", ratio);
    return valid;
  }

  private boolean validateLabel(final int index, final Recognition object) {
    if (mLabels == null || mLabels.isEmpty()) return true; // Not filtering by label.

    final String label = object.getLabel();
    boolean found = false;
    for (int i = 0; i < mLabels.size() && !found; i++) {
      if (label.equals(mLabels.get(i))) found = true;
    }

    if (!found) Log.d(TAG, "Filtered out [", index, "] by label: ", label);
    return found;
  }
}
