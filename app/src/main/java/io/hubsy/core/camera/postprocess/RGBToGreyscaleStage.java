package io.hubsy.core.camera.postprocess;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Trace;
import android.support.annotation.NonNull;

import java.nio.ByteBuffer;

import io.hubsy.core.imgproc.Utils;
import io.hubsy.core.util.Log;

/**
 * A postprocessor stage that converts an 8-bit 3-channel RGB image to an 8-bit single-channel
 * greyscale image. Both the input and output {@link ByteBuffer ByteBuffers} are retrieved from /
 * stored in {@link Postprocessor.Data#attributes}.
 */
public class RGBToGreyscaleStage implements Postprocessor.Stage {
  private static final String TAG = "RGBToGreyscaleStage";

  private final String mInputKey;
  private final int mImageWidth;
  private final int mImageHeight;
  private final String mResultsKey;

  /**
   * @param inputKey    The key under which the RGB {@link ByteBuffer} is stored in the attributes.
   * @param imageWidth  The image width.
   * @param imageHeight The image height.
   * @param resultsKey  The key under which the greyscale {@link ByteBuffer} will be stored in the
   *                    attributes.
   */
  public RGBToGreyscaleStage(@NonNull String inputKey, int imageWidth, int imageHeight,
                             @NonNull String resultsKey) {
    mInputKey = inputKey;
    mImageWidth = imageWidth;
    mImageHeight = imageHeight;
    mResultsKey = resultsKey;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    if (!Utils.isAvailable) {
      Log.w(TAG, "Library not available");
      done.run();
      return;
    }

    final ByteBuffer rgb = MapUtils.getDirectByteBuffer(data.attributes, mInputKey, TAG, true);
    if (rgb == null) {
      done.run();
      return;
    }

    new Worker(data, done, rgb).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }

  // Not a leak, the outer class will not be collected until this completes anyway.
  @SuppressLint("StaticFieldLeak")
  private class Worker extends AsyncTask<Void, Void, Void> {
    private final Postprocessor.Data mData;
    private final Runnable mDone;
    private final ByteBuffer mRGB;

    Worker(Postprocessor.Data data, Runnable done, ByteBuffer rgb) {
      mData = data;
      mDone = done;
      mRGB = rgb;
    }

    @Override
    protected Void doInBackground(Void... voids) {
      final int greyBufSize = mImageWidth * mImageHeight; // 8-bit single channel.
      ByteBuffer grey;
      try {
        Trace.beginSection("convertRGBToGreyscale");
        grey = ByteBuffer.allocateDirect(greyBufSize);
        Utils.convertRGB888ToGreyscale(mRGB, mImageWidth, mImageHeight, grey);
      } catch (Exception e) {
        Log.e(TAG, mData.id, ": ", e.getMessage());
        return null;
      } finally {
        Trace.endSection(); // convertRGBToGreyscale
      }

      mData.attributes.put(mResultsKey, grey);
      Log.d(TAG, mData.id, ": Conversion successful");

      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      finish();
    }

    @Override
    protected void onCancelled() {
      finish();
    }

    private void finish() {
      mDone.run();
    }
  }
}
