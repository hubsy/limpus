package io.hubsy.core.camera.postprocess;

import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Locale;

/**
 * A classification/detection result.
 */
public class Recognition {
  private final int mID;
  private final String mLabel;
  private final int mLabelID;
  private final float mConfidence;
  private final RectF mBbox;
  private final int mInputWidth;
  private final int mInputHeight;

  /**
   * @param id          The recognition ID within a given result set.
   * @param label       The label.
   * @param labelID     The label ID.
   * @param confidence  The confidence in range [0,1].
   * @param bbox        The bounding box, if any.
   * @param inputWidth  The width of the input image.
   * @param inputHeight The height of the input image.
   */
  public Recognition(int id, @Nullable String label, int labelID, float confidence,
                     @Nullable RectF bbox, int inputWidth, int inputHeight) {
    mID = id;
    mLabel = label == null ? "" : label;
    mLabelID = labelID;
    mConfidence = confidence;
    mBbox = bbox;
    mInputWidth = inputWidth;
    mInputHeight = inputHeight;
  }

  /**
   * Returns the unique ID of this recognition within the corresponding inference run.
   */
  public int getID() {
    return mID;
  }

  /**
   * Returns the label, or empty string if the label is unknown.
   */
  @NonNull
  public String getLabel() {
    return mLabel;
  }

  /**
   * Returns the unique ID of the label within the corresponding model's labels file.
   */
  public int getLabelID() {
    return mLabelID;
  }

  /**
   * Returns the confidence in range [0,1].
   */
  public float getConfidence() {
    return mConfidence;
  }

  /**
   * Returns the normalised bounding box of the object if available.
   */
  @Nullable
  public RectF getBbox() {
    return mBbox;
  }

  /**
   * The image input width. Can be used to scale the bbox to the input dimensions.
   */
  public int getInputWidth() {
    return mInputWidth;
  }

  /**
   * The image input height. Can be used to scale the bbox to the input dimensions.
   */
  public int getInputHeight() {
    return mInputHeight;
  }

  @NonNull
  @Override
  public String toString() {
    return String.format(Locale.ENGLISH, "[%d] %s (%.1f%%)%s",
        mID,
        mLabel.isEmpty() ? "???" : mLabel,
        mConfidence * 100,
        mBbox == null ? "" : String.format(Locale.ENGLISH, " [%.2f,%.2f][%.2f,%.2f]",
            mBbox.left, mBbox.top, mBbox.right, mBbox.bottom));
  }
}
