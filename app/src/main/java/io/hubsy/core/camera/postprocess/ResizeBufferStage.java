package io.hubsy.core.camera.postprocess;

import android.annotation.SuppressLint;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Trace;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Size;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Locale;

import io.hubsy.core.imgproc.Utils;
import io.hubsy.core.util.Log;

/**
 * A postprocessor stage that resizes {@link Postprocessor.Data#buffer} and stores the output buffer
 * in {@link Postprocessor.Data#attributes}.
 */
public class ResizeBufferStage implements Postprocessor.Stage {
  private static final String TAG = "ResizeBufferStage";

  /**
   * A special value for the output width and height parameters, indicating that the parameter in
   * question should be set to the ROI width or height, respectively. These fall back to the input
   * width and height if no ROI is provided.
   */
  @SuppressWarnings("WeakerAccess")
  public static final int MATCH_ROI_SIZE = -1;

  private static final int NUM_BYTES_PER_CHANNEL = 1;

  private final int mInWidth;
  private final int mInHeight;
  private final int mNumChannels;
  private final Rect mROI;
  private final String mROIKey;
  private final int mOutWidth;
  private final int mOutHeight;
  private final int mMaxOutSize;
  private final boolean mAllowUpsampling;
  private final boolean mExpandToOutRatio;
  private final boolean mQualityOverSpeed;
  private final String mOutBufferKey;
  private final String mOutTransformKey;
  private final String mOutSizeKey;

  /**
   * @param inWidth          The input width.
   * @param inHeight         The intput height.
   * @param numChannels      The number of input (and output) channels.
   * @param roi              An optional region of interest to crop from the buffer for resizing.
   * @param roiKey           Another optional way to specify a ROI. This is a key into
   *                         {@link Postprocessor.Data#attributes} which, if non-null, must store
   *                         the {@link Rect} for the crop region.
   * @param outWidth         The output width or {@link #MATCH_ROI_SIZE}.
   * @param outHeight        The output height or {@link #MATCH_ROI_SIZE}.
   * @param maxOutSize       The maximum output size or <=0 to disable. This is meant for use in
   *                         conjunction with {@link #MATCH_ROI_SIZE} for one or both of outWidth
   *                         and outHeight, where the value is unknown up front.
   * @param allowUpsampling  When true and one or both input (or ROI) dimensions are smaller than
   *                         the output dimensions, the image will be upsampled. When false, it
   *                         will only be downsampled.
   * @param expandToOutRatio When true and the ROI has a different aspect ratio from the output
   *                         dimensions, then the ROI is expanded to maintain the same aspect
   *                         ratio (as far as possible within the image dimensions).
   * @param qualityOverSpeed Prefer higher quality over speed when true.
   * @param outBufferKey     The key to use when storing the output in
   *                         {@link Postprocessor.Data#attributes}.
   * @param outTransformKey  If non-null, the key to store the transformation
   *                         {@link android.graphics.Matrix} (ROI translation and scaling) that is
   *                         applied to the buffer in {@link Postprocessor.Data#attributes}.
   * @param outSizeKey       If non-null, the key under which to store the output {@link Size} in
   *                         the attributes.
   */
  public ResizeBufferStage(int inWidth, int inHeight, int numChannels, @Nullable Rect roi,
                           @Nullable String roiKey, int outWidth, int outHeight, int maxOutSize,
                           boolean allowUpsampling, boolean expandToOutRatio,
                           boolean qualityOverSpeed, @NonNull String outBufferKey,
                           @Nullable String outTransformKey, @Nullable String outSizeKey) {
    mInWidth = inWidth;
    mInHeight = inHeight;
    mNumChannels = numChannels;
    mROI = roi;
    mROIKey = roiKey;
    mOutWidth = outWidth;
    mOutHeight = outHeight;
    mMaxOutSize = maxOutSize;
    mAllowUpsampling = allowUpsampling;
    mExpandToOutRatio = expandToOutRatio;
    mQualityOverSpeed = qualityOverSpeed;
    mOutBufferKey = outBufferKey;
    mOutTransformKey = outTransformKey;
    mOutSizeKey = outSizeKey;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    if (data.buffer == null) {
      Log.e(TAG, "Invalid buffer: null");
      done.run();
      return;
    }

    int len = mInHeight * mInWidth * mNumChannels * NUM_BYTES_PER_CHANNEL;
    if (data.buffer.limit() != len) {
      Log.e(TAG, "Invalid buffer size ", data.buffer.limit(), ", expected ", len);
      done.run();
      return;
    }

    // Check if there is a crop region to use.
    // Do not modify member variables to keep the instance reusable.
    Rect roi = mROI == null ? null : new Rect(mROI);
    if (roi == null && mROIKey != null) {
      Rect tmp = MapUtils.getRect(data.attributes, mROIKey, TAG, true);
      if (tmp == null) {
        done.run();
        return;
      }
      roi = new Rect(tmp);
    }

    // Set the ROI to the intersection with the image.
    if (roi == null) {
      roi = new Rect(0, 0, mInWidth, mInHeight);
    } else {
      Rect image = new Rect(0, 0, mInWidth, mInHeight);
      if (!image.intersect(roi)) {
        Log.w(TAG, "The ROI does not intersect the image");
        done.run();
        return;
      }
      roi = image;
    }

    if (roi.isEmpty()) {
      Log.w(TAG, "The ROI is empty");
      done.run();
      return;
    }

    int roiWidth = roi.width();
    int roiHeight = roi.height();

    // Set the out width and height from the ROI if requested.
    int outWidth = mOutWidth == MATCH_ROI_SIZE ? roiWidth : mOutWidth;
    int outHeight = mOutHeight == MATCH_ROI_SIZE ? roiHeight : mOutHeight;
    if (mMaxOutSize > 0 && (outWidth > mMaxOutSize || outHeight > mMaxOutSize)) {
      // Keep the aspect ratio.
      int larger = Math.max(outWidth, outHeight);
      float scale = ((float) mMaxOutSize) / larger;
      outWidth = Math.round(outWidth * scale);
      outHeight = Math.round(outHeight * scale);
    }

    if (outWidth <= 0 || outHeight <= 0) {
      Log.w(TAG, "Invalid output dimensions: ", outWidth, "x", outHeight);
      done.run();
      return;
    }

    if (mExpandToOutRatio) {
      // Maintain the output's aspect ratio by growing one dimension of the ROI.
      float roiRatio = ((float) roiWidth) / roiHeight;
      float outRatio = ((float) outWidth) / outHeight;
      if (outRatio > roiRatio) { // Grow horizontally.
        int growBy = (int) ((Math.round(roiHeight * outRatio) - roiWidth) * 0.5f);
        roi.left -= growBy;
        roi.right += growBy;
      } else if (outRatio < roiRatio) { // Grow vertically.
        int growBy = (int) ((Math.round(roiWidth / outRatio) - roiHeight) * 0.5f);
        roi.top -= growBy;
        roi.bottom += growBy;
      }
      // Shift right/down if necessary.
      if (roi.left < 0) {
        roi.right -= roi.left;
        roi.left = 0;
      }
      if (roi.top < 0) {
        roi.bottom -= roi.top;
        roi.top = 0;
      }
      // And clip it to the input dimensions if necessary (in this case the aspect ratio won't be
      // maintained).
      if (roi.right > mInWidth) roi.right = mInWidth;
      if (roi.bottom > mInHeight) roi.bottom = mInHeight;

      roiWidth = roi.width();
      roiHeight = roi.height();
    }

    if (!mAllowUpsampling && (roiWidth < outWidth || roiHeight < outHeight)) {
      // Do not upsample, set the output to the same size as the ROI.
      outWidth = roiWidth;
      outHeight = roiHeight;
    }

    if (data.attributes == null) data.attributes = new HashMap<>();
    if (mOutSizeKey != null) data.attributes.put(mOutSizeKey, new Size(outWidth, outHeight));

    new Worker(data, roi, outWidth, outHeight, done)
        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }

  // Not a leak, the outer class will not be collected until this completes anyway.
  @SuppressLint("StaticFieldLeak")
  private class Worker extends AsyncTask<Void, Void, Void> {
    private final Postprocessor.Data mData;
    private final Rect mROI;
    private final int mOutWidth;
    private final int mOutHeight;
    private final Runnable mDone;

    Worker(Postprocessor.Data data, Rect roi, int outWidth, int outHeight, Runnable done) {
      mData = data;
      mROI = roi;
      mOutWidth = outWidth;
      mOutHeight = outHeight;
      mDone = done;
    }

    @Override
    protected Void doInBackground(Void... voids) {
      Trace.beginSection("resize");

      // Allocate the output buffer.
      Trace.beginSection("malloc");
      final ByteBuffer buf = ByteBuffer.allocateDirect(
          mOutHeight * mOutWidth * mNumChannels * NUM_BYTES_PER_CHANNEL);
      buf.order(ByteOrder.nativeOrder());
      Trace.endSection(); // malloc

      // Resize.
      try {
        Utils.resize(mData.buffer, mInWidth, mInHeight, mNumChannels, mROI.left, mROI.top,
            mROI.right, mROI.bottom, mOutWidth, mOutHeight, mQualityOverSpeed, buf);
        mData.attributes.put(mOutBufferKey, buf);
      } catch (Exception e) {
        Log.e(TAG, e.getMessage());
        return null;
      } finally {
        Trace.endSection(); // resize
      }

      // Store the transformation matrix if requested.
      if (mOutTransformKey != null) {
        int roiWidth = mROI.width();
        int roiHeight = mROI.height();
        Matrix m = new Matrix();
        if (roiWidth != mOutWidth || roiHeight != mOutHeight) {
          m.preScale(((float) mOutWidth) / roiWidth, ((float) mOutHeight) / roiHeight);
        }
        if (mROI.left != 0 || mROI.top != 0) {
          m.preTranslate(-mROI.left, -mROI.top);
        }
        mData.attributes.put(mOutTransformKey, m);
      }

      Log.d(TAG, Locale.ENGLISH, "%s: Resized ROI %s to %dx%d",
          mData.id, mROI.toShortString(), mOutWidth, mOutHeight);

//      YUVToRGBStage.debugWriteRGBBufferToFile(buf, mOutWidth, mOutHeight);
      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      finish();
    }

    @Override
    protected void onCancelled() {
      finish();
    }

    private void finish() {
      mDone.run();
    }
  }
}
