package io.hubsy.core.camera.postprocess;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Locale;
import java.util.function.Function;

import io.hubsy.core.util.Log;

/**
 * A postprocessor stage that transforms a {@link RectF}, possibly retrieved from a
 * {@link Recognition} by calling {@link Recognition#getBbox()}, and stores a new rect with the
 * transformed values. Both the input and output are retrieved from / stored in
 * {@link Postprocessor.Data#attributes}.
 */
public class TransformRectStage implements Postprocessor.Stage {
  private static final String TAG = "TransformRectStage";

  private final String mInputKey;
  private final Matrix mTransform;
  private final String mTransformKey;
  private final Function<RectF, Matrix> mTransformFn;
  private final boolean mInvertMatrix;
  private final boolean mFloatOut;
  private final String mResultKey;

  /**
   * @param inputKey     The key under which either a {@link RectF} or a {@link Recognition} must be
   *                     stored in the attributes.
   * @param transform    The transformation to apply to the rectangle.
   * @param transformKey Another way to specify the transformation. When provided, this key is used
   *                     to retrieve a {@link Matrix} from {@link Postprocessor.Data#attributes}.
   * @param transformFn  A third way to specify the transformation, suitable when the {@link Matrix}
   *                     depends on the {@link RectF} to be transformed. The function is called with
   *                     the RectF as argument and returns the Matrix to be applied.
   * @param invertMatrix Apply the inverse of the matrix when true and the matrix is invertible.
   * @param floatOut     If true, then the result is stored as a RectF; otherwise it is stored as a
   *                     {@link Rect}, with each element rounded to the nearest int.
   * @param resultKey    The key to store the result under.
   */
  public TransformRectStage(@NonNull String inputKey, @Nullable Matrix transform,
                            @Nullable String transformKey,
                            @Nullable Function<RectF, Matrix> transformFn, boolean invertMatrix,
                            boolean floatOut, @NonNull String resultKey) {
    mInputKey = inputKey;
    mTransform = transform;
    mTransformKey = transformKey;
    mTransformFn = transformFn;
    mInvertMatrix = invertMatrix;
    mFloatOut = floatOut;
    mResultKey = resultKey;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    // Get the RectF.
    Object obj;
    if (data.attributes == null || (obj = data.attributes.get(mInputKey)) == null) {
      Log.e(TAG, "Invalid input: null");
      done.run();
      return;
    }

    final RectF r = (obj instanceof RectF)
        ? (RectF) obj
        : (obj instanceof Recognition)
        ? ((Recognition) obj).getBbox()
        : null;
    if (r == null) {
      Log.e(TAG, "Invalid input: null or not of type RectF or Recognition");
      done.run();
      return;
    }

    // Get the matrix.
    Matrix m = mTransform;
    if (m == null) { // Get it from the attributes.
      Object v;
      if (mTransformKey != null && ((v = data.attributes.get(mTransformKey)) instanceof Matrix)) {
        m = (Matrix) v;
      }
    }
    if (m == null && mTransformFn != null) {
      m = mTransformFn.apply(r); // Get it from the function.
    }
    if (m == null) {
      Log.e(TAG, "Missing the transformation matrix");
      done.run();
      return;
    }

    if (mInvertMatrix) {
      Matrix inverse = new Matrix();
      if (!m.invert(inverse)) {
        Log.e(TAG, "Cannot invert the matrix");
        done.run();
        return;
      }
      m = inverse;
    }

    // Transform the rect and store it using the desired type.
    RectF transformed = new RectF();
    m.mapRect(transformed, r);
    if (mFloatOut) {
      data.attributes.put(mResultKey, transformed);
      Log.d(TAG, Locale.ENGLISH, "%s: Transformed rect to [%.2f,%.2f][%.2f,%.2f]",
          data.id, transformed.left, transformed.top, transformed.right, transformed.bottom);
    } else {
      Rect dst = new Rect();
      transformed.round(dst);
      data.attributes.put(mResultKey, dst);
      Log.d(TAG, data.id, ": Transformed rect to ", dst.toShortString());
    }

    done.run();
  }
}
