package io.hubsy.core.camera.postprocess;

import android.graphics.ImageFormat;
import android.graphics.RectF;
import android.media.Image;
import android.os.AsyncTask;
import android.support.annotation.Nullable;

import io.hubsy.core.imgproc.Utils;
import io.hubsy.core.util.Log;

/**
 * A postprocessor stage that encodes a {@link android.graphics.ImageFormat#YUV_420_888} image as
 * a greyscale JPEG and stores it in {@link Postprocessor.Data#buffer}.
 * <p>
 * <b>NOTE:</b> The output buffer must be released using {@link Utils#freeBuffer} or indirectly via
 * {@link BufferCloserStage}.
 */
public class YUVToJPEGStage implements Postprocessor.Stage {
  private static final String TAG = "YUVToJPEGStage";

  private final RectF mROI;
  private final int mQuality;

  /**
   * <b>NOTE: The output buffer must be released explicitly!</b
   *
   * @param roi     The normalised region of interest to encode.
   * @param quality The JPEG encoding quality in [0,100].
   */
  public YUVToJPEGStage(@Nullable RectF roi, int quality) {
    mROI = roi;
    mQuality = quality;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    if (!Utils.isAvailable) {
      Log.w(TAG, "Library not available");
      done.run();
      return;
    }

    if (data.image == null || data.captureParams == null) {
      Log.e(TAG, "Image or capture params are null");
      done.run();
      return;
    }
    final Image image = data.image.data;

    int format = image.getFormat();
    if (format != ImageFormat.YUV_420_888) {
      Log.e(TAG, "Invalid image format: " + format);
      done.run();
      return;
    }

    final Image.Plane yplane = image.getPlanes()[0];
    new EncodeJPEGStage.Worker(data, yplane.getBuffer(), image.getWidth(), image.getHeight(),
        1, false, false, yplane.getRowStride(), data.captureParams.rotation, mROI,
        mQuality, null, data.imageJpeg, done).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }
}
