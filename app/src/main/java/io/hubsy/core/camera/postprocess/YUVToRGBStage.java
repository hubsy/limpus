package io.hubsy.core.camera.postprocess;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Trace;
import android.support.annotation.NonNull;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Locale;

import io.hubsy.core.imgproc.Utils;
import io.hubsy.core.util.Log;

/**
 * A postprocessor stage that converts a {@link android.graphics.ImageFormat#YUV_420_888} image to
 * RGB and stores it in {@link Postprocessor.Data#buffer}.
 */
public class YUVToRGBStage implements Postprocessor.Stage {
  private static final String TAG = "YUVToRGBStage";

  private static final int NUM_CHANNELS = 3;
  private static final int NUM_BYTES_PER_CHANNEL = 1;

  private final Rect mROI;
  private final RectF mROINorm;

  /**
   * Converts the entire image.
   */
  @SuppressWarnings("unused")
  public YUVToRGBStage() {
    mROI = null;
    mROINorm = null;
  }

  /**
   * Converts the specified roi.
   *
   * @param roi An absolute region of interest.
   */
  @SuppressWarnings("unused")
  public YUVToRGBStage(@NonNull Rect roi) {
    mROI = roi;
    mROINorm = null;
  }

  /**
   * Converts the specified roi.
   *
   * @param roi A normalised region of interest in [0.0, 1.0].
   */
  @SuppressWarnings("unused")
  public YUVToRGBStage(@NonNull RectF roi) {
    mROI = null;
    mROINorm = roi;
  }

  @Override
  public void process(Postprocessor.Data data, Runnable done) {
    if (data.image == null || data.captureParams == null) {
      Log.e(TAG, "Image or capture params are null");
      done.run();
      return;
    }

    int format = data.image.data.getFormat();
    if (format != ImageFormat.YUV_420_888) {
      Log.e(TAG, "Invalid image format: ", format);
      done.run();
      return;
    }

    if (!Utils.isAvailable) {
      Log.w(TAG, "Library not available");
      done.run();
      return;
    }

    new Worker(data, done).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }

  // Not a leak, the outer class will not be collected until this completes anyway.
  @SuppressLint("StaticFieldLeak")
  private class Worker extends AsyncTask<Void, Void, Void> {
    private final Postprocessor.Data mData;
    private final Runnable mDone;

    Worker(Postprocessor.Data data, Runnable done) {
      mData = data;
      mDone = done;
    }

    @Override
    protected Void doInBackground(Void... voids) {
      final Image yuv = mData.image.data;
      final int width = yuv.getWidth();
      final int height = yuv.getHeight();
      final int rotation = mData.captureParams.rotation;

      // Determine the absolute crop region (specified in view coordinates).
      Rect roi = mROI;
      if (mROINorm != null) {
        // The scaling to absolute coordinates must happen with respect to the view orientation.
        int viewWidth = width;
        int viewHeight = height;
        if (rotation == 90 || rotation == 270) {
          //noinspection SuspiciousNameCombination
          viewWidth = height;
          //noinspection SuspiciousNameCombination
          viewHeight = width;
        }
        roi = Utils.absoluteRegion(mROINorm, viewWidth, viewHeight);
      }

      if (roi == null) {
        // Initialise the ROI to the full image (in data coordinates).
        roi = new Rect(0, 0, width, height);
      } else {
        // Transform the ROI into data coordinates.
        roi = Utils.rotateToDataCoords(roi, rotation, width, height);
      }

      final int roiWidth = roi.width();
      final int roiHeight = roi.height();
      if (roiWidth < 0 || roiWidth > width || roiHeight < 0 || roiHeight > height) {
        Log.e(TAG, Locale.ENGLISH, "%s: Cannot crop %s from an image of size %dx%d",
            mData.id, roi.flattenToString(), width, height);
        return null;
      }

      Trace.beginSection("convertYUVToRGB");

      // Allocate the output buffer.
      Trace.beginSection("malloc");
      ByteBuffer rgb = ByteBuffer.allocateDirect(
          roiHeight * roiWidth * NUM_CHANNELS * NUM_BYTES_PER_CHANNEL);
      rgb.order(ByteOrder.nativeOrder());
      Trace.endSection(); // malloc

      // Convert the input region of the YUV image to RGB.
      final Image.Plane[] planes = yuv.getPlanes();
      final Image.Plane yPlane = planes[0];
      final Image.Plane uPlane = planes[1];
      final Image.Plane vPlane = planes[2];
      try {
        Utils.convertYUV420ToRGB888(yPlane.getBuffer(), uPlane.getBuffer(), vPlane.getBuffer(),
            width, height, yPlane.getRowStride(), uPlane.getRowStride(), uPlane.getPixelStride(),
            mData.captureParams.rotation, roi.left, roi.top, roi.right, roi.bottom, rgb);
        mData.buffer = rgb;
      } catch (Exception e) {
        Log.e(TAG, mData.id, ": ", e.getMessage());
        return null;
      } finally {
        Trace.endSection(); // convertYUVToRGB
      }

      Log.d(TAG, mData.id, ": Converted ROI ", roi.toShortString(),
          " and rotated by ", mData.captureParams.rotation);

//      if (rotation == 90 || rotation == 270) {
//        //noinspection SuspiciousNameCombination
//        debugWriteRGBBufferToFile(rgb, roiHeight, roiWidth);
//      } else {
//        debugWriteRGBBufferToFile(rgb, roiWidth, roiHeight);
//      }

      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      finish();
    }

    @Override
    protected void onCancelled() {
      finish();
    }

    private void finish() {
      mDone.run();
    }
  }

  /**
   * Inefficient, only for test purposes.
   */
  @SuppressWarnings({"unused"})
  public static void debugWriteRGBBufferToFile(File out, ByteBuffer buf, int width, int height) {
    Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
    buf.rewind();
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        int colour = 0xFF000000 | ((buf.get() & 0xFF) << 16) | ((buf.get() & 0xFF) << 8)
            | (buf.get() & 0xFF);
        bmp.setPixel(x, y, colour);
      }
    }

    try (OutputStream stream = new BufferedOutputStream(new FileOutputStream(out))) {
      bmp.compress(Bitmap.CompressFormat.JPEG, 93, stream);
    } catch (Exception e) {
      Log.w(TAG, "Failed to write image to file: ", e.getMessage());
    }
    bmp.recycle();
  }
}
