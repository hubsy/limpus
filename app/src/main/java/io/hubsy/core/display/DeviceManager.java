package io.hubsy.core.display;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.List;

import io.hubsy.core.util.Log;
import io.hubsy.core.util.Radio;
import nz.co.cloudstore.airconsole.Airconsole;
import nz.co.cloudstore.airconsole.AirconsoleDevice;
import nz.co.cloudstore.airconsole.AirconsoleMgr;
import nz.co.cloudstore.airconsole.AirconsoleMgrListener;

/**
 * Discovers supported devices.
 */
public class DeviceManager implements AirconsoleMgrListener {
  private static final String TAG = "DeviceManager";

  private static final int BAUD_RATE = 9600;

  private Context mContext;
  private AirconsoleMgr mManager;
  private RS232Session mSession;

  private boolean mShouldBeScanning;
  private BroadcastReceiver mBluetoothStateReceiver;

  public DeviceManager(Context context) {
    mContext = context.getApplicationContext();

    // a futile attempt to disconnect any orphaned connections
    BluetoothManager bluetoothManager =
        (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
    List<BluetoothDevice> connectedDevices =
        bluetoothManager.getConnectedDevices(BluetoothProfile.GATT);
    for (BluetoothDevice connectedDevice : connectedDevices) {
      int deviceType = connectedDevice.getType();
      if (deviceType == BluetoothDevice.DEVICE_TYPE_LE
          || deviceType == BluetoothDevice.DEVICE_TYPE_DUAL) {
        Log.d(TAG, "Device already connected: ", connectedDevice.getName());
        // TODO what now?
      }
    }

    mManager = Airconsole.getAirconsoleMgr();
    mManager.addAirconsoleMgrListener(this);

    listenForBluetoothStateChanges();
    if (Radio.isBluetoothEnabled(context)) {
      startScanning();
    } else {
      Log.d(TAG, "Bluetooth is disabled, turning it on...");
      Radio.turnOnBluetooth(mContext);
    }
  }

  /**
   * Registers a BroadcastReceiver to listen for BT state changes. Starts scanning when BT is turned
   * on and {@link #mShouldBeScanning} is true.
   */
  private void listenForBluetoothStateChanges() {
    mBluetoothStateReceiver = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (action == null || !action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) return;

        int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF);
        if (state == BluetoothAdapter.STATE_ON && mShouldBeScanning) {
          startScanning();
        }
      }
    };
    mContext.registerReceiver(mBluetoothStateReceiver,
        new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
  }

  /**
   * Starts scanning for devices.
   */
  private void startScanning() {
    mShouldBeScanning = true;
    HashMap<String, Object> btleParams = new HashMap<>();
    btleParams.put(AirconsoleMgr.KEY_BLUETOOTH_ADAPTER, BluetoothAdapter.getDefaultAdapter());
    btleParams.put(AirconsoleMgr.KEY_CONTEXT, mContext);
    if (mManager.startScanning(AirconsoleDevice.Transport.TRANSPORT_BTLE, btleParams)) {
      Log.d(TAG, "BTLE scan started");
    } else {
      Log.w(TAG, "Failed to start the BTLE scan");
    }
  }

  /**
   * Stop scanning for devices.
   */
  public void stopScanning() {
    mShouldBeScanning = false;
    if (mManager != null) mManager.stopScanning(AirconsoleDevice.Transport.TRANSPORT_BTLE);
    if (mBluetoothStateReceiver != null) {
      mContext.unregisterReceiver(mBluetoothStateReceiver);
      mBluetoothStateReceiver = null;
    }
  }

  /**
   * Returns the session to the discovered device.
   */
  @Nullable
  public RS232Session getSession() {
    return mSession;
  }

  /**
   * Releases all resources associated with this manager. This should be done when the manager and
   * the session it maintains, if any, will not be used for an extended period of time.
   */
  public void release() {
    stopScanning();

    if (mSession != null && mSession.isReady()) {
      mSession.disconnect();
    }
  }

  /**
   * A callback with a confirmation that a device has been added.
   * <p>
   * Invoked on the main thread.
   */
  @Override
  public void deviceAdded(final AirconsoleDevice device) {
    String addr = device.address();
    if (addr.contains("LEDConsole")) {
      Log.d(TAG, "Connecting to ", addr);
      mSession = new RS232Session(device, BAUD_RATE);
      stopScanning();
    } else {
      Log.d(TAG, "Unknown device detected ", addr);
    }
  }

  @Override
  public void deviceRemoved(final AirconsoleDevice device) {
    Log.d(TAG, "Device removed ", device.address());
  }
}
