package io.hubsy.core.display;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import io.hubsy.core.Constants;
import io.hubsy.core.util.Log;

public class DisplayService extends IntentService {
  private static final String TAG = "DisplayService";

  /**
   * The text to show on line 1 of the display. Type String.
   */
  public static final String EXTRA_DISPLAY_LINE_1 = Constants.NS_PREFIX + ".extra.display_line_1";
  /**
   * The text to show on line 2 of the display. Type String.
   */
  public static final String EXTRA_DISPLAY_LINE_2 = Constants.NS_PREFIX + ".extra.display_line_2";

  // Created the first time the service is used and then kept around for further use.
  @SuppressLint("StaticFieldLeak") // It only stores the app context.
  private static DeviceManager sDeviceManager;

  public DisplayService() {
    super(TAG);
  }

  @Override
  public void onCreate() {
    if (sDeviceManager == null) {
      sDeviceManager = new DeviceManager(this);
    }

    // The worker thread is created by the parent implementation. Call this after creating the
    // device manager to ensure its reference is visible to the worker thread.
    super.onCreate();
  }

  /**
   * Release all resources associated with this service. This should be done when the service will
   * not be used again for a while.
   */
  public static void releaseResources() {
    DeviceManager manager = sDeviceManager;
    if (manager != null) {
      manager.release();
      sDeviceManager = null;
    }
  }

  @Override
  protected void onHandleIntent(@Nullable Intent intent) {
    if (intent == null) return;

    final Bundle extras = intent.getExtras();
    if (extras == null) {
      Log.e(TAG, "The intent extras is null");
      return;
    }

    // Grab the reference, as the member may be set to null at any time from a different thread.
    DeviceManager manager = sDeviceManager;
    if (manager == null) return;

    final RS232Session session = manager.getSession();
    if (session == null || !session.isReady()) {
      Log.d(TAG, "The device session is not ready yet");
      return;
    }

    // Send the first line.
    String line1 = extras.getString(EXTRA_DISPLAY_LINE_1);
    if (line1 != null) session.send(WipathLED.message(line1, 0));

    // And the second line.
    String line2 = extras.getString(EXTRA_DISPLAY_LINE_2);
    if (line2 != null) session.send(WipathLED.message(line2, 1));
  }
}
