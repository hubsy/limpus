package io.hubsy.core.display;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * An async. worker task to send lines of text to a display session.
 */
public class DisplayTask extends AsyncTask<Void, Void, Void> {
  private final RS232Session mSession;
  private final String mLine1;
  private final String mLine2;
  private final long mSequenceNo;

  private static long sLastSequenceNo;

  /**
   * @param session    The session to use.
   * @param line1      The first line of the display. If null, it will not be updated.
   * @param line2      The second line of the display. If null, it will not be updated.
   * @param sequenceNo A sequence number. If > 0, it must also be >= the highest sequence number
   *                   seen so far for the update to proceed. If <= 0, the update always proceeds
   *                   and the sequence counter is reset.
   */
  public DisplayTask(@NonNull RS232Session session, @Nullable String line1,
                     @Nullable String line2, long sequenceNo) {
    mSession = session;
    mLine1 = line1;
    mLine2 = line2;
    mSequenceNo = sequenceNo;
  }

  @Override
  protected Void doInBackground(Void... voids) {
    if (!mSession.isReady()) return null;

    // Synchronise the sequence number check/update and to ensure both lines are updated
    // consistently by the same thread to avoid displaying lines with values from concurrent tasks.
    synchronized (DisplayTask.class) {
      if (mSequenceNo > 0) {
        if (mSequenceNo < sLastSequenceNo) return null; // Drop this update request.
        sLastSequenceNo = mSequenceNo;
      } else {
        sLastSequenceNo = 0;
      }

      if (mLine1 != null) mSession.send(WipathLED.message(mLine1, 0));
      if (mLine2 != null) mSession.send(WipathLED.message(mLine2, 1));
    }

    return null;
  }
}
