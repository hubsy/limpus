package io.hubsy.core.display;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;
import nz.co.cloudstore.airconsole.Airconsole;
import nz.co.cloudstore.airconsole.AirconsoleDevice;
import nz.co.cloudstore.airconsole.AirconsoleSession;
import nz.co.cloudstore.airconsole.AirconsoleSessionListener;

/**
 * A session represents a serial port connection to a device.
 */
public class RS232Session implements AirconsoleSessionListener {
  private static final String TAG = "RS232Session";

  private AirconsoleSession mSession;
  private int mBaudRate;

  private RXThread mRXThread; // To read data from a remote RS232.

  private DataOutputStream mWriter; // To write data to a remote RS232.
  private final Object mWriterLock = new Object(); // Monitor for mWriter.

  private volatile boolean mIsReady;


  /**
   * Creates a new connection to the specified device.
   *
   * @param device   The device to connect to.
   * @param baudRate The connection baud rate.
   */
  RS232Session(AirconsoleDevice device, int baudRate) {
    mBaudRate = baudRate;
    mSession = Airconsole.createSession(device);
    mSession.addSessionListener(this);
    if (!mSession.connect()) {
      Log.w(TAG, "Failed to start the connection attempt");
    }
  }

  // This is the main event called when the connection is ready.
  @Override
  public void sessionDidConnect(AirconsoleSession session) {
//    Log.d(TAG, "Callback invoked on main thread = "
//        + (Thread.currentThread() == Looper.getMainLooper().getThread())
//        + " thread=" + Thread.currentThread().getName());
    Log.d(TAG, "Session connected");
    startTerminal();
  }

  @Override
  public void sessionFailedToConnect(AirconsoleSession session, String errorMessage) {
    Log.w(TAG, "Session connection failed: ", errorMessage);
    mIsReady = false;
  }

  @Override
  public void sessionDidDisconnect(AirconsoleSession session) {
    Log.d(TAG, "Session disconnected");
    mIsReady = false;
  }

  @Override
  public void sessionDidOverflow(AirconsoleSession session) {
    Log.e(TAG, "Error: session buffer overflow");
  }

  @Override
  public void sessionLinePropertiesChanged(AirconsoleSession session) {
//    Log.d(TAG, "Session properties changed");
  }

  @Override
  public void sessionModemStatusChanged(AirconsoleSession session) {
//    Log.d(TAG, "Session modem status changed");
  }

  @Override
  public void sessionSignatureChanged(AirconsoleSession session) {
//    Log.d(TAG, "Session signature changed");
  }

  @Override
  public void sessionDidAuthenticate(AirconsoleSession session) {
    Log.d(TAG, "Session authenticated");
  }

  @Override
  public void sessionDidFailToAuthenticate(AirconsoleSession session) {
    Log.w(TAG, "Session failed to authenticate");
  }

  @Override
  public void sessionBatteryLevelChanged(AirconsoleSession session) {
//    Log.d(TAG, "Session battery level changed");
  }

  @Override
  public void sessionWillConnect(AirconsoleSession airconsoleSession) {
//    Log.d(TAG, "Session connecting");
  }

  /**
   * Indicates if the session is ready to be used.
   */
  public boolean isReady() {
    return mIsReady && mSession.isConnected();
  }

  /**
   * Initialise the connection parameters and start the reader.
   * Must be called before sending any data out.
   */
  private void startTerminal() {
    // Set default params.
    try {
      mSession.setLineParameters(mBaudRate, AirconsoleSession.DataBits.AC_DATABITS_8,
          AirconsoleSession.Parity.AC_PARITY_NONE, AirconsoleSession.StopBits.AC_STOPBITS_1);
    } catch (Exception ex) {
      Log.w(TAG, "Failed to set session params");
      return;
    }

    // Start reading from the serial port.
    cleanUpReader();
    mRXThread = new RXThread();
    mRXThread.start();

    // Initialise the writer.
    try {
      synchronized (mWriterLock) {
        cleanUpWriter();
        mWriter = new DataOutputStream(mSession.getOutputStream());
      }
    } catch (Exception ex) {
      Log.e(TAG, "Failed to initialise the output stream: ", ex.getMessage());
      return;
    }

    mIsReady = true;
  }

  /**
   * Ends the serial port session.
   */
  void disconnect() {
    cleanUpReader();
    cleanUpWriter();
    mSession.disconnect();
    mIsReady = false;
  }

  /**
   * Writes out the bytes to a remote RS232 terminal.
   * <p>
   * This method is thread-safe.
   *
   * @param bytes The data to be sent.
   */
  public void send(byte[] bytes) {
    synchronized (mWriterLock) {
      if (mWriter == null) {
        Log.e(TAG, "Cannot write data, the output stream is null");
        return;
      }

      try {
        mWriter.write(bytes);
        mWriter.flush();
        Log.d(TAG, "Wrote ", bytes.length, " bytes");
      } catch (IOException ex) {
        Log.w(TAG, "Failed to write to the output stream: ", ex.getMessage());
      }
    }
  }

  private void cleanUpReader() {
    if (mRXThread != null) {
      Log.d(TAG, "Stopping the reader thread");
      mRXThread.interrupt();
      mRXThread = null;
    }
  }

  private void cleanUpWriter() {
    synchronized (mWriterLock) {
      if (mWriter != null) {
        try {
          mWriter.close();
        } catch (IOException e) {
          Log.w(TAG, "Failed to close the output stream: ", e.getMessage());
        }
        mWriter = null;
      }
    }
  }

  @Override
  protected void finalize() {
    disconnect();
  }

  /**
   * Reads data from a remote RS232 port and logs it.
   */
  private class RXThread extends Thread {
    @Override
    public void run() {
      // The input stream will return bytes, we need to convert to characters...
      try (InputStreamReader isr = new InputStreamReader(
          mSession.getInputStream(), StandardCharsets.UTF_8)) {
        final int bufferSize = 8;
        char[] buffer = new char[bufferSize];
        int charsRead;

        do {
          charsRead = isr.read(buffer, 0, bufferSize);
          if (charsRead > 0) {
            // Convert the characters we read into a hex string and log it.
            Log.d(TAG, "Read: ", Helpers.charsToHex(buffer, 0, charsRead));
          } else if (charsRead == 0) {
            // Slow down the duty cycle if there is no data.
            try {
              sleep(1000);
            } catch (InterruptedException ex) {
              interrupt();
            }
          }
        } while (!isInterrupted() && (charsRead != -1));
      } catch (IOException e) {
        Log.w(TAG, "Read failed: ", e.getMessage());
      }

      try {
        if (mSession != null) {
          mSession.flush();
        }
      } catch (IOException e) {
        Log.w(TAG, "Failed to flush the session: ", e.getMessage());
      }

      mRXThread = null;
      Log.d(TAG, "RXThread terminated");
    }
  }
}
