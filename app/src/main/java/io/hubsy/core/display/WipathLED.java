package io.hubsy.core.display;

import java.nio.ByteBuffer;

/**
 * WiPath-specific code to talk to their LEDs.
 */
class WipathLED {
  private static final char colorRed = 0xff;
  private static final char colorGreen = 0xff;
  private static final char colorBlue = 0xff;

  /**
   * Returns a properly formatted and encoded message for sending to a WiPath LED sign.
   *
   * @param text        Text to display
   * @param panelNumber 0, 1, 2 - configured per sign
   * @return The encoded data.
   */
  public static byte[] message(String text, int panelNumber) {
    // prepare the constant part of the array
    char[] prefix = {0xa5, 0x68, 0x32, 0x01, 0x7B, 0x01, 0x12, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00,
        0x01, 0x00, 0x00, 0x03, 0x04, 0xff, 0xff, 0x00};
    char[] suffix = {0x0, 0x0, 0x0, 0xae};
    char[] txt = text.toCharArray();
    int charLen = prefix.length + suffix.length + txt.length;
    char[] out = new char[charLen]; // the output container array

    // concatenate them all together
    System.arraycopy(prefix, 0, out, 0, prefix.length);
    System.arraycopy(txt, 0, out, prefix.length, txt.length);
    System.arraycopy(suffix, 0, out, prefix.length + txt.length, suffix.length);

    // store panel number
    out[6] = (char) (out.length - 13);
    out[11] = (char) panelNumber;

    // set colour
    out[18] = colorRed;
    out[19] = colorGreen;
    out[20] = colorBlue;

    // calculate the checksum
    char[] checksum = new char[out.length - 5];
    System.arraycopy(out, 1, checksum, 0, checksum.length);
    int sum = 0;
    for (char c : checksum) {
      sum += c;
    }
    char[] sumArray = new char[2];
    sumArray[0] = (char) (sum & 0xFF);
    sumArray[1] = (char) (sum >>> 8);

    // put checksum, etc in the output
    out[out.length - 3] = sumArray[0];
    out[out.length - 2] = sumArray[1];

    // convert the chars to a byte array
    ByteBuffer byteBuffer = ByteBuffer.allocate(out.length);
    for (int i = 0; i < out.length; i++) {
      byteBuffer.put(i, (byte) (out[i] & 0xff));
    }

    return byteBuffer.array();
  }
}
