package io.hubsy.core.dummy;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

public class ComposeSMSActivity extends Activity {
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    finish(); // Ignore compose message requests.
  }
}
