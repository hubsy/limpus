package io.hubsy.core.dummy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import io.hubsy.core.util.Log;

public class MMSReceiver extends BroadcastReceiver {
  private static final String TAG = "MMSReceiver";

  @Override
  public void onReceive(Context context, Intent intent) {
    Log.d(TAG, "MMS received and ignored");
  }
}
