package io.hubsy.core.dummy;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

public class SendSMSService extends Service {
  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    stopSelf(startId);
    return START_NOT_STICKY;
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }
}
