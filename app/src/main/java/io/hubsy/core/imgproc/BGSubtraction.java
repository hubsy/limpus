package io.hubsy.core.imgproc;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicReference;

import io.hubsy.core.util.Log;

/**
 * Background subtraction.
 * <p>
 * Instances are safe for use from multiple threads, but not for concurrent use, with the exception
 * of {@link #getInstance}, which is thread-safe.
 */
public class BGSubtraction {
  private static final String TAG = "BGSubtraction";

  /**
   * Flag to indicate if the functionality provided by this class is available.
   */
  public static final boolean isAvailable = NativeLib.isAvailable;

  // Existing instance when marked as unused.
  private static final AtomicReference<BGSubtraction> sUnused = new AtomicReference<>();

  // The pointer to the native object managed by this instance. Zero if not initialized.
  private volatile long mNativePtr;

  private final float[] mKernel;
  private final int mKernelWidth;
  private final int mKernelHeight;
  private final String mMaskPath;
  private final String mMaskVersion;

  /**
   * Returns a new or reuses a cached instance of the model. A cached instance is only returned if
   * it was created with the same parameter values.
   * <p>
   * To avoid memory leaks, {@link #release} must be called when the instance will not be used
   * again. To cache it for later reuse, call {@link #releaseForReuse()} instead.
   * <p>
   * Thread-safe.
   *
   * @param kernel       The convolution kernel.
   * @param kernelWidth  The kernel width.
   * @param kernelHeight The kernel height.
   * @param maskPath     The file path to an input mask to apply to images before processing.
   * @param maskVersion  The version string for the mask, used to recognise version changes when
   *                     stored under the same path.
   */
  public static BGSubtraction getInstance(@NonNull float[] kernel, int kernelWidth,
                                          int kernelHeight, @Nullable String maskPath,
                                          @Nullable String maskVersion) throws Exception {
    // Get an unused instance or create and initialise a new one.
    BGSubtraction instance = sUnused.getAndSet(null);

    if (instance != null) {
      // Test if the cached instance was created with the same parameters. Release it if not.
      boolean identical =
          kernelWidth == instance.mKernelWidth && kernelHeight == instance.mKernelHeight
              && instance.mKernel != null && kernel.length == instance.mKernel.length;
      for (int i = 0; i < kernel.length && identical; i++) {
        identical = kernel[i] == instance.mKernel[i];
      }

      identical = identical && ((maskPath != null && maskPath.equals(instance.mMaskPath))
          || (maskPath == null && instance.mMaskPath == null));
      identical = identical && ((maskVersion != null && maskVersion.equals(instance.mMaskVersion))
          || (maskVersion == null && instance.mMaskVersion == null));

      if (!identical) {
        instance.release();
        instance = null;
      }
    }

    if (instance == null) { // Create a new instance.
      instance = new BGSubtraction(kernel, kernelWidth, kernelHeight, maskPath, maskVersion);
      instance.mNativePtr = instance.initModel();
      Log.d(TAG, "New model instance created with: mask=", maskPath);
    }

    return instance;
  }

  private BGSubtraction(float[] kernel, int kernelWidth, int kernelHeight, String maskPath,
                        String maskVersion) {
    mKernel = kernel;
    mKernelWidth = kernelWidth;
    mKernelHeight = kernelHeight;
    mMaskPath = maskPath;
    mMaskVersion = maskVersion;
  }

  @Override
  protected void finalize() {
    if (isAvailable && mNativePtr != 0) {
      // Clean up native resources.
      try {
        release();
      } catch (Exception e) {
        Log.e(TAG, "Failed to release native resources in finalize()");
      }
    }
  }

  /**
   * Marks the instance as unused. Any remaining references to it must no longer be used.
   * <p>
   * Obtain a new instance from {@link BGSubtraction#getInstance} when needed.
   */
  public void releaseForReuse() throws Exception {
    // Cache the instance if the cache is empty, release native resources otherwise.
    if (!sUnused.compareAndSet(null, this)) {
      release();
    }
  }

  private native long initModelNative(float[] kernel, int kernelWidth, int kernelHeight,
                                      String maskPath);

  private long initModel() throws Exception {
    NativeLib.checkState();

    try {
      return initModelNative(mKernel, mKernelWidth, mKernelHeight, mMaskPath);
    } catch (UnsatisfiedLinkError e) {
      throw new FunctionalityUnavailableException("Function initModelNative is not available");
    }
  }

  private native void releaseNative(long ptr);

  /**
   * Releases all resources associated with the instance. Calling any other method on this instance
   * after this method returns will result in a {@link NotInitializedException}.
   */
  public void release() throws Exception {
    NativeLib.checkState();

    if (mNativePtr == 0) {
      Log.d(TAG, "No native pointer to release");
      return;
    }

    try {
      releaseNative(mNativePtr);
      mNativePtr = 0;
    } catch (UnsatisfiedLinkError e) {
      throw new FunctionalityUnavailableException("Function releaseNative is not available");
    }
  }

  private native void updateModelNative(long ptr, ByteBuffer image, int width, int height,
                                        int rowStride, float roiX, float roiY, float roiW,
                                        float roiH, int rotation, boolean invert,
                                        double learningRate, String imageFileOut);

  /**
   * Update the model with the given greyscale image.
   *
   * @param image        The image. Must be a <b>direct</b> ByteBuffer with pixel stride 1.
   * @param width        The image width.
   * @param height       The image height.
   * @param rowStride    The stride between rows, in bytes.
   * @param roiX         The normalised x-coordinate of the crop region.
   * @param roiY         The normalised y-coordinate of the crop region.
   * @param roiW         The normalised width of the crop region.
   * @param roiH         The normalised height of the crop region.
   * @param rotation     The clockwise degrees to rotate the image by to view it upright.
   * @param invert       Invert the image before updating the model.
   * @param learningRate The learning rate to use for the update as defined by the OpenCV
   *                     cv::BackgroundSubtractor interface. Range [0,1], or -1 to have the rate
   *                     automatically chosen.
   * @param imageFileOut If non-null, also write the image to a file at this path.
   */
  public void updateModel(@NonNull ByteBuffer image, int width, int height, int rowStride,
                          float roiX, float roiY, float roiW, float roiH,
                          int rotation, boolean invert, double learningRate,
                          @Nullable String imageFileOut)
      throws Exception {

    NativeLib.checkState();
    checkNativePtr();

    try {
      updateModelNative(mNativePtr, image, width, height, rowStride, roiX, roiY, roiW, roiH,
          rotation, invert, learningRate, imageFileOut);
    } catch (UnsatisfiedLinkError e) {
      throw new FunctionalityUnavailableException("Function updateModelNative is not available");
    }
  }

  private native boolean analyseNative(long ptr, float threshold, boolean cleanFGMask,
                                       String maskFileOut, String cleanedMaskFileOut,
                                       String resultFileOut);

  /**
   * Analyse the model's current foreground mask from the lastest {@link #updateModel} call.
   * <p>
   *
   * @param threshold          The threshold to apply to the output of filtering, range [0,1].
   * @param cleanFGMask        Toggle cleaning of the foreground mask produced by bgsub before
   *                           further analysis.
   * @param maskFileOut        If non-null, also write the foreground mask to an image at this path.
   * @param cleanedMaskFileOut If non-null and cleanFGMask is true, write the cleaned mask to an
   *                           image at this path.
   * @param resultFileOut      If non-null, write the output of processing to an image at this path.
   * @return True if foreground changes are detected, false if not.
   */
  public boolean analyse(float threshold, boolean cleanFGMask, @Nullable String maskFileOut,
                         @Nullable String cleanedMaskFileOut, @Nullable String resultFileOut)
      throws Exception {

    NativeLib.checkState();
    checkNativePtr();

    try {
      return analyseNative(mNativePtr, threshold, cleanFGMask,
          maskFileOut, cleanedMaskFileOut, resultFileOut);
    } catch (UnsatisfiedLinkError e) {
      throw new FunctionalityUnavailableException("Function analyseNative is not available");
    }
  }

  /**
   * Checks that {@link #mNativePtr} is non-zero and throws an exception if it is zero.
   */
  private void checkNativePtr() throws NotInitializedException {
    if (mNativePtr == 0) throw new NotInitializedException("The instance is not initialized");
  }
}

/**
 * The functionality is not available. The instance must first be initialized.
 */
class NotInitializedException extends Exception {
  public NotInitializedException(String message) {
    super(message);
  }
}
