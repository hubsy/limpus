package io.hubsy.core.imgproc;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;

import io.hubsy.core.Settings;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;

/**
 * Converts DNG images to JPG.
 */
public class DngConverter extends AsyncTask<Void, Void, Void> {
  private static final String TAG = "DngConverter";

  private final File mDngSrc;
  private final File mDngDest;
  private final File mJpgTmp;
  private final File mJpgDest;
  private final int mJpgQuality;
  private final float mScaleFactor;
  private final RectF mROI;
  private final int mOrientation;
  private final String mDcrawExtra;
  private final Runnable mCallback;
  private final Handler mHandler;

  /**
   * Converts a raw DNG image to a JPG in a background thread and moves the DNG to a new directory
   * if requested.
   *
   * @param ctx         A context.
   * @param dng         The DNG image to convert.
   * @param dngDestDir  The DNG is moved to this directory if non-null.
   * @param jpgDestDir  The JPG destination directory. The file will have the same name as
   *                    {@code dng} with the extension changed to .jpg.
   * @param jpgQuality  The JPG encoding quality.
   * @param iso         The ISO sensitivity used for the image capture.
   * @param scale       A rescale factor to apply during the conversion. A factor of 1.0 produces a
   *                    JPG of (roughly) the same size as the DNG. Not all factors are supported, the
   *                    closest supported factor is used.
   * @param roi         An optional, normalised crop region in [0.0, 1.0].
   * @param orientation The view rotation with respect to the image pixel data orientation.
   * @param callback    The callback to invoke when done, if any.
   * @param handler     The handler for callback. Must be non-null if callback is provided.
   */
  public DngConverter(@NonNull Context ctx, @NonNull File dng, @Nullable File dngDestDir,
                      @NonNull File jpgDestDir, int jpgQuality, int iso, float scale,
                      @Nullable RectF roi, int orientation,
                      @Nullable Runnable callback, @Nullable Handler handler) {
    final String dngName = dng.getName();

    mDngSrc = dng;
    mDngDest = dngDestDir == null ? dng : new File(dngDestDir, dngName);
    mJpgQuality = jpgQuality;
    mScaleFactor = scale;
    mROI = roi;
    mOrientation = orientation;
    mCallback = callback;
    mHandler = handler;

    if (dngName.endsWith(".dng")) {
      mJpgDest = new File(jpgDestDir, dngName.substring(0, dngName.length() - 3) + "jpg");
    } else {
      Log.w(TAG, "DNG file does not have extension .dng: ", dngName);
      mJpgDest = new File(jpgDestDir, dngName + ".jpg");
    }
    mJpgTmp = new File(Settings.getAppFolderPath() + Settings.APP_DIR_TMP, mJpgDest.getName());

    // Load extra conversion settings.
    final Settings settings = Settings.getInstance(ctx);
    String dcrawArgs = settings.getString(Settings.ConfigKeys.DCRawArgs);
    dcrawArgs = dcrawArgs.isEmpty() ? "" : " " + dcrawArgs;

    // If -b and/or -n are not explicitly specified, then derive suitable values from the image ISO.
    // -b (brightness adjustment)
    final double minB = 1.5; // Min. adjustment.
    final double maxB = 7.5; // Max. adjustment, at ISO 1600.
    double brightness = Math.min(minB + (iso / 1600.) * (maxB - minB), maxB);
    dcrawArgs = dcrawArgs.contains(" -b ") ? dcrawArgs
        : dcrawArgs + " -b " + String.format(Locale.ENGLISH, "%.1f", brightness);
    // -n (denoise)
    dcrawArgs = dcrawArgs.contains(" -n ") ? dcrawArgs : dcrawArgs + " -n " + (int) (iso / 1.5);

    mDcrawExtra = dcrawArgs;
  }

  /**
   * Returns the output path for the JPEG file, which will be available when the task has completed.
   *
   * @return The output path.
   */
  public File getJPEG() {
    return mJpgDest;
  }

  @Override
  protected Void doInBackground(Void... voids) {
    if (isCancelled()) return null;

    // Create the parent directories if necessary.
    if (!createParentDir(mDngDest)) return null;
    if (!createParentDir(mJpgDest)) return null;
    if (!createParentDir(mJpgTmp)) return null;

    // If scale factor is 0.5 then scale during demosaicing. Otherwise generate the full size JPG
    // at high quality and then downsample it.
    int quality = mJpgQuality;
    String scaleArg = "";
    boolean downsample = false;
    if (mScaleFactor > 0.49f && mScaleFactor < 0.51f) { // Approx. 0.5.
      scaleArg = " -h"; // Scale during demosaicing.
    } else if (mScaleFactor > 0.f && mScaleFactor < 1.f) {
      quality = 100;
      downsample = true;
    }

    // Start the external conversion process.
    final String[] cmdArray = new String[]{"dng_to_jpg",
        "-w -c" + scaleArg + mDcrawExtra, mDngSrc.getAbsolutePath(),  // dcraw options and input.
        "-revert -quality " + quality, mJpgTmp.getAbsolutePath()      // cjpeg options and output.
    };
    Log.d(TAG, "Executing: ", Arrays.toString(cmdArray));
    Process process = Helpers.executeCommandAsync(cmdArray);
    int exitval = 1;
    if (process == null) {
      Log.w(TAG, "Failed to create the conversion process");
    } else {
      // Wait for the process to finish.
      try {
        exitval = process.waitFor();
      } catch (InterruptedException e) {
        Log.w(TAG, "Conversion interrupted: ", e.getMessage());
      }
    }

    boolean success = false;
    if (exitval == 0) {
      success = true;
    } else {
      Log.w(TAG, "Conversion failed, the process exited with error code ", exitval);
      // Continue to move the files to their destination directories to avoid leaving them orphaned.
    }

    // Crop and/or resize the JPG if requested and not done by the process above.
    if (success && (downsample || mROI != null)) {
      // Decode the image.
      Bitmap bmp = BitmapFactory.decodeFile(mJpgTmp.getAbsolutePath());
      if (bmp == null) {
        Log.w(TAG, "Failed to decode ", mJpgTmp.getPath());
        success = false;
      }

      // Delete the old file.
      if (success && !mJpgTmp.delete()) {
        Log.w(TAG, "Failed to delete intermediate JPG file ", mJpgTmp.getPath());
        // Continue anyway.
      }

      // Crop then resize given that resizing is more expensive.
      if (success && mROI != null) {
        Rect region = Utils.absoluteRegion(mROI, bmp.getWidth(), bmp.getHeight());
        bmp = Utils.crop(bmp, mOrientation, region);
      }
      if (success && downsample) bmp = resize(bmp);
      if (success) success = writeJPEG(bmp, mJpgTmp);
    }

    // Move the images to their destination.
    if (!mDngSrc.equals(mDngDest) && !mDngSrc.renameTo(mDngDest)) {
      Log.w(TAG, "Failed to move ", mDngSrc.getPath(), " to ", mDngDest.getPath());
    }
    if (!mJpgTmp.equals(mJpgDest) && !mJpgTmp.renameTo(mJpgDest)) {
      Log.w(TAG, "Failed to move ", mJpgTmp.getPath(), " to ", mJpgDest.getPath());
    }

    if (success) Log.d(TAG, "DNG to JPG conversion successful");
    return null;
  }

  /**
   * Creates the parent directory for {@code file}.
   *
   * @return Returns true on success.
   */
  private boolean createParentDir(final File file) {
    File parent = file.getParentFile();
    if (parent != null && !parent.exists() && !parent.mkdirs()) {
      Log.w(TAG, "Failed to create directory ", parent.getPath());
      return false;
    }
    return true;
  }

  /**
   * Resizes the image. Should by executed on the background thread.
   *
   * @param bmp The bitmap to resize.
   * @return True on success, false otherwise.
   */
  private Bitmap resize(Bitmap bmp) {
    int scaledWidth = Math.max(Math.round(bmp.getWidth() * mScaleFactor), 1);
    int scaledHeight = Math.max(Math.round(bmp.getHeight() * mScaleFactor), 1);
//    Log.d(TAG, "Resizing image from ", bmp.getWidth(), "x", bmp.getHeight(),
//        " to ", scaledWidth, "x", scaledHeight);

    return Bitmap.createScaledBitmap(bmp, scaledWidth, scaledHeight, true);
  }

  /**
   * Encodes the image to JPEG and writes it to a file. Should by executed on the background thread.
   *
   * @param bmp The bitmap.
   * @param out The output file.
   * @return True on success, false otherwise.
   */
  private boolean writeJPEG(Bitmap bmp, File out) {
    try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(out))) {
      bmp.compress(Bitmap.CompressFormat.JPEG, mJpgQuality, bos);
    } catch (IOException e) {
      Log.w(TAG, "Failed to write JPEG: ", e.getMessage());
      return false;
    }

    return true;
  }

  @Override
  protected void onPostExecute(Void aVoid) {
    finish();
  }

  @Override
  protected void onCancelled() {
    finish();
  }

  private void finish() {
    if (mCallback != null && mHandler != null) mHandler.post(mCallback);
  }
}
