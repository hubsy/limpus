package io.hubsy.core.imgproc;

import io.hubsy.core.util.Log;

/**
 * Generic helpers for the use of the native library.
 */
abstract class NativeLib {
  private static final String TAG = "NativeLib";

  /**
   * Flag to indicate if the functionality provided by this class is available.
   */
  static final boolean isAvailable;

  static {
    boolean success = false;
    try {
      System.loadLibrary("bgsub");
      success = true;
    } catch (SecurityException | UnsatisfiedLinkError e) {
      Log.e(TAG, "Failed to load library 'bgsub': " + e.getMessage());
    }

    isAvailable = success;
  }

  /**
   * Checks that {@link #isAvailable} is true and throws an exception if it is false.
   */
  static void checkState() throws FunctionalityUnavailableException {
    if (!isAvailable) throw new FunctionalityUnavailableException("The library is not available");
  }

}

/**
 * This exception is thrown when a required functionality is currently not available.
 */
class FunctionalityUnavailableException extends Exception {
  public FunctionalityUnavailableException(String message) {
    super(message);
  }
}
