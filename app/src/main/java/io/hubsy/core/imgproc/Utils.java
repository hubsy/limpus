package io.hubsy.core.imgproc;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.nio.ByteBuffer;

public class Utils {
  /**
   * Flag to indicate if the functionality provided by this class is available.
   */
  public static final boolean isAvailable = NativeLib.isAvailable;

  private native static ByteBuffer encodeJPEGNative(
      @NonNull ByteBuffer image, int width, int height, int channels, boolean isRGB, boolean toGrey,
      int rowStride, int rotation, int left, int top, int right, int bottom, int quality);

  /**
   * Encodes a ByteBuffer as a JPEG image.
   *
   * @param image     The image. Must be a <b>direct</b> ByteBuffer with pixel stride 1.
   * @param width     The image width.
   * @param height    The image height.
   * @param channels  The number of channels in the image buffer (1 or 3).
   * @param isRGB     For 3-channel images, set this to true if the image is in RGB channel order
   *                  rather than BGR.
   * @param toGrey    For 3-channel images, set this to true to convert the output to greyscale.
   * @param rowStride The stride between rows, in bytes.
   * @param rotation  The clockwise degrees to rotate the image by to view it upright.
   * @param left      The left edge of the region of interest.
   * @param top       The top edge of the roi.
   * @param right     The right edge of the roi.
   * @param bottom    The bottom edge of the roi.
   * @param quality   The encoding quality.
   * @return The encoded image as a direct ByteBuffer. <b>The underlying memory of the buffer must
   * be released with {@link #freeBuffer(ByteBuffer)}.</b>
   */
  public static ByteBuffer encodeJPEG(@NonNull ByteBuffer image, int width, int height,
                                      int channels, boolean isRGB, boolean toGrey,
                                      int rowStride, int rotation, int left, int top, int right,
                                      int bottom, int quality)
      throws Exception {
    NativeLib.checkState();
    try {
      return encodeJPEGNative(image, width, height, channels, isRGB, toGrey, rowStride, rotation,
          left, top, right, bottom, quality);
    } catch (UnsatisfiedLinkError e) {
      throw new FunctionalityUnavailableException("Function encodeJPEG is not available");
    }
  }

  private native static void freeBufferNative(ByteBuffer buf);

  /**
   * Frees the given direct ByteBuffer. The underlying memory must have been allocated by native
   * code.
   */
  public static void freeBuffer(@NonNull ByteBuffer buf) throws Exception {
    NativeLib.checkState();
    try {
      freeBufferNative(buf);
    } catch (UnsatisfiedLinkError e) {
      throw new FunctionalityUnavailableException("Function freeBuffer is not available");
    }
  }

  private native static void convertYUV420ToRGB888Native(
      ByteBuffer yPlane, ByteBuffer uPlane, ByteBuffer vPlane, int width, int height,
      int yRowStride, int uvRowStride, int uvPixelStride, int rotation,
      int left, int top, int right, int bottom, ByteBuffer out);

  /**
   * Converts (a region of) a YUV 4:2:0 image to an RGB image.
   * <p>
   * The input image is expected to have a plane of 8 bit Y samples followed by separate u and v
   * planes with arbitrary row and column strides, containing 8 bit 2x2 subsampled chroma samples.
   * <p>
   * The crop region is specified in the coordinate frame of the image data (before rotation into
   * view coordinates). All offsets are relative to the top-left corner of the image.
   *
   * @param yPlane        The y plane data.
   * @param uPlane        The u plane data.
   * @param vPlane        The v plane data.
   * @param width         The image width.
   * @param height        The image height.
   * @param yRowStride    The y plane row stride.
   * @param uvRowStride   The u and v plane row strides.
   * @param uvPixelStride The u and v plane pixel strides.
   * @param rotation      The clockwise degrees to rotate the image by to view it upright.
   * @param left          The left offset of the crop region.
   * @param top           The top offset of the crop region.
   * @param right         The right offset of the crop region.
   * @param bottom        The bottom offset of the crop region.
   * @param out           The RGB data output buffer. Its capacity must be no less than
   *                      (bottom-top) * (right-left) * 3.
   */
  public static void convertYUV420ToRGB888(
      @NonNull ByteBuffer yPlane, @NonNull ByteBuffer uPlane, @NonNull ByteBuffer vPlane,
      int width, int height, int yRowStride, int uvRowStride, int uvPixelStride, int rotation,
      int left, int top, int right, int bottom, @NonNull ByteBuffer out) throws Exception {
    NativeLib.checkState();
    try {
      convertYUV420ToRGB888Native(yPlane, uPlane, vPlane, width, height, yRowStride, uvRowStride,
          uvPixelStride, rotation, left, top, right, bottom, out);
    } catch (UnsatisfiedLinkError e) {
      throw new FunctionalityUnavailableException(
          "Function convertYUV420ToRGB888 is not available");
    }
  }

  private native static void convertRGB888ToGreyscaleNative(
      ByteBuffer rgb, int width, int height, ByteBuffer out);

  /**
   * Converts an RGB image to greyscale.
   * <p>
   * All {@link ByteBuffer ByteBuffers} must be <b>direct</b>.
   *
   * @param rgb    The 8-bit 3-channel RGB input image.
   * @param width  The image width.
   * @param height The image height.
   * @param out    Output buffer for the 8-bit single-channel greyscale image. Its capacity must be
   *               no less than height * width.
   */
  public static void convertRGB888ToGreyscale(@NonNull ByteBuffer rgb, int width, int height,
                                              @NonNull ByteBuffer out) throws Exception {
    NativeLib.checkState();
    try {
      convertRGB888ToGreyscaleNative(rgb, width, height, out);
    } catch (UnsatisfiedLinkError e) {
      throw new FunctionalityUnavailableException(
          "Function convertRGB888ToGreyscale is not available");
    }
  }

  private native static void convertImageToFloatNative(
      ByteBuffer image, int width, int height, int channels, float scale, float add,
      ByteBuffer out);

  /**
   * Converts an image in uint8 to float32, rescaling and shifting its values in the process.
   * <p>
   * The result for each pixel channel value will be out[i] = float(image[i]) * scale + add.
   * <p>
   * All {@link ByteBuffer ByteBuffers} must be <b>direct</b>.
   *
   * @param image    The 8-bit input image.
   * @param width    The image width.
   * @param height   The image height.
   * @param channels The number of channels in the input.
   * @param scale    The scale factor.
   * @param add      The offset to add after scaling.
   * @param out      Output buffer for the 32-bit float image. Its capacity must be no less than
   *                 height * width * channels * 4.
   */
  public static void convertImageToFloat(@NonNull ByteBuffer image, int width, int height,
                                         int channels, float scale, float add,
                                         @NonNull ByteBuffer out) throws Exception {
    NativeLib.checkState();
    try {
      convertImageToFloatNative(image, width, height, channels, scale, add, out);
    } catch (UnsatisfiedLinkError e) {
      throw new FunctionalityUnavailableException("Function convertImageToFloat is not available");
    }
  }

  private native static void calcOpticalFlowNative(
      ByteBuffer frame0, ByteBuffer frame1, int width, int height, int quality, ByteBuffer out);

  /**
   * Calculates the optical flow from frame0 to frame1 and writes the motion vectors to the
   * output buffer.
   * <p>
   * All {@link ByteBuffer ByteBuffers} must be <b>direct</b>.
   *
   * @param frame0  The reference frame as an 8-bit single-channel greyscale image.
   * @param frame1  The next frame, of the same type and size as frame0.
   * @param width   The frame width.
   * @param height  The frame height.
   * @param quality Select the quality of the result, where higher values represent higher quality
   *                results and an increase in the computational cost. Defaults to 1 if the value is
   *                outside [0,2].
   * @param out     Output buffer for the flow vectors from frame0 to frame1. Its capacity must be
   *                no less than height * width * 2 * sizeof(float32). The vector x-coordinates are
   *                stored at [y,x,0] and the y-coordinates at [y,x,1], where (y,x) are the
   *                coordinates of the target pixel in frame1.
   */
  public static void calcOpticalFlow(@NonNull ByteBuffer frame0, @NonNull ByteBuffer frame1, int width,
                                     int height, int quality, @NonNull ByteBuffer out)
      throws Exception {
    NativeLib.checkState();
    try {
      calcOpticalFlowNative(frame0, frame1, width, height, quality, out);
    } catch (UnsatisfiedLinkError e) {
      throw new FunctionalityUnavailableException("Function calcOpticalFlow is not available");
    }
  }

  private native static float[] calcMeanFlowVectorNative(
      ByteBuffer flow, int width, int height, int left, int top, int right, int bottom);

  /**
   * Calculates the mean vector for the region of interest into the optical flow vector array.
   *
   * @param flow   The flow vectors as returned by {@link #calcOpticalFlow}.
   * @param width  The width of the corresponding image.
   * @param height The height of the corresponding image.
   * @param left   The left offset of the region of interest.
   * @param top    The top offset of the region of interest.
   * @param right  The right offset of the region of interest.
   * @param bottom The bottom offset of the region of interest.
   * @return A float[2] array with the mean vector (x,y) or null on error.
   */
  @Nullable
  public static float[] calcMeanFlowVector(@NonNull ByteBuffer flow, int width, int height,
                                           int left, int top, int right, int bottom)
      throws Exception {
    NativeLib.checkState();
    try {
      return calcMeanFlowVectorNative(flow, width, height, left, top, right, bottom);
    } catch (UnsatisfiedLinkError e) {
      throw new FunctionalityUnavailableException("Function calcMeanFlowVector is not available");
    }
  }

  private native static void resizeNative(
      ByteBuffer image, int width, int height, int channels,
      int left, int top, int right, int bottom, int outWidth, int outHeight,
      boolean qualityOverSpeed, ByteBuffer out);

  /**
   * Resizes image buffers.
   *
   * @param image            The input image buffer.
   * @param width            The input width.
   * @param height           The input height.
   * @param channels         The number of input (and output) channels.
   * @param left             The left offset of the crop region.
   * @param top              The top offset of the crop region.
   * @param right            The right offset of the crop region.
   * @param bottom           The bottom offset of the crop region.
   * @param outWidth         The output width.
   * @param outHeight        The output height.
   * @param qualityOverSpeed Prefer higher quality over speed when true.
   * @param out              The output buffer. Its capacity must be no less than
   *                         (outHeight * outWidth * 3).
   */
  public static void resize(@NonNull ByteBuffer image, int width, int height, int channels,
                            int left, int top, int right, int bottom, int outWidth, int outHeight,
                            boolean qualityOverSpeed, @NonNull ByteBuffer out) throws Exception {
    NativeLib.checkState();
    try {
      resizeNative(image, width, height, channels, left, top, right, bottom, outWidth, outHeight,
          qualityOverSpeed, out);
    } catch (UnsatisfiedLinkError e) {
      throw new FunctionalityUnavailableException("Function resize is not available");
    }
  }

  /**
   * Crops a region-of-interest from the source bitmap.
   *
   * @param src         The source bitmap.
   * @param orientation The view rotation with respect to the image pixel data orientation.
   * @param roi         The crop region.
   * @return The cropped bitmap. If the roi equals the src dimensions, then src is returned.
   */
  public static Bitmap crop(Bitmap src, int orientation, Rect roi) {
    final int srcWidth = src.getWidth();
    final int srcHeight = src.getHeight();

    int x, x0, y, y0, w, w0, h, h0;
    x0 = roi.left;
    y0 = roi.top;
    w = w0 = roi.width();
    h = h0 = roi.height();

    // Adjust the specified crop region for the image pixel data orientation if the view
    // orientation is rotated with reference to the former and ensure it is within the image.
    switch (orientation) {
      case 90:
        x = Math.min(srcWidth - 1, y0);
        y = Math.max(0, srcHeight - x0 - w0);
        w = h0;
        h = w0;
        break;
      case 180:
        x = Math.max(0, srcWidth - x0 - w0);
        y = Math.max(0, srcHeight - y0 - h0);
        break;
      case 270:
        x = Math.max(0, srcWidth - y0 - h0);
        y = Math.min(srcHeight - 1, x0);
        w = h0;
        h = w0;
        break;
      default:
        x = Math.min(srcWidth - 1, x0);
        y = Math.min(srcHeight - 1, y0);
        break;
    }
    w = Math.min(srcWidth - x, w);
    h = Math.min(srcHeight - y, h);
//      Log.d(TAG, "CROP " + x + ", " + y + ", " + w + ", " + h + " (" + orientation + ") "
//          + srcWidth + ", " + srcHeight);

    // Clip the crop region to the image size.
    if (x == 0 && y == 0 && w == srcWidth && h == srcHeight) {
      return src; // Nothing to crop.
    }

    return Bitmap.createBitmap(src, x, y, w, h);
  }

  /**
   * Rotates a region-of-interest from view coordinates (which show the image upright) to the
   * data coordinate frame it is stored in.
   *
   * @param roi      The region-of-interest in view coordinates.
   * @param rotation The clockwise rotation, in degrees, from data coordinates to view coordinates.
   *                 Must be a positive integer multiple of 90.
   * @param width    The width of the image in data coordinates.
   * @param height   The height of the image in data coordinates.
   * @return The adjusted roi.
   */
  @SuppressWarnings("SuspiciousNameCombination")
  @NonNull
  public static Rect rotateToDataCoords(@NonNull Rect roi, int rotation, int width, int height) {
    if (rotation >= 360) rotation %= 360;

    switch (rotation) {
      case 90:
        return new Rect(roi.top, height - roi.right, roi.bottom, height - roi.left);
      case 180:
        return new Rect(width - roi.right, height - roi.bottom, width - roi.left, height - roi.top);
      case 270:
        return new Rect(width - roi.bottom, roi.left, width - roi.top, roi.right);
      default:
        return roi;
    }
  }

  /**
   * Returns the absolute region given a normalised region and a width and height.
   *
   * @param roi The normalised region-of-interest.
   * @param w   The width.
   * @param h   The height.
   * @return The absolute region, or null if the normalised rect is null.
   */
  @Nullable
  public static Rect absoluteRegion(@Nullable RectF roi, int w, int h) {
    if (roi == null) return null;

    return new Rect(Math.round(roi.left * w), Math.round(roi.top * h),
        Math.round(roi.right * w), Math.round(roi.bottom * h));
  }
}
