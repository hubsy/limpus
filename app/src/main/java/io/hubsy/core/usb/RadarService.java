package io.hubsy.core.usb;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.HexDump;
import com.hoho.android.usbserial.util.SerialInputOutputManager;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.hubsy.core.Constants;
import io.hubsy.core.Settings;
import io.hubsy.core.Settings.ConfigKeys;
import io.hubsy.core.util.Helpers;
import io.hubsy.core.util.Log;

public class RadarService extends Service {
  private static final String TAG = "RadarService";

  /**
   * Event action used when a new speed reading is available.
   */
  public static final String ACTION_SPEED_READING = Constants.NS_PREFIX + ".action.speed_reading";

  /**
   * Speed reading as formatted text. Type String.
   */
  public static final String EXTRA_SPEED_TEXT = Constants.NS_PREFIX + ".extra.speed_text";
  /**
   * Speed reading. Type int.
   */
  public static final String EXTRA_SPEED = Constants.NS_PREFIX + ".extra.speed";
  /**
   * Event timestamp. Type long.
   */
  public static final String EXTRA_TIMESTAMP = Constants.NS_PREFIX + ".extra.timestamp";

  /**
   * A base64-URL-encoded list of newline-separated commands to be sent to the radar. Type String.
   */
  public static final String EXTRA_CMD_LIST = Constants.NS_PREFIX + ".extra.cmd.list";

  private static volatile boolean sIsRunning;

  // A buffer for the current speed reading, which may be received in multiple transmissions.
  private StringBuilder mSpeedData;
  private long mUSBInputTimestamp; // Timestamp of the last reading.
  // Drop incomplete reading if it's older than this constant (milliseconds).
  private static final long USB_SINGLE_READING_TIMEOUT = 100;

  // Regex for speed readings. May change between radar modules.
  private static final Pattern sSpeedPattern = Pattern.compile("(\\+\\d{1,3}(\\.\\d)?)");

  // USB connection params.
  private int mUSBBaudRate;
  private int mUSBDataBits;
  private int mUSBStopBits;
  private int mUSBParity;

  private UsbSerialPort mPort;
  private SerialInputOutputManager mSerialIoManager;

  private Settings mSettings;
  private Handler mMainHandler;
  private ExecutorService mExecutor; // Must be a single worker thread.

  private long mMinBroadcastDelay = -1; // The min delay between broadcasts.
  private int mMinSpeed; // The min speed reading for broadcasts.
  private long mLastSettingsUpdate; // The timestamp (elapsedRealtime) of the last settings update.
  private long mLastBroadcast; // The timestamp (elapsedRealtime) of the last broadcast.

  // NOTE: Callback methods are invoked on the executor thread.
  private final SerialInputOutputManager.Listener mListener =
      new SerialInputOutputManager.Listener() {

        @Override
        public void onRunError(Exception e) {
          Log.w(TAG, "Device communication failed: ", e.getMessage());
          mMainHandler.post(() -> updateServiceState(getApplicationContext(), true, null));
        }

        @Override
        public void onNewData(final byte[] data) {
          updateReceivedData(data);
        }
      };


  /**
   * Returns true if the service is running.
   */
  public static boolean isRunning() {
    return sIsRunning;
  }

  /**
   * (Re)starts or stops the radar service depending on the value of {@link ConfigKeys#EnableRadar}
   * and its current state.
   *
   * @param context      A context.
   * @param forceRestart Forces a restart of the service if it is enabled and already running.
   * @param cmd          Optional commands to be sent to the radar. See {@link #EXTRA_CMD_LIST}.
   */
  public static void updateServiceState(@NonNull Context context, boolean forceRestart,
                                        @Nullable String cmd) {
    final Settings settings = Settings.getInstance(context);

    final Intent intent = new Intent(context, RadarService.class);
    if (settings.getBool(ConfigKeys.EnableRadar)) {
      // (Re)start the radar service. It will fetch the new settings.
      final boolean isRunning = isRunning();
      if (isRunning && forceRestart) Log.d(TAG, "Restarting the service");
      else if (!isRunning) Log.d(TAG, "Starting the service");
      else Log.d(TAG, "Updating the service state");

      // Try to stop it independent of the assumed state.
      if (forceRestart) context.stopService(intent);

      // Add commands to be sent to the radar if any.
      if (cmd != null && !cmd.isEmpty()) intent.putExtra(EXTRA_CMD_LIST, cmd);
      context.startService(intent);
    } else {
      if (isRunning()) Log.d(TAG, "Stopping the service");
      context.stopService(intent); // Try to stop it independent of the assumed state.
    }
  }

  @Override
  public void onCreate() {
    mMainHandler = new Handler(Looper.getMainLooper());
    mExecutor = Executors.newSingleThreadExecutor();
    mSettings = Settings.getInstance(this);

    // Read the config values.
    mUSBBaudRate = mSettings.getInt(ConfigKeys.USBBaudRate);
    mUSBDataBits = mSettings.getInt(ConfigKeys.USBDataBits);
    mUSBStopBits = mSettings.getInt(ConfigKeys.USBStopBits);

    // Parity.
    final String parity = mSettings.getString(ConfigKeys.USBParity);
    switch (parity) {
      case "none":
        mUSBParity = UsbSerialPort.PARITY_NONE;
        break;
      case "odd":
        mUSBParity = UsbSerialPort.PARITY_ODD;
        break;
      case "even":
        mUSBParity = UsbSerialPort.PARITY_EVEN;
        break;
      case "mark":
        mUSBParity = UsbSerialPort.PARITY_MARK;
        break;
      case "space":
        mUSBParity = UsbSerialPort.PARITY_SPACE;
        break;
      default:
        Log.w(TAG, "Invalid setting: USBParity = \"", parity, "\", falling back to \"none\"");
        mUSBParity = UsbSerialPort.PARITY_NONE;
        break;
    }
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    final String cmd = intent == null ? null : intent.getStringExtra(EXTRA_CMD_LIST);

    if (isRunning()) {
      if (cmd != null) sendCommands(cmd);
      else Log.d(TAG, "Service already started");
      return START_STICKY;
    }
    Log.d(TAG, "Service starting");

    final UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

    List<UsbSerialDriver> availableDrivers =
        UsbSerialProber.getDefaultProber().findAllDrivers(usbManager);
    if (availableDrivers.isEmpty()) {
      Log.w(TAG, "Failed to find USB drivers");
      stopSelf(startId);
      return START_NOT_STICKY;
    }

    // Open a connection to the first available driver.
    UsbSerialDriver driver = availableDrivers.get(0);

    // Read some data! Most have just one port (port 0).
    mPort = driver.getPorts().get(0);
    final UsbDevice device = driver.getDevice();

    Log.d(TAG, "Vendor ID ", HexDump.toHexString((short) device.getVendorId()),
        ", Prod ID ", HexDump.toHexString((short) device.getProductId()),
        ", Driver ", driver.getClass().getSimpleName());

    UsbDeviceConnection connection = null;
    try {
      connection = usbManager.openDevice(driver.getDevice());
    } catch (Exception e) {
      //do nothing - we check later anyway
    }
    if (connection == null) {
      Log.w(TAG, "No USB devices connected");
      stopSelf(startId);
      return START_NOT_STICKY;
    }

    try {
      mPort.open(connection);
      mPort.setParameters(mUSBBaudRate, mUSBDataBits, mUSBStopBits, mUSBParity);
    } catch (Exception e) {
      Log.w(TAG, "Cannot connect to device");
      stopSelf(startId);
      return START_NOT_STICKY;
    }

    startIoManager();

    sIsRunning = true;
    if (cmd != null) sendCommands(cmd);

    return START_STICKY; // If we get killed, after returning from here, restart.
  }

  /**
   * Sends the given command sequence to the USB device.
   *
   * @param base64Cmds Base64-URL-encoded commands.
   */
  @SuppressWarnings("StatementWithEmptyBody")
  private void sendCommands(@NonNull String base64Cmds) {
    if (mSerialIoManager == null) {
      Log.e(TAG, "Cannot send commands: the serial IO manager is null");
      return;
    }
    Log.d(TAG, "Decoding USB commands");

    // Decode.
    byte[] decoded;
    try {
      decoded = Base64.decode(base64Cmds, Base64.URL_SAFE);
    } catch (IllegalArgumentException e) {
      Log.w(TAG, Locale.ENGLISH, "Failed to decode command string \"%s\": %s",
          base64Cmds, e.getMessage());
      return;
    }

    // Split commands on line breaks.
    final ArrayList<byte[]> cmds = new ArrayList<>();
    final int bufSize = 64;
    final ByteBuffer buf = ByteBuffer.allocate(bufSize);
    boolean fakeNewline = false; // Pretend there is a newline at the end of data if there isn't.
    for (int i = 0; i < decoded.length || fakeNewline; i++) {
      final byte cmdByte = fakeNewline ? (byte) '\r' : decoded[i];
      final int cmdInt = cmdByte & 0xFF; // Unsigned data, do not propagate the sign bit.

      if (cmdByte == '\r' || cmdByte == '\n') {
        fakeNewline = false;

        // Discard empty commands, either due to an empty line or a sequence of line break chars.
        if (buf.position() == 0) continue;

        // Append \r\n to terminate the command in a consistent manner and add it to the cmd list.
        buf.put((byte) '\r').put((byte) '\n');
        byte[] cmd = new byte[buf.position()];
        buf.flip();
        buf.get(cmd);
        buf.clear();
        cmds.add(cmd);
      } else if (cmdInt == 0x09 /*tab*/ || cmdInt == 0x0B /*vtab*/ || cmdInt == 0x0C /*form feed*/
          || cmdInt == 0x20 /*space*/ || cmdInt == 0x85 /*next line*/
          || cmdInt == 0xA0 /*non-breaking space*/) {
        // Ignore whitespace.
      } else if (buf.position() < bufSize - 2) {
        buf.put(cmdByte);
      } else {
        Log.w(TAG, "USBCommand #", (cmds.size() + 1),
            " is too long, none of the commands will be applied");
        return;
      }

      if ((i + 1) == decoded.length && cmdByte != '\r' && cmdByte != '\n') {
        fakeNewline = true; // This is the last byte, but not a newline char. Pretend there is one.
      }
    }

    final Runnable send = new Runnable() {
      int i = 0;

      @Override
      public void run() {
        // Decode and send the command.
        final byte[] cmd = cmds.get(i);
        final String ascii = Helpers.bytesToPrintableASCII(cmd, 0, cmd.length - 2, null);
        Log.i(TAG, Locale.ENGLISH, "Sending command %d/%d: 0x%s -> %s\\r\\n",
            i + 1, cmds.size(), Helpers.bytesToHex(cmd), ascii);
        mSerialIoManager.writeAsync(cmd);

        // Schedule the next command to be executed after a short delay.
        i++;
        if (i < cmds.size()) mMainHandler.postDelayed(this, 200);
      }
    };

    if (cmds.size() > 0) send.run();
    else Log.d(TAG, "No valid commands to send");
  }

  @Override
  public IBinder onBind(Intent intent) {
    return null; // Disable bindings.
  }

  @Override
  public void onDestroy() {
    sIsRunning = false;
    stopIoManager();
    if (mPort != null) {
      try {
        mPort.close();
      } catch (IOException e) {
        Log.w(TAG, "Error while closing the USB serial port: ", e.getMessage());
      }
      mPort = null;
    }

    mExecutor.shutdown();
    mExecutor = null;
  }

  private void stopIoManager() {
    if (mSerialIoManager != null) {
      Log.d(TAG, "Stopping the IO Manager...");
      mSerialIoManager.stop();
      mSerialIoManager = null;
    }
  }

  private void startIoManager() {
    if (mPort != null) {
      Log.d(TAG, "Starting the IO Manager...");
      mSerialIoManager = new SerialInputOutputManager(mPort, mListener);
      mExecutor.submit(mSerialIoManager);
    }
  }

  /**
   * NOTE: Invoked on a worker thread.
   */
  private void updateReceivedData(byte[] data) {
    if (mSpeedData == null) mSpeedData = new StringBuilder();

//      Log.v(TAG, "Read data len = ", data.length);
//      Log.v(TAG, "Hex: ", Helpers.bytesToHex(data));
//      Log.v(TAG, "UTF-8: ", new String(data, StandardCharsets.UTF_8));

    // Check the duration since the previous transmission.
    final long ts = SystemClock.elapsedRealtime();
    if (mUSBInputTimestamp == 0) mUSBInputTimestamp = ts; // Initialise on the 1st run.
    final long tsDiff = ts - mUSBInputTimestamp;
    if (tsDiff > USB_SINGLE_READING_TIMEOUT) {
      mSpeedData.setLength(0); // Message timeout exceeded, reset the buffer.
    }
    mUSBInputTimestamp = ts;

    // Parse CR-terminated data.
    int firstUnloggedByte = 0;
    int byteIdx = -1;
    for (final byte usbByte : data) {
      byteIdx++;

      switch (usbByte) {
        case '+':
        case '-': {
          // Start of a new speed reading.
          mSpeedData.setLength(0);
          mSpeedData.append((char) usbByte);
          break;
        }

        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case '.': {
          // Add the value to the current buffer for speed readings if one has been started. The
          // upper limit is merely there to avoid unexpected buffer growth in case of invalid data.
          int len = mSpeedData.length();
          if (len > 0 && len < 16) mSpeedData.append((char) usbByte);
          break;
        }

        case '\r': { // CR - end of message.
          // Check if this is a complete speed reading.
          final Matcher matcher = sSpeedPattern.matcher(mSpeedData);
          if (matcher.matches()) {
            final String raw = matcher.group(1);
            final long uptime = SystemClock.elapsedRealtime();

            // Get the settings for the first time or when the update interval has passed.
            if (mMinBroadcastDelay < 0 || uptime >= (mLastSettingsUpdate + 60000)) {
              mLastSettingsUpdate = uptime;
              mMinBroadcastDelay = mSettings.getInt(ConfigKeys.RadarMaxFrequency);
              mMinSpeed = mSettings.getInt(ConfigKeys.RadarMinSpeed);
            }

            // Parse the speed value.
            int speed;
            try {
              speed = Math.round(Float.parseFloat(raw));
            } catch (NumberFormatException e) {
              Log.e(TAG, "Failed to parse speed: ", e.getMessage());
              mSpeedData.setLength(0);
              continue;
            }

            Log.i(TAG, "Speed=", speed);
            firstUnloggedByte = byteIdx + 1;

            // Send out a broadcast if the conditions are satisfied.
            if (speed >= mMinSpeed && uptime >= (mLastBroadcast + mMinBroadcastDelay)) {
              mLastBroadcast = uptime;
              Intent broadcast = new Intent(ACTION_SPEED_READING);
              broadcast.putExtra(EXTRA_SPEED, speed);
              broadcast.putExtra(EXTRA_SPEED_TEXT, raw);
              broadcast.putExtra(EXTRA_TIMESTAMP, System.currentTimeMillis());
              sendBroadcast(broadcast);
            }
          } else { // Speed value pattern not matched.
            // Log unless the message is "000<CR>" (no sign), which is received when the radar no
            // longer measures any motion.
            final int from = firstUnloggedByte;
            final int to = byteIdx + 1;
            final int charsToLog = to - from;
            firstUnloggedByte = to;
            if (charsToLog != 4 || data[from] != '0' || data[from + 1] != '0'
                || data[from + 2] != '0') { // from+3 == '\r' if charsToLog == 4.
              logUnknownData(data, from, to, mSpeedData.toString());
            }
          }

          mSpeedData.setLength(0); // Reset the buffer.
          break;
        } // CR

        default: { // Not a valid char for a speed reading. It may be a response to a command.
          mSpeedData.setLength(0);
          break;
        }
      }
    }

    // Log any unmatched data, except a lonely LF, which may optionally follow the end-of-
    // transmission CR. Other data may be from a partial speed reading or a command response.
    final int from = firstUnloggedByte;
    final int to = byteIdx + 1;
    final int charsToLog = to - from;
    if (charsToLog > 0 && (charsToLog != 1 || data[from] != '\n')) {
      logUnknownData(data, from, to, mSpeedData.toString());
    }
  }

  private void logUnknownData(byte[] data, int from, int to, String buf) {
    final String ascii = Helpers.bytesToPrintableASCII(data, from, to, null);
    Log.i(TAG, Locale.ENGLISH, "Unknown data: 0x%s -> %s (buffer = \"%s\")",
        Helpers.bytesToHex(Arrays.copyOfRange(data, from, to)), ascii, buf);
  }
}
