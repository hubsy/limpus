package io.hubsy.core.util;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.Telephony;
import android.support.annotation.NonNull;

/**
 * A background task which clears all SMS messages from the {@link Telephony.Sms} provider.
 */
public class ClearSMSTask extends AsyncTask<Void, Void, Void> {
  private static final String TAG = "ClearSMSTask";

  private final ContentResolver mResolver;

  public ClearSMSTask(@NonNull ContentResolver resolver) {
    mResolver = resolver;
  }

  @Override
  protected Void doInBackground(Void... voids) {
    // Find out how many messages are stored and delete them all.
    Cursor cursor = mResolver.query(Telephony.Sms.CONTENT_URI,
        new String[]{Telephony.Sms.THREAD_ID}, null, null, null);
    if (cursor != null) {
      int numMessages = cursor.getCount();
      cursor.close();

      int numDeleted = 0;
      if (numMessages > 0) {
        numDeleted = mResolver.delete(Telephony.Sms.CONTENT_URI, null, null);
      }

      if (numMessages > 0) {
        Log.d(TAG, "Deleted ", numDeleted, " of ", numMessages, " messages");
      } else {
        Log.d(TAG, "No messages to delete");
      }
    } else {
      Log.e(TAG, "Query failed, cursor is null");
    }

    return null;
  }
}
