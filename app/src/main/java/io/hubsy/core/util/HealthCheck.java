package io.hubsy.core.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;

/**
 * A singleton class to record the application's health status.
 */
public class HealthCheck {
  private static final String TAG = "HealthCheck";
  private static final String SHARED_PREFS_NAME = "health";

  /**
   * The keys for timestamp based health check records. The corresponding values are no absolute
   * timestamps, but relative to system boot and therefore reset every time the system starts.
   */
  public enum Timestamp { // Ensure that the enum values are unique across all enums of this class.
    /**
     * A Bluetooth LE device found notification has been sent.
     */
    BleBroadcast,
    /**
     * A Bluetooth LE scan has started.
     */
    BleScan,
    /**
     * The Bluetooth adapter has been turned on successfully.
     */
    BluetoothOn,
    /**
     * The Bluetooth adapter has been requested to turn on.
     */
    BluetoothTurnOn,
    /**
     * A request to take a photo has been issued.
     */
    PhotoRequest,
    /**
     * A photo was successfully taken.
     */
    PhotoSuccess,
    /**
     * The task scheduler has been triggered.
     */
    ScheduledTask,
  }

  /**
   * Flags used to indicate certain conditions. <b>All flags are initialised to false on start.</b>
   */
  public enum Flag { // Ensure that the enum values are unique across all enums of this class.
    /**
     * Indicates to an external service that the application should be forced to restart.
     */
    RestartNeeded,
  }

  private static volatile HealthCheck sSingleton;

  /**
   * Returns the singleton instance of this class.
   *
   * @param context Context used for initialisation if this is the first time getInstance is called.
   * @return The instance.
   */
  @NonNull
  public static HealthCheck getInstance(@NonNull final Context context) {
    if (sSingleton == null) {
      synchronized (HealthCheck.class) {
        if (sSingleton == null) { // Check again in case another thread has entered the mutex first.
          // Clear all values and initialise the singleton.
          final SharedPreferences data = context.getApplicationContext().getSharedPreferences(
              SHARED_PREFS_NAME, Context.MODE_PRIVATE);
          data.edit().clear().apply();
          sSingleton = new HealthCheck(data);
        }
      }
      Log.d(TAG, "Initialized");
    }
    return sSingleton;
  }

  /**
   * Returns the singleton instance of this class.
   * <p>
   * <b>This function must not be called until {@link #getInstance(Context)} has been called at
   * least once to initialise the singleton (e.g. from
   * {@link io.hubsy.core.SecurityCamApplication})!</b>
   *
   * @return The instance.
   */
  @NonNull
  public static HealthCheck getInstance() {
    if (sSingleton == null) return new DummyHealthCheck();
    return sSingleton;
  }

  private final SharedPreferences healthData;

  private final Thread mainThread;
  private final Handler mainHandler;

  private HealthCheck(@NonNull SharedPreferences data) {
    healthData = data;

    Looper mainLooper = Looper.getMainLooper();
    mainThread = mainLooper.getThread();
    mainHandler = new Handler(mainLooper);
  }

  /**
   * Only to be used as super constructor of {@link DummyHealthCheck}.
   */
  private HealthCheck() {
    healthData = null;
    mainThread = null;
    mainHandler = null;
  }

  /**
   * Updates the timestamp for `key` with the elapsed time since boot in milliseconds.
   *
   * @param key One of the timestamp based health check keys.
   */
  public void updateTimestamp(@NonNull final Timestamp key) {
    update(key.name(), SystemClock.elapsedRealtime(), false);
  }

  /**
   * Enables/disables flag `key`.
   *
   * @param key         The flag key.
   * @param enabled     The flag value, true (enabled) or false (disabled).
   * @param forceCommit The value is updated on the calling thread rather than the main
   *                    thread and synchronously commited to persistent storage. This should
   *                    only be set to true if the main thread may no longer be alive.
   */
  public void setFlag(@NonNull final Flag key, final boolean enabled, final boolean forceCommit) {
    Log.d(TAG, "Setting flag ", key.name(), " to ", enabled);
    update(key.name(), enabled ? 1 : 0, forceCommit);
  }

  /**
   * Upate the value of entry key. The update is performed on the main thread, either synchronously
   * if the current thread already is the main thread or asynchronously if it isn't. This avoids the
   * risk of updates being discarded during concurrent calls.
   * <p>
   * Note that synchronous update does <b>not</b> mean that it has been written to peristent storage
   * when the call returns.
   * <p>
   * This behaviour can be overwritten with forceCommit to force the update on the current thread
   * and thus support writing even if the main thread has exited. This will block until the changes
   * have been committed to persistent storage.
   *
   * @param key         The key.
   * @param value       The value.
   * @param forceCommit Force using the current thread with a synchronous commit to disk.
   */
  private void update(@NonNull final String key, final long value, final boolean forceCommit) {
    if (Thread.currentThread() == mainThread || forceCommit) {
      // Already on main thread or forced to current thread.
      if (forceCommit) { // Commit to disk.
        boolean success = false;
        for (int i = 0; !success && i < 3; i++) {
          success = healthData.edit().putLong(key, value).commit();
        }
        if (!success) Log.w(TAG, "Failed to commit update to persistent storage");
      } else { // Apply changes normally.
        healthData.edit().putLong(key, value).apply();
      }
    } else { // Enqueue on main handler.
      mainHandler.post(() -> healthData.edit().putLong(key, value).apply());
    }
  }

  /**
   * Used when {@link #getInstance()} is called when the singleton is not initialised.
   */
  private static class DummyHealthCheck extends HealthCheck {
    private static final String TAG = "DummyHealthCheck";

    @Override
    public void updateTimestamp(@NonNull final Timestamp key) {
      Log.w(TAG, "Not initialized, cannot update timestamp for: ", key.name());
    }

    public void setFlag(@NonNull final Flag key, final boolean enabled, final boolean forceCommit) {
      Log.w(TAG, "Not initialized, cannot set flag: ", key.name());
    }
  }

}
