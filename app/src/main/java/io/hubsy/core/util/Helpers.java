package io.hubsy.core.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.StatFs;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.hubsy.core.AwsClient;
import io.hubsy.core.LocationUpdateReceiver;
import io.hubsy.core.Settings;
import io.hubsy.core.Settings.ConfigKeys;

/**
 * Misc reusable static functions
 */
public class Helpers {

  private static final String TAG = "Helpers";

  private static PhoneStateListener sPhoneStateListener;
  private static List<CellInfo> sLatestCellInfo;
  private static SignalStrength sSignalStrength;
  private static long sSignalStrengthTimestamp; // Nanoseconds since boot.

  /**
   * Creates path in the external storage directory.
   *
   * @param path Relative path to create.
   * @return The resulting File or null on error.
   */
  @Nullable
  public static File createDirIfNotExists(String path) {
    File dir = new File(Environment.getExternalStorageDirectory(), path);
    if (!dir.exists()) {
      if (!dir.mkdirs()) {
        Log.e(TAG, "Cannot create folder ", dir.getPath());
        return null;
      }
    }
    return dir;
  }

  /**
   * Log the current battery level.
   */
  private static void logBatteryLevel(Context context, boolean detailed) {
    StringBuilder out = new StringBuilder();

    //Get easy and essential details
    BatteryManager bm = (BatteryManager) context.getSystemService(Context.BATTERY_SERVICE);
    Long batCurrentNow = bm.getLongProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW);
    Long batCurrentAvg = bm.getLongProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_AVERAGE);
    Long batCapacity = bm.getLongProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

    out.append("Curr now: ");
    out.append(batCurrentNow);
    out.append(", Curr avg: ");
    out.append(batCurrentAvg);
    out.append(", Capacity: ");
    out.append(batCapacity);

    //Get everything else if requested
    if (detailed) {
      IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
      Intent batteryInfo = context.registerReceiver(null, filter);

      int batteryLevel, batteryScale;
      int phonePlugged, phoneStatus, phoneHealth, phoneTemp, phoneVoltage;
      if (batteryInfo != null) {
        batteryLevel = batteryInfo.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        batteryScale = batteryInfo.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        phonePlugged = batteryInfo.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        phoneStatus = batteryInfo.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        phoneHealth = batteryInfo.getIntExtra(BatteryManager.EXTRA_HEALTH, -1);
        phoneTemp = batteryInfo.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1);
        phoneVoltage = batteryInfo.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);
      } else {
        Log.w(TAG, "Battery info not available");
        batteryLevel = batteryScale = 0;
        phonePlugged = phoneStatus = phoneHealth = phoneTemp = phoneVoltage = 0;
      }

      //Power source
      out.append(", Source: ");
      switch (phonePlugged) {
        case BatteryManager.BATTERY_PLUGGED_AC:
          out.append("AC");
          break;
        case BatteryManager.BATTERY_PLUGGED_USB:
          out.append("USB");
          break;
        case BatteryManager.BATTERY_PLUGGED_WIRELESS:
          out.append("Qi");
          break;
        default:
          out.append(phonePlugged);
      }

      //Level
      out.append(", Level: ");
      if (batteryScale != 0) out.append((int) (batteryLevel / (float) batteryScale * 100));
      out.append("%");

      //Status
      out.append(", Status: ");
      switch (phoneStatus) {
        case BatteryManager.BATTERY_STATUS_CHARGING:
          out.append("in");
          break;
        case BatteryManager.BATTERY_STATUS_DISCHARGING:
          out.append("out");
          break;
        case BatteryManager.BATTERY_STATUS_FULL:
          out.append("full");
          break;
        case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
          out.append("no charge");
          break;
        case BatteryManager.BATTERY_STATUS_UNKNOWN:
          out.append("unknown");
          break;
        default:
          out.append(phoneStatus);
      }

      //Health
      out.append(", Health: ");
      switch (phoneHealth) {
        case BatteryManager.BATTERY_HEALTH_COLD:
          out.append("cold");
          break;
        case BatteryManager.BATTERY_HEALTH_DEAD:
          out.append("dead");
          break;
        case BatteryManager.BATTERY_HEALTH_GOOD:
          out.append("good");
          break;
        case BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE:
          out.append("over-volt");
          break;
        case BatteryManager.BATTERY_HEALTH_OVERHEAT:
          out.append("over-heat");
          break;
        case BatteryManager.BATTERY_HEALTH_UNKNOWN:
          out.append("unknown");
          break;
        case BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE:
          out.append("failure");
          break;
        default:
          out.append(phoneHealth);
      }

      //Temp, voltage, amperage, etc
      out.append(", Temp: ");
      out.append(phoneTemp);
      out.append(", Volt: ");
      out.append(phoneVoltage);
    }

    Log.i("BATTERY", out.toString());
  }

  /**
   * Returns the latest cell data for all observed cells, or null if no data is available.
   * <p>
   * Depending on the phone's capabilities, a {@link PhoneStateListener} is registered in an attempt
   * to obtain data for future calls if this is the first call since the app started.
   */
  @Nullable
  @SuppressLint("MissingPermission")
  private static List<CellInfo> getCellInfo(final TelephonyManager tm) {
    // Obtain the latest cell info from getAllCellInfo or fallback to using a listener for state
    // changes on older devices.
    List<CellInfo> cells = tm.getAllCellInfo();
    if (cells == null) {
      // Register a listener to get cell info updates if that has not been done yet.
      if (sPhoneStateListener == null) {
        Log.d(TAG, "Falling back on phone state listener for cell info updates");
        synchronized (Helpers.class) {
          if (sPhoneStateListener == null) {
            sPhoneStateListener = new PhoneStateListener() {
              @Override
              public void onCellInfoChanged(List<CellInfo> cellInfo) {
                sLatestCellInfo = cellInfo;
              }

              @Override
              public void onSignalStrengthsChanged(SignalStrength signalStrength) {
                sSignalStrength = signalStrength;
                sSignalStrengthTimestamp = SystemClock.elapsedRealtimeNanos();
              }
            };
            tm.listen(sPhoneStateListener,
                PhoneStateListener.LISTEN_CELL_INFO | PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
          }
        }
      }

      // Use the latest data from the listener if any.
      cells = sLatestCellInfo;
    }

    return cells;
  }

  /**
   * Logs information about available cells, including cell signal strength.
   *
   * @param context  The context to use.
   * @param detailed Print much more information if true, basic information only if false.
   */
  private static void logCellInfo(final Context context, final boolean detailed) {
    final String TAG = "CELL";
    final TelephonyManager tm = (TelephonyManager)
        context.getSystemService(Context.TELEPHONY_SERVICE);

    // Log general info about the network.
    StringBuilder msg = new StringBuilder();
    msg.append("Operator: ");
    String operator = tm.getNetworkOperatorName();
    msg.append(operator == null || operator.isEmpty() ? "Unknown" : operator);
    msg.append("; type=").append(getDataNetworkType(tm));
    msg.append(", state=").append(getDataNetworkState(tm));

    // Get the cell data.
    List<CellInfo> cells = getCellInfo(tm);
    if (cells == null) {
      cells = new ArrayList<>(0); // Empty cell list, we use sSignalStrength below.
    }

    // If the cell list is empty, append what data we have from the state change listener.
    final SignalStrength ss = sSignalStrength;
    if (cells.isEmpty() && ss != null) {
      if (ss.isGsm()) {
        int asu = ss.getGsmSignalStrength();
        int dbm = 2 * asu - 113;
        if (asu == 99) {
          msg.append("; GSM signal strength N/A");
        } else {
          msg.append("; GSM signal ").append(dbm).append(" dBm");
          msg.append(", asu ").append(asu);
        }
      } else {
        msg.append("; CDMA signal ").append(ss.getCdmaDbm()).append(" dBm");
        msg.append("; EVDO signal ").append(ss.getEvdoDbm()).append(" dBm");
      }
      msg.append(", age ");
      msg.append((SystemClock.elapsedRealtimeNanos() - sSignalStrengthTimestamp) / (1000000000L));
      msg.append(" sec");
    } else {
      msg.append(" (observed cells ").append(cells.size()).append(")");
    }
    Log.i(TAG, msg.toString());

    // Log cell info for all observed cells (detailed) or only the registered cell (!detailed).
    int count = 0;
    for (CellInfo info : cells) {
      count++;
      if (!info.isRegistered()) continue;

      msg = new StringBuilder();
      StringBuilder extra = detailed ? new StringBuilder() : null;

      msg.append(count).append(":");
      msg.append(" registered=").append(info.isRegistered());

      int level = 0;
      int asu = 0;
      int dbm = 0;
      if (info instanceof CellInfoCdma) {
        msg.append("; CDMA");
        CellSignalStrengthCdma signal = ((CellInfoCdma) info).getCellSignalStrength();
        level = signal.getLevel();
        asu = signal.getAsuLevel();
        dbm = signal.getDbm();

        if (detailed) {
          extra.append("; CDMA signal ").append(signal.getCdmaDbm()).append(" dBm");
          extra.append(", Ec/Io ").append(signal.getCdmaEcio()).append(" dB*10");
          extra.append(", level ").append(signal.getCdmaLevel());

          extra.append("; EVDO signal ").append(signal.getEvdoDbm()).append(" dBm");
          extra.append(", Ec/Io ").append(signal.getEvdoEcio()).append(" dB*10");
          extra.append(", level ").append(signal.getEvdoLevel());
          extra.append(", SNR ").append(signal.getEvdoSnr());

          CellIdentityCdma id = ((CellInfoCdma) info).getCellIdentity();
          extra.append("; base station id ").append(valueOrNA(id.getBasestationId()));
          extra.append(", network id ").append(valueOrNA(id.getNetworkId()));
          extra.append(", system id ").append(valueOrNA(id.getSystemId()));
        }
      } else if (info instanceof CellInfoGsm) {
        msg.append("; GSM");
        CellSignalStrengthGsm signal = ((CellInfoGsm) info).getCellSignalStrength();
        level = signal.getLevel();
        asu = signal.getAsuLevel();
        dbm = signal.getDbm();

        if (detailed) {
          if (Build.VERSION.SDK_INT >= 26) {
            extra.append("; timing advance ").append(valueOrNA(signal.getTimingAdvance()));
          }

          CellIdentityGsm id = ((CellInfoGsm) info).getCellIdentity();
          extra.append("; cell id ").append(valueOrNA(id.getCid()));
          extra.append(", LAC ").append(valueOrNA(id.getLac()));
          extra.append(", MCC ").append(valueOrNA(id.getMcc()));
          extra.append(", MNC ").append(valueOrNA(id.getMnc()));
          extra.append(", ARFCN ").append(valueOrNA(id.getArfcn()));
          extra.append(", BSIC ").append(valueOrNA(id.getBsic()));
        }
      } else if (info instanceof CellInfoLte) {
        msg.append("; LTE");
        CellSignalStrengthLte signal = ((CellInfoLte) info).getCellSignalStrength();
        level = signal.getLevel();
        asu = signal.getAsuLevel();
        dbm = signal.getDbm();

        if (detailed) {
          extra.append("; timing advance ").append(valueOrNA(signal.getTimingAdvance()));

          if (Build.VERSION.SDK_INT >= 26) {
            extra.append(", CQI ").append(valueOrNA(signal.getCqi()));
            extra.append(", RSRP ").append(valueOrNA(signal.getRsrp()));
            extra.append(", RSRQ ").append(valueOrNA(signal.getRsrq()));
            extra.append(", RSSNR ").append(valueOrNA(signal.getRssnr()));
          }

          CellIdentityLte id = ((CellInfoLte) info).getCellIdentity();
          extra.append("; cell id ").append(valueOrNA(id.getCi()));
          extra.append(", EARFCN ").append(valueOrNA(id.getEarfcn()));
          extra.append(", MCC ").append(valueOrNA(id.getMcc()));
          extra.append(", MNC ").append(valueOrNA(id.getMnc()));
          extra.append(", PCI ").append(valueOrNA(id.getPci()));
          extra.append(", TAC ").append(valueOrNA(id.getTac()));
        }
      } else if (info instanceof CellInfoWcdma) {
        msg.append("; WCDMA");
        CellSignalStrengthWcdma signal = ((CellInfoWcdma) info).getCellSignalStrength();
        level = signal.getLevel();
        asu = signal.getAsuLevel();
        dbm = signal.getDbm();

        if (detailed) {
          CellIdentityWcdma id = ((CellInfoWcdma) info).getCellIdentity();
          extra.append("; cell id ").append(valueOrNA(id.getCid()));
          extra.append(", LAC ").append(valueOrNA(id.getLac()));
          extra.append(", MCC ").append(valueOrNA(id.getMcc()));
          extra.append(", MNC ").append(valueOrNA(id.getMnc()));
          extra.append(", PSC ").append(valueOrNA(id.getPsc()));
          extra.append(", UARFCN ").append(valueOrNA(id.getUarfcn()));
        }
      } else {
        msg.append("; Unknown");
      }

      msg.append(" signal ").append(dbm).append(" dBm");
      msg.append(", asu ").append(asu == 99 ? "N/A" : asu);
      msg.append(", level ").append(level);
      msg.append(", age ");
      msg.append((SystemClock.elapsedRealtimeNanos() - info.getTimeStamp()) / (1000000000L));
      msg.append(" sec");
      if (extra != null) msg.append(extra);

      Log.i(TAG, msg.toString());
    }
  }

  /**
   * Returns int v as a string or "N/A" if v == Integer.MAX_VALUE.
   */
  private static String valueOrNA(int v) {
    return v == Integer.MAX_VALUE ? "N/A" : Integer.toString(v);
  }

  /**
   * Returns a string indicating the radio technology (network type) currently in use on the device
   * for data transmission.
   * <p>
   * See {@link TelephonyManager#getDataNetworkType()}.
   */
  @SuppressLint("MissingPermission")
  private static String getDataNetworkType(@NonNull TelephonyManager tm) {
    switch (tm.getDataNetworkType()) {
      case TelephonyManager.NETWORK_TYPE_1xRTT:
        return "1xRTT";
      case TelephonyManager.NETWORK_TYPE_CDMA:
        return "CDMA";
      case TelephonyManager.NETWORK_TYPE_EDGE:
        return "EDGE";
      case TelephonyManager.NETWORK_TYPE_EHRPD:
        return "eHRPD";
      case TelephonyManager.NETWORK_TYPE_EVDO_0:
        return "EVDO_0";
      case TelephonyManager.NETWORK_TYPE_EVDO_A:
        return "EVDO_A";
      case TelephonyManager.NETWORK_TYPE_EVDO_B:
        return "EVDO_B";
      case TelephonyManager.NETWORK_TYPE_GPRS:
        return "GPRS";
      case TelephonyManager.NETWORK_TYPE_GSM:
        return "GSM";
      case TelephonyManager.NETWORK_TYPE_HSDPA:
        return "HSDPA";
      case TelephonyManager.NETWORK_TYPE_HSPA:
        return "HSPA";
      case TelephonyManager.NETWORK_TYPE_HSPAP:
        return "HSPA+";
      case TelephonyManager.NETWORK_TYPE_HSUPA:
        return "HSUPA";
      case TelephonyManager.NETWORK_TYPE_IDEN:
        return "iDen";
      case TelephonyManager.NETWORK_TYPE_IWLAN:
        return "IWLAN";
      case TelephonyManager.NETWORK_TYPE_LTE:
        return "LTE";
      case TelephonyManager.NETWORK_TYPE_TD_SCDMA:
        return "TD_SCDMA";
      case TelephonyManager.NETWORK_TYPE_UMTS:
        return "UMTS";
      default:
        return "N/A";
    }
  }

  /**
   * Returns a string indicating the current data connection state.
   * <p>
   * See {@link TelephonyManager#getDataState()}.
   */
  private static String getDataNetworkState(@NonNull TelephonyManager tm) {
    switch (tm.getDataState()) {
      case TelephonyManager.DATA_DISCONNECTED:
        return "disconnected";
      case TelephonyManager.DATA_CONNECTING:
        return "connecting";
      case TelephonyManager.DATA_CONNECTED:
        return "connected";
      case TelephonyManager.DATA_SUSPENDED:
        return "suspended";
      default:
        return "N/A";
    }
  }

  /**
   * Convenience function to log system state, rotate logs and initiate file transfers.
   *
   * @param context         A context.
   * @param checkForUpdates If true, also check for updates to be downloaded.
   */
  public static void prepareAndShipLogs(Context context, boolean checkForUpdates) {
    logSystemState(context, true);
    Log.rotate();

    // Log rotation is async, so wait a moment before triggering file transfers.
    final Context appContext = context.getApplicationContext();
    new Handler().postDelayed(() -> AwsClient.uploadFiles(appContext, checkForUpdates), 1000);
  }

  /**
   * Logs the system state, including battery level, cell info, free memory and software version.
   *
   * @param context  The context to use.
   * @param detailed More verbose logging if true.
   */
  private static void logSystemState(Context context, boolean detailed) {
    Log.i("APP", getAppVersionString(context));
    logBatteryLevel(context, detailed);
    logCellInfo(context, detailed);
    Log.i(TAG, "Free memory: ", getFreeMemoryInBytes());

    Location loc = LocationUpdateReceiver.getLatestLocation();
    if (loc != null) Log.i("LocationUpdater", LocationUpdateReceiver.fmt(loc));

    if (detailed) {
      Settings s = Settings.getInstance(context);
      Log.i("INSTALLED", ConfigKeys.AwsAppName, "=", s.getVersion(ConfigKeys.AwsAppName),
          " ; ", ConfigKeys.AwsHealthMonitorName, "=", s.getVersion(ConfigKeys.AwsHealthMonitorName),
          " ; ", ConfigKeys.AwsImageMaskName, "=", s.getVersion(ConfigKeys.AwsImageMaskName),
          " ; ", ConfigKeys.AwsToolsName, "=", s.getVersion(ConfigKeys.AwsToolsName),
          " ; ", ConfigKeys.AwsUpdaterName, "=", s.getVersion(ConfigKeys.AwsUpdaterName),
          " ; ", ConfigKeys.AwsUpgradeName, "=", s.getVersion(ConfigKeys.AwsUpgradeName));
    }
  }

  /**
   * Returns the app's package name and version as a string.
   */
  public static String getAppVersionString(Context context) {
    final String pkgName = context.getPackageName();
    String msg = pkgName + " build #";
    try {
      msg += context.getPackageManager().getPackageInfo(pkgName, 0).versionCode;
    } catch (Exception ex) {
      Log.e(TAG, "Failed to get the app version code: ", ex.getMessage());
    }
    return msg;
  }

  /**
   * Strips the value from {@link Settings#getAppFolderPath()} from the beginning of path and
   * returns the result.
   */
  private static String cleanPath(@NonNull String path) {
    final String appRoot = Settings.getAppFolderPath();
    return path.startsWith(appRoot) ? path.substring(appRoot.length()) : path;
  }

  /**
   * Move the source file to one of the predefined destination directories defined in Settings while
   * keeping the file name.
   *
   * @param fileFrom The source file.
   * @param dirTo    The destination directory, relative to the app's root data directory.
   * @return The destination file or null on error.
   */
  @Nullable
  public static File moveFile(@NonNull File fileFrom, @NonNull String dirTo) {
    return moveFile(fileFrom, dirTo, fileFrom.getName());
  }

  /**
   * Move the source file to one of the predefined destination directories defined in Settings,
   * renaming it to newFileName in the process.
   *
   * @param fileFrom    The source file.
   * @param dirTo       The destination directory, relative to the app's root data directory.
   * @param newFileName The destination file name.
   * @return The destination file or null on error.
   */
  @Nullable
  public static File moveFile(@NonNull File fileFrom, @NonNull String dirTo,
                              @NonNull String newFileName) {
    final File destDir = Helpers.createDirIfNotExists(Settings.APP_DIR + "/" + dirTo);
    if (destDir == null) return null;

    final File fileTo = new File(destDir, newFileName);

    // Check if the source and dest files are the same.
    if (fileFrom.equals(fileTo)) {
      Log.d(TAG, "Nothing to move, source and dest are identical: ", cleanPath(fileTo.getPath()));
      return fileTo;
    }

    try {
      move(fileFrom, fileTo);
      Log.d(TAG, "File moved to: ", cleanPath(fileTo.getPath()));
    } catch (IOException ex) {
      Log.w(TAG, ex.getMessage());
      return null;
    }

    return fileTo;
  }

  /**
   * Deletes a file.
   *
   * @param file The file to delete.
   * @return True if the file was successfully deleted.
   */
  @SuppressWarnings("UnusedReturnValue")
  public static boolean deleteFile(@NonNull final File file) {
    try {
      delete(file);
      Log.d(TAG, "File deleted: ", cleanPath(file.getPath()));
    } catch (IOException ex) {
      Log.w(TAG, ex.getMessage());
      return false;
    }
    return true;
  }

  /**
   * Memory-map a file in the application assets. Returns null on error.
   */
  @Nullable
  public static MappedByteBuffer mapFile(AssetManager assets, String filename) {
    try (
        AssetFileDescriptor aFD = assets.openFd(filename);
        FileInputStream stream = new FileInputStream(aFD.getFileDescriptor())
    ) {
      long offset = aFD.getStartOffset();
      long length = aFD.getDeclaredLength();
      FileChannel channel = stream.getChannel(); // Closed with the stream.
      // The mapped buffer remains valid when the channel is closed.
      MappedByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, offset, length);
      Log.d(TAG, "Memory-mapped asset file: ", filename);
      return buf;
    } catch (IOException e) {
      Log.w(TAG, "Failed to map file \"", filename, "\" from assets: ", e.getMessage());
    }
    return null;
  }

  /**
   * Reads a UTF-8 character file from the application assets into a list of lines.
   *
   * @param assets   The asset manager.
   * @param filename The asset file name.
   * @return The lines read from the file. Empty on error or if the file is empty.
   */
  @NonNull
  public static List<String> readLines(AssetManager assets, String filename) {
    ArrayList<String> lines = new ArrayList<>();
    try (InputStream stream = assets.open(filename);
         BufferedReader br = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))) {
      String line;
      while ((line = br.readLine()) != null) {
        lines.add(line);
      }
    } catch (IOException e) {
      Log.w(TAG, "Failed to read file \"", filename, "\" from assets: ", e.getMessage());
      lines = new ArrayList<>(0);
    }

    return lines;
  }

  /**
   * Execute the specified command without waiting for it to finish.
   *
   * @param cmdarray Array containing the command to call and its arguments.
   * @return The new subprocess.
   */
  @Nullable
  public static Process executeCommandAsync(String... cmdarray) {
    if (cmdarray == null || cmdarray.length == 0) return null;
    try {
      return Runtime.getRuntime().exec(cmdarray);
    } catch (IOException ex) {
      Log.w(TAG, "Command failed: ", ex.getMessage());
    }
    return null;
  }

  /**
   * Reboots the phone.
   *
   * @param context A context.
   * @param delay   A delay in millis before the reboot is performed.
   */
  public static void reboot(final Context context, int delay) {
    final PowerManager pm =
        (PowerManager) context.getApplicationContext().getSystemService(Context.POWER_SERVICE);

    // Delay the reboot as requested.
    new Handler().postDelayed(() -> pm.reboot("Hubsy reboot"), delay);
  }

  /**
   * Get amount of free memory on the filesystem available to the app.
   *
   * @return Free memory in bytes.
   */
  public static long getFreeMemoryInBytes() {
    StatFs statFs = new StatFs(Settings.getAppFolderPath());
    return statFs.getAvailableBytes();
  }

  private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

  /**
   * Returns the byte array as a hex string.
   * <p>
   * Credits: https://stackoverflow.com/a/9855338/5739829
   *
   * @param bytes The byte array.
   * @return The string of hex values, or empty string if bytes is null.
   */
  @NonNull
  public static String bytesToHex(@Nullable byte[] bytes) {
    if (bytes == null) return "";
    char[] hexChars = new char[bytes.length * 2];
    for (int j = 0; j < bytes.length; j++) {
      int v = bytes[j] & 0xFF;
      hexChars[j * 2] = hexArray[v >>> 4];
      hexChars[j * 2 + 1] = hexArray[v & 0x0F];
    }
    return new String(hexChars);
  }

  /**
   * Converts a char array with non-printable characters into a readable hex string. Only the lower
   * 8-bits of each char are converted, the higher byte is discarded.
   *
   * @param chars  The char array.
   * @param offset An offset into chars where to start reading.
   * @param count  The number of characters to convert.
   * @return The string of hex values, or empty string if chars is null.
   */
  public static String charsToHex(@Nullable char[] chars, int offset, int count) {
    if (chars == null || (offset + count) > chars.length) return "";
    char[] hexChars = new char[count * 2];
    for (int j = 0; j < count; j++) {
      int v = chars[offset + j] & 0xFF;
      hexChars[j * 2] = hexArray[v >>> 4];
      hexChars[j * 2 + 1] = hexArray[v & 0x0F];
    }
    return new String(hexChars);
  }

  /**
   * Converts printable ASCII values in the byte array into the equivalent ASCII characters.
   * <p>
   * This covers all printable characters and space, but no control or other whitespace characters;
   * ASCII 0x20 to 0x7E (inclusive). The substitute String is inserted for all other byte values.
   *
   * @param bytes      The byte array.
   * @param offset     An offset into bytes where to start reading.
   * @param count      The number of bytes to convert.
   * @param substitute The substitution sequence.
   * @return The printable ASCII string.
   */
  public static String bytesToPrintableASCII(@Nullable byte[] bytes, int offset, int count,
                                             @Nullable String substitute) {
    if (bytes == null || (offset + count) > bytes.length) return "";

    if (substitute != null && substitute.isEmpty()) substitute = null;

    final StringBuilder buf = new StringBuilder(count);
    for (int i = 0; i < count; i++) {
      final byte b = bytes[offset + i];
      final int v = b & 0xFF;
      if (v >= 0x20 && v <= 0x7E) {
        buf.append((char) b);
      } else if (substitute != null) {
        buf.append(substitute);
      }
    }
    return buf.toString();
  }

  /**
   * Returns true if the current time is within the specified range.
   *
   * @param from The start time of the range in the format HH:MM.
   * @param to   The end time of the range in the format HH:MM (inclusive).
   * @return True if within range or if unable to parse; false if outside of range.
   */
  public static boolean isTimeInRange(@Nullable String from, @Nullable String to) {
    Calendar now = Calendar.getInstance();
    Pair<Calendar, Calendar> range = parseTimeRange(from, to, true, now);
    if (range == null) return true;

    // Check if inside the range.
    return !(now.before(range.first) || now.after(range.second));
  }

  /**
   * Parses two times as described by {@link #parseTime} and sets their dates to work correctly
   * across day boundaries, e.g. 8pm - 6am.
   * <p>
   * If `to` is before or equal to `from`, either subtract one day from `from` or add one to `to`,
   * depending on the current time `now`, to correct for intervals that cross midnight.
   * <p>
   * If `to` is treated as inclusive, then the seconds and milliseconds of the end time of the
   * range will be set to 59 and 999, respectively. Otherwise they are set to 0.
   *
   * @param from          The start time of the range.
   * @param to            The end time of the range.
   * @param toIsInclusive Whether the `to` time is treated as inclusive (true) or exclusive (false).
   * @param now           The date-time to use to decide which date to adjust when `from` < `to`.
   * @return The parsed times with dates set so the time range is less than 24 hours and at least
   * one of the dates is today. The returned pair is null on error, but its values are never null.
   */
  @Nullable
  public static Pair<Calendar, Calendar> parseTimeRange(@Nullable String from, @Nullable String to,
                                                        boolean toIsInclusive,
                                                        @Nullable final Calendar now) {
    if (from == null || from.isEmpty() || to == null || to.isEmpty() || now == null) return null;

    // Get calendars for the respective time on the current day.
    final Calendar timeFrom = parseTime(from, true);
    final Calendar timeTo = parseTime(to, !toIsInclusive);
    if (timeFrom == null || timeTo == null) return null;

    // If timeTo is before or equal to timeFrom, either subtract one day from timeFrom or add one to
    // timeTo, depending on where we are in the interval, to correct for intervals across midnight.
    if (timeTo.compareTo(timeFrom) <= 0) {
      if (now.before(timeTo)) {
        timeFrom.add(Calendar.DAY_OF_MONTH, -1);
      } else {
        timeTo.add(Calendar.DAY_OF_MONTH, 1);
      }
    }

    return new Pair<>(timeFrom, timeTo);
  }

  /**
   * Parses a time string in the format HH:MM and adds the current date to it to produce a
   * {@link Calendar} instance set to the given time on the current day in the local time zone.
   *
   * @param time              A time value.
   * @param beginningOfMinute When true, the seconds and milliseconds of the returned date-time are
   *                          set to zero. When false, they are set to 59 and 999, respectively.
   * @return The parsed time on the current day.
   */
  @Nullable
  public static Calendar parseTime(@Nullable final String time, final boolean beginningOfMinute) {
    if (time == null || time.isEmpty()) return null;

    // Parse hours and minutes.
    final String[] timeParts = time.split(":");
    int minutes;
    int hours;
    try {
      hours = Integer.parseInt(timeParts[0]);
      minutes = Integer.parseInt(timeParts[1]);
    } catch (NumberFormatException | IndexOutOfBoundsException ex) {
      Log.w(TAG, "Invalid time value: ", time);
      return null;
    }

    // Add the current date and replace the time part.
    Calendar c = Calendar.getInstance();
    c.set(Calendar.HOUR_OF_DAY, hours);
    c.set(Calendar.MINUTE, minutes);
    if (beginningOfMinute) {
      c.set(Calendar.SECOND, 0);
      c.set(Calendar.MILLISECOND, 0);
    } else {
      c.set(Calendar.SECOND, 59);
      c.set(Calendar.MILLISECOND, 999);
    }

    return c;
  }

  /**
   * Deletes file.
   * <p>
   * <b>NOTE:</b> This is a stopgap until we can use Files.delete (API level 26), which also uses
   * classes derived from IOException to signal I/O errors.
   *
   * @param file The file to delete.
   * @throws IOException If {@link File#delete} returns false.
   */
  private static void delete(@NonNull final File file) throws IOException {
    if (!file.delete()) {
      throw new IOException("Failed to delete file: " + cleanPath(file.getPath()));
    }
  }

  /**
   * Moves file `from` to `to`.
   * <p>
   * <b>NOTE:</b> This is a stopgap until we can use Files.move (API level 26), which also uses
   * classes derived from IOException to signal I/O errors.
   *
   * @param from The file to move.
   * @param to   The destination file.
   * @throws IOException If {@link File#renameTo(File)} returns false.
   */
  private static void move(@NonNull final File from, @NonNull final File to) throws IOException {
    if (!from.renameTo(to)) {
      throw new IOException("Failed to move file " + cleanPath(from.getPath()) + " to "
          + cleanPath(to.getPath()));
    }
  }
}
