package io.hubsy.core.util;

import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import io.hubsy.core.Settings;

/**
 * An asynchronous file logger.
 */
public class Log {
  private static final String TAG = "Log";

  private static final String LOG_FILE_EXT = ".log";
  private static final String MAIN_LOG_NAME = "hubsy.log";
  private static final File MAIN_LOG_PATH;

  private static final BlockingQueue<Message> sLogQueue;
  private static final LogThread sLogThread;

  private volatile static Level sLogLevel = Level.INFO;

  static {
    // ArrayBlockingQueue is also an option. It pre-allocates its full capacity, but therefore it
    // does not need an additional wrapper object for each element.
    sLogQueue = new LinkedBlockingQueue<>(1024);

    // Create the log folder if necessary.
    final File logDir = new File(Settings.getAppFolderPath(), Settings.APP_DIR_LOGS);
    if (!logDir.exists() && !logDir.mkdirs()) {
      android.util.Log.e(TAG, "Failed to create the log dir: " + logDir.getAbsolutePath());
    }

    MAIN_LOG_PATH = new File(logDir, MAIN_LOG_NAME);

    sLogThread = new LogThread();
    sLogThread.start();
  }

  /**
   * The supported log levels.
   */
  public enum Level {
    VERBOSE("V"),
    DEBUG("D"),
    INFO("I"),
    WARN("W"),
    ERROR("E");

    public final String value;

    Level(String v) {
      value = v;
    }

    /**
     * Like {@link #valueOf}, but instead of throwing an exception if the given name does not match
     * one of the constants, it returns {@link #INFO}.
     *
     * @param name The name of the enum constant.
     * @return The matching enum constant.
     */
    public static Level from(String name) {
      try {
        return valueOf(name.toUpperCase(Locale.ENGLISH));
      } catch (IllegalArgumentException ex) {
        Log.w(TAG, "Unknown log level: " + name);
      }
      return INFO;
    }
  }

  /**
   * Sets the minimum log level. Log messages with a lower level will be discarded.
   */
  public static void setLogLevel(@NonNull Level l) {
    sLogLevel = l;
  }

  /**
   * Rotates the log file, queueing the old one for uploading, but does not initiate file transfers.
   */
  public static void rotate() {
    sLogQueue.offer(new ControlMsg(true));
  }

  /**
   * Adds a {@link BasicLogMsg} to the queue.
   */
  private static void log(@NonNull Level level, @NonNull String tag, @NonNull String msg,
                          @Nullable Throwable tr) {
    if (level.ordinal() >= sLogLevel.ordinal()) {
      sLogQueue.offer(new BasicLogMsg(System.currentTimeMillis(), level, tag, msg, tr));
    }
  }

  /**
   * Adds a {@link ConcatLogMsg} to the queue.
   */
  private static void log(@NonNull Level level, @NonNull String tag, @Nullable Object... args) {
    if (level.ordinal() >= sLogLevel.ordinal()) {
      sLogQueue.offer(new ConcatLogMsg(System.currentTimeMillis(), level, tag, args));
    }
  }

  /**
   * Adds a {@link FormattedLogMsg} to the queue.
   */
  private static void log(@NonNull Level level, @NonNull String tag, @NonNull Locale locale,
                          @NonNull String format, Object... args) {
    if (level.ordinal() >= sLogLevel.ordinal()) {
      sLogQueue.offer(new FormattedLogMsg(
          System.currentTimeMillis(), level, tag, locale, format, args));
    }
  }

  /**
   * Logs a verbose message.
   */
  public static void v(@NonNull String tag, @NonNull String msg) {
    log(Level.VERBOSE, tag, msg, null);
  }

  /**
   * Logs a debug message.
   */
  public static void d(@NonNull String tag, @NonNull String msg) {
    log(Level.DEBUG, tag, msg, null);
  }

  /**
   * Logs an info message.
   */
  public static void i(@NonNull String tag, @NonNull String msg) {
    log(Level.INFO, tag, msg, null);
  }

  /**
   * Logs a warning message.
   */
  public static void w(@NonNull String tag, @NonNull String msg) {
    log(Level.WARN, tag, msg, null);
  }

  /**
   * Logs a warning message with the stacktrace of a throwable appended to it.
   */
  public static void w(@NonNull String tag, @NonNull String msg, @Nullable Throwable tr) {
    log(Level.WARN, tag, msg, tr);
  }

  /**
   * Logs an error message.
   */
  public static void e(@NonNull String tag, @NonNull String msg) {
    log(Level.ERROR, tag, msg, null);
  }

  /**
   * Logs an error message with the stacktrace of a throwable appended to it.
   */
  public static void e(@NonNull String tag, @NonNull String msg, @Nullable Throwable tr) {
    log(Level.ERROR, tag, msg, tr);
  }


  /**
   * Logs a verbose message. The message is constructed by concatenating the results of calling
   * toString on each value in args.
   */
  public static void v(@NonNull String tag, Object... args) {
    log(Level.VERBOSE, tag, args);
  }

  /**
   * Logs a debug message. The message is constructed by concatenating the results of calling
   * toString on each value in args.
   */
  public static void d(@NonNull String tag, Object... args) {
    log(Level.DEBUG, tag, args);
  }

  /**
   * Logs an info message. The message is constructed by concatenating the results of calling
   * toString on each value in args.
   */
  public static void i(@NonNull String tag, Object... args) {
    log(Level.INFO, tag, args);
  }

  /**
   * Logs a warning message. The message is constructed by concatenating the results of calling
   * toString on each value in args.
   */
  public static void w(@NonNull String tag, Object... args) {
    log(Level.WARN, tag, args);
  }

  /**
   * Logs an error message. The message is constructed by concatenating the results of calling
   * toString on each value in args.
   */
  public static void e(@NonNull String tag, Object... args) {
    log(Level.ERROR, tag, args);
  }


  /**
   * Logs a verbose message. The message is constructed by passing the corresponding parameters to
   * String.format.
   */
  public static void v(@NonNull String tag, @NonNull Locale locale, @NonNull String fmt,
                       Object... args) {
    log(Level.VERBOSE, tag, locale, fmt, args);
  }

  /**
   * Logs a debug message. The message is constructed by passing the corresponding parameters to
   * String.format.
   */
  public static void d(@NonNull String tag, @NonNull Locale locale, @NonNull String fmt,
                       Object... args) {
    log(Level.DEBUG, tag, locale, fmt, args);
  }

  /**
   * Logs an info message. The message is constructed by passing the corresponding parameters to
   * String.format.
   */
  public static void i(@NonNull String tag, @NonNull Locale locale, @NonNull String fmt,
                       Object... args) {
    log(Level.INFO, tag, locale, fmt, args);
  }

  /**
   * Logs a warning message. The message is constructed by passing the corresponding parameters to
   * String.format.
   */
  public static void w(@NonNull String tag, @NonNull Locale locale, @NonNull String fmt,
                       Object... args) {
    log(Level.WARN, tag, locale, fmt, args);
  }

  /**
   * Logs an error message. The message is constructed by passing the corresponding parameters to
   * String.format.
   */
  public static void e(@NonNull String tag, @NonNull Locale locale, @NonNull String fmt,
                       Object... args) {
    log(Level.ERROR, tag, locale, fmt, args);
  }


  private static class LogThread extends Thread {
    private long mLastErrorTime;

    LogThread() {
      super("logger");
      setDaemon(true);
    }

    @Override
    public void run() {
      // This outer loop recreates the writer following a log rotation or error.
      while (!isInterrupted()) {
        // Run the log loop.
        boolean rotateLog = false;
        try (BufferedWriter w = new BufferedWriter(new OutputStreamWriter(
            new FileOutputStream(MAIN_LOG_PATH, true), StandardCharsets.UTF_8))) {
          rotateLog = runLogLoop(w);
        } catch (Exception e) {
          android.util.Log.e(TAG, "Error in log loop", e);
          logOnError(new BasicLogMsg(System.currentTimeMillis(), Level.ERROR, TAG,
              "Error in log loop", e));
        }

        if (rotateLog) {
          Helpers.moveFile(MAIN_LOG_PATH, Settings.APP_DIR_NEW,
              Settings.getTimestampFileName(System.currentTimeMillis(), LOG_FILE_EXT));
        }
      }

      android.util.Log.w(TAG, "The log thread is quitting");
    }

    /**
     * Runs the log loop until the log file needs to be rotated or an error is encountered.
     *
     * @param w The log writer.
     * @return True if the log file needs to be rotated, false otherwise.
     */
    private boolean runLogLoop(final BufferedWriter w) throws IOException {
      while (true) {
        final Message msg;
        try {
          msg = sLogQueue.take();
        } catch (InterruptedException e) {
          android.util.Log.w(TAG, "Interrupted while waiting on log queue");
          logOnError(new BasicLogMsg(System.currentTimeMillis(), Level.WARN, TAG,
              "Interrupted while waiting on log queue", e));
          continue;
        }

        // Write the log message.
        if (msg.format(w)) {
          w.write('\n');
          w.flush();
        }

        if (msg.rotateLog()) return true;
      }
    }

    /**
     * When the log thread itself wants to add a message to the queue following an error, this
     * method does that with some additional safeguards to ensure ongoing errors (e.g. I/O problems)
     * do not cause the queue to be flooded.
     */
    private void logOnError(LogMsg msg) {
      if (msg.mLevel.ordinal() >= sLogLevel.ordinal()) {
        final long now = SystemClock.elapsedRealtime();
        if ((mLastErrorTime == 0 || now > mLastErrorTime + 10000) && sLogQueue.size() < 64) {
          sLogQueue.offer(msg);
          mLastErrorTime = now;
        }
      }
    }
  } // LogThread

  private interface Message {
    /**
     * Formats and writes this message to w.
     *
     * @return True if any characters were written to w, false if w has not been modified.
     */
    boolean format(@NonNull BufferedWriter w) throws IOException;

    /**
     * Indicates whether the log file should be rotated immediately after this message has been
     * written.
     */
    boolean rotateLog();
  }

  private static class ControlMsg implements Message {
    private final boolean mRotateLog;

    ControlMsg(boolean rotateLog) {
      mRotateLog = rotateLog;
    }

    @Override
    public boolean format(@NonNull BufferedWriter w) {
      return false;
    }

    @Override
    public boolean rotateLog() {
      return mRotateLog;
    }
  }

  private static abstract class LogMsg implements Message {
    private final long mTimestamp;
    private final Level mLevel;
    private final String mTag;

    LogMsg(long timestamp, @NonNull Level level, @NonNull String tag) {
      mTimestamp = timestamp;
      mLevel = level;
      mTag = tag;
    }

    @Override
    public boolean rotateLog() {
      return false;
    }

    /**
     * Writes this log message to w.
     */
    @Override
    public boolean format(@NonNull BufferedWriter w) throws IOException {
      w.write(mLevel.value);
      w.write(" ");
      writeTimestamp(w, mTimestamp);
      w.write(" ");
      w.write(mTag);
      w.write(": ");
      return true;
    }

    /**
     * Writes the formatted timestamp to w.
     */
    private void writeTimestamp(@NonNull BufferedWriter w, long timestamp) throws IOException {
      w.write(DateFormat.format("yyyy-MM-dd HH:mm:ss", new Date(timestamp)).toString());
      w.write(String.format(Locale.ENGLISH, ".%03d", timestamp % 1000));
    }
  } // LogMsg

  private static class BasicLogMsg extends LogMsg {
    private final String mMsg;
    private final Throwable mThrowable;

    BasicLogMsg(long timestamp, @NonNull Level level, @NonNull String tag, @NonNull String msg,
                @Nullable Throwable tr) {
      super(timestamp, level, tag);
      mMsg = msg;
      mThrowable = tr;
    }

    @Override
    public boolean format(@NonNull BufferedWriter w) throws IOException {
      super.format(w);
      w.write(mMsg);

      if (mThrowable != null) {
        w.write(": ");
        w.write(android.util.Log.getStackTraceString(mThrowable));
      }

      return true;
    }
  } // BasicLogMsg

  private static class ConcatLogMsg extends LogMsg {
    private final Object[] mArgs;

    ConcatLogMsg(long timestamp, @NonNull Level level, @NonNull String tag,
                 @Nullable Object... args) {
      super(timestamp, level, tag);
      mArgs = args;
    }

    @Override
    public boolean format(@NonNull BufferedWriter w) throws IOException {
      super.format(w);

      if (mArgs != null) {
        for (Object arg : mArgs) {
          w.write(arg != null ? arg.toString() : "null");
        }
      }

      return true;
    }
  } // ConcatLogMsg

  private static class FormattedLogMsg extends LogMsg {
    private final Locale mLocale;
    private final String mFormat;
    private final Object[] mArgs;

    FormattedLogMsg(long timestamp, @NonNull Level level, @NonNull String tag,
                    @NonNull Locale locale, @NonNull String format, Object... args) {
      super(timestamp, level, tag);
      mLocale = locale;
      mFormat = format;
      mArgs = args;
    }

    @Override
    public boolean format(@NonNull BufferedWriter w) throws IOException {
      super.format(w);
      w.write(String.format(mLocale, mFormat, mArgs));
      return true;
    }
  } // FormattedLogMsg

}