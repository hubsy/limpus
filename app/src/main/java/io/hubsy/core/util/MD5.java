package io.hubsy.core.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public class MD5 {
  private static final String TAG = "MD5";

  /**
   * Returns true if the data file digest matches the contents of the MD5 file.
   *
   * @param dataFile The data file to validate.
   * @param md5File  The file with the MD5 checksum.
   * @return True if the digests match.
   */
  public static boolean checkMD5(@NonNull final File dataFile, @NonNull final File md5File) {
    Log.d(TAG, "Validating MD5 checksum");

    // Get the digests.
    String expectedMD5 = readMD5(md5File);
    String calculatedMD5 = calculateMD5(dataFile);
    if (expectedMD5 == null || calculatedMD5 == null) {
      Log.w(TAG, (expectedMD5 == null ? "Expected" : "Calculated") + " MD5 is null");
      return false;
    }

    // Normalise case and compare.
    expectedMD5 = expectedMD5.toLowerCase(Locale.ROOT);
    calculatedMD5 = calculatedMD5.toLowerCase(Locale.ROOT);
    boolean match = calculatedMD5.equals(expectedMD5);
    Log.d(TAG, "Checksum matches: " + match);
    if (!match) Log.w(TAG, "Expected != calculated MD5: " + expectedMD5 + " != " + calculatedMD5);

    return match;
  }

  /**
   * Calculates the MD5 digest for the given file.
   *
   * @param dataFile The input data.
   * @return The digest or null on error.
   */
  @Nullable
  private static String calculateMD5(File dataFile) {
    MessageDigest digest;
    try {
      digest = MessageDigest.getInstance("MD5");
    } catch (NoSuchAlgorithmException e) {
      Log.e(TAG, "Failed to get digest algorithm: " + e.getMessage());
      return null;
    }

    InputStream is;
    try {
      is = new FileInputStream(dataFile);
    } catch (FileNotFoundException e) {
      Log.e(TAG, "Failed to open file: " + e.getMessage());
      return null;
    }

    try {
      // Read data and update digest.
      byte[] buffer = new byte[8192];
      int read;
      while ((read = is.read(buffer)) > 0) {
        digest.update(buffer, 0, read);
      }
      byte[] md5sum = digest.digest();
      BigInteger bigInt = new BigInteger(1, md5sum);
      String output = bigInt.toString(16);

      // Pad with zeros to 32 chars.
      return String.format("%32s", output).replace(' ', '0');
    } catch (IOException e) {
      Log.e(TAG, "Failed to calculate MD5 digest: " + e.getMessage());
    } finally {
      try {
        is.close();
      } catch (IOException e) {
        Log.e(TAG, "Failed to close data input stream: " + e.getMessage());
      }
    }
    return null;
  }

  /**
   * Read a single line from the file, assuming it is an MD5 digest.
   *
   * @param md5File The file to read.
   * @return The digest or null on error.
   */
  @Nullable
  private static String readMD5(File md5File) {
    try (BufferedReader br = new BufferedReader(new FileReader(md5File))) {
      String line = br.readLine();
      if (line == null) return null;

      // Strip anything other than the first whitespace free character sequence.
      final String[] matches = line.trim().split("[ \t]", 2);
      if (matches.length > 1) {
        line = matches[0];
      }
      return line;
    } catch (IOException ex) {
      Log.e(TAG, "Failed to read MD5 from file: " + ex.getMessage());
    }
    return null;
  }
}
