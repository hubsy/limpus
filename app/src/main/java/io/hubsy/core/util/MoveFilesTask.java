package io.hubsy.core.util;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;
import java.util.regex.Pattern;

/**
 * Moves files from one folder to another folder using a worker thread.
 */
public class MoveFilesTask extends AsyncTask<Void, Void, Void> {
  private static final String TAG = "MoveFilesTask";

  private final File mSrc;
  private final File[] mSrcFiles;
  private final File mDestDir;

  @Nullable
  private final Pattern mInclude;
  private final boolean mDeleteUnmatched;

  private final Runnable mCallback;
  private final Handler mHandler;

  /**
   * If srcFileOrDir is a file, then it is moved. If it is a directory, then its contents are moved
   * non-recursively, skipping any child directories.
   *
   * @param srcFileOrDir    The source file or directory.
   * @param destDir         The destination directory.
   * @param include         An optional regex to match against file names. If non-null, only files
   *                        matching the pattern are moved, otherwise all files are moved.
   * @param deleteUnmatched If true and an include pattern is given, delete all unmatched files.
   * @param callback        An optional callback to post on `handler` when finished.
   * @param handler         The handler to post the callback on, if any.
   */
  public MoveFilesTask(@NonNull File srcFileOrDir, @NonNull File destDir, @Nullable Pattern include,
                       boolean deleteUnmatched, @Nullable Runnable callback,
                       @Nullable Handler handler) {
    mSrc = srcFileOrDir;
    mSrcFiles = null;
    mDestDir = destDir;
    mInclude = include;
    mDeleteUnmatched = deleteUnmatched;
    mCallback = callback;
    mHandler = handler;
  }

  /**
   * Moves the specified files to a different directory.
   *
   * @param srcFiles The source files.
   * @param destDir  The destination directory.
   * @param callback An optional callback to post on `handler` when finished.
   * @param handler  The handler to post the callback on, if any.
   */
  public MoveFilesTask(@NonNull File[] srcFiles, @NonNull File destDir, @Nullable Runnable callback,
                       @Nullable Handler handler) {
    mSrc = null;
    mSrcFiles = srcFiles;
    mDestDir = destDir;
    mInclude = null;
    mDeleteUnmatched = false;
    mCallback = callback;
    mHandler = handler;
  }

  /**
   * Executes the task synchronously on the calling thread rather than the usual mode of executing
   * on a worker thread with one of the other execute methods.
   */
  public void executeInForeground() {
    doInBackground();
    finish();
  }

  @Override
  protected Void doInBackground(Void... voids) {
    // Check if the source exists.
    if (mSrc != null && !mSrc.exists()) return null;

    // Check if the dest exists, is a directory and different from source. Create it if necessary.
    if (mDestDir.exists()) {
      if (mSrc != null && mSrc.equals(mDestDir)) {
        Log.w(TAG, "Cannot move, source and destination are equal: ", mSrc);
        return null;
      }
      if (!mDestDir.isDirectory()) {
        Log.e(TAG, "Invalid destination, not a directory: ", mDestDir);
        return null;
      }
    } else {
      // Create the directory with all its parents.
      if (!mDestDir.mkdirs()) {
        Log.w(TAG, "Failed to create directory ", mDestDir);
        return null;
      }
    }

    // Get a list of all the files to consider.
    final File[] files = mSrc == null ? mSrcFiles
        : mSrc.isDirectory() ? mSrc.listFiles() : new File[]{mSrc};
    if (files == null || files.length == 0) return null;

    // Move/delete files.
    int ignoredCount = 0;
    for (File src : files) {
      if (src.isFile()) {
        final String srcName = src.getName();

        // Match the filename against the include pattern if specified.
        boolean move = false;
        boolean delete = false;
        if (mInclude == null || mInclude.matcher(srcName).matches()) {
          move = true;
        } else {
          delete = mDeleteUnmatched;
        }

        if (move) {
          File dest = new File(mDestDir, srcName);
          if (src.renameTo(dest)) {
            Log.d(TAG, "Moved ", src, " to ", mDestDir);
          } else {
            Log.w(TAG, "Failed to move ", src, " to ", mDestDir);
          }
        } else if (delete) {
          if (src.delete()) {
            Log.d(TAG, "Deleted ", src);
          } else {
            Log.w(TAG, "Failed to delete ", src);
          }
        } else {
//          mDebugMsgs.add("Ignoring " + src);
          ignoredCount++;
        }
      } else {
        if (src.isDirectory()) Log.d(TAG, "Skipping directory ", src);
        else Log.d(TAG, "Skipping missing file ", src);
      }
    }

    Log.d(TAG, "Files ignored: ", ignoredCount);

    return null;
  }

  @Override
  protected void onPostExecute(Void aVoid) {
    finish();
  }

  @Override
  protected void onCancelled() {
    finish();
  }

  private void finish() {
    if (mCallback != null && mHandler != null) {
      mHandler.post(mCallback);
    }
  }
}
