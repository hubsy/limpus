package io.hubsy.core.util;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Determines the device orientation relative to its natural orientation using the gravity sensor.
 */
public class PhoneOrientation implements SensorEventListener {
  private static final String TAG = "PhoneOrientation";

  /**
   * Clockwise rotation of the device, in degrees, relative to its default orientation.
   * Rounded to multiples of 90 degrees (i.e. portrait or landscape orientations).
   */
  public static int Rotation_0_90_180_270 = 0;

  private SensorManager mSensorManager;
  private Sensor mSensGrav;

  /**
   * The instance updates {@link #Rotation_0_90_180_270} exactly once and then deregisters itself
   * from the sensor.
   */
  public PhoneOrientation(Context context) {
    mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
    mSensGrav = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
    mSensorManager.registerListener(this, mSensGrav, SensorManager.SENSOR_DELAY_FASTEST);
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
  }

  /**
   * Get the readings and deregister.
   */
  @Override
  public void onSensorChanged(SensorEvent event) {
    //Get the readings and deregister ASAP
    final float[] gravityReading = new float[3];
    System.arraycopy(event.values, 0, gravityReading, 0, gravityReading.length);
    mSensorManager.unregisterListener(this, mSensGrav);

    //Convert the readings into orientation
    float[] gravWeighted = new float[3];
    //Make values slightly biased towards landscape orientation
    gravWeighted[0] = (Math.abs(gravityReading[0]) + 0.5f) * (gravityReading[0] / Math.abs(gravityReading[0]));
    gravWeighted[1] = (Math.abs(gravityReading[1]) - 0.5f) * (gravityReading[1] / Math.abs(gravityReading[1]));

    if (Math.abs(gravWeighted[0]) >= Math.abs(gravWeighted[1])) {
      //Horizontal
      if (gravWeighted[0] < 0) Rotation_0_90_180_270 = 90;
      else Rotation_0_90_180_270 = 270;
    } else {
      if (gravWeighted[1] < 0) Rotation_0_90_180_270 = 180;
      else Rotation_0_90_180_270 = 0;
    }

    Log.d(TAG, "Gravity: x=", Float.toString(gravityReading[0]),
        "(", Float.toString(gravWeighted[0]), "), y=", Float.toString(gravityReading[1]),
        "(", Float.toString(gravWeighted[1]), "), z=", Float.toString(gravityReading[2]),
        " -> rotation ", Rotation_0_90_180_270);
  }
}
