package io.hubsy.core.util;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import io.hubsy.core.Settings.ConfigKeys;

/**
 * Misc reusable static functions
 */
public class Radio {
  private static final String TAG = "Radio";

  private static int sDesiredWifiState = WifiManager.WIFI_STATE_UNKNOWN;
  private static NetworkInfo.State sDesiredWifiConnectionState = NetworkInfo.State.UNKNOWN;
  private static BroadcastReceiver sWifiStateListener;
  private static BroadcastReceiver sWifiConnectionStateListener;
  private static boolean sNewWifiConfigAvailable;
  // Indicates if the wifi config has been checked since the app started. If it hasn't, then it is
  // possible that it was changed while wifi was disabled and the app restarted subsequently.
  private static boolean sWifiConfigCheckedSinceStart;

  /**
   * Turns WiFi on.
   *
   * @return True if WiFi is already on or if it can be enabled and the process has been started.
   */
  @SuppressWarnings("UnusedReturnValue")
  public static boolean turnOnWifi(Context context) {
    final io.hubsy.core.Settings settings = io.hubsy.core.Settings.getInstance(context);

    // Turn on only if not disabled in the settings.
    if (!settings.getBool(ConfigKeys.UseWifi)) {
      Log.d(TAG, "WiFi is disabled in the settings");
      return false;
    }

    sDesiredWifiState = WifiManager.WIFI_STATE_ENABLED;

    final Context appContext = context.getApplicationContext();
    final WifiManager wifiManager = (WifiManager) appContext.getSystemService(Context.WIFI_SERVICE);

    // Check the current Wifi state.
    final int state = wifiManager.getWifiState();
    if (state == WifiManager.WIFI_STATE_ENABLED) {
      Log.d(TAG, "WiFi is already on");
      // Try connecting if it isn't already. Don't do this if it is still enabling the adapter,
      // there should be a listener in this case that will connect when ready.
      connectWifi(context, 0);
      return true;
    }

    // Try turning the adapter on.
    if (state != WifiManager.WIFI_STATE_ENABLING) {
      try {
        if (!wifiManager.setWifiEnabled(true)) {
          Log.w(TAG, "Cannot enable WiFi");
          return false;
        }
      } catch (Exception e) {
        Log.w(TAG, "Cannot enable WiFi: ", e.getMessage());
        return false;
      }
    }

    // Register a receiver to be notified of state changes if none is active yet.
    if (sWifiStateListener == null) {
      sWifiStateListener = new WifiStateListener(appContext);
      IntentFilter filter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
      appContext.registerReceiver(sWifiStateListener, filter);
    }

    Log.d(TAG, "WiFi turning on");
    return true;
  }

  /**
   * Turns WiFi off.
   */
  public static void turnOffWifi(Context context) {
    sDesiredWifiState = WifiManager.WIFI_STATE_DISABLED;
    sDesiredWifiConnectionState = NetworkInfo.State.DISCONNECTED;

    final Context appContext = context.getApplicationContext();
    final WifiManager wifiManager = (WifiManager) appContext.getSystemService(Context.WIFI_SERVICE);

    // Check the current Wifi state.
    final int state = wifiManager.getWifiState();
    if (state == WifiManager.WIFI_STATE_DISABLED) {
      Log.d(TAG, "WiFi is already off");
      return;
    } else if (state == WifiManager.WIFI_STATE_DISABLING) {
      Log.d(TAG, "WiFi is already turning off");
      return;
    }

    // Disable the adapter.
    try {
      if (!wifiManager.setWifiEnabled(false)) {
        Log.w(TAG, "Cannot disable WiFi");
        return;
      }
    } catch (Exception e) {
      Log.w(TAG, "Cannot disable WiFi: ", e.getMessage());
      return;
    }

    // Register a receiver to be notified of state changes if none is active yet.
    if (sWifiStateListener == null) {
      sWifiStateListener = new WifiStateListener(appContext);
      IntentFilter filter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
      appContext.registerReceiver(sWifiStateListener, filter);
    }

    Log.d(TAG, "WiFi turning off");
  }

  /**
   * Listener for {@link WifiManager#WIFI_STATE_CHANGED_ACTION}.
   */
  private static class WifiStateListener extends BroadcastReceiver {
    private Context mContext;

    private WifiStateListener(Context appContext) {
      mContext = appContext;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
      if (!WifiManager.WIFI_STATE_CHANGED_ACTION.equals(intent.getAction())) return;

      final Bundle extras = intent.getExtras();
      if (extras == null) return;

      // Unregister the receiver if a persistent state has been reached.
      boolean isEnabled = false;
      int state = extras.getInt(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_DISABLED);
//      Log.d(TAG, "WiFi adapter state: " + state
//          + " on thread " + Thread.currentThread().getName());
      switch (state) {
        case WifiManager.WIFI_STATE_ENABLED:
          if (sDesiredWifiState == WifiManager.WIFI_STATE_ENABLED) {
            sWifiStateListener = null;
            mContext.unregisterReceiver(this);
            isEnabled = true;
            Log.d(TAG, "WiFi enabled");
          }
          break;
        case WifiManager.WIFI_STATE_DISABLED:
          if (sDesiredWifiState == WifiManager.WIFI_STATE_DISABLED) {
            sWifiStateListener = null;
            mContext.unregisterReceiver(this);
            Log.d(TAG, "WiFi disabled");
          }
          break;
        case WifiManager.WIFI_STATE_UNKNOWN:
          Log.d(TAG, "WiFi state unknown");
          break;
      }

      if (isEnabled) connectWifi(mContext, 0); // Connect to an access point.
    }
  } // WifiStateListener

  /**
   * Connects to a WiFi network. The adapter must already be turned on for this to succeed.
   *
   * @param context        The context to use.
   * @param recursionDepth The recursion depth. Should be zero for non-recursive calls.
   */
  private static void connectWifi(Context context, int recursionDepth) {
    sDesiredWifiConnectionState = NetworkInfo.State.CONNECTED;

    if (sNewWifiConfigAvailable || !sWifiConfigCheckedSinceStart) {
      Log.d(TAG, "Applying the latest WiFi config before the connection attempt is made");
      addOrUpdateWiFiNetwork(context);
      return;
    }

    // Check if it is already connected.
    if (recursionDepth == 0 && isWiFiConnected(context)) return;

    final Context appContext = context.getApplicationContext();
    final WifiManager wifiManager = (WifiManager) appContext.getSystemService(Context.WIFI_SERVICE);

    boolean connecting = false;
    if (recursionDepth == 0) {
      // Try to connect to the previously used network first.
      recursionDepth++;
      if (wifiManager.reconnect()) {
        Log.d(TAG, "Trying to reconnect to the last used WiFi access point");
        connecting = true;
      }
    }

    if (!connecting) {
      // The reconnect failed or this is already a recursive call (depth >= 1). Try other networks.
      final List<WifiConfiguration> wifiConfigs = wifiManager.getConfiguredNetworks();

      // Try connecting to the next network in the list that is not disabled.
      if (wifiConfigs == null) {
        Log.d(TAG, "Cannot connect to WiFi, the list of configured networks is null");
      } else {
        for (; recursionDepth <= wifiConfigs.size() && !connecting; recursionDepth++) {
          WifiConfiguration wifiConfig = wifiConfigs.get(recursionDepth - 1);

          Log.d(TAG, "Trying WiFi network ", recursionDepth, ": ", wifiConfig.SSID);
          if (!wifiManager.enableNetwork(wifiConfig.networkId, true)) {
            Log.d(TAG, "Cannot enable WiFi network ", recursionDepth, ": ", wifiConfig.SSID);
            continue;
          }

          connecting = true;
        }
      }
    }

    if (connecting) {
      // Register a state change listener.
      final int depth = recursionDepth;
      sWifiConnectionStateListener = new BroadcastReceiver() {
        private long mFirstCallTime;

        @Override
        public void onReceive(Context context, Intent intent) {
          if (!WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(intent.getAction())) return;

          // Check if this is still the active listener.
          if (this != sWifiConnectionStateListener) {
            appContext.unregisterReceiver(this);
            return;
          }

          if (mFirstCallTime == 0) mFirstCallTime = SystemClock.elapsedRealtime();

          final NetworkInfo network = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
          if (network == null) return;

          final NetworkInfo.State state = network.getState();
//          Log.d(TAG, "WiFi connection state: " + state
//              + " on thread " + Thread.currentThread().getName());

          // Try reaching the desired state for up to 10 seconds.
          if (state != sDesiredWifiConnectionState
              && SystemClock.elapsedRealtime() < (mFirstCallTime + 10000)) {
            return;
          }

          // Unregister the receiver if a persistent state has been reached.
          boolean tryNextAP = false;
          switch (state) {
            case CONNECTED:
              appContext.unregisterReceiver(this);
              WifiInfo connection = intent.getParcelableExtra(WifiManager.EXTRA_WIFI_INFO);
              if (connection != null) Log.d(TAG, "WiFi connected to: ", connection.getSSID());
              else Log.d(TAG, "WiFi connected, but the WifiInfo is null");
              break;
            case DISCONNECTED:
              tryNextAP = true;
              appContext.unregisterReceiver(this);
              Log.d(TAG, "WiFi disconnected");
              break;
            case SUSPENDED:
              tryNextAP = true;
              appContext.unregisterReceiver(this);
              Log.d(TAG, "WiFi connection suspended");
              break;
            case UNKNOWN:
              tryNextAP = true;
              appContext.unregisterReceiver(this);
              Log.d(TAG, "WiFi connectivity state unknown");
              break;
          }

          // Try the next access point if any and if still trying to connect.
          if (tryNextAP && sDesiredWifiConnectionState == NetworkInfo.State.CONNECTED) {
            connectWifi(appContext, depth);
          }
        }
      };

      IntentFilter filter = new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION);
      appContext.registerReceiver(sWifiConnectionStateListener, filter);
    } else {
      Log.d(TAG, "Unable to connect to any WiFi network");
    }
  }

  /**
   * Returns true if there is an active WiFi connection with Internet connectivity.
   */
  private static boolean isWiFiConnected(Context context) {
    final ConnectivityManager cm =
        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

    // Get information about the active data network.
    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    if (activeNetwork == null) {
      Log.d(TAG, "No active WiFi network");
      return false;
    }

    // Determine the network status and type.
    boolean isConnected = activeNetwork.isConnectedOrConnecting();
    boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
    if (isConnected && isWiFi) {
      Log.d(TAG, "WiFi is connected and active");
      return true;
    }

    if (isWiFi) Log.d(TAG, "WiFi is active, but not connected");
    return false;
  }

  /**
   * Adds a new WiFi network or updates an existing config. This will also enable WiFi if it isn't
   * already and {@link ConfigKeys#UseWifi} is true.
   *
   * @param context A context.
   */
  public static void addOrUpdateWiFiNetwork(Context context) {
    sNewWifiConfigAvailable = false;
    sWifiConfigCheckedSinceStart = true;

    final io.hubsy.core.Settings settings = io.hubsy.core.Settings.getInstance(context);
    final Context appContext = context.getApplicationContext();
    final WifiManager wifiManager = (WifiManager) appContext.getSystemService(Context.WIFI_SERVICE);

    // Load and check the settings.
    final boolean isHidden = settings.getBool(ConfigKeys.WifiIsHidden);
    final String psk = settings.getString(ConfigKeys.WifiPSK);
    final String ssid = settings.getString(ConfigKeys.WifiSSID);
    final boolean useWifi = settings.getBool(ConfigKeys.UseWifi);
    if (psk.isEmpty()) {
      if (useWifi) Log.d(TAG, "The WiFi network config is missing the pre-shared key");
      return;
    }
    if (ssid.isEmpty()) {
      if (useWifi) Log.d(TAG, "The WiFi network config is missing the SSID");
      return;
    }

    // Check if wifi is enabled and do that first if necessary.
    if (wifiManager.getWifiState() != WifiManager.WIFI_STATE_ENABLED) {
      sNewWifiConfigAvailable = true;
      if (useWifi) {
        Log.d(TAG, "Trying to enable WiFi to apply the new config");
        turnOnWifi(context);
      } else {
        Log.d(TAG,
            "WiFi is disabled in the settings, the new config will be applied when it is enabled");
      }
      return;
    }

    // Create the new wifi config.
    final WifiConfiguration newConfig = new WifiConfiguration();
    newConfig.hiddenSSID = isHidden;
    newConfig.SSID = "\"" + ssid + "\"";
    newConfig.preSharedKey = "\"" + psk + "\"";

    newConfig.allowedGroupCiphers = new BitSet();
    newConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
    newConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);

    newConfig.allowedKeyManagement = new BitSet();
    newConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);

    newConfig.allowedPairwiseCiphers = new BitSet();
    newConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);

    newConfig.allowedProtocols = new BitSet();
    newConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN); // WPA2.

    // Get the currently configured networks.
    List<WifiConfiguration> wifiConfigs = wifiManager.getConfiguredNetworks();
    if (wifiConfigs == null) wifiConfigs = new ArrayList<>(0);

    // Remove all other networks.
    for (WifiConfiguration config : wifiConfigs) {
      if (!newConfig.SSID.equals(config.SSID)) {
        if (wifiManager.removeNetwork(config.networkId)) {
          Log.d(TAG, "Removing the old WiFi network config for ", config.SSID);
        } else {
          Log.w(TAG, "Unable to remove the old WiFi network config for ", config.SSID);
        }
      }
    }

    // Note: The updateNetwork method always seems to fail, addNetwork on the other hand works okay
    // whether updating or adding a network.

    // Add/update the network.
    boolean success = wifiManager.addNetwork(newConfig) != -1;
    if (success) {
      Log.d(TAG, "Added/updated WiFi network ", newConfig.SSID);

      // The network needs to be enabled and connected to. Disconnect from the previous network, if
      // connected, and skip the reconnection attempt happening at recursionDepth 0.
      if (isWiFiConnected(context) && !wifiManager.disconnect()) {
        Log.w(TAG, "Failed to disconnect from the current WiFi network");
      }
      connectWifi(context, 1);
    } else {
      Log.w(TAG, "Failed to configure new WiFi network ", newConfig.SSID);
    }
  }

  /**
   * Turns on the default {@link BluetoothAdapter}. Returns true if already on or if the
   * asynchronous process to turn it on has begun. The latter may still fail, see
   * {@link BluetoothAdapter#enable()}.
   *
   * @param context The context to use.
   * @return True if on or turning on, false if the task failed immediately.
   */
  @SuppressWarnings("UnusedReturnValue")
  public static boolean turnOnBluetooth(final Context context) {
    final io.hubsy.core.Settings settings = io.hubsy.core.Settings.getInstance(context);
    if (!settings.getBool(ConfigKeys.BleOn)) {
      Log.d(TAG, "Bluetooth is disabled in the settings");
      return false;
    }

    final BluetoothManager btManager =
        (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
    final BluetoothAdapter btAdapter = btManager.getAdapter();

    int state = btAdapter.getState();
    if (state != BluetoothAdapter.STATE_ON && state != BluetoothAdapter.STATE_TURNING_ON) {
      // Off, try turning it on.
      Log.d(TAG, "Enabling Bluetooth");
      HealthCheck.getInstance(context).updateTimestamp(HealthCheck.Timestamp.BluetoothTurnOn);
      if (!btAdapter.enable()) {
        Log.w(TAG, "Failed to enable Bluetooth");
        return false;
      }
    }

    return true;
  }

  /**
   * Returns true if the bluetooth adapter is either on or in the process of turning on.
   */
  public static boolean isBluetoothEnabled(final Context context) {
    final BluetoothManager btManager =
        (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
    final BluetoothAdapter btAdapter = btManager.getAdapter();

    int state = btAdapter.getState();
    return state == BluetoothAdapter.STATE_ON || state == BluetoothAdapter.STATE_TURNING_ON;
  }

  /**
   * Turns off the default {@link BluetoothAdapter}. Returns true if already off or if the
   * asynchronous process to turn it off has begun. The latter may still fail, see
   * {@link BluetoothAdapter#enable()}.
   *
   * @param context The context to use.
   */
  public static void turnOffBluetooth(final Context context) {
    final BluetoothManager btManager = (BluetoothManager) context.getApplicationContext()
        .getSystemService(Context.BLUETOOTH_SERVICE);
    final BluetoothAdapter btAdapter = btManager.getAdapter();

    int state = btAdapter.getState();
    if (state != BluetoothAdapter.STATE_OFF && state != BluetoothAdapter.STATE_TURNING_OFF) {
      // On, turn it off.
      Log.d(TAG, "Disabling Bluetooth");
      if (!btAdapter.disable()) {
        Log.w(TAG, "Cannot disable Bluetooth");
      }
    }
  }

  /**
   * Turn cellular network and WiFi radios on or off.
   * <p>
   * Adheres to {@link ConfigKeys#RadioAlwaysOn}.
   *
   * @param context The context to use.
   * @param radioOn The new state, true for radios on, false for off.
   */
  public static void toggleRadios(Context context, boolean radioOn) {
    Log.d(TAG, "Radio toggle request: ", (radioOn ? "on" : "off"));

    final io.hubsy.core.Settings settings = io.hubsy.core.Settings.getInstance(context);

    // The new state for airplane mode.
    int newAirplaneMode = radioOn ? 0 : 1;

    // Check if turning radio off is actually allowed by the settings.
    if (!radioOn && settings.getBool(ConfigKeys.RadioAlwaysOn)) {
      Log.d(TAG, "Radio is always on");
      return;
    }

    final ContentResolver resolver = context.getContentResolver();

    // Check if airplane mode is already in the desired state.
    int currentAirplaneMode = Settings.Global.getInt(resolver, Settings.Global.AIRPLANE_MODE_ON, 0);
    if (currentAirplaneMode == newAirplaneMode) {
      Log.d(TAG, "Radio is already ", (radioOn ? "on" : "off"));
    } else {
      // Update and broadcast the state of airplane mode.
      Settings.Global.putInt(resolver, Settings.Global.AIRPLANE_MODE_ON, newAirplaneMode);
      Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
      intent.putExtra("state", !radioOn);
      context.sendBroadcast(intent);
    }

    // Turn WiFi on/off along with the cellular radio.
    if (radioOn) {
      turnOnWifi(context);
    } else {
      turnOffWifi(context);
    }

    // Check the value of airplane mode.
//    int changedAirplaneMode = Settings.Global.getInt(resolver, Settings.Global.AIRPLANE_MODE_ON, 0);
//    Log.d(TAG, "Radio toggle request: " + radioOn + ", airplane mode set to (req/act): "
//        + newAirplaneMode + "/" + changedAirplaneMode);
  }
}
