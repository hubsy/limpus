package io.hubsy.core.util;

import android.support.annotation.NonNull;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;

/**
 * A composite {@link Executor} that serialises the execution of tasks submitted to the encapsulated
 * Executor.
 */
public class SerialExecutor implements Executor {
  private final Executor mExecutor;
  private final ArrayDeque<Runnable> mTasks;

  private Runnable mActive;

  /**
   * Tasks submitted to this Executor run in a serial manner on the encapsulated Executor.
   *
   * @param executor        The encapsulated Executor.
   * @param initialCapacity The initial capacity of the serial task queue. It will grow as required.
   */
  public SerialExecutor(@NonNull Executor executor, int initialCapacity) {
    mExecutor = executor;
    mTasks = new ArrayDeque<>(initialCapacity);
  }

  @Override
  public synchronized void execute(final Runnable r) {
    mTasks.offer(() -> {
      try {
        r.run();
      } finally {
        scheduleNext();
      }
    });
    if (mActive == null) {
      scheduleNext();
    }
  }

  private synchronized void scheduleNext() {
    if ((mActive = mTasks.poll()) != null) {
      mExecutor.execute(mActive);
    }
  }
}
