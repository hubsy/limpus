package io.hubsy.core.util;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * An {@link AsyncTask} to write a {@link ByteBuffer} to a file.
 */
public class WriteBufferToFileTask extends AsyncTask<Void, Void, Void> {
  private static final String TAG = "WriteBufferToFileTask";

  private final ByteBuffer mBuffer;
  private final File mPath;
  private final Runnable mDone;
  private final Handler mHandler;

  /**
   * Writes a buffer to a file.
   *
   * @param buffer  The buffer to write.
   * @param path    The file output path.
   * @param done    An optional callback to invoke when finished (successfully or not).
   * @param handler If non-null, this handler is used to schedule the callback. Otherwise, it is
   *                invoked on the main thread.
   */
  public WriteBufferToFileTask(@NonNull ByteBuffer buffer, @NonNull File path,
                               @Nullable Runnable done, @Nullable Handler handler) {
    mBuffer = buffer;
    mPath = path;
    mDone = done;
    mHandler = handler;
  }

  @Override
  protected Void doInBackground(Void... voids) {
    try (FileChannel fc = new FileOutputStream(mPath).getChannel()) {
      mBuffer.rewind();
      fc.write(mBuffer);
      Log.d(TAG, "Buffer written to ", mPath.getName());
    } catch (Exception e) {
      Log.e(TAG, "Failed to write to ", mPath.getName(), ": ", e.getMessage());
    }
    return null;
  }

  @Override
  protected void onPostExecute(Void aVoid) {
    finish();
  }

  @Override
  protected void onCancelled() {
    finish();
  }

  private void finish() {
    if (mDone != null) {
      if (mHandler != null) mHandler.post(mDone);
      else mDone.run();
    }
  }
}
