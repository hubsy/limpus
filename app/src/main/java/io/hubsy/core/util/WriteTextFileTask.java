package io.hubsy.core.util;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * Write an array of string values to a file using a worker thread.
 */
public class WriteTextFileTask extends AsyncTask<String, Void, Void> {
  private static final String TAG = "WriteTextFileTask";

  private final File mOut;
  private final boolean mAppend;
  private final Runnable mCallback;

  private boolean mSuccess;

  /**
   * @param out      The output file.
   * @param append   Append to the file if it exists instead of truncating.
   * @param callback Optional callback to be executed on the UI thread after the task has completed
   *                 successfully.
   */
  public WriteTextFileTask(@NonNull File out, boolean append, @Nullable Runnable callback) {
    mOut = out;
    mAppend = append;
    mCallback = callback;
  }

  @Override
  protected Void doInBackground(String... strings) {
    if (strings == null || strings.length == 0) return null;

    try (BufferedWriter w = new BufferedWriter(new FileWriter(mOut, mAppend))) {
      for (String s : strings) {
        w.write(s);
      }
      w.flush();
      mSuccess = true;
    } catch (Exception e) {
      Log.w(TAG, "Failed to write to ", mOut.getName(), ": ", e.getMessage());
    }

    return null;
  }

  @Override
  protected void onPostExecute(Void aVoid) {
    if (mSuccess && mCallback != null) mCallback.run();
  }
}
