cd C:\Users\max\AppData\Local\Android\sdk\platform-tools\
adb root
adb shell "mkdir /system/vendor/hubsy/cron"
adb push C:\Users\max\Documents\Projects\android\limpus\ext-tools\crontab\root /system/vendor/hubsy/cron

mount -o rw,remount /

echo "root:x:0:0::/data/cron:/system/bin/bash" > /etc/passwd
ln -s /system/bin/ /bin
crond -c /system/vendor/hubsy/cron
