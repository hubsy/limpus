#!/bin/bash
# Run inside the opencv repository root.
set -e

export ANDROID_SDK=/opt/android/sdk
export ANDROID_NDK=${ANDROID_SDK}/ndk-bundle
minSdk=22

mkdir build
cd build/

# See https://github.com/opencv/opencv/issues/8460#issuecomment-418232967 for
# the Android SDK Tools version error workaround.
cmake -DANDROID_SDK_ROOT=$ANDROID_SDK \
    -DCMAKE_TOOLCHAIN_FILE=$ANDROID_NDK/build/cmake/android.toolchain.cmake \
    -DANDROID_ABI="arm64-v8a" -DANDROID_STL=c++_static \
    -DCMAKE_BUILD_TYPE=Release -DANDROID_NATIVE_API_LEVEL=android-${minSdk} \
    -DBUILD_JAVA=OFF -DBUILD_ANDROID_EXAMPLES=OFF ..

make install
cd install

