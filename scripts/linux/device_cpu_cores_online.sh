# Print the number of online (active) CPU cores.
adb -d shell cat '/sys/devices/system/cpu/cpu*/online' | egrep 1 | wc -l
