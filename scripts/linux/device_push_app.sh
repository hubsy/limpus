#!/bin/bash

appFile="$(dirname "$0")/../../app/build/outputs/apk/release/app-release.apk"

set -e
set -x

adb -d root
adb -d remount
adb -d push "${appFile}" /system/app/sensorable_cam.apk
adb -d reboot
