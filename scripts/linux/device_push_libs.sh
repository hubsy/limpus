#!/bin/bash

buildDir="$(dirname "$0")/../../app/build"
buildVariant=release
installDir=/system/vendor/lib64

set -e
set -x

adb -d root
adb -d remount
adb -d push "${buildDir}/intermediates/transforms/stripDebugSymbol/${buildVariant}/0/lib/arm64-v8a/"*.so "${installDir}/"
