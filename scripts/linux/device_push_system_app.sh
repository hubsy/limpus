#!/bin/bash

if [[ $# -ne 1 ]]; then
    echo "Usage: $(basename "$0") <executable>" 1>&2
    exit 1
fi
if [[ ! -f "$1" ]] || [[ ! -x "$1" ]]; then
    echo "File not found or not executable: $1" 1>&2
    exit 1
fi

set -e
set -x

adb -d root
adb -d remount
adb -d push "$1" "/vendor/bin/$(basename "$1")"
