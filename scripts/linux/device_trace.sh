#!/bin/bash

ANDROID_SDK=/opt/android/sdk

if [[ $# -lt 1 ]]; then
    echo "Usage: $(basename $0) <out.html>" >&2
    exit 1
fi

python2 ${ANDROID_SDK}/platform-tools/systrace/systrace.py \
    -a io.hubsy.core -t 10 -o "$1"
