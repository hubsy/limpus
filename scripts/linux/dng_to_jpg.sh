#!/bin/bash
set -e

if [[ $# -lt 1 ]]; then
    echo "Usage: $(basename "$0") <dng_file> [<jpg_quality>]"
    exit 1
fi

dng=$1
jpg=${dng/%dng/jpg}

quality=93
if [[ $# -eq 2 ]]; then
    quality=$2
fi

set -x

#dcraw -w -T -c "$dng" | convert - -quality $quality "$jpg"
dcraw -w -c "$dng" | cjpeg -quality $quality -progressive > "$jpg"
