#!/bin/bash
#
# Filters unique BLE sensor activations in the piped in data with the count of
# the collapsed lines prefixed to each line in the output.

egrep 'Found (device|Ruuvi) with address' \
    | sed -r 's/.*Found .+ with address ([^,]+),.*/\1/' \
    | sort -k 1 \
    | uniq -c \
    | column -t -R 1
