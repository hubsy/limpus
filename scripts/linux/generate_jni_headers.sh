ANDROID_SDK=/opt/android/sdk
PLATFORM=android-28
CLASSPATH=app/build/intermediates/javac/release/compileReleaseJavaWithJavac/classes/

javah -classpath ${CLASSPATH} \
    -o app/src/main/cpp/bgsub/bgsub_jni.h \
    io.hubsy.core.imgproc.BGSubtraction
javah -classpath ${CLASSPATH}:${ANDROID_SDK}/platforms/${PLATFORM}/android.jar \
    -o app/src/main/cpp/bgsub/utils_jni.h \
    io.hubsy.core.imgproc.Utils
