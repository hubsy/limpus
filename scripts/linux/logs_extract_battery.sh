#!/bin/bash

pattern='BATTERY'

if [[ $# -lt 1 ]]; then
    echo "Usage: $(basename $0) PATH [PATH ...]" 1>&2
    exit 1
fi

for dir in "${@:1}"; do
    file="${dir}/$(ls -1 "$dir" | tail -n1)"
    echo -n "$(basename "$dir"): "
    egrep "$pattern" "$file" | tail -n 1 | column -t -s ,
done
