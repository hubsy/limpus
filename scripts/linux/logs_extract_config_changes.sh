#!/bin/bash

pattern='Settings: [kK]ey:.*value:.*'
replace='.*log\/([^\/]+)\/([0-9]+)T([0-9]+)\..*[kK]ey: (.*); value: (.*)$'
replacement='\1: \2 \3 Settings: \4: \5'

if [[ $# -lt 2 ]]; then
    echo "Usage: $(basename $0) PATTERN PATH [PATH ...]" 1>&2
    exit 1
fi

for dir in "${@:2}"; do
    find "$dir" -name "$1" -print0 | sort -z | while read -d $'\0' file; do
        out=`egrep -Hn "$pattern" "$file" | sed -r "s/$replace/$replacement/"`
        if [[ ! -z "$out" ]]; then
            echo "$out"
            echo
        fi
    done
done
