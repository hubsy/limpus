#!/bin/bash

pattern='(^[EW]|exception|error|fail|cannot|invalid|missing|unexpected|warning|not enough)'
ignore='(AwsClient.*FAILED|File moved|MoveFilesTask: (Moved|Deleted)|No address|Cannot focus|SSL handshake timed out|HTTP request:.*time|SocketTimeoutException|ETIMEDOUT)'
replace='.*log\/([^\/]+)\/([^\/]+\.log):[0-9]*:?([A-Z] )?([0-9 :-]+)\.[0-9]* (.*)$'
replacement='\1: \2 \4 \3\5'

if [[ $# -lt 2 ]]; then
    echo "Usage: $(basename $0) PATTERN PATH [PATH ...]" 1>&2
    exit 1
fi

for dir in "${@:2}"; do
    find "$dir" -name "$1" -print0 | sort -z | while read -d $'\0' file; do
        egrep -iHn "$pattern" "$file" \
            | egrep -v "$ignore" \
            | sed -r "s/$replace/$replacement/"
    done
done
