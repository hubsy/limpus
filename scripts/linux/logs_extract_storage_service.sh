#!/bin/bash

pattern='StorageService'
replace='.*log\/([^\/]+)\/([0-9]+)T([0-9]+)\..*(StorageService: .*)$'
replacement='\1: \2 \3 \4'

if [[ $# -lt 2 ]]; then
    echo "Usage: $(basename $0) PATTERN PATH [PATH ...]" 1>&2
    exit 1
fi

for dir in "${@:2}"; do
    find "$dir" -name "$1" -print0 | sort -z | while read -d $'\0' file; do
        egrep -Hn "$pattern" "$file" | sed -r "s/$replace/$replacement/"
    done
done
