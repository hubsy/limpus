#!/bin/bash
#
# Expects 4 or 5 arguments depending on the update method (ADB or S3).
#
# <app_name> specifies the name of the app. Must be one of the values from
#   knownKeys below, but without the common prefix 'Aws' and suffix 'Name'.
# <build_number> should match the value in the commit's version.properties.
# <app_file> specifies the file path to the binary.
# [<s3_bucket>] [S3 only] is the bucket to access.
# <device> is the name of the device to update.
set -e

# List of known keys (excluding AwsUpgradeName).
knownKeys=( AwsAppName AwsHealthMonitorName AwsTgzArchiveName )

# The number of arguments determines if the update is done locally or via S3.
if [[ $# -eq 4 ]]; then
    method=adb
elif [[ $# -eq 5 ]]; then
    method=s3
else
    echo "Usage: $(basename "$0") <app_name> <build_number> <app_file> [<s3_bucket>] <device>" 1>&2
    exit 1
fi

app="$1"
key="Aws${app}Name"
build="$2"
appFileIn="$3"
if [[ "$method" == "adb" ]]; then
    device="$4"
else
    bucket="$4"
    device="$5"
fi

# Check the path to the update file.
if [[ ! -f "$appFileIn" ]]; then
    echo "Cannot read update file ${appFileIn}" 1>&2
    exit 1
fi

# Check if the key is in the known set.
keyIsKnown=0
for k in "${knownKeys[@]}"; do
    if [[ "$key" == "$k" ]]; then
        keyIsKnown=1
        break
    fi
done
if [[ "$keyIsKnown" -ne 1 ]]; then
    echo "Unknown key ${key}" 1>&2
    exit 1
fi

# Look for the upgrade template which will be adapted.
upgradeTemplate="$(dirname "$0")/upgrade_template.sh"
if [[ ! -f "$upgradeTemplate" ]]; then
    echo "Missing required file ${upgradeTemplate}" 1>&2
    exit 1
fi

# Create a temp directory for all upgrade files.
timestamp=$(date '+%Y%m%dT%H%M%S')
tmpDir="ota-${device}-${app}-v${build}-${timestamp}"
mkdir "$tmpDir"

# Set-up variables for paths.
appFileInBase="$(basename "${appFileIn}")"
appFile="${tmpDir}/${appFileInBase}.v${build}"
appFileBase="$(basename "${appFile}")"
appMD5="${appFile}.md5"
configFile="${tmpDir}/${appFileInBase}.config.v${build}"
upgradeFile="${tmpDir}/${appFileInBase}.upgrade.v${build}"
upgradeFileBase="$(basename "${upgradeFile}")"
upgradeMD5="${upgradeFile}.md5"
otaDir="/sdcard/hubsy/ota"

# Generate the upgrade config. Add the key and upgrade script, unset all other
# OTA files to avoid an incomplete upgrade from interferring.
echo "{" > "$configFile"
for k in "${knownKeys[@]}"; do
    if [[ "$k" != "$key" ]] && [[ "$k" != "AwsUpgradeName" ]]; then
      # Unset.
      echo "  \"${k}\": \"\"," >> "$configFile"
    fi
done
echo "  \"${key}\": \"app/${device}/${appFileBase}\",
  \"AwsUpgradeName\": \"app/${device}/${upgradeFileBase}\"
}" >> "$configFile"

# Generate the upgrade script and its checksum.
cp "${upgradeTemplate}" "${upgradeFile}"
sed -i -r \
    -e "s/^key=.*$/key=${key}/" \
    -e "s/^build=.*$/build=${build}/" \
    -e "s|^otaFile=.*$|otaFile=\"${otaDir}/${appFileBase}\"|" \
    "${upgradeFile}"
md5sum "${upgradeFile}" > "${upgradeMD5}"

# Append the build number to the app file and generate the checksum.
cp "${appFileIn}" "${appFile}"
md5sum "${appFile}" > "${appMD5}"

# Ask before pushing the update to the device.
echo -n "Push update via ${method}? [ y | n ]: "
read proceed
if [[ "$proceed" != "y" ]] && [[ "$proceed" != "Y" ]]; then
    echo "Update aborted" 1>&2
    exit 1
fi

# Perform the update.
if [[ "$method" == "adb" ]]; then
    adb -d push "${appFile}"        "${otaDir}/"
    adb -d push "${appMD5}"         "${otaDir}/"
    adb -d push "${upgradeFile}"    "${otaDir}/"
    adb -d push "${upgradeMD5}"     "${otaDir}/"
    adb -d push "${configFile}"     "${otaDir}/config.json"
    echo "Successfully pushed the update for local device ${device}"
elif [[ "$method" == "s3" ]]; then
    s3AppDirURI="s3://${bucket}/app/${device}"
    s3ConfigDirURI="s3://${bucket}/config/${device}"
    aws s3 cp "${appFile}"     "${s3AppDirURI}/"
    aws s3 cp "${appMD5}"      "${s3AppDirURI}/"
    aws s3 cp "${upgradeFile}" "${s3AppDirURI}/"
    aws s3 cp "${upgradeMD5}"  "${s3AppDirURI}/"
    aws s3 cp "${configFile}"  "${s3ConfigDirURI}/config-s3.json"
    echo "Successfully pushed the update for device ${device} to ${bucket}"
fi
