#!/bin/bash

if [[ $# -lt 3 ]]; then
    echo "Usage: $(basename $0) PATTERN <bucket> <device>" 1>&2
    exit 1
fi

pattern="$1"
bucket=$2
device=$3
workdir=$(pwd)

set -x

aws s3 sync --delete --exclude "*" --include "$pattern" \
    "s3://${bucket}/full/${device}/" "${workdir}/full/${device}/"
