#!/bin/bash

if [[ $# -lt 1 ]]; then
    echo "Usage: $(basename $0) <bucket> [<device>]" 1>&2
    exit 1
fi

bucket=$1
device=$2 # Can be empty string.
workdir=$(pwd)

# Add a / to the end of device if it is specified.
if [[ -n "$device" ]]; then
    device="${device}/"
fi

set -x

aws s3 sync --delete "s3://${bucket}/log/${device}" "${workdir}/log/${device}"
