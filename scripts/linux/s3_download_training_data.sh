#!/bin/bash

bucket=sensorable-data
prefix=training/vehicles/original/
workdir=$(pwd)

set -x

aws s3 sync --delete "s3://${bucket}/${prefix}" "${workdir}/${prefix}"
