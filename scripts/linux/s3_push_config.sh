#!/bin/bash

if [[ $# -lt 3 ]]; then
    echo "Usage: $(basename $0) <json_file> <bucket> <device>" 1>&2
    exit 1
fi

file=$1
bucket=$2
device=$3

set -x

aws s3 cp "$file" s3://${bucket}/config/${device}/config-s3.json
