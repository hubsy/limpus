################################################################################
# NOTE this needs to run on the device, do not use bash-only features !!!
################################################################################

# These need to be set for each update, generally by push_ota.sh.
key=
#key=AwsAppName
#key=AwsHealthMonitorName
#key=AwsTgzArchiveName
build=
otaFile=

################################################################################
# Configuration, these usually do not need to be changed.
prefFile=/data/data/io.hubsy.core/shared_prefs/pref.dat.xml
versionFile=/data/data/io.hubsy.core/shared_prefs/version.xml
healthDataFile=/data/data/io.hubsy.core/shared_prefs/health.xml
errLog="/sdcard/hubsy/new/$(date '+%Y%m%dT%H%M%S').upgrade.log"
otaDir="/sdcard/hubsy/ota"

# Check arguments.
if [[ -z "$build" ]] || [[ -z "$key" ]] || [[ -z "$otaFile" ]]; then
    echo "Missing argument, upgrade aborted" >> "$errLog"
    exit 1
fi

# Verify that the OTA file exists before doing anything.
if [[ ! -e "$otaFile" ]]; then
    echo "Missing file, upgrade aborted: $otaFile" >> "$errLog"
    exit 1
fi

################################################################################
# Upgrade specific code.

# Remount /system rw.
mount -o remount,rw /system
if [[ $? -ne 0 ]]; then
    echo "Failed to remount /system" >> "$errLog"
    exit 1
fi

# Perform the key specific upgrade.
if [[ "$key" = "AwsAppName" ]]; then
    destDir="/system/app"
    destFile="${destDir}/app-debug.apk"
    tempFile="${destFile}.new"

    cp "$otaFile" "$tempFile"               2>> "$errLog" \
        && chmod 644 "$tempFile"            2>> "$errLog" \
        && mv -f "$tempFile" "$destFile"    2>> "$errLog" \
        && rm "$otaFile"*                   2>> "$errLog"
    exitCode=$?
elif [[ "$key" = "AwsHealthMonitorName" ]]; then
    destDir="/system/vendor/bin"
    destFile="${destDir}/health_monitor"
    tempFile="${destFile}.new"
    initdFile="/system/etc/init.d/99health_monitor"

    mkdir -p "${destDir}"                   2>> "$errLog" \
        && chown root:shell "$destDir"      2>> "$errLog" \
        && chmod 755 "$destDir"             2>> "$errLog" \
        && cp "$otaFile" "$tempFile"        2>> "$errLog" \
        && chown root:shell "$tempFile"     2>> "$errLog" \
        && chmod 755 "$tempFile"            2>> "$errLog" \
        && mv -f "$tempFile" "$destFile"    2>> "$errLog" \
        && rm "$otaFile"*                   2>> "$errLog" \
        && rm "$healthDataFile"             2>> "$errLog"
    # Remove the health data to avoid a boot-loop when the health
    # monitor starts up before the camera app.
    exitCode=$?

    # Write the init.d script to start the health monitor service.
    if [[ ${exitCode} -eq 0 ]]; then
        echo "${destFile} >/dev/null 2>&1 &" > "$initdFile"   \
            && chown root:shell "$initdFile"    2>> "$errLog" \
            && chmod 755 "$initdFile"           2>> "$errLog"
        exitCode=$?
    fi
elif [[ "$key" = "AwsTgzArchiveName" ]]; then
    tar -C / -p -xzf "$otaFile"     2>> "$errLog"
    exitCode=$?
else
    echo "Unknown key: ${key}" >> "$errLog"
    exitCode=1
fi

# Check if the upgrade was successful.
if [[ ${exitCode} -ne 0 ]]; then
    echo "Upgrade failed with exit code $exitCode" >> "$errLog"
    exit 1
fi

################################################################################
# Update the version.xml file.

# Check if the version file exists and create it if necessary.
if [[ ! -f "$versionFile" ]]; then
    # Create a new file with the basic XML structure.
    echo -e "<?xml version='1.0' encoding='utf-8' standalone='yes' ?>\n<map>\n</map>" > "$versionFile"
    if [[ $? -ne 0 ]]; then
        echo "Failed to create empty version file, permission denied" >> "$errLog"
        exit 1
    fi
fi

# Check if the version file contains an entry for key.
grep "<long name=\"${key}\"" "$versionFile" > /dev/null
hasKey=$?

# Update or insert the entry.
if [[ ${hasKey} -eq 0 ]]; then
    # Key found, replace build number.
    sed -i -r "s/(<long name=\"${key}\".*value=)(\"[^\"]*\")/\1\"${build}\"/" "$versionFile" 2>> "$errLog"
else
    # Key not found, insert new entry at end of map.
    sed -i -r "/<\/map>/i \ \ \ \ <long name=\"${key}\" value=\"${build}\" />" "$versionFile" 2>> "$errLog"
fi
exitCode=$?

# Set the correct owner and permissions after all modifications.
chown system:system "$versionFile"
chmod 660 "$versionFile"

# Check if version update was successful.
if [[ ${exitCode} -ne 0 ]]; then
    echo "Failed to upate version of ${key} to ${build}" >> "$errLog"
    exit ${exitCode}
fi

################################################################################
# Clean-up.

# Remove the upgrade entry from the prefs to avoid unnecessary file checks.
sed -i -r "/[[:blank:]]*<string name=\"AwsUpgradeName\"/d" "$prefFile" 2>> "$errLog"
chown system:system "$prefFile"

# Delete the checksum file for upgrade.sh, 00banner only removes the upgrade
# script itself.
rm "${otaDir}/"*.md5

echo "Successfully upgraded ${key} to v${build}" >> "$errLog"
exit 0
