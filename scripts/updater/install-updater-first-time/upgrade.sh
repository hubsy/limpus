################################################################################
# NOTE this needs to run on the device, do not use bash-only features !!!
################################################################################

otaDir="/sdcard/hubsy/ota"
otaFile="${otaDir}/updater"
updateCfg="${otaDir}/update.cfg"
errLog="/sdcard/hubsy/new/$(date '+%Y%m%dT%H%M%S').upgrade.log"

# Verify that the OTA file exists before doing anything.
if [[ ! -e "$otaFile" ]] || [[ ! -e "$updateCfg" ]]; then
    echo "Missing file, upgrade aborted: $(ls "$otaDir")" >> "$errLog"
    exit 1
fi

################################################################################
# Upgrade specific code.

# Remount /system rw.
mount -o remount,rw /system
if [[ $? -ne 0 ]]; then
    echo "Failed to remount /system" >> "$errLog"
    exit 1
fi

binDir="/system/vendor/bin"
tempFile="${binDir}/bootstrap-updater"

# Note that the otaDir partition is mounted noexec.
mkdir -p "$binDir"                  2>> "$errLog" \
    && chown root:shell "$binDir"   2>> "$errLog" \
    && chmod 755 "$binDir"          2>> "$errLog" \
    && cp "$otaFile" "$tempFile"    2>> "$errLog" \
    && chmod 755 "$tempFile"        2>> "$errLog" \
    && chown root:shell "$tempFile" 2>> "$errLog" \
    && "$tempFile" "$updateCfg"     2>> "$errLog" \
    && rm "$tempFile"               2>> "$errLog" \
    && rm "$updateCfg"              2>> "$errLog"
exitCode=$?

# Check if the upgrade was successful.
if [[ ${exitCode} -ne 0 ]]; then
    echo "Upgrade failed with exit code $exitCode" >> "$errLog"
    exit 1
fi

echo "Successfully installed ${otaFile}" >> "$errLog"
exit 0
