################################################################################
# NOTE this needs to run on the device, do not use bash-only features !!!
################################################################################

otaDir="/sdcard/hubsy/ota"
updateCfg="${otaDir}/update.cfg"
errLog="/sdcard/hubsy/new/$(date '+%Y%m%dT%H%M%S').upgrade.log"

# Remount /system rw.
mount -o remount,rw /system
if [[ $? -ne 0 ]]; then
    echo "Failed to remount /system" >> "$errLog"
    exit 1
fi

# Run the updater.
/system/vendor/bin/updater "$updateCfg" 2>> "$errLog"
exitCode=$?

rm "$updateCfg" 2>> "$errLog"

if [[ ${exitCode} -ne 0 ]]; then
    echo "Update failed with exit code $exitCode" >> "$errLog"
    exit 1
fi

echo "Update successful" >> "$errLog"
exit 0
