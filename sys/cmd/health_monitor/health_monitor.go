package main

import (
	"bufio"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math"
	"os"
	"os/exec"
	"os/signal"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/hubsy/limpus/sys/serial"
)

// Configuration.
const (
	// The health data file.
	healthDataFilepath = `/data/data/io.hubsy.core/shared_prefs/health.xml`

	appDirpath        = `/sdcard/hubsy/hmon` // Directory for actively used files.
	logArchiveDirpath = `/sdcard/hubsy/new`  // Directory where logs are archived.
	appDataFilepath   = appDirpath + `/data.json`

	// The monitored application package and entry point.
	applicationPackage    = `io.hubsy.core`
	applicationEntryPoint = applicationPackage + `/` + applicationPackage + `.SecurityCamAppActivity`

	// The minimum frequency of log file rotations. Other events may cause the log to rotate sooner.
	logRotationInterval = 24 * time.Hour

	// How frequently to check the health data file for changes.
	healthCheckInterval = 60 * time.Second

	minRestartInterval = 30 * time.Minute // The monitored app cannot be restarted more frequently.
	minRebootInterval  = 3 * time.Hour    // The device cannot be rebooted more frequently.

	// Time to wait before a photo request is treated as failed.
	maxPhotoDelay = 30 * time.Second
	// Time to wait for the Bluetooth adapter to reach STATE_ON after entering STATE_TURNING_ON.
	maxBTOnDelay = 30 * time.Second
)

var (
	dlog *log.Logger // The debug logger.
	elog *log.Logger // The error logger.

	trackedPhotoRequest Timestamp // The first observed, unfulfilled photo request.
	trackedBTOnRequest  Timestamp // The first observed, unfulfilled Bluetooth adapter ON request.
)

func main() {
	// Create the app data and archive directories.
	if err := os.MkdirAll(appDirpath, 0770); err != nil {
		log.Print("Failed to create the app dir: ", err)
		return // This better not fail.
	}
	if err := os.MkdirAll(logArchiveDirpath, 0770); err != nil {
		log.Print("Failed to create the log archive dir: ", err)
		// Not good but not fatal either.
	}

	// Create the log file.
	logFile, err := openLogFile(appDirpath)
	if err != nil {
		log.Print("Failed to create log file, using stderr: ", err)
		logFile = os.Stderr
	}

	cancel := make(chan struct{})

	// Create loggers for different log levels. Use a serial writer to safely interleave all writes to
	// the underlying file.
	logWriter := serial.NewWriter(bufio.NewWriter(logFile), cancel)
	dlog = log.New(logWriter, "D/main ", log.Ldate|log.Ltime|log.Lshortfile)
	elog = log.New(logWriter, "E/main ", log.Ldate|log.Ltime|log.Lshortfile)

	// Archive any old logs that did not get archived on exit for some reason.
	if err := archiveStaleLogs(appDirpath, logArchiveDirpath, logFile); err != nil {
		elog.Print("Error moving stale logs: ", err)
	}

	// Monitor the health data file for changes.
	modTime := monitorFile(healthDataFilepath, healthCheckInterval, cancel)

	// Receive interrupt signals to cleanly shutdown.
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	appData, err := loadAppData(appDataFilepath)

	// Next time to rotate the log (may happen sooner for other reasons).
	nextLogRotation := time.After(logRotationInterval)

	// Handle file modifications and system interrupts.
MonitorLoop:
	for {
		select {
		case <-interrupt:
			dlog.Print("Interrupt received, quitting...")
			break MonitorLoop
		case <-modTime:
			//dlog.Print("Health data modified at: ", t)
			action := checkHealthData(healthDataFilepath)
			action = chooseAction(appData, action)
			dataChanged := performAction(action, applicationPackage, applicationEntryPoint,
				healthDataFilepath, &appData)
			if dataChanged { // Write to disk.
				if err := saveAppData(appDataFilepath, appData); err != nil {
					elog.Print("Failed to save app data: ", err)
				}
			}
		case <-nextLogRotation:
			logFile = replaceLog(logFile, appDirpath, logArchiveDirpath, logWriter)
			nextLogRotation = time.After(logRotationInterval)
		}
	}

	// Clean-up.
	close(cancel)
	logWriter.WaitUntilFinished()
	if logFile != os.Stderr {
		// Rotate the log to move the file to the archive dir.
		archiveFile := filepath.FromSlash(path.Join(logArchiveDirpath, filepath.Base(logFile.Name())))
		if err := rotateLog(logFile, os.Stderr, archiveFile, logWriter); err != nil {
			log.Print(err)
		}
	}
}

// openLogFile returns a log file in dir with a name based on the current time.
func openLogFile(dir string) (*os.File, error) {
	fname := time.Now().Format(`20060102T150405.000-0700`) + `_hmon.log`
	fpath := filepath.FromSlash(path.Join(dir, fname))
	file, err := os.OpenFile(fpath, os.O_CREATE|os.O_WRONLY, 0640)
	if err != nil {
		return nil, err
	}
	return file, nil
}

// replaceLog creates a new log file in logDir using openLogFile, and then rotates the logs with
// rotateLog. Returns the new log file.
func replaceLog(oldLog *os.File, logDir, archiveDir string, writer *serial.Writer) (newLog *os.File) {
	var err error
	if newLog, err = openLogFile(logDir); err != nil {
		elog.Print("Failed to create log file, using stderr: ", err)
		newLog = os.Stderr
	}

	archiveFile := filepath.FromSlash(path.Join(archiveDir, filepath.Base(oldLog.Name())))
	if err := rotateLog(oldLog, bufio.NewWriter(newLog), archiveFile, writer); err != nil {
		elog.Print(err) // Continue anyway.
	}
	return newLog
}

// rotateLog replaces the underlying writer in writer with newLog, then closes and moves oldLog to
// archiveFile.
func rotateLog(oldLog *os.File, newLog io.Writer, archiveFile string, writer *serial.Writer) error {
	// Swap old for new file in the serialiser.
	writer.Replace(newLog)

	// Stop here if the old file is stdout or stderr.
	if oldLog == os.Stderr || oldLog == os.Stdout {
		return nil
	}

	// Now it is safe to close and move the old file.
	if err := oldLog.Close(); err != nil {
		return err
	}
	if err := os.Rename(oldLog.Name(), archiveFile); err != nil {
		return err
	}

	return nil
}

// archiveStaleLogs moves any old log files (ending in .log) that were not cleanly archived to the
// archive. If currentLog is non-nil then it will be skipped.
func archiveStaleLogs(logDir, archiveDir string, currentLog *os.File) error {
	// Convert paths to system-specific variants.
	logDir = filepath.FromSlash(logDir)
	archiveDir = filepath.FromSlash(archiveDir)

	entries, err := ioutil.ReadDir(logDir)
	if err != nil {
		return err
	}

	var skip string
	if currentLog != nil {
		skip = filepath.Base(currentLog.Name())
	}

	// Move logs.
	var archived int
	for _, entry := range entries {
		name := entry.Name()
		if !entry.Mode().IsRegular() || name == skip || !strings.HasSuffix(name, `.log`) {
			continue
		}

		// Move to archive.
		srcFile := filepath.Join(logDir, name)
		archiveFile := filepath.Join(archiveDir, name)
		if err := os.Rename(srcFile, archiveFile); err != nil {
			elog.Print()
			continue
		}
		archived++
	}

	if archived > 0 {
		dlog.Print("Archived ", archived, " stale log files")
	}

	return nil
}

// monitorFile monitors the file at path, checking for modifications in the specified intervals.
//
// Returns a channel on which every new modification time is reported. This is called at least once
// after the first interval, assuming no errors prevent it from accessing the file and the monitor
// is not cancelled.
//
// The monitor is cancelled when cancel is closed.
func monitorFile(path string, interval time.Duration, cancel <-chan struct{}) <-chan time.Time {
	results := make(chan time.Time, 1)

	go func() {
		lastModified := time.Time{}
		for {
			select {
			case <-cancel:
				return
			case <-time.After(interval):
				t, err := fileModTime(path)
				if err != nil {
					dlog.Print("Failed to read mod time: ", err)
					continue
				}

				if t.After(lastModified) {
					select {
					case results <- t:
						lastModified = t
					default: // Do not wait for channel, continue monitoring.
					}
				}
			}
		}
	}()

	return results
}

// fileModTime returns the modification time of the file at path.
func fileModTime(path string) (time.Time, error) {
	file, err := os.Open(path)
	if err != nil {
		return time.Time{}, err
	}
	defer file.Close()

	info, err := file.Stat()
	if err != nil {
		return time.Time{}, err
	}

	return info.ModTime(), nil
}

// checkHealthData reads health data from filepath and evaluates the condition of the application.
//
// Returns the suggested actions based on this data (more than one action flag may be set).
func checkHealthData(filepath string) action {
	// Read file.
	encoded, err := ioutil.ReadFile(filepath)
	if err != nil {
		return NoAction
	}

	data, err := parseHealthData(encoded)
	if err != nil {
		elog.Print("Failed to decode health data: ", err)
		return NoAction
	}
	//dlog.Printf("%+v", data)

	// Check flags.
	suggested := NoAction
	if data.RestartNeeded {
		suggested |= RestartAction
	}

	// Check timestamps for discrepancies.
	suggested |= checkRequestSuccess(data.PhotoRequest, data.PhotoSuccess, &trackedPhotoRequest,
		maxPhotoDelay, RestartAction)
	suggested |= checkRequestSuccess(data.BluetoothTurnOn, data.BluetoothOn, &trackedBTOnRequest,
		maxBTOnDelay, RebootAction)

	return suggested
}

// checkRequestSuccess validates whether a request has succeeded within the allowed timeframe
// maxDelay and returns the action to perform, which is either NoAction or resolve.
//
// When a potential problem is detected for the first time, it starts tracking the request in
// trackedRequest, which must be passed in again with the next call to checkRequestSuccess. This
// enables it to track the time since the problem first started.
//
// Returns the given resolve action when the duration between trackedRequest and request exceeds
// maxDelay, and NoAction otherwise.
func checkRequestSuccess(request, success Timestamp, trackedRequest *Timestamp,
		maxDelay time.Duration, resolve action) action {

	if trackedRequest == nil {
		elog.Print("trackedRequest is nil")
		return NoAction
	}

	if success.After(request) {
		*trackedRequest = 0 // Successful request, reset tracker.
	} else if request.After(success) {
		// More recent request than success, but is this a known unfilled request sequence?
		if trackedRequest.IsZero() || success.After(*trackedRequest) {
			// This is a new unfulfilled request and may just be the timing of this check.
			*trackedRequest = request
		} else if request.Difference(*trackedRequest) > maxDelay {
			// This is an existing issue and it has crossed the allowed time threshold.
			*trackedRequest = 0 // Reset to start tracking anew.
			return resolve
		}
	}

	return NoAction
}

// chooseAction selects the most severe action from list a. Each value may be the OR of multiple
// action flags.
//
// After this first step, the selected action is compared against the last time it was executed,
// which may result in a further raising of the severity level.
//
// The chosen action to be performed is returned.
func chooseAction(data appData, a ...action) action {
	// Combine actions into one value.
	suggested := NoAction
	for _, v := range a {
		suggested |= v
	}

	// Select the most severe of the suggested actions.
	selected := NoAction
	switch {
	case suggested&RebootAction != 0:
		selected = RebootAction
	case suggested&RestartAction != 0:
		selected = RestartAction
	}

	// Check whether the selected action can be performed at this time and escelate it if not.
	chosen := selected
	if chosen == RestartAction && time.Now().Sub(data.LastRestart) < minRestartInterval {
		chosen = RebootAction // Cannot restart again, reboot instead.
	}
	if chosen == RebootAction && time.Now().Sub(data.LastReboot) < minRebootInterval {
		chosen = NoAction // Cannot reboot again, do nothing for now.
	}

	if chosen != selected {
		dlog.Printf("Cannot perform %s again, changed to %s instead", selected, chosen)
	}
	return chosen
}

// performAction executes the specified action for package pkg with entry point entryPoint. The
// health data is stored in healthFile.
//
// Returns true if the appData has been modified due to the action, false if not.
func performAction(a action, pkg, entryPoint, healthFile string, data *appData) bool {
	switch a {
	case NoAction:
		return false
	case RestartAction:
		if err := restartAndroidApp(pkg, entryPoint); err != nil {
			elog.Print("Failed to restart ", pkg, ": ", err)
			return false
		}
		data.LastRestart = time.Now()
		return true
	case RebootAction:
		if err := rebootDevice(pkg, healthFile); err != nil {
			elog.Print("Failed to reboot: ", err)
			return false
		}
		data.LastReboot = time.Now()
		return true
	}
	return false
}

// restartAndroidApp force stops the app identified by pkg and then restarts it with entryPoint.
func restartAndroidApp(pkg, entryPoint string) error {
	dlog.Print("Restarting ", pkg)
	if err := runCmdAndWait("am", "force-stop", pkg); err != nil {
		elog.Print("Failed to stop app (may not have been running): ", err)
		// Try starting the process even if stopping failed, as it may not have been running.
	}
	if err := runCmdAndWait("am", "start", entryPoint); err != nil {
		return err
	}
	return nil
}

// rebootDevice prepares for reboot by stopping app `pkg` and deleting healthFile in preparation
// for a fresh start. The reboot command is issued to the system asynchronously after a short delay
// to allow clean-up tasks to run.
func rebootDevice(pkg, healthFile string) error {
	// Stop the app so the health data file can be deleted safely.
	dlog.Print("Stopping the app in preparation for reboot")
	if err := runCmdAndWait("am", "force-stop", pkg); err != nil {
		dlog.Print("Failed to stop app (probably already dead): ", err)
	}

	// Delete the health file to avoid flags like HealthData.RestartNeeded from causing a boot-loop.
	if err := os.Remove(healthFile); err != nil {
		elog.Print("Failed to delete health data: ", err)
	}

	dlog.Print("Rebooting in 3 seconds")
	go func() {
		<-time.After(3 * time.Second)
		// Try to stop Android cleanly and reboot.
		if err := runCmdAndWait("am", "start", "-a", "android.intent.action.REBOOT"); err != nil {
			// This can happen if Android is not running, only the underlying Linux.
			dlog.Print("Failed to reboot using `am`, falling back on `reboot`: ", err)
			if err := runCmdAndWait("reboot"); err != nil {
				elog.Print(err)
			}
		}
	}()

	return nil
}

// runCmdAndWait executes an external command cmdName with arguments arg and waits for it to exit.
func runCmdAndWait(cmdName string, arg ...string) error {
	cmd := exec.Command(cmdName, arg...)
	if err := cmd.Run(); err != nil {
		// Try to determine the exit status of the child process.
		status := 1 // Unknown reason.
		if err, ok := err.(*exec.ExitError); ok {
			if s, ok := err.Sys().(syscall.WaitStatus); ok {
				status = s.ExitStatus()
			}
		}
		return fmt.Errorf("failed to run: %v: exit code %d: %v", cmd.Args, status, err)
	}

	return nil
}

// appData stores application data.
type appData struct {
	LastRestart time.Time // The most recent RestartAction.
	LastReboot  time.Time // The most recent RebootAction.
}

// loadAppData reads the application data from the file named filename.
//
// The file not existing is not treated as an error, the zero value of appData is returned.
func loadAppData(filename string) (appData, error) {
	// Read file and parse data.
	data := appData{}
	filename = filepath.FromSlash(filename)
	encoded, err := ioutil.ReadFile(filename)
	if err != nil {
		return data, nil // Assume the file does not exist, which is not an error.
	}
	if err := json.Unmarshal(encoded, &data); err != nil {
		return data, err
	}
	return data, nil
}

// saveAppData writes appData to the file named filename.
func saveAppData(filename string, data appData) error {
	encoded, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		return err
	}

	// Write to temp file and then rename to ensure the data is never left in an inconsistent state.
	filename = filepath.FromSlash(filename)
	tmpfile := filename + `.new`
	if err := ioutil.WriteFile(tmpfile, encoded, 0640); err != nil {
		return err
	}
	if err := os.Rename(tmpfile, filename); err != nil {
		return err
	}

	return nil
}

// HealthData contains values used to perform health checks on the monitored application.
type HealthData struct {
	BleBroadcast    Timestamp // Time of the last Bluetooth LE device found broadcast.
	BleScan         Timestamp // Time of the last Bluetooth LE scan.
	BluetoothOn     Timestamp // Time of the last Bluetooth adapter change to STATE_ON.
	BluetoothTurnOn Timestamp // Time of the last Bluetooth adapter turn on request.
	PhotoRequest    Timestamp // Time of the last photo request.
	PhotoSuccess    Timestamp // Time of the last successfully taken photo.
	ScheduledTask   Timestamp // Time of the last scheduled task execution.

	RestartNeeded Flag // Flag requesting an app restart.
}

// parseHealthData parses encoded health data.
func parseHealthData(encoded []byte) (HealthData, error) {
	type XMLData struct {
		Long []struct {
			Name  string `xml:"name,attr"`
			Value string `xml:"value,attr"`
		} `xml:"long"`
	}

	// Unmarshal XML.
	xmlData := XMLData{}
	err := xml.Unmarshal(encoded, &xmlData)
	if err != nil {
		return HealthData{}, err
	}

	// Parse data.
	data := HealthData{}
	for _, l := range xmlData.Long {
		switch l.Name {
		case "BleBroadcast":
			data.BleBroadcast = ParseTimestamp(l.Value, 0)
		case "BleScan":
			data.BleScan = ParseTimestamp(l.Value, 0)
		case "BluetoothOn":
			data.BluetoothOn = ParseTimestamp(l.Value, 0)
		case "BluetoothTurnOn":
			data.BluetoothTurnOn = ParseTimestamp(l.Value, 0)
		case "PhotoRequest":
			data.PhotoRequest = ParseTimestamp(l.Value, 0)
		case "PhotoSuccess":
			data.PhotoSuccess = ParseTimestamp(l.Value, 0)
		case "ScheduledTask":
			data.ScheduledTask = ParseTimestamp(l.Value, 0)

		case "RestartNeeded":
			data.RestartNeeded = ParseFlag(l.Value, false)
		}
	}

	return data, nil
}

// Timestamp represents an offset in milliseconds since an arbitrary zero value (e.g. system boot).
//
// Although the zero value is arbitrary, it is the same for all timestamps in a process.
type Timestamp int64

// ParseTimestamp returns the Timstamp represented by string s, or defaultValue on error.
func ParseTimestamp(s string, defaultValue Timestamp) Timestamp {
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return defaultValue
	}
	return Timestamp(i)
}

// Add returns the timestamp t + d.
func (t Timestamp) Add(d time.Duration) Timestamp {
	return t + Timestamp(math.Floor(d.Seconds()*1000+0.5))
}

// After is true if t follows u.
func (t Timestamp) After(u Timestamp) bool {
	return t > u
}

// Before is true if t precedes u.
func (t Timestamp) Before(u Timestamp) bool {
	return t < u
}

// Equal is true if timestamps t and u match.
func (t Timestamp) Equal(u Timestamp) bool {
	return t == u
}

// IsZero returns true if t is equal to the epoch.
func (t Timestamp) IsZero() bool {
	return t == 0
}

// SinceEpoch returns the time t since the epoch of the Timstamp. Note that this is not the usual
// Unix epoch, but an arbitrary point in the past such as system boot.
func (t Timestamp) SinceEpoch() time.Duration {
	return time.Duration(t) * time.Millisecond
}

// Difference returns the duration between t and u.
func (t Timestamp) Difference(u Timestamp) time.Duration {
	if t < u {
		return time.Duration(u-t) * time.Millisecond
	}
	return time.Duration(t-u) * time.Millisecond
}

// A Flag indicates the state of some condition.
type Flag bool

// ParseFlag returns the Flag represented by string s, or defaultValue on error.
func ParseFlag(s string, defaultValue Flag) Flag {
	b, err := strconv.ParseBool(s)
	if err != nil {
		return defaultValue
	}
	return Flag(b)
}

// Action indicates some action to execute.
type action int

func (a action) String() string {
	switch a {
	case NoAction:
		return "NoAction"
	case RestartAction:
		return "RestartAction"
	case RebootAction:
		return "RebootAction"
	default:
		return "UnknownAction"
	}
}

// The enum values for action.
const (
	NoAction      action = 1 << iota // No action required.
	RestartAction                    // Restart application action.
	RebootAction                     // Reboot system action.
)
