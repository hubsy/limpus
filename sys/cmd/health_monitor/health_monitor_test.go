package main

import (
	"testing"
	"time"
)

func TestCheckRequestSuccess(t *testing.T) {
	type Test struct {
		init func()
		in   HealthData
		want action
	}
	const maxDelay = 60 * time.Second
	var tracked Timestamp
	table := []Test{
		// No problems.
		{
			in:   HealthData{PhotoRequest: 5, PhotoSuccess: 7},
			want: NoAction,
		},
		// First unfulfilled request.
		{
			init: func() { tracked = 0 },
			in:   HealthData{PhotoRequest: 7, PhotoSuccess: 5},
			want: NoAction,
		},
		// No longer the first unfulfilled request, but the wait time is not up.
		{
			init: func() { tracked = 3 },
			in:   HealthData{PhotoRequest: 7, PhotoSuccess: 0},
			want: NoAction,
		},
		// No longer the first unfulfilled request, and wait time exceeded.
		{
			init: func() { tracked = 3 },
			in:   HealthData{PhotoRequest: Timestamp(4).Add(maxDelay), PhotoSuccess: 0},
			want: RestartAction,
		},
		// Photo success, should clear all previous issues.
		{
			in:   HealthData{PhotoRequest: 5, PhotoSuccess: 7},
			want: NoAction,
		},
	}

	// Add an actual incrementing sequence of failures.
	for i := 0; i < 5; i++ {
		table = append(table,
			Test{
				in: HealthData{
					// Follow up requests to the tracked one are >maxDelay later to trigger new action.
					PhotoRequest: 8 + Timestamp(float64(i)*1.1*maxDelay.Seconds()*1000),
					PhotoSuccess: 7,
				},
			},
		)
		if i&1 == 0 { // Alternate between new issue no action and restart action.
			table[len(table)-1].want = NoAction
		} else {
			table[len(table)-1].want = RebootAction
		}
	}

	for _, tt := range table {
		if tt.init != nil {
			tt.init()
		}
		got := checkRequestSuccess(tt.in.PhotoRequest, tt.in.PhotoSuccess, &tracked, maxDelay, tt.want)
		if got != tt.want {
			t.Errorf("checkRequestSuccess(%+v tracked:%d) = %d; want %d",
				tt.in, tracked, got, tt.want)
		}
	}
}

func TestTimestamp_Add(t *testing.T) {
	table := []struct {
		t    Timestamp
		in   time.Duration
		want Timestamp
	}{
		{t: 10, in: 5 * time.Second, want: 5010},
		{t: 20, in: 499 * time.Microsecond, want: 20},
		{t: 20, in: 500 * time.Microsecond, want: 21},
		{t: 20, in: -500 * time.Microsecond, want: 20},
		{t: 20, in: -501 * time.Microsecond, want: 19},
		{t: 20, in: -2 * time.Millisecond, want: 18},
	}

	for _, tt := range table {
		if got := tt.t.Add(tt.in); got != tt.want {
			t.Errorf("Timestamp.Add(%v) = %v; want %v", tt.in, got, tt.want)
		}
	}
}

func TestParseHealthData(t *testing.T) {
	xmlHeader := `<?xml version='1.0' encoding='utf-8' standalone='yes' ?>`

	table := []struct {
		in        string
		want      HealthData
		wantError bool
	}{
		// Correct.
		{
			in: xmlHeader + `
			<map>
				<long name="PhotoRequest" value="10"/>
				<long name="PhotoSuccess" value="15"/>
				<long name="RestartNeeded" value="1"/>
			</map>
			`,
			want: HealthData{PhotoRequest: 10, PhotoSuccess: 15, RestartNeeded: true},
		},
		// Invalid flag value, default value expected.
		{
			in: xmlHeader + `
			<map>
				<long name="PhotoSuccess" value="15"/>
				<long name="RestartNeeded" value="234"/>
			</map>
			`,
			want: HealthData{PhotoSuccess: 15},
		},
		// XML not well-formed (tag not closed).
		{
			in: xmlHeader + `
			<map>
				<long name="RestartNeeded" value="1">
			</map>
			`,
			wantError: true,
		},
		// Unexpected XML element.
		{
			in: xmlHeader + `<map><longer name="RestartNeeded" value="1"/></map>`,
		},
	}

	for _, tt := range table {
		if got, err := parseHealthData([]byte(tt.in)); (err != nil) != tt.wantError || got != tt.want {
			t.Errorf("parseHealthData(%v) = %v; want %v", tt.in, got, tt.want)
		}
	}
}
