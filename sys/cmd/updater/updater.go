package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"bitbucket.org/hubsy/limpus/sys/update/change"
	"bitbucket.org/hubsy/limpus/sys/update/config"
	"go.uber.org/zap"
)

const versionsDBPath = `/data/data/io.hubsy.core/shared_prefs/version.xml`

func main() {
	// Exit code handling.
	exitCode := 1
	defer func() {
		// Print the error from panic if any, as it will not happen automatically after os.Exit.
		if err := recover(); err != nil {
			fmt.Fprintln(os.Stderr, "Panic:", err)
		}
		os.Exit(exitCode)
	}()

	// Configure and create the root logger.
	//zapcfg := zap.NewDevelopmentConfig()
	zapcfg := zap.NewProductionConfig()
	zapcfg.Encoding = "console"
	zapcfg.EncoderConfig = zap.NewDevelopmentEncoderConfig()
	rootLogger, err := zapcfg.Build()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Failed to initialise the logger:", err)
		return
	}
	log := rootLogger.Sugar()
	defer log.Sync()

	// Check that the path to the config file has been passed in.
	if len(os.Args) < 2 {
		log.Warn("Missing argument: path to config file")
		return
	}
	configPath := os.Args[1]

	// Open the config file and ensure it exists.
	configFile, err := os.Open(configPath)
	if err != nil {
		if os.IsNotExist(err) {
			log.Warnw("The config file does not exist", "path", configPath)
		} else {
			log.Errorw("Error accessing the config file", "err", err)
		}
		return
	}
	defer func() {
		if configFile != nil {
			configFile.Close()
		}
	}()

	// Load the versions database if it already exists.
	versions, err := loadVersionsDBFromFile(versionsDBPath)
	if err != nil && os.IsNotExist(err) {
		log.Debug("Versions DB not found, creating a new one")
		versions = &versionsDB{}
	} else if err != nil {
		log.Errorw("Failed to load the versions database", "err", err)
		return
	}

	// Schedule file system changes to be rolled back if the function returns early for any reason.
	changes := &change.Changeset{}
	defer func() {
		if changes != nil {
			if err := changes.Undo(); err != nil {
				log.Errorw("Failed to undo some changes", "err", err)
			}
		}
	}()

	// Process all update configurations in the file.
	sourceDir := filepath.Dir(configPath)
	var updateMsgs []string
	for i, decoder := 0, json.NewDecoder(configFile); ; i++ {
		// Parse the next update config in the file.
		var cfg config.Config
		if err := decoder.Decode(&cfg); err == io.EOF {
			break
		} else if err != nil {
			log.Warnw("Failed to parse the config", "index", i, "err", err)
			return
		}

		// Process the config.
		if err := update(&cfg, versions, sourceDir, changes); err != nil {
			log.Warnw("Update failed", "index", i, "key", cfg.Version.Key,
				"version", cfg.Version.Value, "err", err)
			return
		}

		updateMsgs = append(updateMsgs, cfg.Version.Key+" to v"+cfg.Version.Value)
	}
	configFile.Close()
	configFile = nil

	// Write the updated versions database to a new file.
	versionsDBTempPath := versionsDBPath + ".new"
	if err = versions.writeToFile(versionsDBTempPath); err != nil {
		log.Errorw("Failed to write the new versions database", "err", err)
		return
	}
	changes.Append(change.CreateFile(versionsDBTempPath))
	changes.Schedule(change.MoveFile{From: versionsDBTempPath, To: versionsDBPath})

	// Change the owner of the versions file to system:system.
	if err := os.Chown(versionsDBTempPath, 1000, 1000); err != nil {
		log.Errorw("Failed to change the owner of version.xml to system:system", "err", err)
		return
	}

	// Apply all scheduled changes.
	if errIdx, err := changes.Apply(); err != nil {
		log.Errorw("Failed to apply a scheduled change", "index", errIdx, "err", err)
		// Note that the scheduled changes already applied when the error is encountered are not
		// undone on return.
		return
	}

	// Success, set changes to nil so they will not be undone.
	changes = nil
	exitCode = 0
	for _, msg := range updateMsgs {
		log.Info("Successfully updated " + msg)
	}
}

// updater performs an update according to an update config.
type updater struct {
	config    *config.Config
	versions  *versionsDB
	sourceDir string
	changes   *change.Changeset
}

// update performs the update specified in config, scheduling the actual changes in the changeset
// for later application in the correct sequence. All preliminary changes already carried out are
// recorded in the changeset so they can be undone if necessary.
//
// Source file paths in the config are assumed to be relative to sourceDir.
//
// The versions database is updated to record the result of applying the changes.
func update(config *config.Config, versions *versionsDB, sourceDir string,
		changes *change.Changeset) error {
	u := &updater{
		config:    config,
		versions:  versions,
		sourceDir: sourceDir,
		changes:   changes,
	}

	if err := u.validateVersionConfig(); err != nil {
		return err
	}

	if err := u.predeleteFiles(); err != nil {
		return err
	}

	if err := u.preinstallFiles(); err != nil {
		return err
	}

	return u.updateVersionsDB()
}

// validateVersionConfig validates that u.Config.Version has the required data and any given
// constraints are satisfied.
func (u *updater) validateVersionConfig() error {
	newVersion := u.config.Version
	if newVersion.Key == "" {
		return errors.New("invalid update config: the version 'key' cannot be empty")
	}
	if newVersion.Value == "" {
		return errors.New("invalid update config: the version 'value' cannot be empty")
	}

	// Check if a particular version is required to be installed and if the requirement is satisfied.
	if newVersion.Expected != "" {
		installed := u.versions.versionsMap()
		oldVersion := installed[newVersion.Key]
		if oldVersion == nil {
			return fmt.Errorf("version constraint not satisfied: %s is not installed",
				newVersion.Key)
		} else if oldVersion.Value != newVersion.Expected {
			return fmt.Errorf("version constraint not satisfied: %s is at v%s, required v%s",
				newVersion.Key, oldVersion.Value, newVersion.Expected)
		}
	}

	// Check if a minimum installed version constraint is specified and satisfied.
	if newVersion.Min != "" {
		installed := u.versions.versionsMap()
		oldVersion := installed[newVersion.Key]
		if oldVersion == nil || oldVersion.Value == "" {
			return fmt.Errorf("version constraint not satisfied: %s is not installed", newVersion.Key)
		}

		err := validateMinVersionConstraint(newVersion.Key, newVersion.Min, oldVersion.Value)
		if err != nil {
			return err
		}
	}

	return nil
}

// validateMinVersionConstraint verifies that oldVersion >= minVersion.
//
// Returns an error if the constraint is not satisfied.
func validateMinVersionConstraint(key, minVersion, oldVersion string) error {
	// Tokenize the versions and compare each token numerically until satisfied.
	minTokens := strings.Split(minVersion, ".")
	oldTokens := strings.Split(oldVersion, ".")
	for i, minVal := range minTokens {
		oldVal := "0"
		if i < len(oldTokens) {
			oldVal = oldTokens[i]
		}

		// Convert the tokens to ints.
		min, err := strconv.Atoi(minVal)
		if err != nil {
			return fmt.Errorf("failed to verify the version constraint for %s: invalid format %s: %v",
				key, minVersion, err)
		}
		old, err := strconv.Atoi(oldVal)
		if err != nil {
			return fmt.Errorf("failed to verify the version constraint for %s: invalid format %s: %v",
				key, oldVersion, err)
		}

		// Check that this token satisfies the constraint.
		if old > min {
			break // The constraint is satisfied.
		} else if old < min {
			return fmt.Errorf("version constraint not satisfied: %s v%s < v%s",
				key, oldVersion, minVersion)
		}
		// The tokens are equal, check the next minor token.
	}

	// All tokens are equal, oldVersion == minVersion.
	return nil
}

// predeleteFiles calls u.safeDelete for all files configured to be deleted.
func (u *updater) predeleteFiles() error {
	for _, path := range u.config.Delete {
		if err := u.safeDelete(path); err != nil {
			return err
		}
	}

	return nil
}

// preinstallFiles moves the files to install to temporary files in their destination directories.
// It also schedules the eventual move to the final file names and the deletion of the existing
// files, if any.
func (u *updater) preinstallFiles() error {
	for _, install := range u.config.Install {
		if install.Source == "" {
			return errors.New("cannot install file: no source path specified")
		}
		if install.Path == "" {
			return fmt.Errorf("cannot install %q: the install path is empty", install.Source)
		}

		// Check if the install directory exists and try to create it if not.
		installDir := filepath.Dir(install.Path)
		switch info, err := os.Stat(installDir); {
		case err != nil && os.IsNotExist(err):
			// Create the directories. Set all perm bits so the final perms are determined by the umask.
			if err := os.MkdirAll(installDir, 0777); err != nil {
				return err
			}
		case err != nil:
			return err
		case !info.IsDir():
			return fmt.Errorf("not a directory: %v", installDir)
		}

		// Copy to a temp file in the install dir. Note that cross-partition renaming does not work.
		fromPath := u.sourceDir + string(os.PathSeparator) + install.Source
		installPathTemp := install.Path + ".new"
		if err := copyFile(fromPath, installPathTemp); err != nil {
			return err
		}
		u.changes.Append(change.CreateFile(installPathTemp))
		u.changes.Schedule(change.MoveFile{From: installPathTemp, To: install.Path})
		u.changes.Schedule(change.DeleteFile(fromPath))

		// Chown if requested. Either both or none must be specified.
		if (install.UID != nil && install.GID == nil) || (install.UID == nil && install.GID != nil) {
			return errors.New("either both or none of uid and gid must be specified in the config")
		}
		if install.UID != nil && install.GID != nil {
			if err := os.Chown(installPathTemp, *install.UID, *install.GID); err != nil {
				return err
			}
		}

		// Chmod if permissions are specified.
		if install.Perm > 0 {
			if err := os.Chmod(installPathTemp, filePermFromBase10(install.Perm)); err != nil {
				return err
			}
		}
	}

	return nil
}

// filePermFromBase10 converts a base 10 value to an os.ModePerm octal Unix permission.
// E.g. 644 -> 0644.
func filePermFromBase10(v uint32) os.FileMode {
	return os.FileMode((((v / 100) & 7) << 6) | ((((v % 100) / 10) & 7) << 3) | ((v % 10) & 7))
}

// copyFile copies a file.
func copyFile(fromPath, toPath string) error {
	// Check that the source is a regular file.
	fromInfo, err := os.Stat(fromPath)
	if err != nil {
		return err
	} else if !fromInfo.Mode().IsRegular() {
		return fmt.Errorf("cannot copy non-regular file %s (%q)", fromPath, fromInfo.Mode().String())
	}

	// Check that the dest file is regular if it exists and that the files are not the same.
	if toInfo, err := os.Stat(toPath); err != nil && !os.IsNotExist(err) {
		return err
	} else if err == nil && !toInfo.Mode().IsRegular() {
		return fmt.Errorf("cannot replace non-regular file %s (%q)", toPath, toInfo.Mode().String())
	} else if err == nil && os.SameFile(fromInfo, toInfo) {
		return nil // Same file.
	}

	from, err := os.Open(fromPath)
	if err != nil {
		return err
	}
	defer from.Close()

	to, err := os.OpenFile(toPath, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0777)
	if err != nil {
		return err
	}
	defer to.Close()

	_, err = io.Copy(to, from)
	if err != nil {
		return err
	}

	return to.Sync()
}

// safeDelete first renames the file within its folder to check that writing is not an issue and to
// be able to undo the operation if necessary. It schedules the actual deletion in the changeset.
func (u *updater) safeDelete(path string) error {
	if _, err := os.Lstat(path); err != nil && os.IsNotExist(err) {
		return nil // File does not exist, ignore.
	} else if err != nil {
		return err
	}

	// Rename it first to ensure writing is not an issue and schedule it for deletion later.
	tempPath := path + ".delete"
	if err := os.Rename(path, tempPath); err != nil {
		return err
	}
	u.changes.Append(change.MoveFile{From: path, To: tempPath})
	u.changes.Schedule(change.DeleteFile(tempPath))

	return nil
}

// updateVersionsDB updates or adds the entry for the new version in u.versions.
func (u *updater) updateVersionsDB() error {
	newVersion := u.config.Version

	// Remove any legacy build number for this key.
	bn := u.versions.BuildNumbers
	for i := len(bn) - 1; i >= 0; i-- {
		if bn[i].Name == newVersion.Key {
			l := len(bn)
			bn[i] = bn[l-1]
			bn = bn[:l-1]
			u.versions.BuildNumbers = bn
		}
	}

	// Modify or add the version string.
	installed := u.versions.versionsMap()
	if oldVersion := installed[newVersion.Key]; oldVersion != nil {
		// Modify the existing value.
		oldVersion.Value = newVersion.Value
	} else {
		// Append a new value.
		u.versions.Versions = append(u.versions.Versions,
			stringElement{Name: newVersion.Key, Value: newVersion.Value})
	}

	return nil
}
