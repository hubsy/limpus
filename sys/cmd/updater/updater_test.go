package main

import (
	"os"
	"testing"
)

func TestValidateMinVersionConstraint(t *testing.T) {
	tests := []struct {
		min, old string
		wantErr  bool
	}{
		{"1", "2", false},
		{"2", "1", true},
		{"2", "10", false},
		{"1.0.0", "1.2.3", false},
		{"1.0.0", "0.9.2", true},
		{"0.9.2", "0.9.2", false},
		{"0.9.2", "0.9.1", true},
		{"0.9.2", "0.9.3", false},
		{"0.0.1", "0.0.2", false},
		{"0.0.2", "0.0.1", true},
		{"768", "765.9.13", true},
		{"768", "768.1.3", false},
		{"768", "769.0.0", false},
		{"768.3", "769.0.5", false},
		{"768.3", "768.2.99", true},
		{"768.3.5.8.2", "769", false},
		{"768.3.5.8.2", "769.0.1", false},
		{"768.3.5.8.2", "768.4", false},
		{"768.3.5.8.2", "768.2", true},
		{"768.a.0", "768.c.0", true},
	}

	for _, test := range tests {
		err := validateMinVersionConstraint("xxx", test.min, test.old)
		if test.wantErr && err == nil {
			t.Errorf("fn(x, %s, %s): want error, got none", test.min, test.old)
		} else if !test.wantErr && err != nil {
			t.Errorf("fn(x, %s, %s): want no error, got error", test.min, test.old)
		}
	}
}

func TestFilePermFromBase10(t *testing.T) {
	tests := []struct {
		in   uint32
		want os.FileMode
	}{
		{644, 0644},
		{777, 0777},
		{751, 0751},
		{000, 0000},
		{844, 0044},
		{690, 0610},
		{648, 0640},
	}

	for _, test := range tests {
		got := filePermFromBase10(test.in)
		if got != test.want {
			t.Errorf("fn(%d): want %#o, got %#o", test.in, test.want, got)
		}
	}
}
