package main

import (
	"encoding/xml"
	"io/ioutil"
	"os"
)

const xmlHeader = `<?xml version='1.0' encoding='utf-8' standalone='yes' ?>` + "\n"

// versionsDB defines the structure of the version.xml file.
type versionsDB struct {
	XMLName      xml.Name        `xml:"map"`
	BuildNumbers []longElement   `xml:"long"`
	Versions     []stringElement `xml:"string"`
}

// longElement defines the structure of a <long> element in the version XML.
type longElement struct {
	Name  string `xml:"name,attr"`
	Value string `xml:"value,attr"`
}

// stringElement defines the structure of a <string> element in the version XML.
type stringElement struct {
	Name  string `xml:"name,attr"`
	Value string `xml:",chardata"`
}

// loadVersionsDBFromFile loads the versions database from the file at path.
func loadVersionsDBFromFile(path string) (*versionsDB, error) {
	enc, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var v versionsDB
	if err = xml.Unmarshal(enc, &v); err != nil {
		return nil, err
	}

	return &v, nil
}

// writeToFile marshals v as XML and writes it to a file at path. The file is created with
// permission 0660 (before umask).
func (v *versionsDB) writeToFile(path string) error {
	enc, err := xml.MarshalIndent(v, "", "    ")
	if err != nil {
		return err
	}

	file, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0660)
	if err != nil {
		return err
	}
	defer file.Close()

	// Write the header followed by the encoded data and a newline.
	if _, err := file.WriteString(xmlHeader); err != nil {
		return err
	}
	if _, err = file.Write(enc); err != nil {
		return err
	}
	if _, err := file.WriteString("\n"); err != nil {
		return err
	}

	return nil
}

// versionsMap maps the names of version elements to the elements themselves.
type versionsMap map[string]*stringElement

// versionsMap returns a name-to-element mapping for v.Versions. The mapping is only valid as long
// as v.Versions is not structurally modified.
func (v *versionsDB) versionsMap() versionsMap {
	m := make(versionsMap, len(v.Versions))
	for i := 0; i < len(v.Versions); i++ {
		m[v.Versions[i].Name] = &v.Versions[i]
	}
	return m
}
