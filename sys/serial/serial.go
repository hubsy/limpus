// Package serial provides thread-safe serialisation to an underlying object.
package serial

import (
	"errors"
	"io"
	"log"
)

// A flusher wraps the Flush method from types like bufio.Writer.
type flusher interface {
	// Flush writes any buffered data to the underlying writer.
	Flush() error
}

// A Writer uses a channel to serialise writes to an underlying io.Writer.
type Writer struct {
	in       chan []byte      // Serialises writes.
	cancel   <-chan struct{}  // Cancels the worker goroutine when closed.
	finished <-chan struct{}  // Used to wait for the buffer of the input channel to drain.
	sinks    chan<- io.Writer // Used to pass new writers to the worker goroutine.
}

// NewWriter initialises a new Writer that is ready to serialise writes to w.
func NewWriter(w io.Writer, cancel <-chan struct{}) *Writer {
	in := make(chan []byte, 8)
	finished := make(chan struct{})
	sinks := make(chan io.Writer) // Unbuffered so the sender can be sure the swap has been done.
	s := &Writer{in, cancel, finished, sinks}

	// Writes data from the channel to the underlying writer until cancelled.
	go func() {
		done := false
		for !done {
			select {
			case v, ok := <-in:
				if !ok { // It should never be closed.
					done = true
					continue
				}

				if _, err := w.Write(v); err != nil {
					// Write to the default logger, better than nothing. Do not write to a custom logger,
					// since it may be using this very instance for its writes.
					log.Print("Failed to log '", v, "': ", err)
				}

				// Flush if w has a flush method.
				if f, ok := w.(flusher); ok {
					f.Flush()
				}
			case sink := <-sinks:
				w = sink // Replace the old writer.
			case <-cancel:
				// Set the channel ref used by Write to nil to drop any further writes and wait until the
				// buffer is drained so all elements sent before cancel was closed are still written.
				s.in = nil
				if len(in) == 0 {
					done = true
				}
			}
		}

		close(finished)
	}()

	return s
}

// Write asynchronously writes p to the underlying io.Writer. The return values are either
// len(p), nil for all successfully submitted writes (note that this does not necessarily mean the
// write to the underlying writer will succeed); or 0 and a non-nil error if this Writer has been
// cancelled.
func (s *Writer) Write(p []byte) (n int, err error) {
	select {
	case s.in <- p: // Nil some time after cancel signal has been received.
	case <-s.cancel: // Drop writes when cancel is closed.
		return 0, errors.New("this Writer is cancelled")
	}
	return len(p), nil
}

// WaitUntilFinished blocks until s has been cancelled and its buffer has been drained.
func (s *Writer) WaitUntilFinished() {
	<-s.finished
}

// Replace changes the underlying writer to w. It is guaranteed that no more data will be written
// to the old writer once replace returns.
func (s *Writer) Replace(w io.Writer) {
	select {
	case s.sinks <- w:
	case <-s.finished:
	}
}
