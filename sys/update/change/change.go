// Package change implements a changeset consisting of changes that can either be undone or applied
// later.
package change

import "os"

// Changeset provides a set of change records in the order the changes were recorded.
type Changeset struct {
	scheduledChanges []Applicable
	undoableChanges  []Undoable
}

// Append appends a new change record to the end of the undoable changes.
func (cs *Changeset) Append(change Undoable) {
	cs.undoableChanges = append(cs.undoableChanges, change)
}

// Schedule appends a new change record to the end of the scheduled changes.
func (cs *Changeset) Schedule(change Applicable) {
	cs.scheduledChanges = append(cs.scheduledChanges, change)
}

// Undo reverts the undoable changes recorded in the changeset, from last to first.
//
// Undo continues with the next change record if an error occurs. The first such error it encounters
// is returned.
func (cs *Changeset) Undo() error {
	var firstErr error
	for i := len(cs.undoableChanges) - 1; i >= 0; i-- {
		if err := cs.undoableChanges[i].Undo(); err != nil && firstErr == nil {
			firstErr = err
		}
	}
	return firstErr
}

// Apply applies the scheduled changes recorded in the changeset, from first to last.
//
// If an error is encountered, the index of the relevant change and the error itself are returned.
func (cs *Changeset) Apply() (errIdx int, err error) {
	for i, change := range cs.scheduledChanges {
		if err := change.Apply(); err != nil {
			return i, err
		}
	}
	return -1, nil
}

// Undoable defines an interface for change records that revert a given change when Undo is called.
type Undoable interface {
	Undo() error
}

// Applicable defines an interface for change records that apply a change when Apply is called.
type Applicable interface {
	Apply() error
}

// CreateFile records the path to a newly created file.
type CreateFile string

// Undo implements interface Undoable.
func (cr CreateFile) Undo() error {
	return os.Remove(string(cr))
}

// MoveFile records the paths from and to which a file was moved.
type MoveFile struct {
	From, To string
}

// Undo implements interface Undoable.
func (cr MoveFile) Undo() error {
	return os.Rename(cr.To, cr.From)
}

// Apply implements interface Applicable.
func (cr MoveFile) Apply() error {
	return os.Rename(cr.From, cr.To)
}

// DeleteFile records the path to a file scheduled for deletion.
type DeleteFile string

// Apply implements interface Applicable.
func (cr DeleteFile) Apply() error {
	return os.Remove(string(cr))
}

// Chmod records a file path and the permissions bits to revert to or apply, depending on its use.
type Chmod struct {
	Path string
	// When used as an Undoable change, these are the permissions to revert to, but when used as an
	// Applicable change, these are the permissions to apply.
	Perm os.FileMode
}

// Undo implements interface Undoable.
func (cr Chmod) Undo() error {
	return os.Chmod(cr.Path, cr.Perm)
}

// Apply implements interface Applicable.
func (cr Chmod) Apply() error {
	return os.Chmod(cr.Path, cr.Perm)
}
