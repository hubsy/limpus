// Package config provides types which specify the update configuration file.
package config

// Config defines the structure of the update configuration file.
type Config struct {
	Version VersionConfig   `json:"version"` // The version config to update.
	Install []InstallConfig `json:"install"` // The files to install.
	Delete  []string        `json:"delete"`  // The files to delete.
}

// VersionConfig defines a version update.
type VersionConfig struct {
	Key   string `json:"key"`   // The version key.
	Value string `json:"value"` // The new version string.
	// The expected version (optional). If specified, it must match the current version.
	Expected string `json:"expected,omitempty"`
	// The mininum installed version (optional). If specified, the installed version must be >= min.
	Min string `json:"min,omitempty"`
}

// InstallConfig specifies how to install a file.
type InstallConfig struct {
	// The path to the file to install, relative to the directory containing the update config file.
	Source string `json:"source"`
	// The path to install to, including the filename.
	Path string `json:"path"`
	// The Unix permissions of the installed file in base 10 (optional).
	Perm uint32 `json:"perm,omitempty"`
	// The uid of the installed file (optional, unless the gid is specified).
	UID *int `json:"uid,omitempty"`
	// The gid of the installed file (optional, unless the uid is specified).
	GID *int `json:"gid,omitempty"`
}
