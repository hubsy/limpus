#!/bin/bash
set -e
set -x

# Clone the repository into /tmp
pushd /tmp
git clone -b v3.2 --depth 1 https://github.com/mozilla/mozjpeg.git
popd

# Configure the local and target systems
NDK_PATH=/opt/android/ndk
BUILD_PLATFORM=linux-x86_64
TOOLCHAIN_VERSION=4.9
ANDROID_VERSION=24

# 64-bit ARMv8 build
HOST=aarch64-linux-android
SYSROOT=${NDK_PATH}/platforms/android-${ANDROID_VERSION}/arch-arm64
ANDROID_CFLAGS="--sysroot=${SYSROOT}"

TOOLCHAIN=${NDK_PATH}/toolchains/${HOST}-${TOOLCHAIN_VERSION}/prebuilt/${BUILD_PLATFORM}
ANDROID_INCLUDES="-I${SYSROOT}/usr/include -I${TOOLCHAIN}/include"
export CPP=${TOOLCHAIN}/bin/${HOST}-cpp
export AR=${TOOLCHAIN}/bin/${HOST}-ar
export AS=${TOOLCHAIN}/bin/${HOST}-as
export NM=${TOOLCHAIN}/bin/${HOST}-nm
export CC=${TOOLCHAIN}/bin/${HOST}-gcc
export LD=${TOOLCHAIN}/bin/${HOST}-ld
export RANLIB=${TOOLCHAIN}/bin/${HOST}-ranlib
export OBJDUMP=${TOOLCHAIN}/bin/${HOST}-objdump
export STRIP=${TOOLCHAIN}/bin/${HOST}-strip


WORKDIR=$(dirname "$(readlink -f "$0")")
SRC_DIR=/tmp/mozjpeg
BUILD_DIR=${WORKDIR}/mozjpeg_${HOST}

rm -rf "${BUILD_DIR}"
mkdir -p "${BUILD_DIR}"
cd "${SRC_DIR}"
autoreconf -fiv
cd "${BUILD_DIR}"
sh "${SRC_DIR}/configure" --host=${HOST} \
  CFLAGS="${ANDROID_INCLUDES} ${ANDROID_CFLAGS} -O3 -fPIE" \
  CPPFLAGS="${ANDROID_INCLUDES} ${ANDROID_CFLAGS}" \
  LDFLAGS="${ANDROID_CFLAGS} -pie" \
  --enable-static --disable-shared --with-simd ${1+"$@"}
make

cp -a "${BUILD_DIR}/cjpeg" "${WORKDIR}/"
