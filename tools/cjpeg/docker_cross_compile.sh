#!/bin/bash

WORKDIR=$(dirname "$(readlink -f "$0")")

docker run -t --rm --name xcompile-cjpeg \
    -v "$WORKDIR":/home/dev/project \
    android-cross-compiler ./build.sh
