#!/bin/bash
set -e
set -x

arm-linux-gnueabihf-gcc -o dcraw -O4 -march=armv8-a dcraw.c -lm -DNODEPS -static
